from ptflistener.core import redis_url


def test_redis_url():
    """



    :return:
    """
    target= "redis://127.0.0.1:6379/0"


    assert redis_url("") == target
    assert redis_url("127.0.0.1") == target
    assert redis_url("127.0.0.1:6379") == target
    assert redis_url("127.0.0.1:6379/0") == target
    assert redis_url(target) == target

    assert redis_url("redis") == "redis://redis:6379/0"
    assert redis_url("localhost:6380") == "redis://localhost:6380/0"
    assert redis_url("localhost:6380/1") == "redis://localhost:6380/1"
    assert redis_url("redis://localhost:6380/1") == "redis://localhost:6380/1"
