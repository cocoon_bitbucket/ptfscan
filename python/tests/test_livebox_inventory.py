from ptflistener.csv_out import csv_line, csv_iter,csv_file



csv_sample_array= ["one","two"]

inventory_result= {

    '192.168.1.1':
        {'DeviceInfo.Country': 'fr', 'DeviceInfo.Description': 'SagemcomFast5360_MIB4 Sagemcom fr',
         'DeviceInfo.BaseMAC': 'a0:1b:29:fc:12:70', 'DeviceInfo.ExternalIPAddress': '82.124.70.85',
         'DeviceInfo.HardwareVersion': 'SG_LB4_1.0.1', 'DeviceInfo.DeviceStatus': 'Up',
         'DeviceInfo.SerialNumber': 'LK15233DP992021', 'DeviceInfo.SoftwareVersion': 'SG40_sip-fr-3.4.3.1_7.21.3.0',
         'DeviceInfo.ModelName': 'SagemcomFast5360_MIB4'},
    '192.168.1.10':
        {'DeviceInfo.Country': 'fr', 'DeviceInfo.Description': 'SagemcomFast5360_MIB4 Sagemcom fr',
         'DeviceInfo.BaseMAC': 'a0:1b:29:fc:12:70', 'DeviceInfo.ExternalIPAddress': '82.124.70.85',
         'DeviceInfo.HardwareVersion': 'SG_LB4_1.0.1', 'DeviceInfo.DeviceStatus': 'Up',
         'DeviceInfo.SerialNumber': 'LK15233DP992021', 'DeviceInfo.SoftwareVersion': 'SG40_sip-fr-3.4.3.1_7.21.3.0',
         'DeviceInfo.ModelName': 'SagemcomFast5360_MIB4'}

}


def test_csv_line():
    """


    :return:
    """
    r = csv_line( csv_sample_array)

    assert r == '"one","two"'
    return




def test_csv_iter():
    """


    :return:
    """
    lines = list(csv_iter(inventory_result))
    assert len(lines) == 3

    # check labels
    headers= lines[0]
    assert headers.startswith('"ip","DeviceInfo.BaseMAC"')

    # line 1
    line1 = lines[1]
    assert line1.startswith('"192.168.1.1","a0:1b:29:fc:12:70"')

    # line 2
    line1 = lines[2]
    assert line1.startswith('"192.168.1.10","a0:1b:29:fc:12:70"')

    #print(lines)


def test_csv_write(tmpdir):
    """
    

    :param tmpdir:
    :return:
    """

    file = tmpdir.join('inventory.csv')

    ioreader = csv_iter(inventory_result)
    csv_file(file.strpath, ioreader)

    assert file.read().startswith('"ip","DeviceInfo.BaseMAC",')


    # with open(file.strpath,"r") as h:
    #     content = h.read()
    #     assert content.startswith('"ip","DeviceInfo.BaseMAC",')

    return