*** Settings ***
Documentation     standard tests
...

# libraries
Library  Collections
Library  RequestsLibrary


#Suite Setup  init suite
#Test Setup     setup pilot     ${platform_name}    ${platform_version}  ${platform_url}
#Test Teardown  close session


*** Variables ***
#${PTFSCAN_HOST}=  http://localhost:3333
#${PTFSCAN_HOST}=  http://192.168.1.233:3333
${PTFSCAN_HOST}=  http://192.168.1.21:3333

${OAUTH_DEVICE_TOKEN}=   Livebox-demo
${OAUTH_ADMIN_TOKEN}=    Livebox-demo

#${headers}    Create Dictionary    Content-Type  application/json  authorization  Bearer ${OAUTH_DEVICE_TOKEN}'}

*** Keywords ***



Unit Ptfscan Home
  [Documentation] 	test ptfscan home page /

  Create Session  ptfscan  ${PTFSCAN_HOST}

  ${resp}=	Get Request	ptfscan	/
  Should Be Equal As Strings	${resp.status_code}	200


Unit Ptfscan Api Home
  [Documentation] 	test ptfscan /api

  Create Session  ptfscan  ${PTFSCAN_HOST}

  # headers =  ${headers}

  ${resp}=	Get Request	ptfscan	/api
  Should Be Equal As Strings	${resp.status_code}	200

  log dictionary  ${resp.json()}
  Dictionary Should Contain Item 	${resp.json()} 	result  api

Unit Ptfscan Api Infra
  [Documentation] 	test ptfscan /api/infra

  Create Session  ptfscan  ${PTFSCAN_HOST}

  ${resp}=	Get Request	ptfscan	/api/infra
  Should Be Equal As Strings	${resp.status_code}	200

  log dictionary  ${resp.json()}
  Dictionary Should Contain Item 	${resp.json()} 	result  api/infra


Unit Ptfscan Api Infra Livebox Livebox_demo
  [Documentation] 	test ptfscan /api/infra/livebox/Livebox-demo

  Create Session  ptfscan  ${PTFSCAN_HOST}

  ${resp}=	Get Request	ptfscan	/api/infra/livebox/Livebox-demo
  Should Be Equal As Strings	${resp.status_code}	200

  log dictionary  ${resp.json()}
  Dictionary Should Contain Item 	${resp.json()} 	livebox  Livebox-demo

  ${result}=  get from dictionary  ${resp.json()}  result
  log dictionary  ${result}
  Dictionary Should Contain Item 	${result} 	ip  192.168.1.1


Unit Ptfscan Livebox get pcbcli
  [Documentation] 	request pcb_cli on a lifebox
  [Arguments] 	${lb}  ${pcbcli}

  ${headers}    Create Dictionary    Content-Type  application/json  authorization  Bearer ${OAUTH_DEVICE_TOKEN}
  log dictionary  ${headers}

  Create Session  ptfscan    ${PTFSCAN_HOST}   headers=${headers}

  ${resp}=	Get Request	 ptfscan  /api/device/livebox/${lb}/pcbcli/${pcbcli}   headers=${headers}
  Should Be Equal As Strings	${resp.status_code}	200

  log dictionary  ${resp.json()}
  Dictionary Should Contain Item 	${resp.json()} 	livebox  Livebox-demo

  ${result}=  get from dictionary  ${resp.json()}  result
  log dictionary  ${result}
  #Dictionary Should Contain Item 	${result} 	ip  192.168.1.1


Unit Ptfscan Livebox set pcbcli
  [Documentation] 	request pcb_cli on a lifebox
  [Arguments] 	${lb}  ${key}  ${value}

  ${headers}    Create Dictionary    Content-Type  application/json  authorization  Bearer ${OAUTH_DEVICE_TOKEN}
  log dictionary  ${headers}

  Create Session  ptfscan    ${PTFSCAN_HOST}   headers=${headers}

  ${params}=   Create Dictionary   key=${key}     value=${value}
#  ${resp}=  Post Request  httpbin  /post		params=${params}

  ${resp}=	POST Request	 ptfscan  /api/device/livebox/${lb}/pcbcli   headers=${headers}  data=${params}
  Should Be Equal As Strings	${resp.status_code}	200

  log dictionary  ${resp.json()}

  ${result}=  get from dictionary  ${resp.json()}  result
  log dictionary  ${result}

  dictionary should contain key  	${result} 	${key}



Unit Ptfscan Livebox shell
  [Documentation] 	request pcb_cli on a lifebox
  [Arguments] 	${lb}  ${command}

  ${headers}    Create Dictionary    Content-Type  application/json  authorization  Bearer ${OAUTH_DEVICE_TOKEN}
  log dictionary  ${headers}

  Create Session  ptfscan    ${PTFSCAN_HOST}   headers=${headers}

  ${params}=   Create Dictionary   cmd=${command}


  ${resp}=	POST Request	 ptfscan  /api/device/livebox/${lb}/shell   headers=${headers}  data=${params}
  Should Be Equal As Strings	${resp.status_code}	200

  log many  ${resp.json()}




*** Test Cases ***


home
  Unit Ptfscan Home

api
  Unit Ptfscan Api Home

infra
  Unit Ptfscan Api infra

infra_livebox_demo
  Unit Ptfscan Api Infra Livebox Livebox_demo

get pcbcli
    Unit Ptfscan Livebox get pcbcli  Livebox-demo  DeviceInfo

set pcbcli
    Unit Ptfscan Livebox set pcbcli  Livebox-demo  Device.UPnP.Device.Enable  1

shell command
    Unit Ptfscan Livebox shell  Livebox-demo  ls -l /tmp