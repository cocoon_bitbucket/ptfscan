"""

    test the ptfscan server basic functions


    pre-requisite:
        * a running ptfscan server at localhost:3333
        * a dev livebox at 192.168.1.1  ( with telnet root/sah )

    check we can reach the server

    server localhost:3333


    GET /api/device/livebox -> 200 application/json
    GET /api/device/livebox/Livebox-demo -> 200 application/json  { error= None,command="livebox_access" }

    GET /api/device/livebox/Livebox-demo/pcbcli/DeviceInfo" -> 200  {command="pcbcli_request }

    POST /api/device/livebox/Livebox-demo/pcbcli  { key:"Device.UPnP.Device.Enable", value="1" }
        -> 200 { command="pcbcli_set" result={ Device.UPnP.Device.Enable:"1" }

    POST /api/device/livebox/Livebox-demo/shell  { cmd="ls -l /tmp" }
        -> 200 { error=None result={ "ls -l /tmp .......}


"""
import time
import json
import requests


#PTFSCAN_HOST = "localhost:3333"
#PTFSCAN_HOST = "192.168.1.101:3333"
PTFSCAN_HOST = "192.168.1.21:3333"

OAUTH_DEVICE_TOKEN = "Livebox-demo"
OAUTH_ADMIN_TOKEN  = "Livebox-demo"


def oauth_header(token):
    """

    :param token:
    :return:    a dict to update headers eg {'Authorization': "Bearer %s" % token}
    """
    return {'Authorization': "Bearer %s" % token}


def ptfscan_host():
    return "http://%s" % PTFSCAN_HOST

def ptfscan_url(path):
    return ptfscan_host() + path


def ptfscan_session(token=None):
    """

    :return: a requets session
    """
    s = requests.Session()
    s.headers.update( { "Content-type":"application/json"})
    if token:
        s.headers.update({'Authorization': "Bearer %s" % token})
    return s

# tests

def test_api_device_home():

    with ptfscan_session(OAUTH_DEVICE_TOKEN) as s :
        # s.get('http://httpbin.org/headers', headers={'x-test2': 'true'})
        #
        r = s.get(ptfscan_url("/api/device/livebox"))
        assert r.status_code == 200
        assert r.headers["Content-Type"] == 'application/json; charset=utf-8'
        response = r.json()
        assert response["result"][0] == "Livebox-demo"
        return

def test_api_device_demo():

    with ptfscan_session(OAUTH_DEVICE_TOKEN) as s :
        # s.get('http://httpbin.org/headers', headers={'x-test2': 'true'})
        #
        livebox = "Livebox-demo"

        r = s.get(ptfscan_url("/api/device/livebox/Livebox-demo"))
        assert r.status_code == 200
        assert r.headers["Content-Type"] == 'application/json; charset=utf-8'
        response = r.json()
        assert response["error"] == None
        assert response["command"] == "livebox_access"
        assert response["result"] == ""
        return

def test_api_device_demo_pcbcli_request():

    with ptfscan_session(OAUTH_DEVICE_TOKEN) as s :
        # s.get('http://httpbin.org/headers', headers={'x-test2': 'true'})
        #
        livebox = "Livebox-demo"

        r = s.get(ptfscan_url("/api/device/livebox/Livebox-demo/pcbcli/DeviceInfo"))
        assert r.status_code == 200
        assert r.headers["Content-Type"] == 'application/json; charset=utf-8'
        response = r.json()
        assert response["error"] == None
        assert response["command"] == "pcbcli_request"
        assert response["livebox"] == "Livebox-demo"
        assert "DeviceInfo.BaseMAC" in response["result"]
        return


def set_pcbcli(s,livebox,key,value):
    """

    :param s: requests session
    :param livebox: string eg Livebox-demo
    :param key: string eg  "Device.UPnP.Device.Enable"
    :param value: string eg "0"
    :return:
    """
    data = dict(key=key, value=value)
    data = json.dumps(data)

    r = s.post(ptfscan_url("/api/device/livebox/%s/pcbcli" % livebox), data=data)
    assert r.status_code == 200
    assert r.headers["Content-Type"] == 'application/json; charset=utf-8'
    response = r.json()
    assert response["error"] == None
    assert response["command"] == "pcbcli_set"
    #assert response["livebox"] == livebox
    assert len(response["result"]) == 1
    assert response["result"][key] == value

    return True


def test_api_device_demo_pcbcli_set():

    with ptfscan_session(OAUTH_DEVICE_TOKEN) as s :
        # s.get('http://httpbin.org/headers', headers={'x-test2': 'true'})
        #
        livebox = "Livebox-demo"
        key = "Device.UPnP.Device.Enable"

        set_pcbcli(s,livebox,key,"0")
        set_pcbcli(s, livebox, key, "1")

        return

def test_api_device_shell():
    """

    :param s: requests session
    :param livebox: string eg Livebox-demo
    :param key: string eg  "Device.UPnP.Device.Enable"
    :param value: string eg "0"
    :return:
    """
    livebox = "Livebox-demo"
    cmd = "ls -l /tmp"

    data = dict(cmd = cmd)
    data = json.dumps(data)

    with ptfscan_session(OAUTH_DEVICE_TOKEN) as s:
        r = s.post(ptfscan_url("/api/device/livebox/%s/shell" % livebox), data=data)
        assert r.status_code == 200
        assert r.headers["Content-Type"] == 'application/json; charset=utf-8'
        response = r.json()
        assert response["error"] == None
        assert response["device"] == livebox
        assert response["ip"] == "192.168.1.1"
        assert response["pdu_tag"].startswith("192.168.1")
        result=  response["result"]
        assert result.startswith(" ls -l /tmp\r\n")

    return True


def ping_livebox(livebox) :
    """

    :param livebox: string livebox name eg Livebox-demo
    :return:
    """


    cmd = "date '+%Y/%m/%d_%H:%M:%S'"

    data = dict(cmd=cmd)
    data = json.dumps(data)

    with ptfscan_session(OAUTH_DEVICE_TOKEN) as s:
        r = s.post(ptfscan_url("/api/device/livebox/%s/shell" % livebox), data=data)
        assert r.status_code == 200
        assert r.headers["Content-Type"] == 'application/json; charset=utf-8'
        response = r.json()
        assert response["error"] == None
        result = response["result"]
        assert result.startswith(" date ")

    return True


def test_api_device_ping():
    """
        check we can reach livebox , log as root and get the current machine time
    :param s: requests session
    :param livebox: string eg Livebox-demo
    :param key: string eg  "Device.UPnP.Device.Enable"
    :param value: string eg "0"
    :return:
    """
    ping_livebox("Livebox-demo")

    return

if __name__=="__main__":


    test_api_device_home()
    test_api_device_demo()

    # test connection with livebox
    test_api_device_ping()

    test_api_device_demo_pcbcli_request()
    test_api_device_demo_pcbcli_set()
    test_api_device_shell()



    print("Done")