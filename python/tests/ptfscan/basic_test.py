"""

    test the ptfscan server basic functions


    pre-requisite: a running ptfscan server at localhost:3333



    check we can reach the server

    server localhost:3333

    GET /    -> 200 text/plain  "home of ptfscan server"
    GET /api -> 200 application/json  { result: "api" }

    GET /api/infra  -> 200 application/json  { result: "/api/infra" }
    GET /api/infra/livebox -> 200 application/json  { result: dict of livebox }
    GET /api/infra/livebox/Livebox-demo -> 200 application/json  { result: dict of properties }

    GET /admin/api  -> 200 application/json  { result: "admin/api" }

    POST /admin/api/acl  { "device" ,2 ,["lb1","lb2"] } -> 200 application/json { error:""}
    GET  /admin/api/acl/device  -> 200  application/json {error:""}

    # test acl conflict
    POST /admin/api/acl  { "device" ,2 ,["lb1","lb9"] } -> 500 application/json { error:"Cancel, a member is already reserved"}


"""
import time
import json
import requests


#PTFSCAN_HOST = "localhost:3333"
#PTFSCAN_HOST = "192.168.1.101:3333"
PTFSCAN_HOST = "192.168.1.21:3333"

OAUTH_DEVICE_TOKEN = "Livebox-demo"
OAUTH_ADMIN_TOKEN  = "Livebox-demo"


def oauth_header(token):
    """

    :param token:
    :return:    a dict to update headers eg {'Authorization': "Bearer %s" % token}
    """
    return {'Authorization': "Bearer %s" % token}


def ptfscan_host():
    return "http://%s" % PTFSCAN_HOST

def ptfscan_url(path):
    return ptfscan_host() + path


def ptfscan_session(token=None):
    """

    :return: a requets session
    """
    s = requests.Session()
    s.headers.update( { "Content-type":"application/json"})
    if token:
        s.headers.update({'Authorization': "Bearer %s" % token})
    return s

# tests

def test_home():

    with ptfscan_session() as s :
        # s.get('http://httpbin.org/headers', headers={'x-test2': 'true'})
        #
        r = s.get(ptfscan_url("/"))
        assert r.status_code == 200

def test_api_url():
    s = ptfscan_session()
    r = s.get(ptfscan_url("/api"))
    assert r.status_code == 200
    assert r.headers["Content-Type"] == 'application/json; charset=utf-8'
    response= r.json()
    assert response['result'] == "api"
    s.close()

def test_api_infra_url():
    s = ptfscan_session()
    r = s.get(ptfscan_url("/api/infra"))
    assert r.status_code == 200
    assert r.headers["Content-Type"] == 'application/json; charset=utf-8'
    response= r.json()
    assert response['result'] == "api/infra"
    s.close()


def test_api_infra_livebox():
    s = ptfscan_session()
    r = s.get(ptfscan_url("/api/infra/livebox"))
    assert r.status_code == 200
    assert r.headers["Content-Type"] == 'application/json; charset=utf-8'
    response= r.json()
    assert type(response['result']) is dict
    livebox_dict = response['result']
    assert len(livebox_dict) >=1
    assert "Livebox-demo" in livebox_dict
    assert livebox_dict["Livebox-demo"]["ip"] == "192.168.1.1"
    s.close()

def test_api_infra_livebox_demo():
    s = ptfscan_session()
    r = s.get(ptfscan_url("/api/infra/livebox/Livebox-demo"))
    assert r.status_code == 200
    assert r.headers["Content-Type"] == 'application/json; charset=utf-8'
    response= r.json()
    assert type(response['result']) is dict
    livebox_demo = response['result']
    assert len(livebox_demo) >=1
    assert livebox_demo["name"] == "Livebox-demo"
    assert livebox_demo["ip"] == "192.168.1.1"
    assert livebox_demo["pdu_name"] == "Energenie-demo"
    s.close()



#  test /api/device/*  access
def test_api_device_livebox_without_token():
    s = ptfscan_session()
    # try to access api without a token
    r = s.get(ptfscan_url("/api/device/livebox"))
    assert r.status_code == 401
    assert r.headers["Content-Type"] == 'application/json; charset=utf-8'
    response= r.json()
    assert response["error"] == "API token required"
    s.close()

def test_api_device_livebox_with_bad_token():
    s = ptfscan_session(token="DUMMY-TOKEN")
    # try to access api without a token
    r = s.get(ptfscan_url("/api/device/livebox"))
    assert r.status_code == 200
    assert r.headers["Content-Type"] == 'application/json; charset=utf-8'
    response= r.json()
    assert response["error"] == None
    assert response["result"] == None
    s.close()

def test_api_device_livebox_with_demo_token():
    s = ptfscan_session(token= OAUTH_DEVICE_TOKEN)  # token= Livebox-demo
    # try to access api without a token
    r = s.get(ptfscan_url("/api/device/livebox"))
    assert r.status_code == 200
    assert r.headers["Content-Type"] == 'application/json; charset=utf-8'
    response= r.json()
    assert response["error"] == None
    assert response["result"] == ["Livebox-demo"]
    s.close()

def test_api_device_livebox_with_real_token():

    ttl = 2
    s = ptfscan_session()  # token= Livebox-demo
    # create a token
    token = acl_create(s, "device", ttl, ["Livebox-demo","Livebox_2798"])
    s.close()

    s = ptfscan_session(token)

    # try to access api without a token
    r = s.get(ptfscan_url("/api/device/livebox"))
    assert r.status_code == 200
    assert r.headers["Content-Type"] == 'application/json; charset=utf-8'
    response= r.json()
    assert response["error"] == None
    assert len(response["result"]) == 2
    assert "Livebox-demo" in response['result']
    assert "Livebox_2798" in response['result']

    s.close()

    time.sleep(ttl)
    return



def test_admin_api_url():
    s = ptfscan_session()
    r = s.get(ptfscan_url("/admin/api"))
    assert r.status_code == 200
    assert r.headers["Content-Type"] == 'application/json; charset=utf-8'
    response = r.json()
    assert response['result'] == "admin/api"
    s.close()
    return


def acl_check_empty(s,realm):
    """
        check acl list for realm "device" is empty

    :return:
    """
    r = s.get(ptfscan_url("/admin/api/acl/%s" % realm))
    assert r.status_code == 200
    assert r.headers["Content-Type"] == 'application/json; charset=utf-8'
    response = r.json()
    assert response['error'] == ""
    assert response['realm'] == realm
    assert len(response['result']) == 0

    return True

def acl_create(s,realm,ttl,members):
    """

    :param s:
    :param realm:
    :param ttl:
    :param members:
    :return:
    """
    data = dict( realm="device", ttl= ttl, members= members )
    data= json.dumps(data)
    r = s.post(ptfscan_url("/admin/api/acl"),data= data)
    assert r.status_code == 200
    response = r.json()
    assert response['error'] == ""
    # return the token
    return response['result']

def test_admin_acl ():
    """
        check acl list for realm "device" is empty
        create an acl in realm device
        check we ca access it


    :return:
    """
    s = ptfscan_session()

    realm = "device"
    ttl = 2

    # check realm empty
    acl_check_empty(s,realm)

    token = acl_create(s,"device",ttl,["Livebox-demo","Livebox_2798"])

    # read all acl of realm "device"
    r = s.get(ptfscan_url("/admin/api/acl/%s" % realm))

    assert r.status_code == 200
    assert r.headers["Content-Type"] == 'application/json; charset=utf-8'
    response = r.json()
    assert response['error'] == "" or None
    assert response['realm'] == "device"
    result = response['result']
    assert type(result) is dict
    assert len(result) == 1
    members = result[token]
    assert len(members)==2
    assert "Livebox-demo" in members
    s.close()

    # wait the ttl of the acl
    time.sleep(ttl)
    return

def test_admin_acl_conflict ():
    """
        check acl list for realm "device" is empty
        create an acl in realm device with Livebox-1 , Livebox-2
        check we can access it

        create a second acl in same realm with Livebox-3 , Livebox-1
        should fail because Livebox-1 is already reserved


    :return:
    """

    s = ptfscan_session()
    realm = "device"
    ttl = 2

    # check realm device is emty
    acl_check_empty(s,realm)

    token = acl_create(s,"device",ttl,["Livebox-demo","Livebox_2798"])

    # read all acl of realm "device"
    r = s.get(ptfscan_url("/admin/api/acl/device"))

    assert r.status_code == 200
    assert r.headers["Content-Type"] == 'application/json; charset=utf-8'
    response = r.json()
    assert response['error'] == "" or None
    assert response['realm'] == "device"
    result = response['result']
    assert type(result) is dict
    assert len(result) == 1
    members = result[token]
    assert len(members)==2
    assert "Livebox-demo" in members


    # try to create another acl with conflict
    data = dict(realm="device", ttl=ttl, members=["Livebox-demo","Livebox_9999"] )
    data = json.dumps(data)
    r = s.post(ptfscan_url("/admin/api/acl"), data=data)
    assert r.status_code == 500
    assert r.headers["Content-Type"] == 'application/json; charset=utf-8'
    response = r.json()
    assert response["result"] == ""
    assert response['error'] == "Cancel, a member is already reserved"

    s.close()

    # wait the ttl of the acl
    time.sleep(ttl)
    return






if __name__=="__main__":


    test_home()
    test_api_url()
    test_api_infra_url()
    test_api_infra_livebox()
    test_api_infra_livebox_demo()

    test_api_device_livebox_without_token()
    test_api_device_livebox_with_bad_token()
    test_api_device_livebox_with_demo_token()
    test_api_device_livebox_with_real_token()
    test_admin_api_url()

    test_admin_acl()
    test_admin_acl_conflict()

    print("Done.")