import os
import re
import subprocess
import logging

log = logging.getLogger(__name__)


# api files  infra.yml livebo
api_files= ["infra_api","livebox_api"]

# directory to deploy doc files ( infra_api.html/ livebox_api.html )
deploy_directory = "../../ginweb/assets/doc"

# directory to store resources js/css for bootstrap/jquery/..
ressource_directory = "../../ginweb/assets/raml"


raml_command= "raml2html"


def search_for_scripts(html_text):
    """
        search for external scripts

        <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.0.min.js"></script>

    :param html_text:
    :return:
    """
    scripts=[]

    log.info('search for <script src="?"')

    r = re.findall(r'<script .*?src="(.*?)"',html_text, re.DOTALL)
    for e in r:
        log.debug("found: %s" % e)
        scripts.append(e)
    return scripts


def search_for_stylesheet(html_text):
    """
        search for external styles

        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    :param html_text:
    :return:
    """
    stylesheets=[]

    log.info('search for <link rel="stylesheet" href="?"')

    r = re.findall(r'<link rel="stylesheet" .*?href="(.*?)"',html_text, re.DOTALL)
    for e in r:
        log.debug("found: %s" % e)
        stylesheets.append(e)
    return stylesheets



def make_http_link_local(link,prefix="/assets/raml"):
    """
    transform a url in a local filepath

    eg https://code.jquery.com/jquery-1.11.0.min.js  to /assets/raml/jquery-1.11.0.min.js

    :param link: string
    :return:
    """
    assert link.startswith("http")
    # "link must begin with 'http' and got: %s" % link
    path,name = os.path.split(link)
    name = prefix + "/" + name
    return name


def replace_links(html_text, links, prefix="/assets/raml"):
    """

    search for external script in html, transform to a local script

    :param html_text:
    :param prefix:
    :return:
    """
    for link in links:
        local_asset = make_http_link_local(link,prefix=prefix)
        html_text=re.sub(link, local_asset, html_text, flags=re.S)
    return html_text



def convert_all_links(html_text, prefix="/assets/raml"):
    """

    search for external script/stylesheet in html, transform to a local script/stulesheet

    :param html_text:
    :param prefix:
    :return:
    """
    scripts = search_for_scripts(html_text)
    html_text = replace_links(html_text, scripts,prefix=prefix)

    stylesheets= search_for_stylesheet(html_text)
    html_text = replace_links(html_text, stylesheets, prefix=prefix)

    return html_text




def check_raml_executable():
    """

    :return:
    """
    return subprocess.call(["raml2html", "--help"])
    

#
# main functions
#

def deploy_doc( name,destination=deploy_directory,source="./",prefix="/assets/raml"):
    """

    :param name: string the source doc eg livabox_api  (.yml)
    :param destination: string directory to store api.html
    :param source: directory to find the source api.yml
    :return:
    """
    log.info("deploying doc %s to %s from %s" % (name,destination,source))

    # generate html from yaml with raml2html
    raml_filename = os.path.join(source,name + ".yml")
    os.stat(source)
    html_filename = os.path.join(source,name + ".html")
    r = subprocess.call(["raml2html","-o", html_filename, raml_filename])

    # read html file
    html_text = file(html_filename,"r").read()

    # convert external links to local links
    html_text = convert_all_links(html_text,prefix=prefix)

    # store html to destination
    destination_file= os.path.join(deploy_directory,name+ ".html")
    with open(destination_file,"w") as fh:
        fh.write(html_text)

    return

if __name__ == "__main__":

    logging.basicConfig(level=logging.DEBUG)
    print("Done")