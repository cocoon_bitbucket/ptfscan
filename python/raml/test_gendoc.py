

from gendoc import search_for_scripts,search_for_stylesheet
from gendoc import make_http_link_local
from gendoc import replace_links, convert_all_links
from gendoc import check_raml_executable
from gendoc import deploy_doc, deploy_directory


sample_html = "./sample.html"

default_assets_prefix ="/assets/raml"


def html_content():
    """

    :return:
    """
    with open(sample_html,"r") as fh:
        text = fh.read()
    return text


def test_search_for_scripts():
    """

    :return:
    """
    text = html_content()
    result = search_for_scripts(text)
    assert len(result) == 3
    return

def test_search_for_stylesheet():
    """

    :return:
    """
    text = html_content()
    result = search_for_stylesheet(text)
    assert len(result) == 2
    return


def test_make_http_link_local():
    """

    :return:
    """
    text = html_content()
    result = search_for_stylesheet(text)
    assert len(result) == 2

    link = result[0]
    local_asset = make_http_link_local(link)
    assert local_asset == '/assets/raml/bootstrap.min.css'

    return


def test_replace_scripts():
    """


    :return:
    """
    text = html_content()
    links = search_for_scripts(text)
    assert len(links) == 3

    text = replace_links(text,links,prefix=default_assets_prefix)
    return

def test_replace_stylesheets():
    """


    :return:
    """
    text = html_content()
    links = search_for_stylesheet(text)
    assert len(links) == 2

    text = replace_links(text,links,prefix=default_assets_prefix)

    return


def test_convert_all_links():
    """


    :return:
    """
    text = html_content()
    text = convert_all_links(text,prefix="/assets/raml")

    assert "/assets/raml/bootstrap.min.css" in text
    assert "/assets/raml/default.min.css" in text

    assert "/assets/raml/jquery-1.11.0.min.js" in text
    assert "/assets/raml/bootstrap.min.js" in text
    assert "/assets/raml/highlight.min.js" in text

    return text


def test_check_raml_executable():
    r = check_raml_executable()
    assert r == 0


def test_deploy_doc():
    """

    :return:
    """
    r = deploy_doc("livebox_api",deploy_directory,"./")

    return r



if __name__=="__main__":

    import logging
    logging.basicConfig(level=logging.DEBUG)

    test_search_for_scripts()
    test_search_for_stylesheet()
    test_make_http_link_local()

    test_replace_scripts()
    test_replace_stylesheets()
    test_convert_all_links()

    #test_check_raml_executable()

    test_deploy_doc()



    print("Done...")