a client for the ptfscan server


api:


  [GIN-debug] GET    /api/livebox/             --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.LiveboxRoute.func1 (5 handlers)
  [GIN-debug] GET    /api/livebox/:ip          --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.LiveboxRoute.func2 (5 handlers)
  [GIN-debug] POST   /api/livebox/:ip/shell    --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.LiveboxShellCommand (5 handlers)
  [GIN-debug] POST   /api/livebox/:ip/set_pcbcli --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.LiveboxPcbliSetCommand (5 handlers)
  [GIN-debug] POST   /api/livebox/:ip/get_pcbcli/:key --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.LiveboxPcbliRequestCommand (5 handlers)
  [GIN-debug] POST   /api/livebox/:ip/reboot   --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.LiveboxRoute.func3 (5 handlers)
  [GIN-debug] GET    /api/energenie/           --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.EnergenieRoute.func1 (5 handlers)
  [GIN-debug] GET    /api/energenie/:ip        --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.EnergenieRoute.func2 (5 handlers)
  [GIN-debug] GET    /api/energenie/:ip/:outlet --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.EnergenieRoute.func3 (5 handlers)
  [GIN-debug] POST   /api/energenie/:ip/:outlet/on --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.EnergenieRoute.func4 (5 handlers)
  [GIN-debug] POST   /api/energenie/:ip/:outlet/off --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.EnergenieRoute.func5 (5 handlers)
  [GIN-debug] GET    /supervisor/              --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.SupervisorRoute.func1 (5 handlers)
  [GIN-debug] GET    /supervisor/pubsub/:channel --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.SupervisorRoute.func2 (5 handlers)
  [GIN-debug] GET    /api/device/livebox/      --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.DeviceLiveboxRoute.func1 (6 handlers)
  [GIN-debug] GET    /api/device/livebox/:name --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.DeviceLiveboxRoute.func2 (6 handlers)
  [GIN-debug] POST   /api/device/livebox/:name/power_on --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.DevicePowerOn (6 handlers)
  [GIN-debug] POST   /api/device/livebox/:name/power_off --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.DevicePowerOff (6 handlers)
  [GIN-debug] POST   /api/device/livebox/:name/power_status --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.DevicePowerStatus (6 handlers)
  [GIN-debug] POST   /api/device/livebox/:name/shell --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.DeviceLiveboxShellCommand (6 handlers)
  [GIN-debug] POST   /api/device/livebox/:name/pcbcli --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.DeviceLiveboxPcbliSetCommand (6 handlers)
  [GIN-debug] GET    /api/device/livebox/:name/pcbcli/:key --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.DeviceLiveboxPcbliRequestCommand (6 handlers)
  [GIN-debug] POST   /api/device/livebox/:name/reboot --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.DeviceLiveboxRoute.func3 (6 handlers)
  [GIN-debug] GET    /api/infra/               --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.InfraRoute.func1 (5 handlers)
  [GIN-debug] GET    /api/infra/livebox        --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.InfraRoute.func2 (5 handlers)
  [GIN-debug] GET    /api/infra/livebox/:name  --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb.InfraRoute.func3 (5 handlers)
  [GIN-debug] GET    /admin/api/               --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb/admin.AdminRoute.func1 (5 handlers)
  [GIN-debug] POST   /admin/api/acl            --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb/admin.NewAcl (5 handlers)
  [GIN-debug] GET    /admin/api/acl/:realm     --> bitbucket.org/cocoon_bitbucket/ptfscan/ginweb/admin.GetAclRealm (5 handlers)
  [GIN-debug] Listening and serving HTTP on :3333