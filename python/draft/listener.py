import redis
import json
import threading
import logging
log = logging.getLogger(__name__)


class Listener(threading.Thread):
    """

    """
    task_tag = "Heartbeat"
    info_tags = []

    def __init__(self, r, channels):
        """

        :param r:
        :param channels:
        """
        threading.Thread.__init__(self)
        self.redis = r

        self._done = False
        self._in_task = False
        self.result = {}

        self.pubsub = self.redis.pubsub()
        self.pubsub.psubscribe(channels)


    def close(self):
        """

        :return:
        """
        self._done = True
        # self.pubsub.unsubscribe()
        # print(self, "unsubscribed and finished")
        # time.sleep(1)
        # self.redis.close()


    def work(self, item):
        """

        :param item:
        :return:
        """
        print(item['channel'], ":", item['data'])


        # channel = str(item["channel"])
        #
        # if "/" in channel:
        #     parts = channel.split("/")
        #     if len(parts) == 3 :
        #         prefix,task,device = parts
        #         if task == "Led" :
        #             self.handle_Led(item['data'])
        #         elif task == "EnergenieStatus":
        #             self.handle_EnergenieStatus(item["data"])
        #         elif task == "DeviceInfo":
        #             self.handle_DeviceInfo(item["data"])
        # return

    # def handle_Led(self,message):
    #     """
    #
    #     :param data:
    #     :return:
    #     """
    #     data= json.loads( message)
    #     print("Led data: %s" % data)
    #
    # def handle_EnergenieStatus(self, message):
    #     """
    #
    #     :param message:
    #     :return:
    #     """
    #     data= json.loads( message)
    #     print("EnergenieStatus data: %s" % data)
    #
    #
    # def handle_DeviceInfo(self, message):
    #     """
    #
    #     :param data:
    #     :return:
    #     """
    #     data = json.loads(message)
    #     print("DeviceInfo data: %s" % data)
    #     response = data["response"]
    #     print("DeviceInfo.Country=%s" % response["DeviceInfo.Country"])
    #     # DeviceInfo.Description
    #     print("DeviceInfo.Description=%s" % response["DeviceInfo.Description"])

    def kill(self):
        """

        :return:
        """
        self.pubsub.unsubscribe()
        print(self, "unsubscribed and finished")

    def run(self):
        """

        :return:
        """
        for item in self.pubsub.listen():
            if self._done is True:
                self.kill()
                break
            else:
                self.work(item)
        # leave listen loop
        return

    def split_channel(self, item):
        """

        :param item:
        :return:
        """
        if item:
            channel = item["channel"].decode("utf-8")
            if "/" in channel:
                parts = channel.split("/")
                if len(parts) == 3:
                    prefix, task, device = parts
                    return prefix, task, device

        return None, None, None


class LiveboxInventory(Listener):
    """

    """
    task_tag = "DeviceInfo"
    info_tags = ["DeviceInfo.Country", "DeviceInfo.Description"]


    def work(self, item):
        """

        :return:
        """
        #task_tag = "DeviceInfo"
        #info_tags = ["DeviceInfo.Country", "DeviceInfo.Description"]

        #print(str(item['channel']), ":", item['data'])

        prefix, task, device = self.split_channel(item)
        log.debug("channel parts: [%s] [%s] [%s]" % (prefix, task, device))
        if task:
            if self._in_task is False:
                # search for Heartbeat of the task
                if task == "Heartbeat" and device == self.task_tag:
                    # we found the start of the task
                    log.info("found start Heartbeat for %s" % self.task_tag)
                    self._in_task = True

            else:
                # we are in the task , detect end of task
                if task == "Heartbeat" and device == self.task_tag:
                    # end of the task run
                    log.info("found stop Heartbeat for %s" % self.task_tag)
                    self._in_task = False
                    self.close()
                    return
                if task == self.task_tag:
                    self.result[device]={}
                    data = json.loads(item["data"])
                    for tag in self.info_tags:
                        try:
                            self.result[device][tag] = data["response"][tag]
                        except KeyError:
                            pass
                    print("result: %s" % self.result)


if __name__ == "__main__":
    """
    
    """
    import time

    logging.basicConfig(level=logging.INFO)

    cnx = redis.Redis()


    #client = Listener(cnx, ['ptf/*'])
    client = LiveboxInventory(cnx, ['ptf/*'])

    client.start()

    log.info("Start Listening")
    
    client.join(timeout=60)

    # r.publish('test', 'this will reach the listener')
    # r.publish('fail', 'this will not')
    #
    # r.publish('test', 'KILL')

    # time.sleep(200)
    # print("stopping...")
    # client.close()
    # time.sleep(2)

    log.info("result is %s" % client.result)

    log.debug("Done")
