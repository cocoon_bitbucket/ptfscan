"""


    setup the infra of the platform


    read csv files describing the platform and write the resolved conf to redis  ( key: infra:livebox => json )


    get redis config from a toml file

    ## publishers
    [redis]
    url = "redis://redis:6379/0"




"""
import os
import click
from click.testing import CliRunner
import logging
import pytoml

from infra.setup_infra import setup_infra


redis_infra_livebox_key = "infra:livebox"


commom_options= {
    "--redis-host" : dict(default="redis://127.0.0.1:6379/0", help='redis host for subscribe'),
    "--log-level": dict(default=logging.DEBUG,type= int,help="level log (10=DEBUG to 50=FATAL)"),
}

csv_files = dict(

    livebox= "infra/files/Base 2-Livebox-Table 1.csv",
    pdu= "infra/files/Base 3-PDU-Table 1.csv"
)



@click.group()
def cli():
    pass

@click.command()
#@click.option('--redis-host', ** commom_options["--redis-host"] )
@click.option('--log-level' , ** commom_options["--log-level"])
def livebox(log_level):
    """

    :param redis_host:
    :return:
    """
    #logging.basicConfig(level=logging.DEBUG)

    logging.basicConfig(level=log_level)

    cnf = toml_config("ptfscan.toml")
    url = redis_url(cnf["redis"]["url"])

    click.echo('setup infra livebox for %s' % url)

    setup_infra(url,csv_filepath("pdu"),csv_filepath("livebox"))

    return

def toml_config(filename="ptfscan.toml"):
    """
        return the configuration

    :param filename:
    :return:
    """
    with open(filename, 'rb') as fin:
        config = pytoml.load(fin)

    return config





def csv_filepath(name):
    """

        return the full path of a csv file

    :param name: string: livebox or pdu
    :return:
    """
    dir_path = os.path.dirname(os.path.realpath(__file__))

    try:
        path = os.path.join(dir_path,csv_files[name])
    except KeyError:
        raise KeyError("unknown csv file: %s , choices:[livebox,pdu]")
    return path


def redis_url(text):
    """
    return a fully qualified redis url from text like
     localhost
     127.0.0.1:6379

    :param text: string representing an incomplete redis host
    :return:
    """
    host = "127.0.0.1"
    port = 6379
    db = 0
    if text:
        if text.startswith("redis://"):
            text = text[8:]

        if ":" in text:
            host,rest = text.split(":")
            if "/" in rest:
                port,db = rest.split("/",1)
            else:
                port = rest
        else:
            host = text
    url = "redis://%s:%s/%s" % ( host,port,db)
    return url


cli.add_command(livebox)



if __name__ == '__main__':


    #from click.testing import CliRunner

    logging.basicConfig(level=logging.DEBUG)

    #runner = CliRunner()
    #result = runner.invoke( livebox)

    #p = csv_filepath('livebox')



    cli()