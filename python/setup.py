from setuptools import setup

setup(
    name='ptf_report',
    version='0.1',
    py_modules=['ptfreport'],
    install_requires=[
        'Click','redis','pytoml','pytest','httpie','robotframework-requests',"robotframework","prometheus_client"
    ],
    entry_points='''
        [console_scripts]
        ptfreport=ptfreport:cli
    ''',
)