

def csv_line( array ):
    """
    :param data: list of values
    :return:
    """
    e = [ '"%s"'%x for x in array ]
    return ",".join(e)

def csv_iter( data ):
    """

    :param data: dict of device_ip,DeviceInfo properties
    :return:
    """

    if data:
        # transform dict of dict in an ordered list of dict
        entries=[]
        device_ips = sorted(data.keys())
        for ip in device_ips:
            entry= data[ip]
            entry["ip"]= ip
            entries.append(entry)

        # read first device entry to evaluate header labels
        if len(entries) > 0:
            first = entries[0]
            labels= sorted(first.keys())
            labels.remove("ip")
            labels.insert(0,"ip")

            # yield the cvs header
            yield csv_line(labels)

            # yield the first line
            #yield first_line

            # process with the lines
            for entry in entries :
                line=[]
                for label in labels:
                    line.append(entry[label])
                yield csv_line(line)

    return

def csv_file(filename,io_reader):
    """

    :param filename:
    :param io_reader:
    :return:
    """
    with open(filename,"w") as h:
        for line in io_reader:
            h.write(line + "\n")
    return