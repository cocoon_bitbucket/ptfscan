import redis
import json
import datetime
import threading
import logging
log = logging.getLogger(__name__)


default_redis_url ="redis://127.0.0.1:6379/0"
heartbeat_tag = "Heartbeat"


def redis_url(text):
    """
    return a fully qualified redis url from text like
     localhost
     127.0.0.1:6379

    :param text: string representing an incomplete redis host
    :return:
    """
    host = "127.0.0.1"
    port = 6379
    db = 0
    if text:
        if text.startswith("redis://"):
            text = text[8:]

        if ":" in text:
            host,rest = text.split(":")
            if "/" in rest:
                port,db = rest.split("/",1)
            else:
                port = rest
        else:
            host = text
    url = "redis://%s:%s/%s" % ( host,port,db)
    return url



class Listener(threading.Thread):
    """
            pusub listener on  ptf/Heartbeat/<task> and ptf/<task>/*



    """
    task_name = "*"
    info_tags = []

    def __init__(self, redis_cnx, task_name= None, prefix = "ptf", timeout=60):
        """

        :param r:
        :param channels:
        """
        threading.Thread.__init__(self)
        self.redis = redis_cnx
        self.prefix= prefix
        self.task_name= task_name or self.task_name
        self.timeout=60

        self._done = False
        self._in_task = False
        self.start_time = 0
        self.end_time = 0

        self.result = {}
        self.error = None


    def subscribe(self):
        """

        :return:
        """
        self.pubsub = self.redis.pubsub()
        # subscribe to Heartbeat channel ( ptf/Heartbeat/<task_name> )
        hchan = "%s/%s/%s" % (self.prefix, heartbeat_tag, self.task_name)
        self.pubsub.subscribe(hchan)
        # subscribe to task channel ( ptf/DeviceInfo/* )
        dchan= "%s/%s/*" % (self.prefix, self.task_name)
        self.pubsub.psubscribe(dchan)


    def close(self):
        """

        :return:
        """
        self._done = True

    def work(self, channel, message):
        """

        :param item:
        :return:
        """
        #channel = item["channel"].decode("utf-8")
        #message = json.loads(item["data"])
        print( channel, ":", message)


    def kill(self):
        """

        :return:
        """
        self.pubsub.unsubscribe()
        log.debug("unsubscribed and finished")
        #print(self, "unsubscribed and finished")

    def run(self):
        """

        :return:
        """
        log.debug("start listen loop")

        self.subscribe()


        self.start_time = datetime.datetime.now()
        self.end_time = self.start_time + datetime.timedelta(seconds=self.timeout)

        # read the pubsub message
        for item in self.pubsub.listen():
            if self._done is True:
                # end of
                self.kill()
                break
            # check timeout
            t = datetime.datetime.now()
            if t > self.end_time:
                self.kill()
                break
            # handle message
            if item["type"] == "psubscribe" or item["type"] == "subscribe":
                log.debug("received subscription confirmation for channel %s" % item["channel"])
                continue
            go_on= self.handle(item)
            if not go_on:
                self.kill()
                break
        # leave listen loop
        log.debug("Leave listen loop")
        return


    def handle(self,item):
        """

        :param item: a redis Message
        :return:
        """
        if item:
            channel = item["channel"].decode("utf-8")
            prefix, task, device = self.split_channel(channel)

            # check heartbeat
            if task and task == heartbeat_tag:
                # this is a heartbeat message
                if not self._in_task:
                    # this heartbeat mark the start of task
                    log.debug("received a start heartbeat")
                    self._in_task = True
                    # recompute timeout
                    self.end_time = datetime.datetime.now() + datetime.timedelta(seconds=self.timeout)
                    # go on
                    return True
                else:
                    # this heartbeat mark the end of the task
                    log.debug("received a stop heartbeat")
                    return False
            else:
                # not a heartbeat
                # confirm we have a correct ptf/<task>/<device> message
                if self._in_task  and task == self.task_name :
                    message = json.loads(item["data"])
                    # treat the message
                    log.debug("handle message: %s %s" % (channel,message))
                    self.work(channel,message)
                    # go on
                    return True
        # no item go on
        return True

    def split_channel(self,channel):
        """
        :param channel: string eg ptf/task/device or ptf/Heartbeat/task
        :return:
        """
        if channel:
            if "/" in channel:
                parts = channel.split("/")
                if len(parts) == 3:
                    prefix, task, device = parts
                    return prefix, task, device
        return None, None, None


if __name__ == "__main__":
    """

    """
    import time

    logging.basicConfig(level=logging.DEBUG)

    cnx = redis.Redis()

    # client = Listener(cnx, ['ptf/*'])
    client = Listener(cnx,task_name="DeviceInfo",prefix="ptf")
    #client = Listener(cnx, task_name="*", prefix="ptf")

    client.start()

    log.info("Start Listening")

    client.join(timeout=60)

    # r.publish('test', 'this will reach the listener')
    # r.publish('fail', 'this will not')
    #
    # r.publish('test', 'KILL')

    # time.sleep(200)
    # print("stopping...")
    # client.close()
    # time.sleep(2)

    log.info("result is %s" % client.result)

    log.debug("Done")