import redis
import threading

from ptflistener.core import Listener, heartbeat_tag ,default_redis_url

import logging
log = logging.getLogger(__name__)


class ActivityReporter(Listener):
    """

    """
    def subscribe(self):
        """

        :return:
        """
        self.pubsub = self.redis.pubsub()
        # subscribe to all channel ( ptf/* )
        dchan= "%s/*" % (self.prefix)
        self.pubsub.psubscribe(dchan)

    def run(self):
        """

        :return:
        """
        log.info("start listen loop")

        self.subscribe()

        # read the pubsub message
        for item in self.pubsub.listen():
            if self._done is True:
                # end of
                self.kill()
                break
            # handle message
            if item["type"] == "psubscribe" or item["type"] == "subscribe":
                log.debug("received subscription confirmation for channel %s" % item["channel"])
                continue

            self.handle(item)

        # leave listen loop
        log.debug("Leave listen loop")
        return

    def handle(self,item):
        """

        :param item: a redis Message
        :return:
        """
        if item:
            channel = item["channel"].decode("utf-8")
            prefix, task, device = self.split_channel(channel)

            # check heartbeat
            if task and task == heartbeat_tag:
                # this is a heartbeat message
                log.info("heartbeat: %s" % channel)
            else:
                # not a heartbeat
                message = item['data']
                log.debug("message: %s %s" % (channel,message))

        # no item go on
        return True




def job(redis_url= default_redis_url,channel_prefix="ptf"):
    """

    :param redis_url:
    :param channel_prefix:
    :return:
    """

    cnx = redis.Redis.from_url(redis_url)
    client = ActivityReporter(cnx, task_name="ActivityReporter", prefix=channel_prefix)
    client.start()



if __name__ == "__main__":
    """

    """
    import redis

    logging.basicConfig(level=logging.DEBUG)

    #redis_url = default_redis_url
    redis_url = "redis:192.168.99.100"


    cnx = redis.Redis(redis_url)

    # client = Listener(cnx, ['ptf/*'])
    client = ActivityReporter(cnx,task_name="ActivityReporter",prefix="ptf")
    client.start()



