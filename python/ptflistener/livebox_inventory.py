
import redis
from ptflistener.core import Listener, default_redis_url
from ptflistener.csv_out import csv_iter,csv_line,csv_file

import logging
log= logging.getLogger(__name__)



class LiveboxInventory(Listener):
    """



    """

    task_name = "DeviceInfo"
    info_tags = ["DeviceInfo.Country", "DeviceInfo.Description","DeviceInfo.BaseMAC","DeviceInfo.ExternalIPAddress","DeviceInfo.HardwareVersion",
                 "DeviceInfo.DeviceStatus","DeviceInfo.SerialNumber","DeviceInfo.SoftwareVersion","DeviceInfo.ModelName"]


    def work(self,channel,message):
        """

        :param channel:
        :param message:
        :return:
        """
        prefix,task,device= self.split_channel(channel)
        self.result[device] = {}

        for tag in self.info_tags:
            try:
                self.result[device][tag] = message["response"][tag]
            except KeyError:
                pass
            except TypeError:
                # response is None or something
                pass
        log.debug("result: %s" % self.result)


def job( redis_url= default_redis_url, filename= "livebox_inventory.csv" , channel_prefix="ptf", timeout=60):
    """

    :param redis_url:
    :param channel_prefix:
    :param timeout:
    :return:
    """

    cnx = redis.Redis.from_url(redis_url)
    client = LiveboxInventory(cnx,prefix=channel_prefix,timeout=timeout)
    client.start()
    client.join(timeout=timeout)

    result = client.result
    lines = csv_iter(result)
    # print(list(lines))

    csv_file(filename=filename,io_reader=lines)







if __name__ == "__main__":
    """

    """
    import time

    logging.basicConfig(level=logging.DEBUG)

    cnx = redis.Redis()

    # client = Listener(cnx, ['ptf/*'])
    client = Listener(cnx,task_name="DeviceInfo",prefix="ptf")
    client = LiveboxInventory(cnx)

    client.start()

    log.info("Start Listening")

    client.join(timeout=60)


    result= client.result
    lines = list(csv_iter(result))

    print(lines)

    log.info("result is %s" % client.result)

    log.debug("Done")


