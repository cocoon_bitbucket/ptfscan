"""


    setup the infra of the platform


    read csv files describing the platform and write the resolved conf to redis  ( key: infra:livebox => json )


    get redis config from a toml file

    ## publishers
    [redis]
    url = "redis://redis:6379/0"



    redis keys
    infra:livebox  => json configuration
    infra:livebox:ip  => string coma separated values  livebox_ip,pdu,ip,pdu_outlet


"""

import redis
import json
from csvload import load_livebox,load_pdu, livebox_pdu

redis_infra_livebox_key = "infra:livebox"
redis_infra_device_ip_key = "infra:device:ip"



def setup_infra(redis_url,pdu_filename,livebox_filename):
    """


    :param redis_url:
    :param pdu_filename:
    :param livebox_filename:
    :return:
    """

    cnx = redis.from_url(redis_url)
    cnx.ping()


    data = livebox_pdu( load_pdu(pdu_filename),load_livebox(livebox_filename))
    json_text= json.dumps(data)

    # set key infra:livebox with json data
    cnx.set(redis_infra_livebox_key,json_text)

    # set hash key infra:livebox:ip with dict livebox_name : "$livebox_ip,$pdu_ip,$pdu_outlet
    # eg  key['livebox-3245'] = "192.168.200.11,192.168.100.22,2"
    #ip_key = redis_infra_livebox_key + ":ip"
    # infra:
    ip_key = redis_infra_device_ip_key
    for livebox_name in data.keys():
        #entry = "%s,%s,%d" % (data[livebox_name]["ip"],data[livebox_name]["pdu_ip"],data[livebox_name]["pdu_outlet"])

        entry_data = dict(
            ip=data[livebox_name]["ip"],
            pdu_ip=data[livebox_name]["pdu_ip"],
            pdu_outlet= data[livebox_name]["pdu_outlet"]
        )
        entry = json.dumps(entry_data)

        cnx.hset(ip_key,livebox_name,entry)


    return



