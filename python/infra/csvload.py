import csv
import json
import logging
log= logging.getLogger(__name__)


pdu_csv_file = "files/Base 3-PDU-Table 1.csv"
livebox_csv_file = "files/Base 2-Livebox-Table 1.csv"


def normalize_labels(row,source,target):
    """
    normalize labels

    :param row:
    :return:
    """
    result=[]
    for i,e in enumerate(row):
        t = e.replace("\n","").replace("\t","").strip()
        if t :
            if t == source[i]:
                t = target[i]
            else:
                log.error("unknown label: %s" % t)
                t = source[i]
        result.append(t)
    return result

def load_pdu(filename= None):
    """

    load csv livebox csv file  and return a dict of dict

    name: { name: ,ip: ,mac: s1: s2:, s3, s4 }


    s1, s2,s3 , s4 are the livebox names connected to the socket 1 , 2 , 3 , 4

    :return:
    """
    log.info("load Pdu")

    origin_labels =  ['Nom PDU', 'Adresse IP', 'Adresse MAC', 'Socket 1', 'Socket 2', 'Socket 3', 'Socket 4', '', '', '', '']
    target_labels =  [ 'name' ,'ip', 'mac', 's1' ,'s2', 's3' ,'s4']
    result = {}

    filename = filename or pdu_csv_file


    with open( filename, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')

        status = 0
        for row in spamreader:

            if status == 0:
                # search for label line
                if row[1] == "Nom PDU":
                    # we found the label line
                    log.info("found a label line: Nom PDU")
                    labels = normalize_labels(row[1:],origin_labels,target_labels)
                    status = 1

            elif status == 1 :
                # read content
                line= {}
                if row[1] != "":
                    for i,e in enumerate(row[1:]):
                        if labels[i]:
                            line[labels[i]]= e.strip()
                    log.debug("add a line: %s" % line)
                    result[line['name']] = line

        if status == 0:
            log.error("No label line found")

    return result

def load_livebox(filename=None):

    """

    load csv livebox csv file  and return a dict of dict

    name : { name=, lan=, ex= ,ip= type= ,serial= mac= wifi= h235= auth= }


    :return:
    """
    log.info("load Livebox")

    origin_labels =  ['Livebox', 'Brassage LAN', 'Brassage EX', 'IP Admin', 'Type', 'Serial Number', 'Adresse MAC', 'Cl\xc3\xa9 WiFi', 'Cl\xc3\xa9 H235', 'AuthCryptPsswd', '', '', '']
    target_labels = ['name' , 'lan' , 'ex' , 'ip' , 'type' ,'serial' ,'mac' , 'wifi' , 'h235' , 'auth']
    result = {}

    filename= filename or livebox_csv_file

    with open( filename, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')

        status = 0
        for row in spamreader:

            if status == 0:
                # search for label line
                if row[1] == "Livebox":
                    # we found the label line
                    log.info("found a label line: Livebox")
                    labels = normalize_labels(row[1:],origin_labels,target_labels)
                    status = 1

            elif status == 1:
                # read content
                line={}
                if row[1] != "":
                    for i, e in enumerate(row[1:]):
                        if labels[i]:
                            line[labels[i]] = e.strip()
                    log.debug("add a line: %s" % line)
                    #result.append(line)
                    result[line['name']] = line

        if status == 0:
            log.error("No label line found")
    return result



def livebox_pdu( pdu_data, livebox_data=None ):
    """

    return the pdu affectation for each livebox

    :param livebox_data: dict ( return by load_livebox )
    :param pdu_data: dict ( return by load_pdu )
    :return: dict  livebox_name: { pdu_name: , pdu_socket:  ..... mac:  }
    """

    result ={}

    for pdu_name in pdu_data.keys():
        # for each livebox
        pdu_record = pdu_data[pdu_name]
        for i, livebox_name in enumerate([ pdu_record["s1"] ,pdu_record["s2"]  ,pdu_record["s3"] ,pdu_record["s4"]]  ):

            pdu_info = dict( pdu_name=pdu_name, pdu_outlet=i + 1, pdu_mac=pdu_record["mac"], pdu_ip = pdu_record["ip"])

            if livebox_name:
                if livebox_data is not None:
                    # if we have livebox data , we return full livebox data + pdu info
                    try:
                        livebox_record = livebox_data[livebox_name]
                        livebox_record.update(pdu_info)
                        result[livebox_name] = livebox_record
                    except KeyError:
                        raise KeyError("the livebox [%s] refered by pdu [%s] does not exists in livebox data" % (
                            livebox_name,pdu_name
                        ))
                else:
                    # we dont have data about livebox : just return the link livebox -> pdu
                    result[livebox_name] = pdu_info

    return result


def make_infra_livebox():
    infra_livebox = livebox_pdu(load_pdu(), load_livebox())

    json_infra_livebox = json.dumps(infra_livebox)

    print json_infra_livebox


if __name__ == "__main__":


    logging.basicConfig(level=logging.DEBUG)

    load_pdu()
    load_livebox()

    livebox_pdu(load_pdu() , load_livebox()   )
    livebox_pdu(load_pdu())

    make_infra_livebox()

    print("Done...")






