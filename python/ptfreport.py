"""


    ptf_report subcommand  -- common-options


    subcommand

    * livebox_inventory    ( --csv-out   = "result.csv" )
    * activity_reporter



    common-options

        --redis-url = redis://localhost:6379/0
        --timeout = 60
        --log-level = DEBUG
        --prefix  = ptf



"""
import os
import click
import redis
import logging

from ptflistener.core import redis_url
from ptflistener import activity_reporter
from ptflistener import livebox_inventory

#from ptflistener.activity_reporter import ActivityReporter
#from ptflistener.livebox_inventory import LiveboxInventory
from ptflistener.csv_out import csv_iter


commom_options= {
    "--redis-host" : dict(default="redis://127.0.0.1:6379/0", help='redis host for subscribe'),
    "--channel-prefix" : dict(default="ptf", help="prefix for redis channel subscription"),
    "--timeout" : dict(default=60,type= int,  metavar='S', help="timeout in seconds"),
    "--log-level": dict(default=logging.DEBUG,type= int,help="level log (10=DEBUG to 50=FATAL)"),
}


@click.group()
def cli():
    pass

@click.command()
@click.option('--redis-host', ** commom_options["--redis-host"] )
@click.option('--channel-prefix' ,** commom_options["--channel-prefix"])
@click.option('--log-level' , ** commom_options["--log-level"])
def Activity_reporter(redis_host,channel_prefix,log_level):
    """

    :param redis_host:
    :return:
    """
    #logging.basicConfig(level=logging.DEBUG)

    logging.basicConfig(level=log_level)

    url = redis_url(redis_host)

    click.echo('activity reporter launched for %s' % url)

    activity_reporter.job(redis_url= url,channel_prefix=channel_prefix)

    x = input("press a key to stop \n")


@click.command()
@click.option('--redis-host', ** commom_options["--redis-host"] )
@click.option('--channel-prefix' ,** commom_options["--channel-prefix"])
@click.option('--log-level' , ** commom_options["--log-level"])
@click.option("--csv-filename", type=click.Path(writable=True) ,default= "livebox_inventory.csv" ,help = "filename for csv ")
def Livebox_inventory(redis_host,channel_prefix,log_level,csv_filename):
    """

    :param redis_host:
    :return:
    """
    logging.basicConfig(level=log_level)

    url = redis_url(redis_host)
    click.echo('livebox_inventory launched for %s' % url)
    livebox_inventory.job(redis_url=url,filename=csv_filename,channel_prefix=channel_prefix,timeout=60)


cli.add_command(Activity_reporter)
cli.add_command(Livebox_inventory)


if __name__ == '__main__':

    #logging.basicConfig(level=logging.DEBUG)

    cli()

