
import os
import re
import tarfile
from vmdelivery.ovf import extract_from_ova, make_ova, ovf_adapt_vbox_to_fbno


source_directory = os.path.expanduser("~/Documents")
filename = "ptfscan-alpine_default_1518458927900_23615.ova"


if __name__=="__main__":

    import logging
    logging.basicConfig(level=logging.DEBUG)

    filepath = os.path.join(source_directory,filename)

    dest = "/tmp"

    r = extract_from_ova(filepath,"/tmp")

    ovf_base, ext = os.path.splitext(filename)
    ovf_filename = os.path.join("/tmp/", ovf_base + ".ovf")

    fh = open(ovf_filename,"r")
    ovf_content =  fh.read()
    fh.close()
    ovf_content = ovf_adapt_vbox_to_fbno(ovf_content)

    with open(ovf_filename,"w") as fh:
        fh.write(ovf_content)


    r = make_ova("/tmp/ptfscan-alpine_default_1518458927900_23615.ova")

    print("Done...")