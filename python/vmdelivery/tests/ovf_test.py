
import pytest
from vmdelivery.ovf import change_virtual_system_type, get_items, replace_item
from vmdelivery.ovf import OvfController , ovf_adapt_vbox_to_fbno


ovf_sample_filename = "samples/ptfscan-alpine-sample.ovf"


controller_sample= """\
<Item>
<rasd:Address>0</rasd:Address>
<rasd:Caption>ideController0</rasd:Caption>
<rasd:Description>IDE Controller</rasd:Description>
<rasd:ElementName>ideController0</rasd:ElementName>
<rasd:InstanceID>3</rasd:InstanceID>
<rasd:ResourceSubType>PIIX4</rasd:ResourceSubType>
<rasd:ResourceType>5</rasd:ResourceType>
</Item>
"""



def sample_ovf():

    with open(ovf_sample_filename,"r") as fh:
        text = fh.read()

    return text


def test_change_virtual_system_type():
    """

    :return:
    """
    text = sample_ovf()
    new_text= change_virtual_system_type(text,"vmx-10")
    assert "<vssd:VirtualSystemType>vmx-10</vssd:VirtualSystemType>" in new_text

    return

def test_get_items():
    """

    :return:
    """
    text = sample_ovf()
    items = get_items(text)
    assert len(items) == 8


    return


def test_delete_item():
    """

    :return:
    """
    item_name = "ideController1"

    text = sample_ovf()
    items = get_items(text)
    assert len(items) == 8

    new_text = replace_item(text,item_name,"")

    items = get_items(new_text)
    assert len(items) == 7

    assert not "<rasd:ElementName>%s</rasd:ElementName>" % item_name in items

    return


def test_controller():
    """


    :return:
    """
    c = OvfController.from_string(controller_sample)
    assert c.data["ElementName"] == "ideController0"

    c.adapt_to_scsi()
    assert c.data["ElementName"] == "SCSIController"

    xml = c.xml_string
    assert xml.startswith("<Item>\n")
    return


def test_replace_item():
    """

    :return:
    """
    item_name = "ideController0"

    text = sample_ovf()
    items = get_items(text)
    assert len(items) == 8

    # transform item
    item_content = items[item_name]
    c = OvfController.from_string(item_content)
    c.adapt_to_scsi()
    new_content = c.xml_string

    new_text = replace_item(text,item_name,new_content)

    items = get_items(new_text)
    assert len(items) == 8

    assert not "<rasd:ElementName>%s</rasd:ElementName>" % item_name in new_text
    assert "<rasd:ElementName>SCSIController</rasd:ElementName>" in new_text

    #print new_text
    return


def test_ovf_adapt_vbox_to_fbno():
    """

    :return:
    """

    text = sample_ovf()

    text= ovf_adapt_vbox_to_fbno(text)

    print text






if __name__== "__main__":

    test_change_virtual_system_type()
    test_get_items()

    test_delete_item()

    test_controller()

    test_replace_item()

    test_ovf_adapt_vbox_to_fbno()


    print("Done...")