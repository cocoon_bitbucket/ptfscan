#!/usr/bin/env python

import os
import re
import tarfile
import glob
import logging

log = logging.getLogger(__name__)

def ovf_adapt_vbox_to_fbno(text):
    """
        change   VirtualSystemType from  virtualbox-2.2 to vmx-10
        delete Item with Elementname = ideController1
        adapt  Item with Elementname = ideController0 to SCSI
            ( ElementName= "SCSIController" , ResourceSubType="lsilogic" )

        return the new ovf content

    :param text:
    :return:
    """
    log.info("adapt ovf content to fbno")

    log.info("change Type to vmx-10")
    new_text = change_virtual_system_type(text, "vmx-10")

    log.info("remove elementName ideController1")
    new_text = replace_item(new_text,'ideController1',"")

    log.info("adapt elementName ideController0 to SCSI")
    item_name= "ideController0"
    items = get_items(new_text)
    item_content = items[item_name]
    c = OvfController.from_string(item_content)
    c.adapt_to_scsi()
    new_content = c.xml_string
    new_text = replace_item(new_text, item_name, new_content)

    return new_text



def replace_item(text,item_name,item_content=""):
    """

    :param text:
    :param items: dict ( obtain via get_items)
    :param item_name:
    :param item_content:
    :return:
    """
    items= get_items(text)
    text_to_replace = items[item_name]
    new_text = re.sub(text_to_replace, item_content, text, flags=re.S)
    return new_text


def get_items(text):
    """

      <Item>
        <rasd:Address>1</rasd:Address>
        <rasd:Caption>ideController1</rasd:Caption>
        <rasd:Description>IDE Controller</rasd:Description>
        <rasd:ElementName>ideController1</rasd:ElementName>
        <rasd:InstanceID>4</rasd:InstanceID>
        <rasd:ResourceSubType>PIIX4</rasd:ResourceSubType>
        <rasd:ResourceType>5</rasd:ResourceType>
      </Item>



    :param text:
    :return: dict  name: content ( <Item> ... </Item>
    """
    result ={}
    pattern = r'<Item>.*?<rasd:ElementName>(.*?)</rasd:ElementName>.*?</Item>'
    all = re.finditer(pattern, text, flags=re.S)
    for item in all:
        name = item.groups(1)[0]
        text = item.group()
        result[name]= text
    return result


def change_virtual_system_type(text, system_type):
    """
    :param string source text
    :param system_type: string eg virtualbox-2.2 , vmx-10 ,
    :return: the modified source text

        <vssd:VirtualSystemType>virtualbox-2.2</vssd:VirtualSystemType>

    """
    pattern= r'<vssd:VirtualSystemType>(.*?)</vssd:VirtualSystemType>'
    new_type = "<vssd:VirtualSystemType>%s</vssd:VirtualSystemType>" % system_type
    new_text = re.sub(pattern, new_type, text, flags=re.S)
    return new_text


class OvfController(object):
    """
    <Item>
    <rasd:Address>0</rasd:Address>
    <rasd:Caption>ideController0</rasd:Caption>
    <rasd:Description>IDE Controller</rasd:Description>
    <rasd:ElementName>ideController0</rasd:ElementName>
    <rasd:InstanceID>3</rasd:InstanceID>
    <rasd:ResourceSubType>PIIX4</rasd:ResourceSubType>
    <rasd:ResourceType>5</rasd:ResourceType>
    </Item>

    :param object:
    :return:
    """
    tags = ["Address","Caption","Description","ElementName","InstanceID","ResourceSubType","ResourceType"]

    scsci_tags = dict(
        Caption="SCSIController",
        Description="SCSI Controller",
        ElementName="SCSIController",
        ResourceSubType="lsilogic"
    )

    def __init__(self,data ):
        """

        :param data:
        :return:
        """
        self.data = data

    @classmethod
    def from_string(cls,text):
        """

        :param string:
        :return:
        """

        pattern=r'<rasd:(.*?)>(.*?)</rasd:.*?>'
        rx = re.compile(pattern)

        data = {}
        tags = rx.finditer(text)
        for tag in tags:
            name = tag.group(1)
            value = tag.group(2)
            data[name]=value
        return cls(data)

    def adapt_to_scsi(self):
        """


        :return:
        """
        for tag in self.scsci_tags.keys():
            self.data[tag] = self.scsci_tags[tag]

    @property
    def xml_string(self):
        """

        :return:
        """
        lines=[]
        lines.append("<Item>")
        for tag in self.tags:
        #for tag in self.data.keys():
            line= "        <rasd:%s>%s</rasd:%s>" % (tag,self.data[tag],tag)
            lines.append(line)
        lines.append("      </Item>")
        return "\n".join(lines)



def ovf_files(members):
    """
    a filter for tar .ovf files

    :param members:
    :return:
    """
    for tarinfo in members:
        if os.path.splitext(tarinfo.name)[1] == ".ovf":
            yield tarinfo


def vmdk_files(members):
    """
    a filter for tar .ovf files

    :param members:
    :return:
    """
    for tarinfo in members:
        if os.path.splitext(tarinfo.name)[1] == ".vmdk":
            yield tarinfo



def extract_from_ova(filepath,destination):
    """

    :param filepath:
    :param destination:
    :return:
    """

    os.stat(filepath)
    if not tarfile.is_tarfile(filepath):
        print("Not an OVA format:%s" % filepath)
        exit(1)

    tar = tarfile.open(filepath)
    #tar.extractall(members=ovf_files(tar),path="./tmp")
    r = tar.extractall(path="/tmp")
    tar.close()

    return r

def make_ova(filepath,destination=None):
    """

      /tmp/ptfscan-alpine_default_1518458927900_23615.ova


    :param filepath: path of the ova file eg ./tmp/ptfscan-alpine_default_1518458927900_23615.ova
    :param destination: directory where to store the ova file /Users/cocoon/Downloads
    :return:
    """
    if not destination:
        destination = os.path.expanduser("~/Downloads")
        if not os.path.exists(destination):
            os.mkdir(destination)

    # split filepath to extract directory to find components
    path,filename = os.path.split(filepath)
    base,ext = os.path.splitext(filename)

    #log.info("make ova file from %s ..." % filepath )

    filelist = [f for f in os.listdir(path) if f.startswith(base) ]

    # compute the destination filename
    target_file = os.path.join(destination,filename)
    target_file = target_file.replace("default","fbno")

    log.info("create ova file: %s" % target_file)

    count = 0
    tar = tarfile.open( target_file, "w")
    for name in filelist:
        fname = os.path.join(path,name)
        _,ext = os.path.splitext(fname)
        if ext in [".ovf",".vmdk"]:
            log.debug("add file to ova: %s" % fname)
            tar.add( fname )
            count = count +1
    tar.close()
    log.info("ova file available: %s" % target_file)

    return count


def make_ova2(filepath,destination=None):
    """

      /tmp/ptfscan-alpine_default_1518458927900_23615.ova


    :param filepath: path of the ova file eg ./tmp/ptfscan-alpine_default_1518458927900_23615.ova
    :param destination: directory where to store the ova file /Users/cocoon/Downloads
    :return:
    """
    if not destination:
        destination = os.path.expanduser("~/Downloads")
        if not os.path.exists(destination):
            os.mkdir(destination)

    # split filepath to extract directory to find components
    path,filename = os.path.split(filepath)
    base,ext = os.path.splitext(filename)

    #log.info("make ova file from %s ..." % filepath )

    filelist = [f for f in os.listdir(path) if f.startswith(base) ]

    os.chdir(path)
    # compute the destination filename
    target_file = os.path.join(destination,filename)
    target_file = target_file.replace("default","fbno")

    log.info("create ova file: %s" % target_file)

    count = 0
    tar = tarfile.open( target_file, "w")
    for name in filelist:
        #fname = os.path.join(path,name)
        _,ext = os.path.splitext(name)
        if ext in [".ovf",".vmdk"]:
            log.debug("add file to ova: %s" % name)
            tar.add( name )
            count = count +1
    tar.close()
    log.info("ova file available: %s" % target_file)

    return count



#
#  main entry
#

def convert_virtualbox_to_vmware ( ova_filename, source_directory,dest_directory= None ):
    """


    :param source_ova: string filename eg: ptfscan-alpine_default_1518458927900_23615.ova
    :param: source_directory string: eg $HOME/Documents
    :param dest_directory string :   eg $HOME/Downloads
    :return:
    """

    filepath = os.path.join(source_directory, ova_filename)

    temp = "/tmp"

    r = extract_from_ova(filepath, temp)

    ovf_base, ext = os.path.splitext(ova_filename)
    ovf_filename = os.path.join( temp, ovf_base + ".ovf")

    fh = open(ovf_filename, "r")
    ovf_content = fh.read()
    fh.close()
    ovf_content = ovf_adapt_vbox_to_fbno(ovf_content)

    with open(ovf_filename, "w") as fh:
        fh.write(ovf_content)

    #r = make_ova("/tmp/ptfscan-alpine_default_1518458927900_23615.ova")
    r= make_ova2(os.path.join(temp,ova_filename),destination=dest_directory)

    return

if __name__=="__main__":

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("ova")
    args = parser.parse_args()

    # if args.a == 'magic.name':
    #     print 'You nailed it!'

    logging.basicConfig(level=logging.DEBUG)

    #filename = "ptfscan-alpine_default_1518458927900_23615.ova"
    filename= args.ova



    source_directory = os.path.expanduser("~/Documents")
    destination_directory = os.path.expanduser("~/Documents")

    convert_virtualbox_to_vmware(filename,source_directory,destination_directory)

    #print("Done...")