package proxy_test


import (

	"bitbucket.org/cocoon_bitbucket/ptfscan/proxy"
	"testing"
	//"github.com/stretchr/testify/assert"
)


/*

	start a simple proxy at localhost:8181

	proxy traffic to 192.168.1.1


 */



func TestSimpleProxy(t *testing.T) {

	proxy.SimpleProxy(":8181","192.168.1.1")


}