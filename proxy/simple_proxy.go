

package proxy

import (
	"log"
	"net/http"
	"net/http/httputil"
	"fmt"
)

var (

	Default_target_host string = "192.168.1.1"

)


func SimpleProxy( host, target_host string) {
	//
	//  a simple http proxy
	//
	//  host: the proxy address eg ":8181
	//  target_host: address to redirect to
	//
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		// extract host from headers
		h := r.Header.Get("X-LIVEBOX")
		//host := h[0]
		fmt.Printf("X-LIVEBOX=%s\n",h)

		director := func(req *http.Request) {
			req = r
			req.URL.Scheme = "http"
			//req.URL.Host = r.Host
			req.URL.Host = target_host
		}
		proxy := &httputil.ReverseProxy{Director: director}
		proxy.ServeHTTP(w, r)
	})
	fmt.Printf("proxy running at %s redirect to %s ...",host,target_host)
	log.Fatal(http.ListenAndServe( host, nil))

}


