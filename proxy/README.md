
see: https://mauricio.github.io/golang-proxies

net/http client to use

    client: &http.Client{
        Timeout: clientTimeout,
        Transport: trace.HTTPTransport(&http.Transport{
            Dial:                  dialer.Dial(),
            DialContext:           dialer.DialContext(),
            TLSHandshakeTimeout:   tlsHandshakeTimeout,
            ResponseHeaderTimeout: responseHeaderTimeout,
            ExpectContinueTimeout: time.Second,
            MaxIdleConns:          int(maxIdleConns),
            DisableCompression:    true,
        }),
        CheckRedirect: func(req *http.Request, via []*http.Request) error {
            return http.ErrUseLastResponse
        },
    },
    

some struct

    type Filter interface {
    	Name() string
    }
    
    type BeforeFilter interface {
    	Filter
    	BlacklistedHeaders() []string
    	DoBefore(context context.Context) BeforeFilterError
    }
    
    type AfterFilter interface {
    	Filter
    	DoAfter(context context.Context) AfterFilterError
    }
                    
    
 