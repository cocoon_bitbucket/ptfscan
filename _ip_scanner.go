package main

import (
	"fmt"
	"log"
	"net"
	//"net/http"
	"time"
	"os/exec"
	"io/ioutil"
	"runtime"
	"golang.org/x/net/html"
	"strings"
	"github.com/gammazero/workerpool"
)

/*

	cidr -- ip -- pingable --
                  ports    __L_  open_ports

 */



// cidr -> ip


func inc(ip net.IP) {
	// increment an IP
	for j := len(ip)-1; j>=0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

func dupIP(ip net.IP) net.IP {
	// duplicate an IP
	dup := make(net.IP, len(ip))
	copy(dup, ip)
	return dup
}

func IpRange( cidr string ) chan net.IP {
	//
	// return a channel of IP from a cidr
	//

	c := make(chan net.IP)
	go func() {

		defer close(c)

		ip, ipnet, err := net.ParseCIDR(cidr)
		if err != nil {
			log.Fatal(err)
		}
		iip := ip.Mask(ipnet.Mask)
		for {
			if ipnet.Contains(iip) {

				//fmt.Println("* ", iip)
				dup := dupIP(iip)
				c <- dup
				inc(iip)
				//time.Sleep(1 * time.Millisecond)

			} else {
				break
			}
		}

	}()
	return c
}


func IpRangeFromList( ips []string ) chan net.IP {
	//
	// return a channel of IP from a ip string list
	//
	c := make(chan net.IP)
	go func() {

		defer close(c)

		for _,ip := range ips {
			q := net.ParseIP(ip)
			//fmt.Println(ip)
			c <- q
			//time.Sleep(1000 * time.Millisecond)
		}
	}()
	return c
}





// pingable ip

func Pingable( ips chan net.IP ) chan net.IP {
	//
	// return a channel of pingable ip address
	//
	c := make(chan net.IP)
	go func() {

		defer close(c)
		for ip := range ips {
			//fmt.Println(ip)
			_, err := exec.Command("ping", "-c1", "-t1", ip.String()).Output()
			//println(string(r))
			if err == nil {
				// ping ok
				c <- ip
			}
		}
	}()
	time.Sleep(1 * time.Millisecond)
	return c

}


func IsPortOpen(host string ,port int, timeout time.Duration) bool {

	//

	uri := fmt.Sprintf("%s:%d",host,port)

	tcpAddr, err := net.ResolveTCPAddr("tcp4", uri)
	if err != nil {
		return false
	}
	conn, err := net.DialTimeout("tcp", tcpAddr.String(), timeout)
	if err != nil {
		return false
	}

	defer conn.Close()

	return true
}


//
//
//

func OpenedTcpAdress ( ipchan chan net.IP , ports []int )  chan net.TCPAddr{
	//
	// return a channel of net.TCPAddr  for  open ports
	//
	c := make(chan net.TCPAddr)
	_ = c

	go func() {
		defer close(c)

		timeout := 100 * time.Millisecond
		for ip := range ipchan {

			for _,port := range ports {

				uri := fmt.Sprintf("%s:%d", ip.String(), port)
				tcpAddr, err := net.ResolveTCPAddr("tcp4", uri)
				if err == nil {
					conn, err := net.DialTimeout("tcp", tcpAddr.String(), timeout)
					//defer conn.Close()
					if err == nil {
						c <- * tcpAddr
						conn.Close()
					}
				}
			}
		}
	}()
	return c
}



func checkAddr( channel chan net.TCPAddr,ip net.IP,port int,timeout time.Duration) {
	// check a port is responding push it to the channel

	uri := fmt.Sprintf("%s:%d", ip.String(), port)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", uri)
	if err == nil {
		fmt.Printf("try to connect to %s\n", tcpAddr.String())
		conn, err := net.DialTimeout("tcp", tcpAddr.String(), timeout)
		//defer conn.Close()
		if err == nil {
			fmt.Printf("-- CONNECTION OK  to %s\n", tcpAddr.String())
			channel <- * tcpAddr
			conn.Close()
		} else {
			fmt.Printf("-- fail to connect to %s\n", tcpAddr.String())
		}
	}
}

func checkAddrTask( channel chan net.TCPAddr,ip net.IP,port int,timeout time.Duration) func() {
	// a curry for checkAddr
	wrapper := func() {
		checkAddr( channel ,ip ,port,timeout )
	}
	return wrapper
}


func WorkOpenedTcpAdress ( ipchan chan net.IP , ports []int )  chan net.TCPAddr{
	//
	// return a channel of net.TCPAddr  for  open ports ( via a worpool )
	//
	c := make(chan net.TCPAddr)
	_ = c

	// create a worker pool
	runtime.GOMAXPROCS(runtime.NumCPU())
	nbWorkers := runtime.NumCPU() * 3
	wp := workerpool.New(nbWorkers * 3)  // took 0.7 seconds
	_= wp

	go func() {
		defer close(c)

		timeout := 100 * time.Millisecond
		for ip := range ipchan {

			for _,port := range ports {

				//go checkAddr(c,ip,port,timeout)
				wp.Submit(checkAddrTask(c,ip,port,timeout))

			}
		}
		wp.Stop()
	}()
	return c
}






///

func PredictHttp( tcpAddr * net.TCPAddr) string {
	/*

		livebox orange constants:

		BUILD_CUSTOMER: 'ft',
		BUILD_PROJECT: 'lbv3fr',
		BUILD_HARDWARE: 'sagem_lbv3',

	 */

	timeout := 100 * time.Millisecond
	duration, _ := time.ParseDuration("3s")


	conn, err := net.DialTimeout("tcp", tcpAddr.String(), timeout)
	//conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		return ""
	}
	defer conn.Close()
	conn.SetDeadline(time.Now().Add(duration))

	//_, err = conn.Write([]byte("HEAD / HTTP/1.0\r\n\r\n"))
	_, err = conn.Write([]byte("GET / HTTP/1.0\r\n\r\n"))
	if err != nil {
		return ""
	}

	result, err := ioutil.ReadAll(conn)
	if err != nil {
		return ""
	}

	reader := strings.NewReader(string(result))


	//url := tcpAddr.String()
	//resp, _ := http.Get(url)
	//bytes, _ := ioutil.ReadAll(resp.Body)
	//
	//fmt.Println("HTML:\n\n", string(bytes))
	//
	//resp.Body.Close()

	z := html.NewTokenizer(reader)
	for {
		tt := z.Next()

		switch {
		case tt == html.ErrorToken:
			// End of the document, we're done
			return ""
		case tt == html.StartTagToken:
			t := z.Token()

			isAnchor := t.Data == "title"
			if isAnchor {
				fmt.Println("We found a title!", t.String())

				//tt := t.Doc.Next()
				//
				//if tt == html.TextToken {
				//	next := node.Doc.Token()
				//	title = strings.TrimSpace(next.Data)
				//}
			}
		}
	}


	//resp := string(resp.Body)
	//return p.PredictResponse(resp, p)
	return "OK"
}




func main() {

	cidr := "192.168.1.0/24"

	iplist := []string{"192.168.1.1","192.168.1.156","192.168.1.50","192.168.1.234"}
	//iplist = []string{"192.168.1.1","192.168.1.2","192.168.1.3"}
	//iplist = []string{}

	ports := []int {80,23 }

	runtime.GOMAXPROCS(runtime.NumCPU())
	nbWorkers := runtime.NumCPU() * 3


	wp := workerpool.New(nbWorkers)



	_ = iplist
	_ = cidr
	_ = ports
	_ = wp

	c := IpRange(cidr)
	for i := range c {
		_ = i
		fmt.Println(i.String())
	}
	println("...")

	for ip := range IpRangeFromList( iplist ) {
		fmt.Println(ip.String())
	}
	println("...")

	//for ip:= range c {
	//	o := IsReachable("en0",ip.String())
	//	fmt.Printf(" %s reachable = %b",ip.String(),o)
	//
	//}

	ipchan := IpRangeFromList( iplist )
	//ipchan := IpRange(cidr)
	for ip := range Pingable( ipchan ) {

		fmt.Printf("pingable: %v\n",ip.String())
	}
	//time.Sleep(5 * time.Second)
	println("...")




	ipchan = IpRangeFromList(iplist)
	for ip := range ipchan {
		port := 80
		if IsPortOpen(ip.String(),port, 100 * time.Millisecond) {
			fmt.Printf("opened port: %v:%d\n", ip.String(), port)
		} else {
			fmt.Printf("closed port: %v:%d\n", ip.String(), port)
		}
	}
	//time.Sleep(5 * time.Second)
	println("...")



	ipchan = IpRangeFromList(iplist)
	addrchan := OpenedTcpAdress(ipchan,ports)

	for addr := range addrchan {
			fmt.Printf("opened addr: %v\n", addr.String())

			r := PredictHttp( & addr)
			_ = r
		    fmt.Printf("response  addr: %v %s\n", addr.String(),r)

	}
	//time.Sleep(5 * time.Second)
	println("Done")



	//// scan all http port in cidr
	//fmt.Printf("scanning all port 80 from 192.168.1.0/24\n")
	//http_ports :=  []int {80}
	//ipchan = IpRange( cidr )
	//addrchan = OpenedTcpAdress(ipchan,http_ports)
	//
	//t0 := time.Now()
	//for addr := range addrchan {
	//	fmt.Printf("http port opened: %v\n",addr.String())
	//}
	//t1 := time.Now()
	//t := t1.Sub(t0)
	////t := time.Duration(t1-t0)
	//
	//fmt.Printf("scanning all port 80 from 192.168.1.0/24 DONE in %v s\n",t.Seconds())
	//


	// scan all telnet port in cidr with worker pool
	fmt.Printf("scanning all port 22 from 192.168.1.0/24\n")
	telnet_ports :=  []int {23}
	ipchan = IpRange( cidr )

	//addrchan = OpenedTcpAdress(ipchan,telnet_ports)
	addrchan = WorkOpenedTcpAdress(ipchan,telnet_ports)

	t0 := time.Now()
	for addr := range addrchan {
		fmt.Printf("telnet port opened: %v\n",addr.String())
	}
	t1 := time.Now()
	t := t1.Sub(t0)
	//t := time.Duration(t1-t0)

	fmt.Printf("scanning all port 22 from 192.168.1.0/24 DONE in %v s\n",t.Seconds())


}


