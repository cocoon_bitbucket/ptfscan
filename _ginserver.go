package main

import (

	"bitbucket.org/cocoon_bitbucket/ptfscan/config"
	"bitbucket.org/cocoon_bitbucket/ptfscan/ginweb"

	"flag"
)



func main() {


	var (

		configFilename = flag.String("filename","ptfscan.toml","configuration filename")
	)


	done := make(chan bool)

	// parse flag options
	flag.Parse()


	// create a config
	cnf := config.NewConfig(* configFilename)
	_ = cnf



	go ginweb.Serve()


	// wait forever
	<- done

}
