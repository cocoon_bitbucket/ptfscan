package infra_test


import (


	"testing"
	"bitbucket.org/cocoon_bitbucket/ptfscan/infra/models"
	"github.com/stretchr/testify/assert"
	//"io/ioutil"
	//"fmt"
	"time"
	"github.com/garyburd/redigo/redis"
	"log"
	"fmt"

	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
)

/*

	pre-requisites: redis + fixture

 */

var (
	redis_url string = "redis://127.0.0.1:6379/0"
)


func getRedisConnection()  redis.Conn {

	option := redis.DialConnectTimeout(1 * time.Second)
	cnx, err := redis.DialURL(redis_url,option)

	if err != nil {
		log.Fatal("cannot connect to redis at %s : %s",redis_url,err.Error())
	}

	return cnx
}

func getInfraDb()  models.InfraDb {

	_,err := redistools.NewPool(redis_url)
	defer redistools.UnsetPool()

	db ,err := models.NewInfraDb(redis_url)
	if err != nil {
		log.Fatal(fmt.Sprintf("cannot connect to redis at %s : %s",redis_url,err.Error()))
	}
	return db

}



func TestModelDeviceIps(t *testing.T) {


	cnx := getRedisConnection()
	defer cnx.Close()


	v, err :=  models.FetchDeviceIp(cnx,"Livebox-2898")
	assert.Equal(t,nil,err)

	json_text,err := v.Marshal()
	assert.Equal(t,nil,err)
	_=json_text
	//fmt.Printf(string(json_text))

	//assert.Equal(t,"Livebox-2898",v.DeviceName)

}


func TestModelInfraLivebox(t *testing.T) {


	_,err := redistools.NewPool(redis_url)
	defer redistools.UnsetPool()

	db := getInfraDb()
	defer db.Close()

	livebox := "Livebox-2898"

	v, err := db.GetInfraLivebox()
	assert.Equal(t,nil,err)

	fmt.Printf("%s\n",v)

	//assert.Equal(t,"",v.GetIp(livebox))
	r,err := v.GetIp(livebox)
	assert.Equal(t,"192.168.200.65",r )

	r,err = v.GetPduRef(livebox)
	assert.Equal(t,"192.168.100.45/3",r )

}

