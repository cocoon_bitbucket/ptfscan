package infra


import(

	"testing"
	"github.com/garyburd/redigo/redis"
	"bitbucket.org/cocoon_bitbucket/ptfscan/infra/models"
	"github.com/stretchr/testify/assert"
	"log"
	"time"
	//"fmt"
	//"strings"
)


/*

	pre-requisites: REDIS DB

 */


func get_db() redis.Conn {

	var redis_url= "redis://localhost:6379/0"

	cnx, err := redis.DialURL(redis_url)

	if err != nil {
		log.Fatal("cannot connect to redis")
	}

	return cnx

}




func TestNewToken(t *testing.T) {


	token := models.NewToken()
	assert.Equal(t,16,len(token))


	//print(token)

}


func TestCreateAcl(t *testing.T) {


	cnx := get_db()

	_,err := models.CreateAcl(cnx,"Livebox",1,"Livebox-1000","Livebox-1001")
	assert.Equal(t,nil,err)

	time.Sleep(1 * time.Second)

	//print(token)

}

func TestGetAcl(t *testing.T) {


	cnx := get_db()

	realm := "Livebox"
	ttl := 5

	// create acl
	token,err := models.CreateAcl(cnx, realm,ttl,"Livebox-1000","Livebox-1001")
	assert.Equal(t,nil,err)

	// get members of acl
	members,err := models.GetAcl(cnx,token, realm)
	assert.Equal(t,2,len(members.Members))
	//print(members)

	// assert Livebox-1000 is member
	is_member,err := models.AssertAcl(cnx,token, realm,"Livebox-1000")
	assert.Equal(t,nil,err)
	assert.Equal(t,true,is_member)

	// assert Livebox-9999 is not member
	is_member2,err := models.AssertAcl(cnx,token, realm,"Livebox-9999")
	assert.Equal(t,nil,err)
	assert.Equal(t,false,is_member2)

	// wait for the ttl
	time.Sleep( time.Duration(ttl) * time.Second)


	// check acl does not exists anymore
	members,err = models.GetAcl(cnx,token, realm)
	assert.Equal(t,0,len(members.Members))
	//print(members)

	// check not member
	is_member,err = models.AssertAcl(cnx,token, realm,"Livebox-1000")
	assert.Equal(t,nil,err)
	assert.Equal(t,false,is_member)

	//print(token)

}


