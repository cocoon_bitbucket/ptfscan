package infra_test


import (

	"runtime"
	"testing"
	"log"
	"bitbucket.org/cocoon_bitbucket/ptfscan/infra"
	"path/filepath"

)



func currentDir() string {

	_, filename, _, _ := runtime.Caller(0)
	//fmt.Println(filename)

	dir, err := filepath.Abs(filepath.Dir(filename))
	if err != nil {
		log.Fatal(err)
	}
	return dir
}



func TestInfraManager(t *testing.T) {

	var redis_url= "redis://localhost:6379/0"

	m,err := infra.NewInfraManager(redis_url)
	if err != nil {
		log.Fatal("cannot create redis connection")
	}

	m.Do("FLUSHDB")



	infra_livebox_json := filepath.Join( currentDir(),"samples/infra_livebox.json")


	err = m.LoadInfraLiveboxFromFile(infra_livebox_json)
	if err != nil {
		log.Fatal(err)
	}

	// test one infra:device:ip entry

	//v,err := m.Do("HGET", models.DeviceIps_key,"Livebox-2898")
	//d := v.
	//assert.Equal(t,"192.168.200.65")
	////192.168.200.65




	return

}