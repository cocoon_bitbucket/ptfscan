package infra_test

import (


	"testing"
	"bitbucket.org/cocoon_bitbucket/ptfscan/infra/models"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"fmt"
)

/*

	pre-requisites: NO

 */

func TestLoadInfraLivebox(t *testing.T) {


	// read sample
	text, err := ioutil.ReadFile("./samples/infra_livebox.json")
	assert.Equal(t,err,nil)

	r,err := models.LoadInfraLivebox(text)


	hit := false
	for k,v := range r {

		fmt.Printf("key: %s => %s\n",k,v)

		if k == "Livebox-2898" {
			assert.Equal(t,v.Mac,"78:94:B4:3D:28:98")
			assert.Equal(t,"WDiCTqUD4RQFWNPvib",v.Wifi)
			password,_ := v.GetPassword()
			assert.Equal(t,"WDiCTqUD",password)
			hit = true
		}
	}

	assert.Equal(t,true,hit)
	if hit {
		ip,_ := r.GetIp( "Livebox-2898")
		assert.Equal(t,"192.168.200.65",ip)
		// 192.168.200.65

		ref,_ := r.GetPduRef( "Livebox-2898")
		assert.Equal(t,"192.168.100.45/3",ref)

		//pwd,err := r.G

		// 192.168.100.45/3

	}

	// test inexistant livebox
	ip,err := r.GetIp( "Livebox-inexistant")
	assert.Equal(t,"no such livebox: Livebox-inexistant",err.Error())
	assert.Equal(t,"",ip)


	fmt.Printf("%s\n",r)

}
