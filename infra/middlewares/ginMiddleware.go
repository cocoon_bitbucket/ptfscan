package middlewares


import (

	"github.com/gin-gonic/gin"
	"bitbucket.org/cocoon_bitbucket/ptfscan/infra/models"

	"log"
)


var (

	RedisContextName string = "InfraDb"

)

/*

	a middleware for gin framework add  context.InfraDb

	usage

	in main

	r.Use( InfraDbMiddleware(redisUrl))

	in handler
	db := c.MustGet("InfraDb").(*models.Infradb)


 */




func InfraDbMiddleware( redisUrl string) gin.HandlerFunc {

	//
	// a midleware to add a redis connection to the context
	//

	cnx ,err := models.NewInfraDb(redisUrl)

	//option := redis.DialConnectTimeout( 1 * time.Second)
	//cnx, err := redis.DialURL(redisUrl,option)
	if err != nil {
		log.Printf("cannot create redis connection: %s",err.Error())
		panic(err)
	}

	return func(c *gin.Context) {
		// set attribute "RedisDb" to cnx in context
		c.Set(RedisContextName, cnx)
		c.Next()
	}
}


