package infra


import (

	//"bitbucket.org/cocoon_bitbucket/ptfscan/config"
	"bitbucket.org/cocoon_bitbucket/ptfscan/infra/models"
	//"errors"
	"log"
	"github.com/garyburd/redigo/redis"
	"io/ioutil"
	"encoding/json"
)


type InfraManager struct {

	redis.Conn

	infra_livebox_key string    // infra:livebox
	infra_device_ip_key string  // infra:device:ip

}


func InitInfra(redis_url , filename string) error {

	// convenient function initialize livebox infra in redis db from json file
	// file is infra_livebox.json


	m,err := NewInfraManager(redis_url)
	if err != nil {
		return err
		//log.Fatal("cannot create redis connection")
	}

	//m.Do("FLUSHDB")

	err = m.LoadInfraLiveboxFromFile(filename)

	return err

}


func NewInfraManager( redis_url string) (* InfraManager,error){


	log.Printf("InfraManager New")

	manager := &InfraManager{}


	cnx,err := redis.DialURL(redis_url)
	if err != nil {
		return manager,err
	}

	manager.Conn = cnx
	manager.infra_livebox_key = models.InfraLiveboxKey
	manager.infra_device_ip_key = models.DeviceIps_key

	return manager,err
}

func ( m *InfraManager) LoadInfraLiveboxFromFile(filename string) error {

	// load json content from filename and write to redis key infra:livebox
	// content is dict of livebox

	log.Printf("InfraManager.LoadLiveboxFromFile:%s\n",filename)

	// read the file content
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}

	// check content is a valid configuration
	liveboxData,err := models.LoadInfraLivebox(content)
	if err != nil {
		log.Printf("InfraManager.LoadLiveboxFromFile:(%s) NOT a VALID livebox configuration\n",filename)
	}

	// add local Livebox-demo to data
	l := m.LocalInfraLivebox()
	name := l.Name
	liveboxData[name] = l
	new_content,err := json.Marshal(liveboxData)
	if err != nil {
		log.Fatal(err)
	}

	// store json data in the key infra:livebox
	_,err = m.Do("SET",m.infra_livebox_key,new_content)
	if err != nil {
		log.Fatal(err)
	}

	// create infra:device:ip
	err= m.createLiveboxIpEntries(liveboxData)
	if err != nil { log.Fatal(err)}

	// create entity:mac:*
	err = m.createMacEntities()

	return err

}

func ( m *InfraManager) createLiveboxIpEntries(infoMap models.InfraLiveboxInfoMap) error {

	// load livebox config from redis infra:livebox
	// create livebox/ip dictionary in redis ( key: infra:device:ip )
	log.Printf("InfraManager.createLiveboxIpEntries\n")
	err :=  models.InitDeviceIpFromMap(m.Conn	, infoMap)
	return err
}

func ( m *InfraManager) createMacEntities() error {

	// create macEntity schema
	models.CreateMacEntitySchema(m.Conn)

	// load livebox config from redis infra:livebox
	// create mac entities dictionary in redis ( entity:mac:index:name , entity:mac:index:mac )
	// from infra:livebox
	log.Printf("InfraManager.MacEntities\n")

	err :=  models.InitializeFromInfraLivebox(m.Conn)
	return err
}


func ( m *InfraManager) LocalInfraLivebox() ( models.InfraLiveboxInfo) {

	//
	// return an entry in InfraLiveboxInfo struct for local test purpose
	//

	l := models.InfraLiveboxInfo{}

	l.Name= "Livebox-demo"
	l.Ip = "192.168.1.1"
	l.Mac = "a0:1b:29:fc:12:70"

	l.Pdu_name="Energenie-demo"
	l.Pdu_ip= "192.168.1.30"
	l.Pdu_outlet = 4
	l.Pdu_mac = "88:B6:27:01:65:E4"

	return l

}

