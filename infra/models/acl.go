package models

import(

	"github.com/garyburd/redigo/redis"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"

	//"fmt"
	//"time"
	//"errors"

)

/*


	acl store Access Control list for devices


	acl:<name>:<token>:  -> list with attl


	we create an acl named Livebox to access a group of livebox lb1,lb2 with a ttl

		acl:Livebox:<token> -> LIST  [lb1,lb2]


	NewAcl ( realm , ttl , ... member) -> return token or err

	Get( token, realm ) -> list of members  or err ()

	Revoke ( token )
	Renew( token, ttl )
	ttl ( token )


	oauth header:

	Authorization: Bearer 0b79bab50daca910b000d4f1a2b675d604257e42



 */


type AclRecord struct {

	Members []string  	`json:"members"`
	Ttl int				`json:"ttl"`

}



func CreateAcl( cnx redis.Conn,  realm string, ttl int, member ...string) (token string, err error) {


	//acl := redistools.NewAclProvider(cnx, realm,"")
	acl := redistools.NewAclManager(cnx)
	r,err := acl.AddAcl(realm,ttl, member...)
	//acl.Close()
	return r,err

}


func GetAcl( cnx redis.Conn ,token string, realm string) ( acl AclRecord,err error) {
	// return an acl record : members + ttl
	acl = AclRecord{}
	p := redistools.NewAclProvider(cnx, realm,token)
	members,err := p.Get()
	if err != nil {
		return acl,err
	}
	ttl,err := p.Ttl()

	acl.Members = members
	acl.Ttl = int(ttl)

	return acl,err
}

func GetAclOfRealm( cnx redis.Conn ,realm string) (map[string][]string,error) {
	// return members of the set

	acl := redistools.NewAclManager(cnx)
	return acl.AclOfRealm(realm)

}



func AssertAcl( cnx redis.Conn ,token string, realm string, member string) (bool,error) {
    // assert member in on the list
	acl := redistools.NewAclProvider(cnx, realm,token)
	return acl.Assert(member)

}


//
// implements Datastore interface
//

func (db * InfraDb) CheckAcl( realm,token,name string) ( bool, error) {
	return AssertAcl( db.Conn , token,realm,name )
}

func (db * InfraDb) CreateAcl( realm string, ttl int, member ...string) (token string, err error) {
	return CreateAcl(db.Conn,realm,ttl,member...)
}

func (db * InfraDb) GetAclOfRealm( realm string) (map[string][]string,error){
	return GetAclOfRealm(db.Conn,realm)
}

func (db * InfraDb) GetAcl( token string, realm string) ( acl AclRecord,err error) {
	return GetAcl( db.Conn ,token , realm)
}
