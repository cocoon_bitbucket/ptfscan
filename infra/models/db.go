package models


import (
	"time"
	"github.com/garyburd/redigo/redis"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"

	"log"
)


type InfraStore interface {
	//
	GetDeviceIp( deviceName string ) ( DeviceIps , error)
	GetInfraLivebox() (InfraLiveboxInfoMap, error)

	// acl
	CreateAcl( realm string, ttl int, member ...string) (token string, err error)
	GetAcl( token string, realm string) ( AclRecord, error)
	GetAclOfRealm( realm string) (map[string][]string,error)
	CheckAcl( realm,token,member string) (bool,error)

}


type InfraDb struct {
	redis.Conn

	RedisPool redistools.Pool
}

func NewInfraDb( redisUrl  string) (db InfraDb, err error) {

	option := redis.DialConnectTimeout( 1 * time.Second)
	cnx, err := redis.DialURL(redisUrl,option)
	if err != nil {
		log.Printf("cannot create redis connection: %s\n",err.Error())
		return db,err
	}

	redis_pool,err := redistools.GetPool()
	// redis_pool,err := redistools.NewPool(redisUrl)
	if err != nil {
		log.Printf("NewInfraDb(%s): %s\n",redisUrl,err.Error())
		return db,err
	}
	return InfraDb{cnx,*redis_pool}, nil
}


//func NewRedisPool(redisUrl string) *redis.Pool {
//	// create a redis pool for a given redis url (eg redis://localhost:6379/0 )
//	return &redis.Pool{
//		MaxIdle: 3,
//		IdleTimeout: 240 * time.Second,
//		//Dial: func () (redis.Conn, error) { return redis.Dial("tcp", addr) },
//		Dial: func () (redis.Conn, error) { return redis.DialURL(redisUrl) },
//	}
//}
//







