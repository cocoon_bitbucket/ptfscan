package models


/*

	topology : describes pdu, livebox and links


	pdu_name [ livebox_name, livebox_name, livebox_name, livebox_name  ]


	pdu_name {  mac }

	livebox_name { mac ...


	name ,mac , kind


	entity:mac:schema  HASH  name:unique , mac:unique , kind:duplicate (livebox / energenie)

	entity:mac:index:name
	entity:mac:index:mac
	entity:mac:members

	topics:

	oid:?
	oid:?:Links  HASH  { outlet1:"name1" , outlet2:"name2" ,outlet3:"name3", outlet4:"name4" }

	oid:?:Livebox  {  }
 */

import (
	"github.com/garyburd/redigo/redis"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools/store"
	"strconv"
	//"errors"
	//"fmt"
	//"strings"
	"encoding/json"
	"net"
	"fmt"
)

//
//
//  model
//


var (

	// mac schema   a MAcEntity is an entity with a name ,a mac address and a kind
	MacEntitySchema = store.EntitySchema{
		Name: "mac",
		Indexes: map[string]string{
			"name": "unique",
			"mac": "unique",
			"kind": "duplicate",
		},
	}
)

type properties map[string]string


func CreateMacEntitySchema(cnx redis.Conn) (err error){
	// create device schema in database
	schema := MacEntitySchema
	err = schema.Save(cnx)
	return err
}


type MacEntity struct {
	store.Entity
}


func (e * MacEntity) Schema() store.EntitySchema {
	return MacEntitySchema
}

func ( e * MacEntity) AddHashTopic( cnx redis.Conn,name string, data  map[string]interface{}) (err error) {

	h := store.NewHash(cnx,e.Oid)
	err= h.Write(name,data)
	return err
}

func ( e * MacEntity) GetHashTopic( cnx redis.Conn,name string) ( content []byte, err error) {

	h := store.NewHash(cnx,e.Oid)
	content, err = h.Read(name)
	return content,err
}



func NewMacEntity( name string,mac string,kind string, properties properties) (entity MacEntity) {

	p := make(map[string]string)
	for k,v := range properties{
		p[k]= v
	}
	p["name"] = name
	p["mac"] = mac
	p["kind"] = kind
	e := store.Entity{MacEntitySchema.Name, p, 0}
	entity = MacEntity{e}

	return entity
}


func LoadMacByName(cnx redis.Conn,name string)  (entity MacEntity,err error)  {

	e,err := store.LoadEntityByIndex(cnx, MacEntitySchema.Name,"name",name)
	if err != nil { return entity, err }

	entity.EntityName = e.EntityName
	entity.Properties = e.Properties
	entity.Oid = e.Oid

	return entity,err
}

func LoadMacByMacAddress(cnx redis.Conn,mac string)  (entity MacEntity,err error)  {

	e,err := store.LoadEntityByIndex(cnx, MacEntitySchema.Name,"mac",mac)
	if err != nil { return entity, err }

	entity.EntityName = e.EntityName
	entity.Properties = e.Properties
	entity.Oid = e.Oid

	return entity,err
}


//
//  Livebox infra ( derived from MacEntity )
//

type PduCoordinates struct {
	Name string
	Outlet string
}

type LiveboxEntity struct {
	MacEntity
}


func NewFromInfraLiveboxInfo( livebox InfraLiveboxInfo) (lb LiveboxEntity){
	//
	// create a Livebox infra entity from struct InfraLivebox Info
	//
	p := make(map[string]string)
	p["kind"] = "livebox"
	p["name"] = livebox.Name

	// store mac
	mac,err := net.ParseMAC(livebox.Mac)
	if err == nil {
		// store mac with canonical format
		p["mac"]  = mac.String()
	} else {
		fmt.Printf("Bad format for mac address: %s , livebox:%s\n",livebox.Mac,livebox.Name)
		p["mac"] = livebox.Mac
	}

	// store IP
	ip := net.ParseIP(livebox.Ip)
	p["ip"] = ip.String()

	p["lan"] = livebox.Lan
	p["wifi"] = livebox.Wifi
	p["auth"] = livebox.Auth
	p["ex"] = livebox.Ex
	p["h235"] = livebox.H235
	p["serial"] = livebox.Serial
	p["type"] = livebox.Type
	p["pdu_name"] = livebox.Pdu_name
	p["pdu_outlet"] = strconv.Itoa(livebox.Pdu_outlet)

	p["pdu_ip"] = livebox.Pdu_ip
	p["pdu_ip"] = net.ParseIP(livebox.Pdu_ip).String()

	//p["pdu_mac"] = livebox.Pdu_mac
	pmac,err := net.ParseMAC(livebox.Pdu_mac)
	if err == nil {
		// store mac with canonical format
		p["pdu_mac"]  = pmac.String()
	} else {
		fmt.Printf("Bad format for pdu mac address: %s , livebox:%s\n",livebox.Mac,livebox.Name)
		p["pdu_mac"] = livebox.Mac
	}

	e := NewMacEntity(p["name"],p["mac"],p["kind"],p)
	lb = MakeLiveboxEntity(e)

	return lb
}


func MakeLiveboxEntity( entity MacEntity ) (LiveboxEntity){
	return LiveboxEntity{entity}
}

func (e * LiveboxEntity) GetPduCoordinates(cnx redis.Conn) (pdu PduCoordinates,err error) {

	data,err := e.GetHashTopic(cnx,"pdu")
	err = json.Unmarshal(data,&pdu)
	if err != nil { return pdu,err}
	return pdu,err
}

func (e * LiveboxEntity) SetPduCoordinates(cnx redis.Conn,pdu_name,pdu_outlet string )(err error) {

	//
	// set the pdu coordinates name,outlet
	//
	//  oid:?:hash:pdu  JSON { name:  , outlet: }
	//
	data := make(map[string]interface{})
	data["name"]= pdu_name
	data["outlet"]= pdu_outlet
	err = e.AddHashTopic(cnx,"pdu",data)
	return err
}


//
//  Pdu infra ( derived from MacEntity )
//

type PduEntity struct {
	MacEntity
}
func MakePduEntity( entity MacEntity ) ( PduEntity){
	return PduEntity{entity}
}
