package models

import (
	"github.com/garyburd/redigo/redis"
	"crypto/rand"
	"fmt"
	"strings"
)


func NewToken() string {

	b := make([]byte, 8)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}




func InitializeFromInfraLivebox( cnx redis.Conn ) (err error) {
	//
	// initialize infra livebox and pdu entities  from  redis key infra:livebox
	//
	//pdu := make(map[string]string)
	data, err := FetchInfraLivebox(cnx)
	if err != nil { return err }
	for _, p := range data {

		// write livebox entry
		e := NewFromInfraLiveboxInfo(p)
		_,err := e.Save(cnx)

		// write pdu entry
		pdu := make(map[string]string)
		pdu["ip"] = e.Properties["ip"]

		epdu  := NewMacEntity( e.Properties["pdu_name"],e.Properties["pdu_mac"],"Energenie",pdu)
		epdu.Save(cnx)

		if err != nil {
			if strings.Contains(err.Error(),"object already exists") {
				err = nil
				continue
			} else {
				break
			}
		}
	}
	return err
}

//func SliceIntersection(a []string, b []string) (inter []string) {
//	// interacting on the smallest list first can potentailly be faster...but not by much, worse case is the same
//	low, high := a, b
//	if len(a) > len(b) {
//		low = b
//		high = a
//	}
//
//	done := false
//	for i, l := range low {
//		for j, h := range high {
//			// get future index values
//			f1 := i + 1
//			f2 := j + 1
//			if l == h {
//				inter = append(inter, h)
//				if f1 < len(low) && f2 < len(high) {
//					// if the future values aren't the same then that's the end of the intersection
//					if low[f1] != high[f2] {
//						done = true
//					}
//				}
//				// we don't want to interate on the entire list everytime, so remove the parts we already looped on will make it faster each pass
//				high = high[:j+copy(high[j:], high[j+1:])]
//				break
//			}
//		}
//		// nothing in the future so we are done
//		if done {
//			break
//		}
//	}
//	return
//}