package models



import (

	"encoding/json"
	"errors"
	"fmt"
	"net"
	"github.com/garyburd/redigo/redis"
)

/*

	infra livebox see python/infra store dict of livebox properties

	infra:livebox -> json content : dict [livebox-name] { properties }



	a json content describing parameters of all livebox  redis:GET infra:livebox

	the content is a dictionary  livebox_name { data }


	'name': 'Livebox-2898',

	'mac': '78:94:B4:3D:28:98',
	'ip': '192.168.200.65',

	'lan': 'B5-27-16',
	'wifi': 'WDiCTqUD4RQFWNPvib',
	'auth': '',
	'ex': 'EX4300-01-16',
	'h235': '',

	'serial': 'FBKL00100189382',
	'type': 'Livebox Sercom',

	'pdu_name': 'PDU-J4-P5'
	'pdu_outlet': 3,
	'pdu_mac': '88:B6:27:01:65:DE',
	'pdu_ip': '192.168.100.45',



	support for mac address
	var hwAddr net.HardwareAddr


 */

var (

	InfraLiveboxKey = "infra:livebox"


)


// a set of info for one livebox coming from csf files ( at redis key: InfraLiveboxKey ("infra:livebox")
type InfraLiveboxInfo struct {

	Name string     	`json:"name"`		// 'Livebox-2898'

	Mac string 			`json:"mac"`		// '78:94:B4:3D:28:98',
	Ip string            `json:"ip"`  		// '192.168.200.65'

	Lan	string			`json:"lan"`		// 'B5-27-16'

	Wifi	string		`json:"wifi"`		//  'WDiCTqUD4RQFWNPvib'
	Auth	string		`json:"auth"`		// ''
	Ex		string		`json:"ex"`			// 'EX4300-01-16'
	H235	string		`json:"h235"`		//'',

	Serial	string		`json:"serial"`		//': 'FBKL00100189382',
	Type	string		`json:"type"`		// ': 'Livebox Sercom',

	Pdu_name	string	`json:"pdu_name"`	// 'PDU-J4-P5
	Pdu_outlet	int		`json:"pdu_outlet"`	// ': 3
	Pdu_mac		string	`json:"pdu_mac"`	// '88:B6:27:01:65:DE',
	Pdu_ip		string	`json:"pdu_ip"`		// '192.168.100.45',

}

//func (r * InfraLiveboxInfo )Marshal() ([]uint8,error) {
//	b, err := json.Marshal(r)
//	return b,err
//}
//
//func (r * InfraLiveboxInfo ) Unmarshal( data []byte )  error {
//	err := json.Unmarshal(data,r)
//	return err
//}

func (r * InfraLiveboxInfo ) GetPassword() (password string ,err error) {
	// extract password from wifi key ( first 8 chars
	if len(r.Wifi) >= 8 {
		password = r.Wifi[:8]
		err = nil
	} else {
		password= ""
		err = errors.New("cannot determine password, no wifi key or wifi key < 8 chars")
	}
	return password,err
}

func (r * InfraLiveboxInfo ) GetIp() ( ip net.IP ) {
	// extract password from wifi key ( first 8 chars
	ip = net.ParseIP(r.Ip)
	return ip
}

func ( r InfraLiveboxInfo) GetPduCoordinates() ( name string,ip net.IP, outlet int){
	// extract pdu data
	name = r.Name
	ip = net.ParseIP(r.Pdu_ip)
	outlet = r.Pdu_outlet
	return name,ip,outlet
}



type InfraLiveboxInfoMap map[string] InfraLiveboxInfo



// return ip adress of a livebox
func ( i InfraLiveboxInfoMap) GetIp( livebox_name string) (string,error){

	var err error = nil
	var ip string = ""

	entry, ok := i[livebox_name]

	if ok == true {
		ip = entry.Ip
	} else {
		err= errors.New(fmt.Sprintf("no such livebox: %s" , livebox_name))
	}
	return ip , err
}

// return  $pdu_ip/$outlet of a livebox
func ( i InfraLiveboxInfoMap) GetPduRef( livebox_name string) (string,error){

	var err error = nil
	var ref string = ""


	entry, ok := i[livebox_name]

	if ok == true {
		ref =  fmt.Sprintf("%s/%d", entry.Pdu_ip,entry.Pdu_outlet)
	} else {
		err= errors.New(fmt.Sprintf("no such livebox: %s" , livebox_name))
	}
	return ref , err
}


func LoadInfraLivebox( json_text []byte ) (InfraLiveboxInfoMap, error) {


	var err error = nil
	var infra InfraLiveboxInfoMap

	err = json.Unmarshal( json_text, &infra)
	if err != nil {
		fmt.Println("LoadInfraLivebox error:", err)
	}

	return infra,err

}



//   module functions
func FetchInfraLivebox( db redis.Conn ) (  InfraLiveboxInfoMap,error) {

	data := &InfraLiveboxInfoMap{}

	v,err  :=  redis.Bytes(db.Do( "GET" , InfraLiveboxKey))
	if err != nil {
		return *data,err
	}
	err = json.Unmarshal( v, &data)
	return *data,err

}



// implements InfraStore interface
func (db * InfraDb) GetInfraLivebox() ( InfraLiveboxInfoMap, error) {
	return FetchInfraLivebox( db.Conn )
}
