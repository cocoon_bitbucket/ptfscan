package models


import (

	"github.com/garyburd/redigo/redis"
	"encoding/json"
	"net"
)

/*

	DeviceIps  store ip , pdu_ip , pdu_outlet for a device_name

	infra:device:ip  hash  [livebox-name] { ip, pdu_ip , pdu_outlet }


 */



var (

	DeviceIps_key string =  "infra:device:ip"

)

// DeviceIps stores ip address of a device and its powerswitch
// HGET  infra:device:ip , $device_name  => json content: { ip=, pdu_ip= ,pdu_outlet= }


// ip address of a device + powerswitch address if any
type DeviceIps struct {

	DeviceIp net.IP		`json:"ip"`
	PduIp net.IP		`json:"pdu_ip"`
	PduOutlet int		`json:"pdu_outlet"`
}

func (r * DeviceIps )Marshal() ([]uint8,error) {
	b, err := json.Marshal(r)
	return b,err
}

func (r * DeviceIps ) Unmarshal( data []byte )  error {
	err := json.Unmarshal(data,r)
	return err
}



// build functions
func InitDeviceIpFromMap( db redis.Conn, infoMap InfraLiveboxInfoMap) error {

	//
	// create redis dictionary infra:device:ip
	//
	var err error = nil


	for k,v := range infoMap{

		ip := net.ParseIP(v.Ip)
		pdu_ip := net.ParseIP(v.Pdu_ip)

		pdu_outlet := v.Pdu_outlet
		entry := &DeviceIps{ip,pdu_ip,pdu_outlet}

		json_text,err := json.Marshal(entry)

		//json_text,err := entry.Marshal()
		if err != nil {
			return err
		}
		_,err  =  db.Do( "HSET" , DeviceIps_key, k, json_text)
		if err != nil {
			return err
		}
	}
	return err

}





//   module functions
func FetchDeviceIp( db redis.Conn, deviceName string) ( DeviceIps,error) {
	//
	// read device data from redis key and return it
	//

	data := &DeviceIps{}

	v,err  :=  redis.Bytes(db.Do( "HGET" , DeviceIps_key, deviceName))
	if err != nil {
		return *data,err
	}

	err = data.Unmarshal(v)
	return *data,err
}



// implements Datastore interface
func (db * InfraDb) GetDeviceIp( deviceName string ) ( DeviceIps , error) {
	return FetchDeviceIp( db.Conn , deviceName)
}



