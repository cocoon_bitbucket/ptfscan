package models_test


import(

	"testing"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools/store"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	infra "bitbucket.org/cocoon_bitbucket/ptfscan/infra/models"
	"github.com/stretchr/testify/assert"
	//"fmt"
	//"strings"
)



var (


	redisUrl = "redis://127.0.0.1:6379/0"

	//Livebox_demo = ipscan.NewDeviceEntity(
	//	"192.168.1.1",
	//	"00:00",
	//	"livebox", make(map[string]string),
	//)


	dataset = map[string]map[string]string{
		"Livebox-demo": {
			"name": "Livebox-demo",
			"mac" : "00:00:00:00:00:01",
			"kind": "livebox",
		},
		"Livebox-2898": {
			"name": "Livebox-2898",
			"mac" : "00:00:00:00:00:02",
			"kind": "livebox",
		},

		"energenie": {
			"name": "Energenie-demo",
			"mac" : "00:00:00:00:01:01",
			"kind": "energenie",
		},
	}

)


func TestMacEntity(t *testing.T) {

	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	cnx.Do("FLUSHDB")

	// create item model
	infra.CreateMacEntitySchema(cnx)

	// create dataset
	for _, v := range dataset {
		e := infra.NewMacEntity(v["name"], v["mac"], v["kind"], nil)
		x, err := e.Save(cnx)
		_ = x
		assert.Equal(t, nil, err)

	}

	o, err := infra.LoadMacByName(cnx, "Livebox-demo")
	assert.Equal(t, nil, err)
	assert.Equal(t, "Livebox-demo", o.Properties["name"])
	assert.Equal(t, "00:00:00:00:00:01", o.Properties["mac"])
	assert.Equal(t, "livebox", o.Properties["kind"])

	o, err = infra.LoadMacByMacAddress(cnx, "00:00:00:00:00:01")
	assert.Equal(t, nil, err)
	assert.Equal(t, "Livebox-demo", o.Properties["name"])
	assert.Equal(t, "00:00:00:00:00:01", o.Properties["mac"])
	assert.Equal(t, "livebox", o.Properties["kind"])

	redistools.UnsetPool()
}


func TestLiveboxMacEntity(t *testing.T) {

	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	cnx.Do("FLUSHDB")

	// create item model
	infra.CreateMacEntitySchema(cnx)

	// create dataset
	for _, v := range dataset {
		e := infra.NewMacEntity(v["name"], v["mac"], v["kind"], nil)
		x, err := e.Save(cnx)
		_ = x
		assert.Equal(t, nil, err)

	}

	o, err := infra.LoadMacByName(cnx, "Livebox-demo")
	assert.Equal(t, nil, err)
	assert.Equal(t, "Livebox-demo", o.Properties["name"])
	assert.Equal(t, "00:00:00:00:00:01", o.Properties["mac"])
	assert.Equal(t, "livebox", o.Properties["kind"])

	lb := infra.MakeLiveboxEntity(o)

	lb.SetPduCoordinates(cnx,"Energenie-demo","4")

	pdu,err := lb.GetPduCoordinates(cnx)
	assert.Equal(t,nil,err)
	assert.Equal(t,"Energenie-demo",pdu.Name)
	assert.Equal(t,"4",pdu.Outlet)


	redistools.UnsetPool()
}


func TestFullLiveboxEntity(t *testing.T) {

	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	cnx.Do("FLUSHDB")

	// create item model
	infra.CreateMacEntitySchema(cnx)

	// create dataset
	p := make(map[string]string)
	p["name"]= "lb"
	p["mac"] ="00:00:00:00:00:01"
	p["kind"] = "livebox"
	p["ip"] = "192.168.1.1"



	e := infra.NewMacEntity(p["name"], p["mac"], p["kind"], p)
	x, err := e.Save(cnx)
	_ = x
	assert.Equal(t, nil, err)

	o, err := infra.LoadMacByName(cnx, "lb")
	assert.Equal(t, nil, err)
	assert.Equal(t, "lb", o.Properties["name"])
	assert.Equal(t, "00:00:00:00:00:01", o.Properties["mac"])
	assert.Equal(t, "livebox", o.Properties["kind"])
	assert.Equal(t, "192.168.1.1", o.Properties["ip"])


	redistools.UnsetPool()

}


// NewFromInfraLiveboxInfo
func TestNewFromInfraLiveboxInfo(t *testing.T) {
	//
	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	cnx.Do("FLUSHDB")

	// create item model
	infra.CreateMacEntitySchema(cnx)

	// create dataset
	data := infra.InfraLiveboxInfo{}
	data.Name= "Livebox-demo"
	data.Mac = "00:00:00:00:00:01"

	data.Ip = "192.168.1.1"
	data.Pdu_name= "Energenie-demo"
	data.Pdu_outlet = 4



	e := infra.NewFromInfraLiveboxInfo(data)
	x, err := e.Save(cnx)
	_ = x
	assert.Equal(t, nil, err)

	o, err := infra.LoadMacByName(cnx, "Livebox-demo")
	assert.Equal(t, nil, err)
	assert.Equal(t, "Livebox-demo", o.Properties["name"])
	assert.Equal(t, "00:00:00:00:00:01", o.Properties["mac"])
	assert.Equal(t, "livebox", o.Properties["kind"])
	assert.Equal(t, "192.168.1.1", o.Properties["ip"])


	redistools.UnsetPool()

}