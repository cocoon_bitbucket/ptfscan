package reporters

import "strings"

func convert_channel_to_key( channel string) string{

	// replace  '/' with ':'
	return strings.Replace(channel,"/",":",-1)

}
