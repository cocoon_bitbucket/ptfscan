package reporters


import (

	"github.com/garyburd/redigo/redis"
	"bitbucket.org/cocoon_bitbucket/ptfscan/subscribers"
	"log"
)


func HeartbeatReporter ( subscriber subscribers.RedisSubscriber){


	url := subscriber.Url

	subscriber.Subscribe("ptf/Heartbeat/*")

	writer,err := redis.DialURL(url)
	if err == nil {
		_=writer
		for {
			m ,err := subscriber.Receive()

			if err == nil {

				log.Printf("received: %s:%s", m.Channel,m.Data)

				key := convert_channel_to_key(m.Channel)
				value := string(m.Data)

				writer.Send("SET",key,value)
				writer.Send("EXPIRE",key, 60 )
				writer.Flush()

			}

		}

	}


}