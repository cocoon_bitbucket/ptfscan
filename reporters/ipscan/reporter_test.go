package ipscan_test

import (

	"testing"
	"bitbucket.org/cocoon_bitbucket/ptfscan/reporters/ipscan"
	"time"
	//"strconv"
	"github.com/magiconair/properties/assert"
)




func TestReporter_HandleEnergenieLanSettings(t *testing.T) {

	pool := get_pool()

	// create schema for device
	cnx := pool.Get()
	defer cnx.Close()
	cnx.Do("FLUSHDB")

	ipscan.CreateDeviceEntitySchema(cnx)


	data := make(map[string]interface{})
	data["mac"]="88B6270165E4"
	response := ipscan.Response{}
	response.Timestamp = int32(time.Now().Unix())
	response.Response = data

	reporter := ipscan.Reporter{pool}

	ip := "192.168.1.30"

	err := reporter.HandleEnergenieLanSettings(cnx,response,ip)
	assert.Equal(t,nil,err)


	return
}


func TestReporter(t *testing.T) {


	pool := get_pool()

	// create schema for device
	cnx := pool.Get()
	defer cnx.Close()
	cnx.Do("FLUSHDB")

	ipscan.CreateDeviceEntitySchema(cnx)


	reporter := ipscan.Reporter{pool}

	// launch mock DeviceInfo publisher
	go func() {
		mockPublisher()
	}()

	reporter.Run()



}

