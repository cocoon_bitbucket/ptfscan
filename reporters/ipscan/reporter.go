package ipscan

import(

	"github.com/garyburd/redigo/redis"
	"bitbucket.org/cocoon_bitbucket/ptfscan/portScanner/inspectors"
	"fmt"
	"encoding/json"
	"errors"
	"strings"
	"strconv"
)


/*

	ipscan reporter :


	subscribe to pubsub channel ptf/DeviceInfo/*

	fecth information
		DeviceInfo.BaseMac
		deviceInfo.Uptime
		deviceInfo.DeviceStatus
		deviceInfo.Description
		DeviceInfo.ExternalIPAddress

	create ipscan entities in redis

		e := ipscan.NewDeviceEntity(ip,mac,kind:"Livebox",nil)
		x,err := e.Save(cnx)

		//store DeviceInfo properties
		x.AddHashTopic( cnx, "DeviceInfo, data  map[string]string)

 */




// c := redis.PubSubConn{Conn: sc}



type Reporter struct {
	Pool redis.Pool
}

type Response inspectors.Response



func ( r * Reporter) Run(){

	cnx := r.Pool.Get()
	defer cnx.Close()

	db := r.Pool.Get()
	defer db.Close()

	sub := redis.PubSubConn{Conn: cnx}
	sub.PSubscribe("ptf/DeviceInfo/*","ptf/EnergenieLanConfig/*","ptf/EnergenieStatus/*")


	// receive loop
	for {
		switch v := sub.Receive().(type) {
		case redis.PMessage:

			fmt.Printf("receive a PMessage on channel %s : %s\n",v.Channel,v.Data)
			err := r.HandleMessage(db,v.Channel,v.Data)
			if err != nil {
				fmt.Printf("Handle message error: %s",err.Error())
			}

		case redis.Subscription:
			fmt.Printf("subscription message: %s: %s %d\n", v.Channel, v.Kind, v.Count)

		case error:
			fmt.Println("error pub/sub, delivery has stopped")
			return
		}
	}

}


func (r * Reporter ) SplitChannel(channel string) (prefix,topic,ip string, err error){
	// split the channel ptf/DeviceInfo/192.168.1.1 into prefix/topic/ip
	if strings.Contains(channel,"/"){
		parts := strings.Split(channel,"/")
		if len(parts) == 3 {
			return parts[0],parts[1],parts[2],err
		}
	}
	err = errors.New("Bad channel: " + channel)
	return "","","",err
}

func (r * Reporter ) HandleMessage(cnx redis.Conn, channel string, message []byte) (err error){


	_,topic,ip , err := r.SplitChannel(channel)
	if err != nil {
		return err
		//fmt.Print(err.Error())
	}

	response := Response{}
	err = json.Unmarshal(message,&response)
	if err != nil {
		//fmt.Printf("error: %s\n",err.Error())
		return err
	}


	if response.Response == nil {
		// an empty response received
		err = errors.New("empty response received for topic: " + topic)
		return err
	}

	switch topic {
	case "DeviceInfo":
		return r.HandleDeviceInfo(cnx,response,ip)
	case "EnergenieLanConfig":
		return r.HandleEnergenieLanSettings(cnx,response,ip)
	case "EnergenieStatus":
		return r.HandleEnergenieStatus(cnx,response,ip)

	}
	err = errors.New("Unknown topic: " + topic)
	return err
}

func (r * Reporter ) HandleDeviceInfo(cnx redis.Conn, response Response, ip string) (err error) {

	timestamp :=  strconv.Itoa(int(response.Timestamp))

	data :=  response.Response.(map[string]interface{})
	data["_timestamp"] = timestamp
	data["_request"] = response.Request

	mac,ok := data["DeviceInfo.BaseMAC"]
	if ok {
		e := NewDeviceEntity(ip, mac.(string), "livebox", nil)
		x, err := e.Save(cnx)
		if err == nil {
			err = x.AddHashTopic(cnx, "DeviceInfo", data)
			if err != nil {
				fmt.Printf("cannot write DeviceInfo topic: %s", err.Error())
				return err
			}
		}
	} else {
		err = errors.New("No DeviceInfo.BaseMAC in message")
	}
	return err
}

func (r * Reporter ) HandleEnergenieLanSettings(cnx redis.Conn, response Response, ip string) (err error) {

	//timestamp := response.Timestamp
	//request := response.Request

	data := response.Response.(map[string]interface{})

	mac,err := CanonicalMacAddress(data["mac"].(string))
	if err == nil  {
		e := NewDeviceEntity(ip, mac.String(), "energenie", nil)
		_, _ = e.Save(cnx)

	} else {
		err = errors.New("No mac field in message")
	}


	//err = errors.New("Not yet implemented")
	return err

}


func (r * Reporter ) HandleEnergenieStatus(cnx redis.Conn, response Response, ip string) (err error) {

	// handle energenie status message

	// ptf/EnergenieStatus/192.168.1.30 : {"_ip":"192.168.1.30","_err":null,"_timestamp":1519663625,"_request":"EnergenieStatus",
	// 	"response":[false,false,false,false]}


	// try to find an entry in mac entity
	entity,err := LoadDevicebyIp(cnx,ip)
	if err == nil {
		// we found an energeny device with this ip
		timestamp := strconv.Itoa(int(response.Timestamp))

		data := response.Response.([]interface{})

		record := make(map[string]interface{})

		record["_timestamp"] = timestamp
		record["_request"] = response.Request
		for i, v := range data {

			key := fmt.Sprintf("outlet_%d", i+1)
			if v == true {
				record[key] = "1"
			} else {
				record[key] = "0"
			}
		}
		// store topic
		err := entity.AddHashTopic(cnx,"EnergenieStatus",record)
		_=err

	} else {
		// no energenie mac entity found
		err = errors.New("HandleEnergenieStatus: error: " + err.Error() + "\n")
	}



	return err

}


// ptf/EnergenieStatus/192.168.1.30 : {"_ip":"192.168.1.30","_err":null,"_timestamp":1519663625,"_request":"EnergenieStatus","response":[false,false,false,false]}
