package ipscan


import (
	"github.com/garyburd/redigo/redis"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools/store"
	//"strconv"
	//"errors"
	//"fmt"
	"strings"
	"github.com/pkg/errors"
)

/*


	ipscan device

	subscribe to redis channel ptf/DeviceInfo


	create an ipscan entity and attach a Device info topic


	entity:ipscan:<ip>    HASH { ip="unique" , mac="unique" , kind="duplicate" }
	entity:ipscan:index:ip HASH
	entity:ipscan:index:mac HASH

	entity:ipscan:members SET

	oid:?  HASH { _entity="ipscan" , kind="livebox, _oid=? ip= , mac= }

	oid:?:DeviceInfo JSON { DeViceInfo. .... }


 */



//
//  model
//


/*

	device is an entity with
	 * a unique index "name"
	 * a unique index mac
	 * and a duplicate index "kind"

	entity:device:schema  HASH  { name:"named"  , mac= kind=}


	oid:1 HASH { _entity:"named" , name:"my_entity" , mac:  , other:"other"}
	entity:device:index:name HASH
	entity:device:index:mac HASH


 */

var (

	// device schema
	DeviceEntitySchema = store.EntitySchema{
		Name: "device",
		Indexes: map[string]string{
			"ip": "unique",
			"mac": "unique",
			"kind": "duplicate",
		},
	}
)

type properties map[string]string


func CreateDeviceEntitySchema(cnx redis.Conn) (err error){
	// create device schema in database
	schema := DeviceEntitySchema
	err = schema.Save(cnx)
	return err
}


type DeviceEntity struct {
	store.Entity
}

// overwrite Entity Save
func ( e * DeviceEntity) Save (cnx redis.Conn) ( entity *DeviceEntity, err error) {

	status := "unknown"
	previous_ip,err := store.LoadEntityByIndex(cnx,e.EntityName,"ip",e.Properties["ip"])
	if err != nil {
		if ! strings.Contains(err.Error(),"redigo: nil returned") {
			// a redis error append
			return e,err
		} else {
			// ip not found
			//print("ip not found\n")
		}
	}
	previous_mac,err := store.LoadEntityByIndex(cnx,e.EntityName,"mac",e.Properties["mac"])
	if err != nil {
		if ! strings.Contains(err.Error(), "redigo: nil returned") {
			// a redis error append
			return e, err
		} else {
			// ip not found
			//print("mac not found\n")
		}
	}
	if previous_ip.Oid != 0 {
		// an entity with this ip already exists ( usual case )
		if previous_mac.Oid != 0 {
			// and an entity with this mac exists (usual case )
			if previous_ip.Oid == previous_mac.Oid {
				// same entity ?
				if previous_ip.Properties["kind"] == e.Properties["kind"] {
					// strictly same entity : dont recreate it
					status = "same entity"
				} else {
					// not striclty the same entity : like ip change destroy and recreate
					status= "ip change"
				}
			} else {
				// we got an ip with a different mac
				status = "ip with another mac"
			}
		} else {
			// and no entity with this mac ( new mac took this ip)
			status = "new mac"
		}
	} else {
		// no entity with this ip exists ( initiallization case ? )
		if previous_mac.Oid != 0 {
			// and a mac exists ,  a device has change it s ip
			status= "ip change"
		} else {
			// and no mac exists , real initialization case
			status= "initialization"
		}
	}
	switch status{
	case "initialization":
		// nop ip and no mac exists : create entity
		i,err := e.Entity.Save(cnx)
		e.Oid=i
		return e,err
	case "same entity":
		// entity already exists : ok
		e.Oid = previous_ip.Oid
		return e,nil
	case "ip change":
		// no ip  but a mac exists : delete mac create entity
		err = previous_mac.Delete(cnx)
		if err != nil {return e,err}
		i,err := e.Entity.Save(cnx)
		e.Oid=i
		return e,err
	case "new mac":
		// a new mac took the ip address : delete previous ip and create
		err = previous_ip.Delete(cnx)
		if err != nil {return e,err}
		i,err := e.Entity.Save(cnx)
		e.Oid=i
		return e,err
	case "ip with another mac":
		// ip exists but with another mac : delete ip , delete mac , create entity
		err = previous_ip.Delete(cnx)
		if err != nil {return e,err}
		err = previous_mac.Delete(cnx)
		if err != nil {return e,err}
		i,err := e.Entity.Save(cnx)
		e.Oid=i
		return e,err
	default:
		// unknown or impossible case
		err = errors.New("unknown or impossible case !!!")
		return e,err
	}
	return e,err
}



func (e * DeviceEntity) Schema() store.EntitySchema {
	return DeviceEntitySchema
}

//func (e * DeviceEntity) All( cnx redis.Conn) ( all []DeviceEntity{}, err errors) {
//
//	//// get all oids of entity:device:memebers
//	//r,err := cnx.Do("SMEMBERS","entity:device:members")
//	//if err != nil { return all,err }
//	//
//	//
//	//for _,v := range r {
//	//	id := string(v.([]byte))
//	//
//	//	e,err := store.LoadEntity(cnx,id)
//	//	if err != nil { return all,err }
//	//
//	//	all = append(all,DeviceEntity{e})
//	//
//	//}
//	return all ,err
//}





func ( e * DeviceEntity) AddHashTopic( cnx redis.Conn,name string, data  map[string]interface{}) (err error) {

	h := store.NewHash(cnx,e.Oid)
	err= h.Write(name,data)
	return err
}

func ( e * DeviceEntity) GetHashTopic( cnx redis.Conn,name string) ( content []byte, err error) {

	h := store.NewHash(cnx,e.Oid)
	content, err = h.Read(name)
	return content,err
}



func NewDeviceEntity( ip string,mac string,kind string, properties properties) (entity DeviceEntity) {

	p := make(map[string]string)
	for k,v := range properties{
		p[k]= v
	}
	p["ip"] = ip
	p["mac"] = mac
	p["kind"] = kind
	e := store.Entity{DeviceEntitySchema.Name, p, 0}
	entity = DeviceEntity{e}

	return entity
}


func LoadDevicebyIp(cnx redis.Conn,name string)  (entity DeviceEntity,err error)  {

	e,err := store.LoadEntityByIndex(cnx, DeviceEntitySchema.Name,"ip",name)
	if err != nil { return entity, err }

	entity.EntityName = e.EntityName
	entity.Properties = e.Properties
	entity.Oid = e.Oid

	return entity,err
}

func LoadDevicebyMacAddress(cnx redis.Conn,mac string)  (entity DeviceEntity,err error)  {

	e,err := store.LoadEntityByIndex(cnx, DeviceEntitySchema.Name,"mac",mac)
	if err != nil { return entity, err }

	entity.EntityName = e.EntityName
	entity.Properties = e.Properties
	entity.Oid = e.Oid

	return entity,err
}


