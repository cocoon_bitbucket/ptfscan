package ipscan


import (

	"github.com/garyburd/redigo/redis"
	infra "bitbucket.org/cocoon_bitbucket/ptfscan/infra/models"
	"time"
	"strconv"
	"net"
	"strings"
)

func InitializeFromInfraLivebox( cnx redis.Conn ) (err error) {
	//
	// initialize ipscan entities  from  redis key infra:livebox
	//
	data, err := infra.FetchInfraLivebox(cnx)
	for _, p := range data {
		e := NewDeviceEntity( p.Ip, p.Mac, "livebox" ,nil )
		_,err := e.Save(cnx)
		if err != nil { return err}
	}
	return err


}

func TimeFromUnixTimestamp(timestamp string) ( t time.Time, err error) {

	i, err := strconv.ParseInt( timestamp, 10, 64)
	if err == nil {
		t = time.Unix(i, 0)
	}
	return t ,err

}


func CanonicalMacAddress( mac string) ( canonical net.HardwareAddr ,err error) {

	// transform a mac address of several form into a net.HardwareAddr
	// input forms
	//    00:00:00:00:00:FF ,  00:00:00:00:00:ff ,  00000000000000FF

	switch {

	case strings.Contains(mac, ":"):
		canonical, err = net.ParseMAC(mac)

	case strings.Contains(mac, "-"):
		canonical, err = net.ParseMAC(mac)

	default:
		// wild format
		if len(mac) == (12) {
			// can be a mac without separators
			s := ""
			for i:=0 ; i < 12 ;i +=2 {
				s = s + ":" + mac[i:i+2]
			}
			canonical, err = net.ParseMAC(s[1:])
		}
	}
	return canonical,err
}