package ipscan

import (

	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools/store"
	infra "bitbucket.org/cocoon_bitbucket/ptfscan/infra/models"

	"time"
	"github.com/garyburd/redigo/redis"
	"encoding/json"
	"net"
	"strconv"
)


/*

	globals:  redistools.GetPool()



 */

type Query struct {

	store.StoreDb    // { redis.Conn , RedisPool redistools.Pool }

}


func NewQuery () ( query * Query , err error) {

	pool,err := redistools.GetPool()
	if err != nil { return query, err }
	_=pool

	cnx := pool.Get()
	store := store.StoreDb{cnx,*pool}

	*query = Query{store}

	return query,err

}



//
//  DeviceInfoQuery
//


type QueryDeviceInfo struct {

	Ip string
	Mac string			`pcbcli:"DeviceInfo.BaseMAC"`

	Kind string

	Timestamp int   `pcbcli:"_instance"`

	Name string

	Uptime time.Duration    `pcbcli:"DeviceInfo.UpTime"`
	Status string			`pcbcli:"DeviceInfo.DeviceStatus"`
	Description string		`pcbcli:"DeviceInfo.Description"`


}

func CollectDeviceInfo( cnx redis.Conn) ( response []*QueryDeviceInfo , err error ) {

	// get all oids of entity:device:memebers
	r,err := redis.Values(cnx.Do("SMEMBERS","entity:device:index:kind:livebox"))
	if err != nil { return response,err }

	var entities []DeviceEntity

	for _,v := range r {
		id := string(v.([]byte))

		e,err := store.LoadEntity(cnx,id)
		if err != nil { return response,err }

		entities = append(entities,DeviceEntity{e})

	}

	for _,e := range entities {
		//
		entry,err := MakeDeviceInfoEntry(cnx,&e)
		if err == nil {
			response = append(response, &entry)
		}
	}

	//FillInNameInfo(cnx,response)


	//print(entities)

	return response,err
}


//func FillInNameInfo( cnx redis.Conn, data []*QueryDeviceInfo) (err error) {
//	// add device name from infra mac data
//	for _,entry := range data {
//
//		//mac := entry.Mac
//
//		// use canonical form of mac address to find livebox name
//		join_mac,err := net.ParseMAC(entry.Mac)
//		if err != nil {
//			return err
//		}
//
//		m,err := infra.LoadMacByMacAddress(cnx,join_mac.String())
//		if err == nil {
//			entry.Name = m.Properties["name"]
//		} else {
//			entry.Name = "?"
//		}
//	}
//	return err
//}


func MakeDeviceInfoEntry( cnx redis.Conn ,e * DeviceEntity) ( entry QueryDeviceInfo,err error){

	entry = QueryDeviceInfo{}
	entry.Ip = e.Properties["ip"]
	entry.Mac = e.Properties["mac"]
	entry.Kind = e.Properties["kind"]
	topic ,err := e.GetHashTopic(cnx,"DeviceInfo")
	if err == nil {
		// we found a DeviceInfo topic
		topics := make(map[string]string)
		err = json.Unmarshal(topic,&topics)
		if err == nil {

			// timestamp
			//v,ok := topics["_timestamp"]
			if v,ok := topics["_timestamp"]; ok {
				entry.Timestamp,_ = MakeElapsed(v)
			}

			// Description
			v,ok := topics["DeviceInfo.Description"]
			if ok {
				entry.Description = v
			}
			// status
			v,ok = topics["DeviceInfo.DeviceStatus"]
			if ok {
				entry.Status = v
			}
			// status
			v,ok = topics["DeviceInfo.UpTime"]
			if ok {
				text := v + "s"
				entry.Uptime,err  = time.ParseDuration(text)
			}

			// add Name info from infra info
			AddNameInfo(cnx,&entry)

		}
	} else {
		// no topic
		_=topic
	}
	return entry,err

}

func AddNameInfo( cnx redis.Conn, entry *QueryDeviceInfo) (err error) {
	// add device name from infra mac data

	// use canonical form of mac address to find livebox name
	join_mac, err := net.ParseMAC(entry.Mac)
	if err != nil {
		return err
	}
	m, err := infra.LoadMacByMacAddress(cnx, join_mac.String())
	if err == nil {
		entry.Name = m.Properties["name"]
	} else {
		entry.Name = "?"
	}
	return err
}

func MakeElapsed( epoch string ) ( elapsed int, err error) {

	// return the elapsed time from the unix epoch
	origin,err := strconv.Atoi(epoch)
	if err == nil {
		now := time.Now().Unix()
		elapsed = int(now) - origin
		//elapsed = strconv.Itoa(int(ago))
	}
	return elapsed,err
}