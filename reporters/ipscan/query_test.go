package ipscan_test

import (
	"testing"
	"time"
	"bitbucket.org/cocoon_bitbucket/ptfscan/reporters/ipscan"
	"github.com/stretchr/testify/assert"

	"strconv"
)



func TestMakeTimestamp(t *testing.T) {

	// get now()
	ut := time.Now().Unix()
	sut := strconv.Itoa(int(ut))

	// wait a little bit
	time.Sleep(2*time.Second)

	// ask for elapsed
	r,err := ipscan.MakeElapsed(sut)
	assert.Equal(t,nil,err)

	print(r)


}

func TestQueryDeviceInfo(t *testing.T) {

	pool := get_pool()

	// create schema for device
	cnx := pool.Get()
	defer cnx.Close()


	all, err := ipscan.CollectDeviceInfo(cnx)
	assert.Equal(t,nil,err)
	if err != nil { return }

	print(all)

}