
package ipscan

import (

"bitbucket.org/cocoon_bitbucket/ptfscan/redistools/store"
infra "bitbucket.org/cocoon_bitbucket/ptfscan/infra/models"

"time"
"github.com/garyburd/redigo/redis"
"encoding/json"
"net"

	"fmt"
)


/*

	globals:  redistools.GetPool()



 */

//type Query struct {
//
//	store.StoreDb    // { redis.Conn , RedisPool redistools.Pool }
//
//}
//
//
//func NewQuery () ( query * Query , err error) {
//
//	pool,err := redistools.GetPool()
//	if err != nil { return query, err }
//	_=pool
//
//	cnx := pool.Get()
//	store := store.StoreDb{cnx,*pool}
//
//	*query = Query{store}
//
//	return query,err
//
//}



//
//  DeviceInfoQuery
//


type QueryEnergenie struct {

	Ip string
	Mac string

	Kind string

	Timestamp int

	Name string

	Uptime time.Duration
	Status string

	Outlet1 string
	Outlet2 string
	Outlet3 string
	Outlet4 string

	Description string


}

func CollectEnergenieInfo( cnx redis.Conn) ( response []*QueryEnergenie , err error ) {

	// get all oids of entity:device:memebers
	r,err := redis.Values(cnx.Do("SMEMBERS","entity:device:index:kind:energenie"))
	if err != nil { return response,err }

	var entities []DeviceEntity

	for _,v := range r {
		id := string(v.([]byte))

		e,err := store.LoadEntity(cnx,id)
		if err != nil { return response,err }

		entities = append(entities,DeviceEntity{e})

	}

	for _,e := range entities {
		//
		entry,err := MakeEnergenieEntry(cnx,&e)
		if err == nil {
			response = append(response, &entry)
		}
	}

	//FillInNameInfo(cnx,response)


	//print(entities)

	return response,err
}




func MakeEnergenieEntry( cnx redis.Conn ,e * DeviceEntity) ( entry QueryEnergenie,err error){

	entry = QueryEnergenie{}
	entry.Ip = e.Properties["ip"]
	entry.Mac = e.Properties["mac"]
	entry.Kind = e.Properties["kind"]
	topic ,err := e.GetHashTopic(cnx,"EnergenieStatus")
	if err == nil {
		// we found a DeviceInfo topic
		topics := make(map[string]string)
		err = json.Unmarshal(topic,&topics)
		if err == nil {

			// timestamp
			//v,ok := topics["_timestamp"]
			if v,ok := topics["_timestamp"]; ok {
				entry.Timestamp,_ = MakeElapsed(v)
			}


			// status string ON OFF ON OFF
			status := ""
			field := ""
			outlet := ""

			for i:=1 ; i<= 4 ;i++{
				field = fmt.Sprintf("outlet_%d" , i)
				v,ok := topics[field]
				if ok {
					if v == "1" {outlet ="ON "} else {outlet = "OFF "}
					status = status + outlet
				}
			}
			if status == "" { status = "?"}
			entry.Status = status


			// add Name info from infra info
			AddEnergenieNameInfo(cnx,&entry)

		}
	} else {
		// no topic
		_=topic
	}
	return entry,err

}

func AddEnergenieNameInfo( cnx redis.Conn, entry *QueryEnergenie) (err error) {
	// add device name from infra mac data

	// use canonical form of mac address to find livebox name
	join_mac, err := net.ParseMAC(entry.Mac)
	if err != nil {
		return err
	}
	m, err := infra.LoadMacByMacAddress(cnx, join_mac.String())
	if err == nil {
		entry.Name = m.Properties["name"]
	} else {
		entry.Name = "?"
	}
	return err
}
