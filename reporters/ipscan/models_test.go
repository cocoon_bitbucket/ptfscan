package ipscan_test


import(

	"testing"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools/store"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	"bitbucket.org/cocoon_bitbucket/ptfscan/reporters/ipscan"
	"github.com/stretchr/testify/assert"
	//"fmt"
	//"strings"
	//"fmt"
)



var (


	redisUrl = "redis://127.0.0.1:6379/0"

	Livebox_demo = ipscan.NewDeviceEntity(
		"192.168.1.1",
		"00:00",
		"livebox", make(map[string]string),
	)


	dataset = map[string]map[string]string{
		"Livebox-demo": {
			"ip": "192.168.1.1",
			"mac" : "00:00:00",
			"kind": "livebox",
		},
		"Livebox-2898": {
			"ip": "192.168.200.15",
			"mac" : "00:00:00:02",
			"kind": "livebox",
		},

		"energenie": {
			"ip": "192.168.1.30",
			"mac" : "00:00:01",
			"kind": "energenie",
		},
	}

)



func TestDeviceEntity(t *testing.T) {

	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	cnx.Do("FLUSHDB")

	// create item model
	ipscan.CreateDeviceEntitySchema(cnx)

	// create dataset
	for _,v := range dataset{
		e := ipscan.NewDeviceEntity(v["ip"],v["mac"],v["kind"],nil)
		x,err := e.Save(cnx)
		_=x
		assert.Equal(t, nil, err)

	}


	o,err := ipscan.LoadDevicebyIp(cnx,"192.168.1.1")
	assert.Equal(t, nil, err)
	assert.Equal(t,"192.168.1.1",o.Properties["ip"])
	assert.Equal(t,"00:00:00",o.Properties["mac"])
	assert.Equal(t,"livebox",o.Properties["kind"])

	o2,err := ipscan.LoadDevicebyMacAddress(cnx,"00:00:00")
	assert.Equal(t, nil, err)
	assert.Equal(t,o,o2)

	o.Delete(cnx)

	_,err = ipscan.LoadDevicebyIp(cnx,"192.168.1.1")
	assert.Equal(t,"redigo: nil returned",err.Error())


	redistools.UnsetPool()

}

func TestDeviceEntity2(t *testing.T) {

	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	cnx.Do("FLUSHDB")

	// create item model
	ipscan.CreateDeviceEntitySchema(cnx)

	// create dataset
	for _,v := range dataset{
		e := ipscan.NewDeviceEntity(v["ip"],v["mac"],v["kind"],nil)
		x,err := e.Save(cnx)
		_=x
		assert.Equal(t, nil, err)

	}

	// create same entity again
	same := ipscan.NewDeviceEntity("192.168.1.1","00:00:00","livebox",nil)
	e,err := same.Save(cnx)
	_=e
	assert.Equal(t, nil, err)


	// create a new mac taking an existing ip
	ip_other_mac := ipscan.NewDeviceEntity("192.168.1.1","00:00:99","livebox",nil)
	e,err = ip_other_mac.Save(cnx)
	assert.Equal(t, nil, err)
	o,err := ipscan.LoadDevicebyIp(cnx,"192.168.1.1")
	assert.Equal(t, nil, err)
	assert.Equal(t,"192.168.1.1",o.Properties["ip"])
	assert.Equal(t,"00:00:99",o.Properties["mac"])
	assert.Equal(t,"livebox",o.Properties["kind"])


	// a known mac change it 's ip to a free one
	mac_change_ip := ipscan.NewDeviceEntity("192.168.1.12","00:00:99","livebox",nil)
	e,err = mac_change_ip.Save(cnx)
	assert.Equal(t, nil, err)
	o2,err := ipscan.LoadDevicebyMacAddress(cnx,"00:00:99")
	assert.Equal(t, nil, err)
	assert.Equal(t,"192.168.1.12",o2.Properties["ip"])


	// a known mac change it 's ip to a existing one
	mac_change_ip_existing := ipscan.NewDeviceEntity("192.168.200.15","00:00:99","livebox",nil)
	e,err = mac_change_ip_existing.Save(cnx)
	assert.Equal(t, nil, err)
	o3,err := ipscan.LoadDevicebyMacAddress(cnx,"00:00:99")
	assert.Equal(t, nil, err)
	assert.Equal(t,"192.168.200.15",o3.Properties["ip"])




	//o.Delete(cnx)

	//_,err = ipscan.LoadDevicebyIp(cnx,"192.168.200.15")
	//assert.Equal(t,"redigo: nil returned",err.Error())


	redistools.UnsetPool()

}
