package portscanner



// a scrutiniser is a function that accepts a text and return a dictionary and err
// analyze a text and produce a result dictionary and err status
type scrutiniser func( []byte) ( map[string]interface{}, error)


//samples

func NullScrutiniser ( text []byte) ( map[string]interface{}, error) {

	var err error = nil
	res := make(map[string]interface{})

	_ = text

	return res,err
}


func NullApplyScrutiniser( fn scrutiniser, text []byte) ( map[string]interface{}, error) {

	res,err:= fn(text)

	return res,err

}



