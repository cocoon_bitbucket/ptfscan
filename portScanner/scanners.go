package portscanner


import (
	"fmt"
	"log"
	"net"
	//"net/http"
	"time"
	"os/exec"
	"io/ioutil"
	"runtime"
	"golang.org/x/net/html"
	"strings"
	"github.com/gammazero/workerpool"
)

/*

	cidr -- ip -- pingable --
                  ports    __L_  open_ports

 */



// cidr -> ip


func inc(ip net.IP) {
	// increment an IP
	for j := len(ip)-1; j>=0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

func dupIP(ip net.IP) net.IP {
	// duplicate an IP
	dup := make(net.IP, len(ip))
	copy(dup, ip)
	return dup
}

func IpRange( cidr string ) chan net.IP {
	//
	// return a channel of IP from a cidr
	//

	c := make(chan net.IP)
	go func() {

		defer close(c)

		ip, ipnet, err := net.ParseCIDR(cidr)
		if err != nil {
			log.Fatal(err)
		}
		iip := ip.Mask(ipnet.Mask)
		for {
			if ipnet.Contains(iip) {

				//fmt.Println("* ", iip)
				dup := dupIP(iip)
				c <- dup
				inc(iip)
				//time.Sleep(1 * time.Millisecond)

			} else {
				break
			}
		}

	}()
	return c
}


func IpRangeFromList( ips []string ) chan net.IP {
	//
	// return a channel of IP from a ip string list
	//
	c := make(chan net.IP)
	go func() {

		defer close(c)

		for _,ip := range ips {
			q := net.ParseIP(ip)
			//fmt.Println(ip)
			c <- q
			//time.Sleep(1000 * time.Millisecond)
		}
	}()
	return c
}


// pingable ip

func Pingable( ips chan net.IP ) chan net.IP {
	//
	// return a channel of pingable ip address
	//
	c := make(chan net.IP)
	go func() {

		defer close(c)
		for ip := range ips {
			//fmt.Println(ip)
			_, err := exec.Command("ping", "-c1", "-t1", ip.String()).Output()
			//println(string(r))
			if err == nil {
				// ping ok
				c <- ip
			}
		}
	}()
	time.Sleep(1 * time.Millisecond)
	return c

}


func IsPortOpen(host string ,port int, timeout time.Duration) bool {

	//

	uri := fmt.Sprintf("%s:%d",host,port)

	tcpAddr, err := net.ResolveTCPAddr("tcp4", uri)
	if err != nil {
		return false
	}
	conn, err := net.DialTimeout("tcp", tcpAddr.String(), timeout)
	if err != nil {
		return false
	}

	defer conn.Close()

	return true
}



// net.TcpAddr channels





func checkAddr( channel chan net.TCPAddr,ip net.IP,port int,timeout time.Duration,log_level int) {
	// check a port is responding push it to the channel

	uri := fmt.Sprintf("%s:%d", ip.String(), port)
	tcpAddr, err := net.ResolveTCPAddr("tcp4", uri)
	if err == nil {
		//fmt.Printf("try to connect to %s\n", tcpAddr.String())
		conn, err := net.DialTimeout("tcp", tcpAddr.String(), timeout)
		//defer conn.Close()
		if err == nil {
			if log_level >= 10 {
				fmt.Printf("-- checkaddr: CONNECTION OK  to %s\n", tcpAddr.String())
			}
			channel <- * tcpAddr
			conn.Close()
		} else {
			if log_level >= 90 {
				fmt.Printf("-- fail to connect to %s\n", tcpAddr.String())
			}
		}
	}
}


func checkAddrTask( channel chan net.TCPAddr,ip net.IP,port int,timeout time.Duration,log_level int) func() {
	// a curry for checkAddr
	wrapper := func() {
		checkAddr( channel ,ip ,port,timeout ,log_level)
	}
	return wrapper
}


func OpenedTcpAdress ( ipchan chan net.IP , ports []int, log_level int )  chan net.TCPAddr{
	//
	// return a channel of net.TCPAddr  for open ports
	//
	c := make(chan net.TCPAddr)
	_ = c

	go func() {
		defer close(c)

		timeout := 100 * time.Millisecond
		for ip := range ipchan {

			for _,port := range ports {

				checkAddr(c,ip,port,timeout,log_level)

			}
		}
	}()
	return c
}



func FastOpenedTcpAdress ( ipchan chan net.IP , ports []int, timeout time.Duration ,log_level int)  chan net.TCPAddr{
	//
	// return a channel of net.TCPAddr  for  open ports ( via a workpool )
	//

	c := make(chan net.TCPAddr)

	// create a worker pool
	runtime.GOMAXPROCS(runtime.NumCPU())
	nbWorkers := runtime.NumCPU() * 3
	wp := workerpool.New(nbWorkers * 3)  // took 0.7 seconds

	go func() {
		defer close(c)
		if timeout == 0 {
			timeout = 200 * time.Millisecond
		}

		//timeout := 200 * time.Millisecond
		for ip := range ipchan {
			for _,port := range ports {
				//go checkAddr(c,ip,port,timeout)
				wp.Submit(checkAddrTask(c,ip,port,timeout,log_level))
			}
		}
		wp.Stop()
	}()
	return c
}





///

func PredictHttp( tcpAddr * net.TCPAddr) string {
	/*

		livebox orange constants:

		BUILD_CUSTOMER: 'ft',
		BUILD_PROJECT: 'lbv3fr',
		BUILD_HARDWARE: 'sagem_lbv3',

	 */

	timeout := 100 * time.Millisecond
	duration, _ := time.ParseDuration("3s")


	conn, err := net.DialTimeout("tcp", tcpAddr.String(), timeout)
	//conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		return ""
	}
	defer conn.Close()
	conn.SetDeadline(time.Now().Add(duration))

	//_, err = conn.Write([]byte("HEAD / HTTP/1.0\r\n\r\n"))
	_, err = conn.Write([]byte("GET / HTTP/1.0\r\n\r\n"))
	if err != nil {
		return ""
	}

	result, err := ioutil.ReadAll(conn)
	if err != nil {
		return ""
	}

	reader := strings.NewReader(string(result))


	//url := tcpAddr.String()
	//resp, _ := http.Get(url)
	//bytes, _ := ioutil.ReadAll(resp.Body)
	//
	//fmt.Println("HTML:\n\n", string(bytes))
	//
	//resp.Body.Close()

	z := html.NewTokenizer(reader)
	for {
		tt := z.Next()

		switch {
		case tt == html.ErrorToken:
			// End of the document, we're done
			return ""
		case tt == html.StartTagToken:
			t := z.Token()

			isAnchor := t.Data == "title"
			if isAnchor {
				fmt.Println("We found a title!", t.String())

				//tt := t.Doc.Next()
				//
				//if tt == html.TextToken {
				//	next := node.Doc.Token()
				//	title = strings.TrimSpace(next.Data)
				//}
			}
		}
	}


	//resp := string(resp.Body)
	//return p.PredictResponse(resp, p)
	return "OK"
}

