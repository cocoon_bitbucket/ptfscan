package portscanner_test


import(

	"testing"
	"bitbucket.org/cocoon_bitbucket/ptfscan/portScanner"
	"github.com/stretchr/testify/assert"

	//"fmt"
	"time"
	"fmt"
)


func TestCidrRange (t *testing.T) {

	cidr := portscanner.Cidr("192.168.1.1/24")

	var collect []string

	start:= time.Now()
	ips := cidr.Range()
	for ip := range ips {
		collect = append(collect,ip.String())
		//fmt.Printf("ip: %s\n",ip)
	}
	elapsed := time.Since(start)
	fmt.Printf("CidrRange took %s\n", elapsed)

	assert.Equal(t,"192.168.1.0",collect[0])
	assert.Equal(t,"192.168.1.255",collect[len(collect)-1])

}

func TestCidrRange2 (t *testing.T) {

	cidr := portscanner.Cidr("192.168.100.1/24")

	var collect []string

	start := time.Now()
	ips := cidr.Range()
	for ip := range ips {
		collect = append(collect,ip.String())
		//fmt.Printf("ip: %s\n",ip)
	}
	elapsed := time.Since(start)
	fmt.Printf("CidrRange took %s\n", elapsed)
	assert.Equal(t,"192.168.100.0",collect[0])
	assert.Equal(t,"192.168.100.255",collect[len(collect)-1])

}


func TestCidrRangeNetAddr (t *testing.T) {

	cidr := portscanner.Cidr("192.168.1.1/24")

	var collect []string

	ports := []int{80,23}

	start := time.Now()
	addrs := cidr.RangeNetAddr(ports)
	for addr := range addrs {
		collect = append(collect,addr.String())
		//fmt.Printf("addr: %s\n",addr.String())
	}
	elapsed := time.Since(start)
	fmt.Printf("CidrRangeNetAddr took %s\n", elapsed)
	assert.Equal(t,"192.168.1.0:80",collect[0])
	assert.Equal(t,"192.168.1.255:23",collect[len(collect)-1])

}


func TestCidrIpList (t *testing.T) {

	cidr := portscanner.Cidr("192.168.1.0/24")


	start:= time.Now()

	ips,err := cidr.IpList()

	elapsed := time.Since(start)
	fmt.Printf("Cidr.IpList took %s\n", elapsed)

	assert.Equal(t,nil,err)


	//for _,ip := range ips {
	//	fmt.Printf("ip: %s\n", ip)
	//}


	assert.Equal(t,254,len(ips))
	assert.Equal(t,"192.168.1.1",ips[0])
	assert.Equal(t,"192.168.1.254",ips[len(ips)-1])

}




//func TestFastOpenedTcpAdress (t *testing.T) {
//	// print all open ports 80 of a cidr
//
//	cidr:= "192.168.1.1/24"
//	ports := []int{80}
//
//	timeout := 500 * time.Millisecond
//
//	ips := portscanner.IpRange(cidr)
//
//
//	addrs := portscanner.FastOpenedTcpAdress ( ips , ports  ,timeout ,90 )
//
//	for addr :=range addrs {
//		fmt.Printf("addr:%s\n",addr.String())
//	}
//
//
//}
