package portscanner_test


import(

	"testing"
	"bitbucket.org/cocoon_bitbucket/ptfscan/portScanner"
	"github.com/magiconair/properties/assert"

	//"net"

	//"fmt"
	"time"
	"fmt"
)




func TestIpRange (t *testing.T) {

	cidr := "192.168.1.1/24"

	var collect []string

	ips := portscanner.IpRange(cidr)
	for ip := range ips {
		collect = append(collect,ip.String())
		//fmt.Printf("ip: %s\n",ip)
	}
	assert.Equal(t,"192.168.1.0",collect[0])
	assert.Equal(t,"192.168.1.255",collect[len(collect)-1])


}

func TestIpRange2 (t *testing.T) {

	cidr := "192.168.100.1/24"

	var collect []string

	ips := portscanner.IpRange(cidr)
	for ip := range ips {
		collect = append(collect,ip.String())
		//fmt.Printf("ip: %s\n",ip)
	}
	assert.Equal(t,"192.168.100.0",collect[0])
	assert.Equal(t,"192.168.100.255",collect[len(collect)-1])

}


func TestFastOpenedTcpAdress (t *testing.T) {
	// print all open ports 80 of a cidr

	cidr:= "192.168.1.1/24"
	ports := []int{80}

	timeout := 500 * time.Millisecond

	ips := portscanner.IpRange(cidr)


	addrs := portscanner.FastOpenedTcpAdress ( ips , ports  ,timeout ,90 )

	for addr :=range addrs {
		fmt.Printf("addr:%s\n",addr.String())
	}


}
