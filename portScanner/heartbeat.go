package portscanner

import (
	"fmt"
	"time"
	"encoding/json"

)
/*

	The heartbeat messages are published by tasks to attest activity


	a task "MyTask" will publish on channel ptf/Heartbeat/Mytask a messagge of type HeartbeatMessage


 */


type HeartbeatMessage struct {

	//Event string				`json:"event"`
	Task  string				`json:"task"`
	Periodicity time.Duration		`json:"periodicity"`
	//Timestamp int32			`json:"timestamp"`

}

func (r * HeartbeatMessage )Jsonify() ([]uint8,error) {
	b, err := json.Marshal(r)
	return b,err
}



func NewHeartbeatMessage(task string, Periodicity time.Duration) * HeartbeatMessage{
	return & HeartbeatMessage{task,Periodicity}

}


func HeartbeatChannel(taskName string, prefix string) string{
	// return the name of the heartbeat channel eg ptf/Heartbeat/MyTask
	return fmt.Sprintf("%s/Heartbeat/%s",prefix,taskName)

}


