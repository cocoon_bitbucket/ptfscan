package inspectors_test

import(

	"testing"
	"bitbucket.org/cocoon_bitbucket/ptfscan/portScanner/inspectors"
	//"bitbucket.org/cocoon_bitbucket/ptfscan/port-scanner/inspectors"
	"fmt"
	"time"
)


var (

	energenie_addr string = "192.168.1.30"
	//energenie_ip = "192.168.1.30"
	energenie_passwd string = "1"


)



func TestEnergenie(t *testing.T) {


	e,err := inspectors.NewEnergenie(energenie_addr,energenie_passwd)
	if err != nil {
		fmt.Printf("not an energenie device at %s\n", energenie_addr)
	} else {
		fmt.Printf("YES we got  an energenie device at %s\n", energenie_addr)


		//// logout
		//r := e.Logout()
		//fmt.Printf("logout: %b\n",r)
		//
		//// logout
		//r = e.Login()
		//fmt.Printf("login: %b\n",r)

		// get status
		status,err := e.GetStatus()
		if err == nil {
			fmt.Printf("status= %v\n",status)
		}

		// get lan config
		// get status
		lan_config,err := e.GetLanConfig()
		if err == nil {
			fmt.Printf("lan_config= %v\n",lan_config)
		}

		// power off
		r := e.SwitchOff(4)
		fmt.Printf("power off : %b \n",r)


		// get status
		status,err = e.GetStatus()
		if err == nil {
			fmt.Printf("status= %v\n",status)

		}
		time.Sleep(1*time.Second)



		// power off
		r = e.SwitchOn(4)
		fmt.Printf("power on : %b \n",r)


		// get status
		status,err = e.GetStatus()
		if err == nil {
			fmt.Printf("status= %v\n",status)

		}

	}


}
