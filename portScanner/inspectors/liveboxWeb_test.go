package inspectors_test

import(

	"testing"
	"bitbucket.org/cocoon_bitbucket/ptfscan/portScanner/inspectors"
	"fmt"

)


func TestLiveboxWeb(t *testing.T) {

	ip := "192.168.1.1"

	w, err := inspectors.NewLiveboxWeb(ip)
	if err != nil {
		fmt.Printf("NO web server found at %s\n", ip)
	} else {
		fmt.Printf("YES we got  a web server at %s\n", ip)

		// try to find a web admin page
		pageInfo,err := w.Identify()
		if err == nil {
			fmt.Printf("YES This is a livebox web admin at %s: \n%v \n" ,ip,pageInfo)

		} else {
			fmt.Printf("NO this is not a livebox at %s\n",ip)

		}
	}
	_= w
}