package inspectors

import(

	"net/http"
	"errors"
	//"time"
	"fmt"
	"io/ioutil"
	"regexp"
	"net"
)


type LiveboxWeb struct {
	// structure of a livebox web

	http.Client
	Addr string    // ip of the server
}


func NewLiveboxWeb ( netAddr string ) (* LiveboxWeb,error) {
	// create a web finder aka an http.Client to try to identify a web site
	// return a WebFinder object

	var err error = nil

	client := http.Client{
		Timeout: WebFinderHttpTimeout,
	}
	e := &LiveboxWeb{ client, netAddr }

	if e.Probe() == false {
		// not a web server
		err = errors.New("Not a web server")
	}
	return e , err

}

func ( w * LiveboxWeb) Probe() bool {
	// try to recognise a web server
	url := fmt.Sprintf("http://%s", w.Addr)
	r, err := w.Get(url)
	//r,err:= e.Head(url)
	_ = r
	if err != nil {
		// cannot reach a server
		fmt.Println(err)
		return false
	}
	return true
}

func ( w * LiveboxWeb) Identify() (map [string]interface{} ,error){
	// ask for root page
	// try to recognise a <title>Livebox</title>
	// and extract information
	response := make (map[string]interface{})

	url := fmt.Sprintf("http://%s", w.Addr)
	r, err := w.Get(url)
	//r,err:= e.Head(url)
	_ = r
	if err == nil {
		// read page to extract title
		buf, _ := ioutil.ReadAll(r.Body)
		response ,err = Identify_livebox(buf)
	}
	return response,err
}



//

func Identify_livebox(text []byte) (map[string]interface{},error) {

	//  <title>Livebox</title>
	//  <meta name="description" content="Livebox Configurator">

	res ,err := SearchTitle(text)
	if err != nil {
		res, err = SearchMetaLivebox(text)
	}
	return res,err

}

func extract_info(text string) map [string]string {

	keywords := []string{ "BUILD_CUSTOMER","BUILD_PROJECT","BUILD_HARDWARE","DUMMY"}

	response := make (map[string]string)
	response["_found"] = "found"

	for _,keyword := range keywords {

		pattern := fmt.Sprintf(`%s\s*:\s*\'(.*?)\'`,keyword)
		rx := regexp.MustCompile(pattern)
		array := rx.FindStringSubmatch(text)
		_ =array
		if len(array) >= 2 {
			response[keyword] = array[1]
		}
	}


	return response
}


// scrutinisers


func SearchTitle( text []byte) (map[string]interface{},error){
	// scan the text to find <title>Livebox</title>

	var err error = errors.New("No Match")
	response := make (map[string]interface{})

	rx := regexp.MustCompile(`<title>\s*([Ll]ivebox)\s*</title>`)

	match := rx.FindStringSubmatch(string(text))
	if len(match) > 0 {
		title := match[1]
		if title == "Livebox" {
			err = nil
			// extract info
			response = extract_info_lbv3(string(text))
			response["_title"] = title
		}
	}
	return response	,err
}

func SearchMetaLivebox( text []byte) (map[string]interface{},error){
	// scan the text to find <meta name="description" content="Livebox Configurator">

	var err error = errors.New("No Match")
	response := make (map[string]interface{})

	rx := regexp.MustCompile(`<meta name="description" content="(.*?)"\s*>`)

	match := rx.FindStringSubmatch(string(text))
	if len(match) > 0 {
		title := match[1]
		if title == "Livebox Configurator" {
			response["_title"] = title
			err = nil
		}
	}
	return response	,err
}


// extractors
func extract_info_lbv3(text string) map [string]interface{} {
	/*

		from page with <title>Livebox</title>

		extract info

		BUILD_CUSTOMER: 'ft',
		BUILD_PROJECT: 'lbv3fr',
		BUILD_HARDWARE: 'sagem_lbv3',

	 */

	keywords := []string{ "BUILD_CUSTOMER","BUILD_PROJECT","BUILD_HARDWARE","DUMMY"}

	response := make (map[string]interface{})
	//response["_found"] = "found"
	for _,keyword := range keywords {

		pattern := fmt.Sprintf(`%s\s*:\s*\'(.*?)\'`,keyword)
		rx := regexp.MustCompile(pattern)
		array := rx.FindStringSubmatch(text)
		_ =array
		if len(array) >= 2 {
			response[keyword] = array[1]
		}
	}
	return response
}


// channel functions


func LiveboxWebChannelResponse( source chan net.TCPAddr,cmd string) chan Response {
	//  return a channel of response for livebox identification , from a TCPAddr source channel
	sink := make(chan Response)
	go func() {
		defer close(sink)

		for addr := range source {
			_ = addr

			var status error = nil

			// prepare a blank response
			response := Response{ addr.IP.String(), nil, timestamp(),cmd,nil}

			// create an LiveboxWeb handler
			ip := addr.String()
			w, err := NewLiveboxWeb(ip)
			if err != nil {
				//fmt.Printf("NO web server found at %s\n", ip)
				status = err
			} else {
				//fmt.Printf("YES we got  a web server at %s\n", ip)

				// try to find a web admin page
				pageInfo,err := w.Identify()
				if err == nil {
					//fmt.Printf("YES This is a livebox web admin at %s: \n%v \n" ,ip,pageInfo)
					response.Response= pageInfo

				} else {
					//fmt.Printf("NO this is not a livebox at %s\n",ip)
					status= err
				}
			}

			if status != nil {
				// this is not a livebox web server
				//response.Err = err
				continue
			}
			sink <- response
		}
	}()
	return sink
}

