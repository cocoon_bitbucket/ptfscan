package inspectors

import (
	"time"
	"net"
	"net/http"
	"fmt"
	"errors"
	"strings"
	"io/ioutil"
	"regexp"
	//"encoding/json"
)

/*

	a powerswitch with a web portal  host:80/energenie.html


	login
		default password: "1"

		POST host/login.html   "pw=<password>"

	logout
		POST host/login.html   "pw="


	status
	   GET host/energenie.html

	   status pattern: "var sockstates = \[(.*?)\]\;"


 */


var (
	http_timeout time.Duration = 1 * time.Second
	server_header string = "EG-Web"
	//status_pattern string = `var sockstates = \[(.*?)\]\;`
	status_pattern string = `var sockstates = \[([01]),([01]),([01]),([01])\]\;`
	status_regexp * regexp.Regexp

	lan_settings_pattern string = `<input .*?id="(.+?)".*?value="(.*?)".*?\/>`
	lan_settings_regexp * regexp.Regexp

)




type EnergenieStatus struct {
	outlet [4]bool
}

type EnergenieLanConfig map[string]string

//


type Energenie struct {
	// structure of a Energenie Element

	http.Client
	Addr string
	Passwd string
	//opened bool
	//logged bool

}


func NewEnergenie ( netAddr ,passwd string ) (* Energenie,error) {
	// return a Energie if we can reach it , return err if not reachable
	//timeout := 1 * time.Second

	timeout := 2 * time.Second

	client := http.Client{
		Timeout: timeout,
	}

	// connected
	e := &Energenie{ client, netAddr , passwd}

	var err error = nil

	err = e.Probe()
	return e, err

}


func (e * Energenie) Probe() error {
	// try to recognise a energenie device
	url := fmt.Sprintf("http://%s",e.Addr)
	r,err:= e.Get(url)
	//r,err:= e.Head(url)
	if err != nil {
		// cannot reach device
		fmt.Println("NewEnergenie.Probe: ",err)
		return err
	}
	// we got a http response
	//fmt.Printf("response (%d) : %s\n",r.StatusCode, r)

	// analyse header to find header server=EG-Web
	if header, ok := r.Header["Server"]; ok {
		server := string(header[0])
		if server == server_header {
			// YES we got a http HEADER Server equal to EG-Web
			//fmt.Printf("Header Server: %v\n", server)
			return nil
		}
	}
	// not an energenie web server
	err = errors.New("not an energenie device ")
	return err
}



func (e * Energenie)  Login() bool {
	// try to log in , a POST host/login.html   pw=<password>
	url := fmt.Sprintf("http://%s/login.html",e.Addr)
	content_type := "text/plain"
	text := fmt.Sprintf("pw=%s",e.Passwd)
	data := strings.NewReader(text)
	_,err := e.Post(url,content_type,data )
	if err == nil {
		return true
	}
	return false}

func (e * Energenie)  Logout() bool {
	// try to log in , a POST host/login.html   pw=<password>
	url := fmt.Sprintf("http://%s/login.html",e.Addr)
	data := strings.NewReader("pw=")
	_,err := e.Post(url,"text/plain",data )
	if err == nil {
		return true
	}
	return false
}


func (e * Energenie)  SwitchOn(outlet int) error {
	// switch on outlet [1-4]
	if outlet < 1 || outlet > 4 {
		err := errors.New("Invalid outlet (must be in [1,4]")
		return err
	}

	e.Logout()
	e.Login()

	// url = self.base_url + "?cte%d=1" % int(channel)
	url := fmt.Sprintf("http://%s?cte%d=1",e.Addr,outlet )
	data := strings.NewReader("")
	_,err := e.Post(url,"text/plain",data )
	return err
}

func (e * Energenie)  SwitchOff(outlet int) error {
	// switch off outlet [1-4]
	// url = self.base_url + "?cte%d=1" % int(channel)

	if outlet < 1 || outlet > 4 {
		err := errors.New("Invalid outlet (must be in [1,4])")
		return err
	}


	e.Logout()
	e.Login()

	url := fmt.Sprintf("http://%s?cte%d=0",e.Addr,outlet )
	data := strings.NewReader("")
	_,err := e.Post(url,"text/plain",data )
	//if err == nil {
	//	return true
	//}
	//return false
	return err
}


func (e * Energenie)  GetStatus() (* Response,error) {
	// try to log in , a POST host/login.html   pw=<password>

	var err error = nil
	var response Response = Response{}

	e.Logout()
	e.Login()


	url := fmt.Sprintf("http://%s/energenie.html",e.Addr)
	r,err := e.Get(url)
	if err != nil {
		return &response,err
	}
	buf, err := ioutil.ReadAll(r.Body)
	if err == nil {
		//fmt.Printf("body: %s\n",string(buf))
		response = Response{e.Addr, nil, timestamp(), "status", nil}
		if err == nil {
			// ok
			data, err := extractStatusInfo(string(buf))
			if err == nil {
				//data := []bool{false,false,false,false}
				response.Response = data
			}
		}
	}
	return &response, err
}

func (e * Energenie)  GetLanConfig() (* Response,error) {
	// try to log in , a POST host/login.html   pw=<password>

	var err error = nil
	var response Response = Response{}

	e.Logout()
	e.Login()


	url := fmt.Sprintf("http://%s/lan_settings.html",e.Addr)
	r,err := e.Get(url)
	if err != nil {
		return &response,err
	}
	buf, err := ioutil.ReadAll(r.Body)
	if err == nil {
		//fmt.Printf("body: %s\n",string(buf))
		response = Response{e.Addr, nil, timestamp(), "lanconfig", nil}
		if err == nil {
			// ok
			data, err := extractLanSettingsInfo(string(buf))
			if err == nil {
				//data := map[string]string
				response.Response = data
			}
		}
	}
	return &response, err
}



// utils

func extractStatusInfo(text string ) ([4]bool ,error) {
	// extract

	var err error = nil
	result := [4]bool{false,false,false,false}


	// search for  var sockstates = [1,1,1,1];
	//status_regexp = regexp.MustCompile(status_pattern)
	found := status_regexp.FindString( text)
	if len(found)> 0 {
		//fmt.Println(found)

		array := status_regexp.FindStringSubmatch(text)
		//for k, v := range array {
		//	fmt.Printf("%d. %s\n", k, v)
		//}
		// update status
		for i := 0 ; i < 4 ; i++ {
			if array[i+1] == "1" {
				result[i] = true
			}
		}

	} else {
		msg:= "Energenie cannot find status string in page"
		fmt.Println(msg)
		err = errors.New(msg)
	}


	return result, err
}


func extractLanSettingsInfo(text string ) ( map[string]string ,error) {
	// extract

	var err error = nil
	result := make(map[string]string)


	// search for  var sockstates = [1,1,1,1];
	//status_regexp = regexp.MustCompile(status_pattern)
	found := lan_settings_regexp.FindString( text)
	if len(found)> 0 {
		//fmt.Println(found)

		array := lan_settings_regexp.FindAllStringSubmatch(text,-1)
		_=array
		for _, v := range array {
			result[v[1]] = v[2]
		    //fmt.Printf("%s = %s\n", v[1], v[2])
		}

	} else {
		msg:= "Energenie cannot find inputs in lan_settings.html page"
		fmt.Println(msg)
		err = errors.New(msg)
	}


	return result, err
}






// channel functions

func EnergenieStatusChannelResponse( source chan net.TCPAddr,cmd string,passwd string) chan Response {
	//  return a channel of response to energenie status requst , from a TCPAddr source channel
	sink := make(chan Response)
	go func() {
		defer close(sink)

		for addr := range source {
			_ = addr

			// prepare a blank response
			response := Response{ addr.IP.String(), nil, timestamp(),cmd,nil}

			// create an Energenie handler
			h,err := NewEnergenie(addr.String(),passwd)
			if err == nil {
				// send the request
				data, err := h.GetStatus()
				if err == nil {
					response.Response = data.Response
				}
			}
			if err != nil {
				// this is not an energenie device
				//response.Err = err
				continue
			}
			sink <- response
		}
	}()
	return sink
}


func EnergenieLanConfigChannelResponse( source chan net.TCPAddr,cmd string,passwd string) chan Response {
	//  return a channel of response to energenie status requst , from a TCPAddr source channel
	sink := make(chan Response)
	go func() {
		defer close(sink)

		for addr := range source {
			_ = addr

			// prepare a blank response
			response := Response{ addr.IP.String(), nil, timestamp(),cmd,nil}

			// create an Energenie handler
			h,err := NewEnergenie(addr.String(),passwd)
			if err == nil {
				// send the request
				data, err := h.GetLanConfig()
				if err == nil {
					response.Response = data.Response
				}
			}
			if err != nil {
				// this is not an energenie device
				//response.Err = err
				continue
			}
			sink <- response
		}
	}()
	return sink
}





func init() {

	status_regexp = regexp.MustCompile(status_pattern)
	lan_settings_regexp = regexp.MustCompile(lan_settings_pattern)

}