package inspectors

import (
	//	"net"
	//"strings"
	"strings"
	"time"
	"encoding/json"

)

func timestamp() int32{
	return int32(time.Now().Unix())
}

type Response struct {
	// response to a request

	Ip string    			`json:"_ip"`
	Err error				`json:"_err"`
	Timestamp int32			`json:"_timestamp"`
	Request string			`json:"_request"`
	Response interface{} 	`json:"response"`
}

func (r * Response )Jsonify() ([]uint8,error) {
	b, err := json.Marshal(r)
	return b,err
}











///




type Inspector interface {
	DetailInspector
	Inspect(host string) string
	InspectResponse(resp string, dp DetailInspector) string
}


type DetailInspector interface {
	InspectResponseDetail(resp string) string
}


type BaseHttpInspector struct {
	//	DetailPredictor
}

func (pa *BaseHttpInspector) InspectResponse(resp string, dp DetailInspector) string {
	if strings.Contains(resp, "HTTP/") {
		rv := ""
		detail := dp.InspectResponseDetail(resp)
		if len(detail) > 0 {
			rv = "web server"
		}
		return strings.TrimSpace(rv + " " + detail)
	}
	return ""
}

func (pa *BaseHttpInspector) InspectResponseDetail(resp string) string {
	return "aoeu"
}
