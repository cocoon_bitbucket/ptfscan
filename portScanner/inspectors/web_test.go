package inspectors_test

import(

	"testing"
	"bitbucket.org/cocoon_bitbucket/ptfscan/portScanner/inspectors"
	"fmt"

)


func TestWebFinder(t *testing.T) {

	ip := "192.168.1.1"

	w, err := inspectors.NewWebFinder(ip)
	if err != nil {
		fmt.Printf("NO web server found at %s\n", ip)
	} else {
		fmt.Printf("YES we got  a web server at %s\n", ip)
	}
	_= w
}