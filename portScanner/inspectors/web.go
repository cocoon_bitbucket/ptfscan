package inspectors

import(

	"net/http"
	"errors"
	"time"
	"fmt"
)


/*

	an http client to search for  web site


 */

var (

	// the timeout to GET or POST
	WebFinderHttpTimeout  = 1 * time.Second

)


type WebFinder struct {
	// structure of a Energenie Element

	http.Client
	Addr string    // ip of the server
}


func NewWebFinder ( netAddr string ) (* WebFinder,error) {
	// create a web finder aka an http.Client to try to identify a web site
	// return a WebFinder object

	var err error = nil

	client := http.Client{
		Timeout: WebFinderHttpTimeout,
	}
	e := &WebFinder{ client, netAddr }

	if e.Probe() == false {
		// not a web server
		err = errors.New("Not a web server")
	}
	return e , err

}

func ( w * WebFinder) Probe() bool {
	// try to recognise a web server
	url := fmt.Sprintf("http://%s", w.Addr)
	r, err := w.Get(url)
	//r,err:= e.Head(url)
	_ = r
	if err != nil {
		// cannot reach a server
		fmt.Println(err)
		return false
	}
	return true
}