package inspectors

import (
	"regexp"
	"time"
	"fmt"
	"strings"
	"bitbucket.org/cocoon_bitbucket/ptfscan/telnet"
	"net"
	//"encoding/json"
)

/*
	detect livebox

	port 23: telnet
	port 80: http admin

 */



var (


	pcb_cli_line_regexp * regexp.Regexp

	PcbCliCommandString string = "pcb_cli"

	// identify a livebox http response



)


//type PcbCliResponse struct {
//	// response to a pcbcli request
//
//	Ip string    			`json:"_ip"`
//	Err error				`json:"_err"`
//	Timestamp int32			`json:"_timestamp"`
//	Request string			`json:"_request"`
//	Response interface{} 	`json:"response"`
//}
//
//func ( r * PcbCliResponse) Jsonify() ([]uint8,error) {
//	b, err := json.Marshal(r)
//	return b,err
//}


type PcbCli struct {
	// structure of a pcbcli Element

	* telnet.TelnetConn
	User string
	Passwd string
	opened bool
	logged bool

}


func NewPcbCli ( netAddr ,user,passwd string ) (* PcbCli,error) {

	timeout := 2 * time.Second
	// open a telnet connection
	var cli * PcbCli = nil

	t ,err :=  telnet.DialTimeout( "tcp", netAddr, timeout )
	if err == nil {
		// connected
		cli = &PcbCli{ t , user, passwd, true, false}
		// try to login
		err = cli.Login(user,passwd)
		if err != nil {
			cli = nil
		} else {
			cli.logged = true
		}
	}
	return cli , err

}


func ( p * PcbCli) Login( user,passwd string) error {

	return telnet.Login(p.TelnetConn,user,passwd)
}

func ( p * PcbCli) Close() error {
	if p.opened == true {
		p.TelnetConn.Close()
		p.opened = false
		p.logged = false
	}
	return nil
}


func ( p * PcbCli) SendLn(command string) error {
	// send a command to the livebox telnet connection eg: ls -l
	return telnet.Sendln( p.TelnetConn,command)

}

func ( p * PcbCli) ReadLn() ([]byte,error) {
	// read a response till the delimiter ( eg a response to "ls -l" command
	timeoutDuration := time.Duration(2000 * time.Millisecond )
	p.SetReadDeadline(time.Now().Add(timeoutDuration))
	return p.TelnetConn.ReadBytes('#')
}
//Sendln(t, "ls -l")
//response, err := t.ReadBytes('#')



func ( p * PcbCli) SendRequest( cmd string)  (map[string]string,error){
	// send a pcb cli command  eg : Device.DeviceInfo => #pcb_cli Device.DeviceInfo?

	wait_response_timeout := 1 * time.Second
	data := make(map[string]string)

	if cmd[len(cmd)-1] != '?' {
		cmd = fmt.Sprintf("%s?",cmd)
	}
	cmd = fmt.Sprintf("pcb_cli %s",cmd)

	// send pcb cli command
	err := telnet.Sendln(p.TelnetConn ,cmd)
	if err == nil {
		//time.Sleep(100 * time.Millisecond)
		err = p.TelnetConn.SetReadDeadline(time.Now().Add(wait_response_timeout))
		if err == nil {
			response, err := p.TelnetConn.ReadBytes('#')
			if err == nil {

				//fmt.Printf("response: %v", string(response))

				lines := strings.Split(string(response), "\r\n")
				_ = lines

				for _,line := range lines {

					result := pcb_cli_line_regexp.FindString(line)
					if len(result) > 0 {
						//println(result)
						parts := strings.Split(result,"=")
						data[parts[0]]= parts[1]
					}
				}
			}
		}
	}
	return data,err
}


// channel functions

func ChannelResponse( source chan net.TCPAddr,cmd string,user,passwd string) chan Response {
	//  return a channel of response to pcbcli request , from a TCPAddr source channel
	sink := make(chan Response)
	go func() {
		defer close(sink)

		for addr := range source {
			_ = addr

			// prepare a blank response
			response := Response{ addr.IP.String(), nil, timestamp(),cmd,nil}

			// create a pcb cli
			pcb,err := NewPcbCli(addr.String(),user,passwd)
			if err == nil {
				// send the request
				data, err := pcb.SendRequest(cmd)
				if err == nil {
					response.Response = data
				}
			}
			if err != nil {
				response.Err = err
			}
			sink <- response
		}
	}()
	return sink
}

//func FindLiveboxChannelResponse( source chan net.TCPAddr) chan Response {
//	//  return a channel of response to pcbcli request , from a TCPAddr source channel
//	// search web server with <title>Livebox</title>
//	sink := make(chan Response)
//	go func() {
//		defer close(sink)
//
//		for addr := range source {
//			_ = addr
//
//
//
//			// prepare a blank response
//			response := PcbCliResponse{ addr.IP.String(), nil, timestamp(),cmd,nil}
//
//			// create a pcb cli
//			pcb,err := NewPcbCli(addr.String(),user,passwd)
//			if err == nil {
//				// send the request
//				data, err := pcb.SendRequest(cmd)
//				if err == nil {
//					response.Response = data
//				}
//			}
//			if err != nil {
//				response.Err = err
//			}
//			sink <- response
//		}
//	}()
//	return sink
//}






//func JsonifyResponse( response *Response) ([]uint8,error) {
//
//	b, err := json.Marshal(response)
//	//if err
//	//_=b
//	//_=err
//
//	return b,err
//}


//func timestamp() int32{
//	return int32(time.Now().Unix())
//}


type pcb_cli_lines map[string]string




var Commands  = map[string]string {

	//"AdditionalSoftwareVersion": "DeviceInfo.AdditionalSoftwareVersion",
	"ProductClass":      "Device.DeviceInfo.ProductClass",   // Livebox 4
	"Manufacturer":      "Device.DeviceInfo.Manufacturer", // Sagemcom
	"SerialNumber":      "Device.DeviceInfo.SerialNumber", //  =LK15233DP992021
	"UpTime":            "Device.DeviceInfo.UpTime=", // =3983173
	"MACAddress":        "Device.DeviceInfo.X_ORANGE-COM_MACAddress" ,  // =A0:1B:29:FC:12:70
	"ExternalIPAddress": "Device.DeviceInfo.X_ORANGE-COM_ExternalIPAddress" ,   // =82.124.70.85
	"HardwareVersion":   "Device.DeviceInfo.HardwareVersion",  // =SG_LB4_1.0.1
	"SoftwareVersion":   "Device.DeviceInfo.SoftwareVersion", // =SG40_sip-fr-3.4.3.1_7.21.3.0
	"DeviceStatus":      "DeviceInfo.DeviceStatus", // =Up

	"wifi0.Channel":      "WiFiBCM.Radio.wifi0.Channel", // =11
	"wifi0.Enable":       "WiFiBCM.Radio.wifi0.Enable", // =1

	"CurrentLocalTime":   "Time.CurrentLocalTime", // =2018-01-03T15:20:15Z
	"LocalTimeZone=":     "Time.LocalTimeZone", //  =CET-1CEST,M3.5.0,M10.5.0/3

	"admin.Enable": "UserManagement.User.admin.Enable", //=1
	"admin.UserName": "UserManagement.User.admin.UserName", // =admin
	"admin.Password": "UserManagement.User.admin.Password", //=31546e8641d67895739485bb38eede92592c36b4ab06c3b1d4aaf2f24992f398
	"admin.PasswordType": "UserManagement.User.admin.PasswordType", // =SSHA256

	"Power Led.Color": "LED.Power Led.Color", // =Green
	"Power Led.State": "LED.Power Led.State",  // =Solid

	"Internet Led.Color": "LED.Internet Led.Color", //=Green
	"Internet Led.State": "LED.Internet Led.State", //=Solid

	"VoIP Led.Color": "LED.VoIP Led.Color", // =Green
	"VoIP Led.State": "LED.VoIP Led.State", //=Solid

	"Lan Led.Color": "LED.Lan Led.Color", //=Green
	"Lan Led.State": "LED.Lan Led.State", //=Off

	"Wifi Led.Color": "LED.Wifi Led.Color", //=Green
	"Wifi Led.State": "LED.Wifi Led.State", //=Solid

	"Upgrade Led.Color": "LED.Upgrade Led.Color", //=Blue
	"Upgrade Led.State": "LED.Upgrade Led.State", //=Off

	"dsl0.Status": "DSL_BROADCOM.Line.dsl0.Status", //=Up
	"dsl0.Name": "DSL_BROADCOM.Line.dsl0.Name", // =dsl0
	"dsl0.LastChange": "DSL_BROADCOM.Line.dsl0.LastChange", //=3983794


	"dsl0.BytesSent": "DSL_BROADCOM.Line.dsl0.Stats.BytesSent", // =203117995
	"dsl0.BytesReceived": "DSL_BROADCOM.Line.dsl0.Stats.BytesReceived", // =442482372
	//DSL_BROADCOM.Line.dsl0.Stats.PacketsSent=645010
	//DSL_BROADCOM.Line.dsl0.Stats.PacketsReceived=727300
	"dsl0.ErrorsSent": "DSL_BROADCOM.Line.dsl0.Stats.ErrorsSent", // =0
	"dsl0.ErrorsReceived": "DSL_BROADCOM.Line.dsl0.Stats.ErrorsReceived", // =0

	//"DNS.Server.Host.dcodeurtv4.Name", // =dcodeurtv4
	//"DNS.Server.Host.dcodeurtv4.IPv4", //=192.168.1.54


	"wl0.Enable":        "WiFiBCM.AccessPoint.wl0.Enable", // =1
	"wl0.KeyPassPhrase": "WiFiBCM.AccessPoint.wl0.Security.KeyPassPhrase", // =admin1234



}


func init() {

	//xpr :=  `(\w[\w\.]*)=(.*)`
	xpr :=  `(.+)=(.*)$`

	//pcb_cli_line_pattern, _ := regexp.Compile(xpr)
	pcb_cli_line_regexp = regexp.MustCompile(xpr)

	_ = pcb_cli_line_regexp

}
