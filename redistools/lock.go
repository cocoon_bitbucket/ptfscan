package redistools


import (
	"log"
	"errors"
	"github.com/garyburd/redigo/redis"

)

/*

	redis distributed locks see: https://kylewbanks.com/blog/distributed-locks-using-golang-and-redis

*/


const (
	lockScript = `
		return redis.call('SET', KEYS[1], ARGV[1], 'NX', 'PX', ARGV[2])
	`
	unlockScript = `
		if redis.call("get",KEYS[1]) == ARGV[1] then
		    return redis.call("del",KEYS[1])
		else
		    return 0
		end
	`
)




type RedisLock struct {
	Pool redis.Pool
}


func NewLock(pool * Pool) ( lock * RedisLock ,err error) {

	if pool == nil {
		// try to get a redistools pool singleton
		log.Printf("NewLock no pool specified: try to get a pool singleton")
		pool,err = GetPool()
		if err != nil {
			return nil,err
		}
		//pool= p.Pool
	}


	lock = &RedisLock{ pool.Pool}
	return  lock,err
}



// Acquire attempts to put a lock on the key for a specified duration (in milliseconds).
// If the lock was successfully acquired, true will be returned.
func (l * RedisLock) Acquire(key, value string, timeoutMs int) (bool, error) {
	r := l.Pool.Get()
	defer r.Close()

	cmd := redis.NewScript(1, lockScript)
	if res, err := cmd.Do(r, key, value, timeoutMs); err != nil {
		return false, err
	} else {
		return res == "OK", nil
	}
}

// Release attempts to remove the lock on a key so long as the value matches.
// If the lock cannot be removed, either because the key has already expired or
// because the value was incorrect, an error will be returned.
func (l * RedisLock) Release(key, value string) error {
	r := l.Pool.Get()
	defer r.Close()

	cmd := redis.NewScript(1, unlockScript)
	if res, err := redis.Int(cmd.Do(r, key, value)); err != nil {
		return err
	} else if res != 1 {
		return errors.New("Release failed, key or secret incorrect")
	}

	// Success
	return nil
}
