package redistools

import(

	"github.com/garyburd/redigo/redis"

	"time"
	"errors"

	"strings"
)

/*


	acl store Access Control list for devices


	acl:<name>:<token>:  -> list of resource names


	we create an acl in a realm named Livebox to access a group of livebox lb1,lb2 with a ttl

		acl:Livebox:<token> -> LIST  [lb1,lb2]


	NewAcl ( realm , ttl , ... member) -> return token or err

	Get( token, realm ) -> list of members  or err ()

	Revoke ( token )
	Renew( token, ttl )
	ttl ( token )


	oauth header:

	Authorization: Bearer 0b79bab50daca910b000d4f1a2b675d604257e42



 */


var (

	AclKey string =  "acl"

	AclPrefix string = "acl"

	secret string = "secret"
)


func acl_key(token,acl_name string) string{
	// eg acl:Livebox:3212fa5f98b85781
	return AclKey + ":" + acl_name + ":" + token
}


func aclHeader(token string) string {
	// the http header for oauth
	return "Authorization: Bearer " + token
}



///  acl Provider


type AclProvider struct {

	redis.Conn

	Realm string
	//Ttl int
	//Members []string
	Token string
	Key string


}


func NewAclProvider( cnx redis.Conn,  realm string, token string) ( AclProvider ) {

	// get a new token
	if token == "" {
		token = NewToken()
	}
	key := acl_key(token, realm)
	acl := AclProvider{cnx, realm, token, key}
	return acl
}

// initialize the acl in redis database
func ( acl * AclProvider ) Set ( ttl int, member ...string) (token string,err error) {

	// create entry
	acl.Send("MULTI")
	for _, e := range member {
		acl.Send("SADD", acl.Key, e)
	}
	_, err = acl.Do("EXEC")
	if err != nil {
		return "no-token", err
	}

	// set ttl
	_, err = acl.Do("EXPIRE", acl.Key, ttl)
	if err != nil {
		return "no-token", err
	}
	return acl.Token,err
}

func ( acl * AclProvider ) Get() ([]string,error) {

	var members []string

	r, err := redis.Values(acl.Do("SMEMBERS", acl.Key ))
	if err != nil {
		return members,err
	}
	//print(r)
	for _,e := range r {
		members = append(members, string(e.([]uint8)))
	}
	return members,nil
}

func ( acl * AclProvider ) Assert( member string) (bool,error) {
	// assert member in on the list
	ok, err := redis.Bool(acl.Do("SISMEMBER", acl.Key, member))
	return ok, err

}

func ( acl * AclProvider ) Revoke() error {
	// Revoke the all the acl
	_, err := acl.Do("DEL", acl.Key)
	return err
}

func ( acl * AclProvider ) Ttl() (time.Duration,error) {
	// return the time to live in second
	t, err := redis.Int(acl.Do("PTTL", acl.Key))
	ttl := time.Duration(t/1000)
	return ttl,err
}

func ( acl * AclProvider ) Renew ( ttl int) (err error) {
	// set ttl again
	_, err = acl.Do("EXPIRE", acl.Key, ttl)
	return err

}


// acl manager

type AclManager struct {

	redis.Conn

	prefix string

	lock * RedisLock
}

func NewAclManager( cnx redis.Conn ) ( AclManager ) {
//func NewAclManager( pool Pool ) ( AclManager ,error) {

	// get a token manager
	//cnx :=

	manager := AclManager{cnx,AclPrefix,nil}
	return manager
}

func NewAclManagerFromPool( pool Pool ) ( manager AclManager , err error,) {
	//func NewAclManager( pool Pool ) ( AclManager ,error) {

	// get a token manager
	cnx := pool.Get()
	manager = AclManager{cnx,AclPrefix,nil}
	manager.lock,err = NewLock( &pool)

	return manager,err
}


// queries

func ( m * AclManager ) AclKeysOfRealm( realm string ) ( acl []string, err error) {
	//  return all the acl keys of a realm  eg  acl:Livebox:*
	// return a list of keys

	pattern := m.prefix + ":" + realm + ":*"
	v, err := redis.Values(m.Do("KEYS", pattern))
	for _,entry := range v {
		acl = append( acl, string(entry.([]uint8)))
	}
	return acl,err
}



func ( m * AclManager ) AclOfRealm( realm string ) ( acls map[string][]string, err error) {
	// returm a dict of all token with members

	acls = make(map[string][]string)
	// gather the acl keys of the realm
	keys, err := m.AclKeysOfRealm(realm)
	if err != nil {
		return acls, err
	}
	// collect members
	for _, key := range keys {
		v, err := redis.Values(m.Do("SMEMBERS", key))
		if err != nil {
			return acls, nil
		}
		var members []string
		for _, member := range v {
			members = append(members, string(member.([]uint8)))
		}
		parts := strings.Split(key,":")
		token := parts[2]
		acls[token] = members
	}
	return acls,err
}



func ( m * AclManager ) MembersOfRealm( realm string ) ( members []string, err error) {
	// retreive all acl this a specific name , collect all members ( ie reserved members )

		// gather the acl keys of the realm
		keys, err := m.AclKeysOfRealm(realm)
		if err != nil {
			return members, err
		}
		// collect members
		for _, key := range keys {
			v, err := redis.Values(m.Do("SMEMBERS", key))
			if err != nil {
				return members, nil
			}
			for _, member := range v {
				members = append(members, string(member.([]uint8)))
			}
		}

	return members,err

}

// realm operations

func ( m * AclManager ) RevokeRealm( realm string ) ( err error) {
	// clear all acl of a realm

	err = m.lock_realm(realm)
	if err == nil {
		// we got a lock
		defer m.unlock_realm(realm)
		keys, err := m.AclKeysOfRealm(realm)
		if err != nil {
			return err
		}
		m.Send("MULTI")
		for _, key := range keys {
			m.Send("DEL", key)
		}
		_, err = m.Do("EXEC")
	}
	return err

}


// individual acl operations

func ( m * AclManager ) AddAcl ( realm string , ttl int, member ...string) ( token string, err error) {

	// create a new acl  acl:<realm>:<token> -> list of members
	// check none of the members are already reserved
	// try to lock the realm
	err = m.lock_realm(realm)
	if err == nil {
		defer m.unlock_realm(realm)
		reserved, err := m.MembersOfRealm(realm)
		if err != nil {
			return token, err
		}
		found := false
		if len(reserved) > 0 {
			// reserved list not empty : check members
			//found := false
			for _, candidate := range member {
				for _, res := range reserved {
					if res == candidate {
						// we found a reserved member : cancel the transaction
						found = true
						break
					}
				}
			}
		}
		if found {
			// one of the member is already reserved: cancel
			err = errors.New("Cancel, a member is already reserved")
			return "",err
		} else {
			// ok proceed with the acl creation
			acl := NewAclProvider(m.Conn, realm, "")
			token, err = acl.Set(ttl, member...)
			return token,err
		}
	}
	return token,err
}

func ( m * AclManager ) RevokeAcl ( realm string ,token string) ( err error) {

	acl := NewAclProvider( m.Conn, realm,token)
	err = m.lock_realm(realm)
	if err == nil {
		defer m.unlock_realm(realm)
		err = acl.Revoke()
	}
	return err
}


// private methods

func ( m * AclManager ) lock_realm ( realm string ) ( err error) {

	if m.lock != nil {
		key := m.prefix + ":" + realm
		// eg acl:Livebox
		acquired,err := m.lock.Acquire(key,secret,1000)
		if err == nil {
			if acquired == true {
				// OK
				return nil
			} else {
				return errors.New("failed: cannot acquire the lock")
			}
		}
		return err
	}
	return nil
}

func ( m * AclManager ) unlock_realm ( realm string ) ( err error) {

	if m.lock != nil {
		key := m.prefix + ":" + realm
		// eg acl:Livebox
		err = m.lock.Release(key,secret)
		return err
	}
	return nil
}
