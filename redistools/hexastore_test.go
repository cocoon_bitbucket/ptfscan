package redistools_test


import(

	"testing"
	"github.com/garyburd/redigo/redis"

	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	"github.com/stretchr/testify/assert"

	//"fmt"
)


var redis_url = "redis://localhost:6379/1"



func TestEncoder(t *testing.T) {

	rdf := redistools.Rdf{"i", "SEE", "you"}

	encoded,_ := rdf.Encode("spo")

	assert.Equal(t,  encoded , "spo:i:SEE:you")

}

func TestDecoder(t *testing.T) {

	rdf := &redistools.Rdf{"?", "?", "?"}

	encoded := "spo:i:SEE:you"

	rdf.Decode(encoded)

	assert.Equal(t, rdf.S,"i")
	assert.Equal(t, rdf.P,"SEE")
	assert.Equal(t, rdf.O,"you")

}

func TestQueryBoundaries(t *testing.T) {

	rdf := redistools.Rdf{"i", "SEE", "you"}

	start,stop := rdf.QueryBoundaries("spo")

	assert.Equal(t,  start, "[spo:i:SEE:")
	assert.Equal(t,  stop, "[spo:i:SEE:\xff")

}



func TestHexastore_HexaStore(t *testing.T) {


	cnx,err := redis.DialURL(redis_url)
	if err == nil {

		h := redistools.Hexastore{cnx}

		// delete hexastore
		h.Do("FLUSHDB")

		h.HexaStore(redistools.Rdf{"i", "SEE", "you"})
		h.HexaStore(redistools.Rdf{"i", "SEE", "her"})

		query := redistools.Rdf{"i", "SEE", "?"}

		r,err := h.HexaQuery("spo",query)
		if err == nil {
			//fmt.Printf("%v\n",r)
			assert.Equal(t,r[0].S,"i")
			assert.Equal(t,r[0].P,"SEE")
			assert.Equal(t,r[0].O,"her")

			assert.Equal(t,r[1].S,"i")
			assert.Equal(t,r[1].P,"SEE")
			assert.Equal(t,r[1].O,"you")


		} else {
			println(err)
		}

	}


}


