package redistools_test


import (

	"testing"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	"github.com/garyburd/redigo/redis"
	"log"
	"fmt"
	"github.com/stretchr/testify/assert"

	"time"
	"strings"
)

func ping(cnx redis.Conn) (string, error) {

	pong,err := redis.String(cnx.Do("PING"))
	if err != nil {
		log.Fatal(fmt.Sprintf("cannot open redis connection at %s : %s", redis_url,err.Error()))
	}
	if pong != "PONG" {
		log.Fatal("response is not PONG")
	}
	return pong,err
}



func TestOriginRedisPool(t *testing.T) {

	redis_url := redistools.Redis_DefaultUrl

	p := redistools.RedisPool( redis_url )

	cnx := p.Get()
	defer cnx.Close()

	ping(cnx)

}



func TestNewPool(t *testing.T) {

	redis_url := redistools.Redis_DefaultUrl


	// create a pool singleton
	p,_ := redistools.NewPool(redis_url)
	defer redistools.UnsetPool()

	// get a connection and test it
	c1 := p.Get()
	defer c1.Close()
	ping(c1)


	// try to recreate pool
	_,err := redistools.NewPool(redis_url)
	assert.Equal(t,true,strings.Contains(err.Error(),"already"))



}


func TestGetPool(t *testing.T) {

	redis_url := redistools.Redis_DefaultUrl

	// create a pool singleton
	p,_ := redistools.NewPool(redis_url)
	defer redistools.UnsetPool()

	c1 := p.Get()
	defer c1.Close()
	ping(c1)


	// obtain the pool singleton
	p2,_ := redistools.GetPool()

	assert.Equal(t,p,p2)

	c2 := p2.Get()
	defer c2.Close()
	ping(c2)


}


func TestUnsetPool(t *testing.T) {

	redis_url := redistools.Redis_DefaultUrl


	// create a pool singleton
	p,_ := redistools.NewPool(redis_url)
	defer redistools.UnsetPool()

	c1 := p.Get()
	defer c1.Close()
	ping(c1)


	// obtain the pool singleton
	p2,_ := redistools.GetPool()

	assert.Equal(t,p,p2)

	c2 := p2.Get()
	defer c2.Close()
	ping(c2)

	// unset the pool singleton
	redistools.UnsetPool()

	_,err := redistools.GetPool()
	assert.Equal(t,true,strings.Contains(err.Error(),"No Pool"))


}

func TestSetPool(t *testing.T) {

	redis_url := redistools.Redis_DefaultUrl

	// create a pool a pool singleton
	rp := &redis.Pool{
		MaxIdle: 3,
		IdleTimeout: 240 * time.Second,
		//Dial: func () (redis.Conn, error) { return redis.Dial("tcp", addr) },
		Dial: func () (redis.Conn, error) { return redis.DialURL(redis_url) },
	}
	// set the pool singleton with it
	redistools.SetPool(rp,redis_url)
	defer redistools.UnsetPool()


	_,err := redistools.SetPool(rp,redis_url)
	assert.Equal(t,true,strings.Contains(err.Error(),"already"))


	// obtain the pool singleton
	p,_ := redistools.GetPool()

	c1 := p.Get()
	defer c1.Close()
	ping(c1)

}


func TestPoolPing(t *testing.T) {

	redis_url := redistools.Redis_DefaultUrl

	// create a pool a pool singleton
	// create a pool singleton
	redistools.NewPool(redis_url)
	defer redistools.UnsetPool()

	// obtain the pool singleton
	p2,_ := redistools.GetPool()

	err := p2.Ping()
	assert.Equal(t,nil,err)


}


func TestNoPool(t *testing.T) {


	p,err := redistools.GetPool()

	assert.Equal(t,true,strings.Contains(err.Error(),"No Pool"))
	_=p


	return



}
