package store_test

import(

	"testing"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools/store"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	"github.com/stretchr/testify/assert"
	"fmt"
)

var (

	redisUrl = redistools.Redis_DefaultUrl
)


func TestStoreLoadDelete(t *testing.T) {

	db,err := store.NewStore(redisUrl)
	assert.Equal(t,nil,err)
	_=db

	prop := make(map[string]string)
	prop["_entity"] = "livebox"
	prop["name"] = "Livebox-demo"
	prop["mac"] = "00:00"
	prop["model"] = "LB0"
	//
	o,_ := store.NewOid(db)
	//
	err = db.Store(o,prop)
	fmt.Printf("%s\n",err)

	prop2,_ := db.Load(o)
	assert.Equal(t,4,len(prop2))
	_=prop2

	err = db.Delete(o)
	assert.Equal(t,nil,err)

	prop3,_ := db.Load(o)
	assert.Equal(t,0,len(prop3))

	redistools.UnsetPool()

}