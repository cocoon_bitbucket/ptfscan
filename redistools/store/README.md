

# a sample for entity and oid storage



## Schema

entity_name: livebox

schema:
  
* name is the Primarykey (pk)
* mac is a uniq index
* model is the model of livebox ( LB1,LB2 ...)


## data set
Livebox-demo mac="mac0" , model=LB0
Livebox-1    mac="mac1" , model=LBA
livebox-2    mac="mac2  , model=LBA


## key/values
entity:livebox:schema  HASH { name:pk ,mac:unique, model:index }

oid:1  HASH { _entity=livebox , name=Livebox-demo , mac=mac0 , model:LB0 }
oid:2  HASH { _entity=livebox , name=Livebox-1 , mac=mac1    , model:LBA }
oid:3  HASH { _entity=livebox , name=Livebox-2 , mac=mac2    , model:LBA }

entity:livebox:index:name HASH {  livebox-demo:1 , Livebox-1:2 Livebox-2:3 }

entity:livebox:index:mac HASH  { mac0:1 , mac1:2 , mac3:3 }

entity:livebox:index:model:LB0  SET [1]  
entity:livebox:index:model:LBA  SET [2,3]

entity:livebox:members SET [1,2,3]


## topic  a Json content associated with an object
oid:1:topic:DeviceInfo  JSON  


