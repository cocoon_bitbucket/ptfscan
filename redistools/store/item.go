

package store

import (
"github.com/garyburd/redigo/redis"
//"errors"
//"fmt"
)
/*

	Item is an ready to use entity with a unique index "name" and a duplicate index "model"

	entity:item:schema  HASH  { name:"named"  }


	oid:1 HASH { _entity:"named" , name:"my_entity" other:"other"}
	entity:named:index:name HASH


 */

var (

	// item schema
	ItemEntitySchema = EntitySchema{
		Name: "item",
		Indexes: map[string]string{
			"name": "unique",
			"model": "duplicate",
		},
	}

)

type ItemEntity struct {
	Entity
}


func CreateItemEntitySchema(cnx redis.Conn) (err error){

	schema := ItemEntitySchema
	err = schema.Save(cnx)
	return err

}


func NewItemEntity( cnx redis.Conn, name string,model string, properties properties) (entity ItemEntity,err error) {

	//prop := make(map[string]string)
	p := make(map[string]string)
	for k,v := range properties{
		p[k]= v
	}
	p["name"] = name
	p["model"] = model
	e := Entity{ItemEntitySchema.Name, p, 0}
	entity = ItemEntity{e}

	return entity,err
}


func LoadItem(cnx redis.Conn,name string)  (entity ItemEntity,err error)  {

	e,err := LoadEntityByIndex(cnx, ItemEntitySchema.Name,"name",name)
	if err != nil { return entity, err }

	entity.EntityName = e.EntityName
	entity.Properties = e.Properties
	entity.Oid = e.Oid

	return entity,err
}


//
// item entity methods
//

func (i * ItemEntity) Schema() EntitySchema {
	return ItemEntitySchema
}



// hash methods

func (i * ItemEntity) AddHash( cnx redis.Conn,name string,data map[string]interface{} ) error {
	h := NewHash(cnx,i.Oid)
	return h.Write(name,data)
}

func (i * ItemEntity) GetHash( cnx redis.Conn,name string ) ( content []byte,err error) {
	h := NewHash(cnx,i.Oid)
	return h.Read(name)
}

func (i * ItemEntity) DeleteHash( cnx redis.Conn,name string ) ( err error) {
	h := NewHash(cnx,i.Oid)
	return h.Delete(name)
}

// List methods

func (i * ItemEntity) Rpush( cnx redis.Conn,name string,data interface{} ) error {
	h := NewList(cnx,i.Oid)
	return h.Rpush(name,data)
}

func (i * ItemEntity) Lpop( cnx redis.Conn,name string) (line []byte,err error ){
	h := NewList(cnx,i.Oid)
	return h.Lpop(name)
}

func (i * ItemEntity) Rpop( cnx redis.Conn,name string) (line []byte,err error ){
	h := NewList(cnx,i.Oid)
	return h.Rpop(name)
}

func (i * ItemEntity) Lrange( cnx redis.Conn,name string, start,stop int) (line [][]byte,err error ){
	h := NewList(cnx,i.Oid)
	return h.Lrange(name,start,stop)
}

func (i * ItemEntity) Lindex( cnx redis.Conn,name string,index int) (line []byte,err error ){
	h := NewList(cnx,i.Oid)
	return h.Lindex(name,index)
}

func (i * ItemEntity) DeleteList( cnx redis.Conn,name string ) ( err error) {
	h := NewList(cnx,i.Oid)
	return h.Delete(name)
}