package store

import (
	"github.com/garyburd/redigo/redis"
	"fmt"
	"errors"
	"encoding/json"
)

/*


	a topic is an object attached to an oid

	it can be a Hash , List or Set and has a name
	it contains a json representation

	oid:1:Hash:DeviceInfo  => Json content { key1:value1 , key2:value2

	oid:1:List:depends_on => Json content [ d1 , d2 ....]

	oid:1:Set:linkedto => [ l1 , l2 .. ]


	api

	NewHash(cnx,oid).Write(name,interface{})





 */


//type topicface interface {
//
//	Write(cnx redis.Conn, id oid , name string,data interface{}) error
//	Read(cnx redis.Conn, id oid , name string) ( []byte,error)
//
//}



type Topic struct {
	cnx redis.Conn
	Oid oid
}
func (h Topic) Key (name string) string {
	// return key eg oid:1:Hash:DeviceInfo
	return fmt.Sprintf("%s:Topic:%s",h.Oid.Key(),name)
}



//
//  HASH
//


type Hash struct {
	Topic
}
func (h Hash) Key (name string) string {
	// return key eg oid:1:Hash:DeviceInfo
	return fmt.Sprintf("%s:Hash:%s",h.Oid.Key(),name)
}

func NewHash(cnx redis.Conn,id oid) Hash {

	//data := make(map[string]interface{})
	t := Topic{cnx,id}
	h := Hash{t}
	return h

}

func (h Hash) Write(name string, data map[string]interface{}) (err error) {
	// return key eg oid:1:Hash:DeviceInfo
	text,err := json.Marshal(data)
	if err != nil { return err }
	_,err = h.cnx.Do("SET",h.Key(name),text)
	return err
}

func (h * Hash) Read(name string) ( json_text []byte, err error) {
	// return json eg oid:1:Hash:DeviceInfo
	v,err := h.cnx.Do("GET",h.Key(name))
	if err != nil { return json_text, err }
	if v != nil {
		json_text = v.([]byte)
	} else {
		err = errors.New("Not found")
	}
	return json_text, err
}

func (h Hash) Delete(name string) (err error) {
	if err != nil { return err }
	_,err = h.cnx.Do("DEL",h.Key(name))
	return err
}



//
// List
//

type List struct {
	Topic
}
func (t List) Key (name string) string {
	// return key eg oid:1:Hash:DeviceInfo
	return fmt.Sprintf("%s:List:%s",t.Oid.Key(),name)
}

func NewList(cnx redis.Conn,id oid) List {

	//data := make(map[string]interface{})
	t := Topic{cnx,id}
	l := List{t}
	return l
}


func (t List) Rpush(name string, data interface{}) (err error) {
	// return key eg oid:1:Hash:DeviceInfo
	text,err := json.Marshal(data)
	if err != nil { return err }
	_,err = t.cnx.Do("RPUSH",t.Key(name),text)
	return err
}

func (t * List) Lpop(name string) ( json_text []byte, err error) {
	// return json eg oid:1:Hash:DeviceInfo
	v,err := t.cnx.Do("LPOP",t.Key(name))
	if err != nil { return json_text, err }
	if v != nil {
		json_text = v.([]byte)
	} else {
		err = errors.New("EOF")
	}
	return json_text, err
}

func (t * List) Rpop(name string) ( json_text []byte, err error) {
	// return json eg oid:1:Hash:DeviceInfo
	v,err := t.cnx.Do("RPOP",t.Key(name))
	if err != nil { return json_text, err }
	if v != nil {
		json_text = v.([]byte)
	} else {
		err = errors.New("EOF")
	}
	return json_text, err
}

func (t * List) Lindex(name string, index int) ( json_text []byte, err error) {
	// return json eg oid:1:Hash:DeviceInfo
	v,err := t.cnx.Do("LINDEX",t.Key(name),index)
	if err != nil { return json_text, err }
	if v != nil {
		json_text = v.([]byte)
	} else {
		err = errors.New("IndexError")
	}
	return json_text, err
}

func (t * List) Lrange(name string, start,stop int) ( array [][]byte, err error) {
	// return json eg oid:1:Hash:DeviceInfo
	lines,err := redis.Values(t.cnx.Do("LRANGE",t.Key(name),start,stop))
	if err != nil { return array, err }
	for _,v := range lines {
		text := v.([]byte)
		array = append(array,text)
	}
	return array, err
}

func (t * List) Delete(name string) (err error) {
	if err != nil { return err }
	_,err = t.cnx.Do("DEL",t.Key(name))
	return err
}
