package store

import (

	"github.com/garyburd/redigo/redis"
	//"fmt"
	//"errors"
	//"strconv"
	"fmt"
)

/*


	members


	entity:<entity_name>:members SET of oid


 */


type Members struct {
	EntityName string
}

func (m Members) Key () string {
	return fmt.Sprintf("entity:%s:members",m.EntityName)
}

func (m Members) Add (cnx redis.Conn, id oid) (err error) {
	// add key to the set
	_,err = cnx.Do("SADD",m.Key(),id)
	return err
}

func (m Members) Remove (cnx redis.Conn, id oid) (err error ){
	_,err = cnx.Do("SREM",m.Key(),id)
	return err
}

func (m Members) IsMember (cnx redis.Conn, id oid) (bool,error) {
	v,err := redis.Bool(cnx.Do("SISMEMBER",m.Key(),id))
	return v,err
}

func (m Members) GetMembers (cnx redis.Conn) (ids []oid,err error) {
	v,err := redis.Values(cnx.Do("SMEMBERS",m.Key()))
	for _,e := range v {
		sid := string(e.([]uint8))
		id,err := OidFromString(sid)
		if err != nil { return ids,err}
		ids = append(ids,id)
	}
	return ids,err
}