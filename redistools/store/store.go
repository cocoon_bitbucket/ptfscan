package store

import (

	"github.com/garyburd/redigo/redis"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	"time"
	"log"

)


type store interface {


	Load(oid) error
	Store(oid,properties properties) error
	//Delete( oid) error

	//
	//GetDeviceIp( deviceName string ) ( DeviceIps , error)
	//GetInfraLivebox() (InfraLiveboxInfoMap, error)
	//
	//// acl
	//CreateAcl( realm string, ttl int, member ...string) (token string, err error)
	//GetAcl( token string, realm string) ( AclRecord, error)
	//GetAclOfRealm( realm string) (map[string][]string,error)
	//CheckAcl( realm,token,member string) (bool,error)

}

type StoreDb struct {

	redis.Conn

	RedisPool redistools.Pool
}

func NewStore( redisUrl  string) (db StoreDb, err error) {

	option := redis.DialConnectTimeout( 1 * time.Second)
	cnx, err := redis.DialURL(redisUrl,option)
	if err != nil {
		log.Printf("cannot create redis connection: %s",err.Error())
		return db,err
	}
	redis_pool,err := redistools.NewPool(redisUrl)
	if err != nil {
		log.Printf("NewStoreDb(%s): %s",redisUrl,err.Error())
		return db,err
	}
	return StoreDb{cnx,*redis_pool}, nil
}

func NewStoreFromPool( pool redistools.Pool) (db StoreDb, err error) {

	cnx := pool.Get()
	return StoreDb{cnx,pool}, nil
}





