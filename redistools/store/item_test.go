package store_test


import(

	"testing"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools/store"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	"github.com/stretchr/testify/assert"
	//"fmt"
	//"strings"
	//"fmt"
	"strings"
	"fmt"
)


func TestItemEntity(t *testing.T) {

	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	cnx.Do("FLUSHDB")

	// create item model
	store.CreateItemEntitySchema(cnx)

	item,err := store.NewItemEntity(cnx,"item1","modelA",nil)
	_,err = item.Save(cnx)
	assert.Equal(t, nil, err)

	o,err := store.LoadItem(cnx,"item1")
	assert.Equal(t, nil, err)

	o.Delete(cnx)

	redistools.UnsetPool()

}



func TestItemEntityLoaded(t *testing.T) {

	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	cnx.Do("FLUSHDB")

	// create item model
	store.CreateItemEntitySchema(cnx)


	// create item1
	item1,err := store.NewItemEntity(cnx,"item1","modelA",nil)
	item1.Save(cnx)

	// create item2
	item2,err := store.NewItemEntity(cnx,"item2","modelA",nil)
	item2.Save(cnx)

	o,err := store.LoadEntity(cnx,"1")
	_=o
	assert.Equal(t,"item",o.EntityName)
	assert.Equal(t,"item1",o.Properties["name"])

	o2,err := store.LoadEntityByIndex(cnx,"item","name","item2")
	assert.Equal(t,"item",o2.EntityName)
	assert.Equal(t,"item2",o2.Properties["name"])


	redistools.UnsetPool()
}


func TestItemEntityLoaded2(t *testing.T) {

	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	cnx.Do("FLUSHDB")

	// create item model
	store.CreateItemEntitySchema(cnx)


	// create item1
	item1,err := store.NewItemEntity(cnx,"item1","modelA",nil)
	item1.Save(cnx)

	// create item2
	item2,err := store.NewItemEntity(cnx,"item2","modelA",nil)
	item2.Save(cnx)

	// load by oid
	o,err := store.LoadEntity(cnx,"1")
	assert.Equal(t,"item",o.EntityName)
	assert.Equal(t,"item1",o.Properties["name"])


	// no item
	o,err = store.LoadEntityByIndex(cnx,"item","name","itemX")
	assert.Equal(t,true,strings.Contains(err.Error(),"redigo: nil returned"))


	// bad index
	o,err = store.LoadEntityByIndex(cnx,"item","bad_index","item1")
	assert.Equal(t,true,strings.Contains(err.Error(),"no such index"))

	// duplicate index
	o,err = store.LoadEntityByIndex(cnx,"item","model","item1")
	assert.Equal(t,true,strings.Contains(err.Error(),"index specifier is not unique"))



	redistools.UnsetPool()
}



func TestItemEntityLoadedDelete(t *testing.T) {

	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	cnx.Do("FLUSHDB")

	// create item model
	store.CreateItemEntitySchema(cnx)


	// create item1
	item1,err := store.NewItemEntity(cnx,"item1","modelA",nil)
	item1.Save(cnx)

	// create item2
	item2,err := store.NewItemEntity(cnx,"item2","modelA",nil)
	item2.Save(cnx)

	// load by oid
	o,err := store.LoadEntity(cnx,"1")
	assert.Equal(t,"item",o.EntityName)
	assert.Equal(t,"item1",o.Properties["name"])


	// Delete entity
	err = o.Delete(cnx)


	redistools.UnsetPool()
}


func TestItemEntityHash(t *testing.T) {

	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	cnx.Do("FLUSHDB")

	// create item model
	store.CreateItemEntitySchema(cnx)


	// create item1
	item,err := store.NewItemEntity(cnx,"item1","modelA",nil)
	item.Save(cnx)


	data := make(map[string]interface{})
	data["DeviceInfo.Enable"] = "1"


	err = item.AddHash(cnx,"DeviceInfo",data)
	assert.Equal(t, nil, err)

	text,err := item.GetHash(cnx,"DeviceInfo")
	assert.Equal(t, nil, err)
	fmt.Printf("%s\n",string(text))
	assert.Equal(t,  "{\"DeviceInfo.Enable\":\"1\"}",string(text))
	fmt.Printf("%s\n",string(text))
	_=text


	err = item.DeleteHash(cnx,"DeviceInfo")
	assert.Equal(t, nil, err)

	_,err = item.GetHash(cnx,"DeviceInfo")
	assert.Equal(t,true,strings.Contains(err.Error(),"Not found"))


	redistools.UnsetPool()
}

func TestItemEntityList(t *testing.T) {

	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	cnx.Do("FLUSHDB")

	// create item model
	store.CreateItemEntitySchema(cnx)


	// create item1
	item,err := store.NewItemEntity(cnx,"item1","modelA",nil)
	item.Save(cnx)


	err= item.Rpush(cnx,"stdin","ls -l")
	assert.Equal(t, nil, err)

	err= item.Rpush(cnx,"stdin","ps -ef")
	assert.Equal(t, nil, err)


	data,err := item.Lpop(cnx,"stdin")
	assert.Equal(t, nil, err)
	//fmt.Printf("%s\n",string(data))
	assert.Equal(t,"\"ls -l\"",string(data))

	data,err = item.Lpop(cnx,"stdin")
	assert.Equal(t, nil, err)
	//fmt.Printf("%s\n",string(data))
	assert.Equal(t,"\"ps -ef\"",string(data))

	data,err = item.Lpop(cnx,"stdin")
	assert.Equal(t,true,strings.Contains(err.Error(),"EOF"))


	redistools.UnsetPool()
}

func TestItemEntityFifo(t *testing.T) {

	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	cnx.Do("FLUSHDB")

	// create item model
	store.CreateItemEntitySchema(cnx)


	// create item1
	item,err := store.NewItemEntity(cnx,"item1","modelA",nil)
	item.Save(cnx)


	err= item.Rpush(cnx,"stdin","ls -l")
	assert.Equal(t, nil, err)

	err= item.Rpush(cnx,"stdin","ps -ef")
	assert.Equal(t, nil, err)


	lines ,err := item.Lrange(cnx,"stdin",0,1)
	assert.Equal(t, nil, err)
	assert.Equal(t,2,len(lines))

	lines ,err = item.Lrange(cnx,"stdin",0,3)
	assert.Equal(t, nil, err)
	assert.Equal(t,2,len(lines))

	lines ,err = item.Lrange(cnx,"stdin",0,-1)
	assert.Equal(t, nil, err)
	assert.Equal(t,2,len(lines))

	lines ,err = item.Lrange(cnx,"stdin",5,-1)
	assert.Equal(t, nil, err)
	assert.Equal(t,0,len(lines))


	data ,err := item.Lindex(cnx,"stdin",0)
	assert.Equal(t, nil, err)
	assert.Equal(t,"\"ls -l\"",string(data))


	data ,err = item.Lindex(cnx,"stdin",3)
	assert.Equal(t,true,strings.Contains(err.Error(),"IndexError"))


	data,err = item.Rpop(cnx,"stdin")
	assert.Equal(t, nil, err)
	//fmt.Printf("%s\n",string(data))
	assert.Equal(t,"\"ps -ef\"",string(data))


	err = item.DeleteList(cnx,"stdin")
	assert.Equal(t, nil, err)

	err = item.DeleteList(cnx,"stdin")
	assert.Equal(t, nil, err)

	//data,err = item.Rpop(cnx,"stdin")
	//assert.Equal(t, nil, err)
	////fmt.Printf("%s\n",string(data))
	//assert.Equal(t,"\"ls -l\"",string(data))


	data,err = item.Rpop(cnx,"stdin")
	assert.Equal(t,true,strings.Contains(err.Error(),"EOF"))


	redistools.UnsetPool()
}


func TestItemEntityMembers(t *testing.T) {

	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	cnx.Do("FLUSHDB")

	// create item model
	store.CreateItemEntitySchema(cnx)


	// create item1
	item1,err := store.NewItemEntity(cnx,"item1","modelA",nil)
	item1.Save(cnx)

	// create item2
	item2,err := store.NewItemEntity(cnx,"item2","modelA",nil)
	item2.Save(cnx)

	// load by oid


	members,err := item1.Schema().GetMembers(cnx)
	assert.Equal(t, nil, err)
	assert.Equal(t,2,len(members))


	id := item1.Oid

	b,err := store.ItemEntitySchema.IsMember(cnx,id)
	assert.Equal(t, nil, err)
	assert.Equal(t, true, b)


	b,err = store.ItemEntitySchema.IsMember(cnx,5)
	assert.Equal(t, nil, err)
	assert.Equal(t, false, b)


	redistools.UnsetPool()
}

