package store

import (
	"github.com/garyburd/redigo/redis"
	"fmt"
	"errors"
	"strconv"

)

/*

	# entity: an object category

	## entity base key
	entity:<entity_name>

	## schema
	entity:<entity_name>:schema  HASH -> index_name: index_specifier  eg name="pk" , mac="unique" , model="index"

	## members
	entity:<entity_name>  SET  a set of oid

	## indexes

	### for pk and unique index
	entity:<entity_name>:unique:<index_name> HASH  object_index: object_oid

	### for duplicate index
	entity:<entity_name>:index:<index_name>


	# oid: the basic storage

	## oid base key
	oid:<num> HASH  { _entity:Livebox , name:Livebox-demo , mac:"?" , model:"LB3"


 */


type entity interface {

	KeyEntity(entity_name string) string // return the base name of the key eg livebox -> entity:livebox
	KeySchema(entity_name string) string // return the key to access the entity schema -> entity:livebox:schema
	KeyMembers( entity_name string) string // return the key to access members -> entity:livebox:members
	//KeyIndex ( entity_name string) string  //  key to access indexes  entity:livebox:index

	Schema(entity_name string) EntitySchema

	// load an entity schema from db
	Load( entity_name string ) (EntitySchema,error)

	// add an object to entity
	Add( entity_name string, properties properties) error

	// delete an object from entity
	Del( entity_name string ) error

	// list members oid of entity
	List( entity_name string) ( oids []string, err error)

}





//
// Entity Candidate
//

type Entity struct {

	EntityName string
	Properties properties
	Oid oid

}

func NewEntity (entity_name string, properties properties) Entity {

	e := Entity{EntityName: entity_name,Properties: properties,Oid:0}
	return e

}


func ( e Entity) CheckUnique (cnx redis.Conn) (err error) {

	// load entity schema
	schema,err := LoadEntitySchema(cnx,e.EntityName)
	if err != nil {
		return err
	}
	// for each index in schema
	for _,idx := range schema.GetIndexes(cnx) {
		if value, ok := e.Properties[idx.IndexName]; ok {
			//  candidate has this index, check it is unique
			if idx.HasUniqueSpecifier() {
				h,err := idx.Handler()
				if err != nil { return err}
				exist,err := h.ReadAll(cnx,value)
				if len(exist) != 0 {
					// the index already exists : cancel
					err = errors.New(fmt.Sprintf("object already exists with this index:%s",idx.IndexName))
					return err
				}
			}
		} else {
			// a unique index is not present in candidate properties
			err = errors.New(fmt.Sprintf("missing mandatory property in entity:%s",idx.IndexName))
			return err
		}
		continue

	}
	return err
}

func ( e Entity) CreateIndexes (cnx redis.Conn, id oid) (err error) {

	// load entity schema
	schema,err := LoadEntitySchema(cnx,e.EntityName)
	if err != nil {
		return err
	}
	// for each index in schema
	for _,idx := range schema.GetIndexes(cnx) {
		if value, ok := e.Properties[idx.IndexName]; ok {
			h, err := idx.Handler()
			if err != nil { return err }
			err = h.Write(cnx,value,id)
			if err != nil { return err }
		}
	}
	return err
}

func ( e Entity) DeleteIndexes (cnx redis.Conn, id oid) (err error) {

	// load entity schema
	schema,err := LoadEntitySchema(cnx,e.EntityName)
	if err != nil {
		return err
	}
	// for each index in schema
	for _,idx := range schema.GetIndexes(cnx) {
		if value, ok := e.Properties[idx.IndexName]; ok {
			h, err := idx.Handler()
			if err != nil { return err }
			err = h.Delete(cnx,value,id)
			if err != nil { return err }
		}
	}
	return err
}

func ( e * Entity) Save (cnx redis.Conn) (id oid, err error) {


	if e.Oid == 0 {
		// the oid does not exists in database => create

		err = e.CheckUnique(cnx)
		if err != nil {
			return id, err
		}

		// get a new object id
		id, err = NewOid(cnx)
		if err != nil {
			return id, err
		}

		// create object
		e.Properties["_entity"] = e.EntityName
		e.Properties["_oid"] = strconv.Itoa(int(id))
		id.Write(cnx, e.Properties)

		// create indexes
		e.CreateIndexes(cnx, id)

		// add to members
		m := Members{e.EntityName}
		m.Add(cnx, id)

		// update entity oid
		e.Oid=id

	} else {
		// the oid already exists
		err = errors.New("cannot save, oid already exists and oid are immutable")
		return id , err
	}

	return id,err
}


//
// Entity Loaded
//


func ( e Entity) Delete (cnx redis.Conn) (err error) {

	if e.Oid == 0 {
		// oid does not exists in database
		err = errors.New("cannot delete, oid does not exists in database")
		return err
	}

	// delete indexes
	//id,err := OidFromString(e.Oid)
	//if err != nil {return err}
	err = e.DeleteIndexes(cnx,e.Oid)
	if err != nil {return err}

	// delete object
	err = e.Oid.Delete(cnx)

	// remove membership
	m := Members{e.EntityName}
	m.Remove(cnx,e.Oid)

	return err
}


func LoadEntity( cnx redis.Conn, id string)  (entity Entity,err error) {

	// load oid
	i,err := strconv.Atoi(id)
	if err != nil { return entity,err}

	x := oid(i )
	prop,err := x.Read(cnx)
	if err != nil {return entity,err}

	// check entity
	if entity_name,ok := prop["_entity"]; ok {
		// we have an entity
		entity= Entity{EntityName:entity_name,Properties:prop,Oid:x}
		return entity,nil
	} else {
		err = errors.New(fmt.Sprintf("this object has no _entity field"))
		return entity,err
	}
	return entity,err
}

func LoadEntityByIndex( cnx redis.Conn, entity_name string,index_name string,index_value string)  (entity Entity,err error) {

	// check schema
	schema,err := LoadEntitySchema(cnx,entity_name)
	if err != nil {return entity,err}

	// check index specifier is unique
	index,err := schema.GetIndex(cnx,index_name)
	if err != nil {return entity,err}
	if index.HasUniqueSpecifier() == false {
		// index is not unique
		err = errors.New("index specifier is not unique")
		return entity,err
	}

	// index entry exists : get oid
	h,err := index.Handler()
	if err != nil {return entity,err}

	idxs,err := h.ReadAll(cnx,index_value)
	if err != nil {return entity,err}
	id := idxs[0]

	_=id
	// fetch the object
	entity,err = LoadEntity(cnx,id.String())

	return entity,err
}

