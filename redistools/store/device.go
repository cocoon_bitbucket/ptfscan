package store

import (
	"github.com/garyburd/redigo/redis"
	//"errors"
	//"fmt"
)
/*

	device is an entity with
	 * a unique index "name"
	 * a unique index mac
	 * and a duplicate index "kind"

	entity:device:schema  HASH  { name:"named"  , mac= kind=}


	oid:1 HASH { _entity:"named" , name:"my_entity" , mac:  , other:"other"}
	entity:device:index:name HASH
	entity:device:index:mac HASH


 */

var (

	// device schema
	DeviceEntitySchema = EntitySchema{
		Name: "device",
		Indexes: map[string]string{
			"name": "unique",
			"mac": "unique",
			"kind": "duplicate",
		},
	}
)


func CreateDeviceEntitySchema(cnx redis.Conn) (err error){
	// create device schema in database
	schema := DeviceEntitySchema
	err = schema.Save(cnx)
	return err

}


type DeviceEntity struct {
	Entity
}

func (d  * DeviceEntity) Schema() EntitySchema {
	return DeviceEntitySchema
}


func ( d * DeviceEntity) AddHashTopic( cnx redis.Conn,name string, data  map[string]interface{}) (err error) {

	h := NewHash(cnx,d.Oid)
	err= h.Write(name,data)
	return err
}

func ( d * DeviceEntity) GetHashTopic( cnx redis.Conn,name string) ( content []byte, err error) {

	h := NewHash(cnx,d.Oid)
	content, err = h.Read(name)
	return content,err
}



func NewDeviceEntity( cnx redis.Conn, name string,mac string,kind string, properties properties) (entity DeviceEntity,err error) {

	//prop := make(map[string]string)
	p := make(map[string]string)
	for k,v := range properties{
		p[k]= v
	}
	p["name"] = name
	p["mac"] = mac
	p["kind"] = kind
	e := Entity{DeviceEntitySchema.Name, p, 0}
	entity = DeviceEntity{e}

	return entity,err
}


func LoadDevicebyName(cnx redis.Conn,name string)  (entity DeviceEntity,err error)  {

	e,err := LoadEntityByIndex(cnx, DeviceEntitySchema.Name,"name",name)
	if err != nil { return entity, err }

	entity.EntityName = e.EntityName
	entity.Properties = e.Properties
	entity.Oid = e.Oid

	return entity,err
}

func LoadDevicebyMacAddress(cnx redis.Conn,mac string)  (entity DeviceEntity,err error)  {

	e,err := LoadEntityByIndex(cnx, DeviceEntitySchema.Name,"mac",mac)
	if err != nil { return entity, err }

	entity.EntityName = e.EntityName
	entity.Properties = e.Properties
	entity.Oid = e.Oid

	return entity,err
}
