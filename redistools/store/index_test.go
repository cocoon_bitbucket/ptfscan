package store_test



import(

	"testing"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools/store"
	//"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	"github.com/stretchr/testify/assert"
	//"fmt"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	"strings"
)


func TestUniqueIndex(t *testing.T) {

	db,err := store.NewStore(redisUrl)
	assert.Equal(t,nil,err)
	_=db

	cnx := db.Conn

	//id := 3
	//idx := store.Index{"livebox","name","pk"}
	//h := idx.Handler()




	i := store.UniqueIndex{"livebox","name","unique"}

	err = i.Write(cnx,"Livebox-demo", 1 )
	assert.Equal(t,nil,err)

	err = i.Write(cnx,"Livebox-1", 2 )
	assert.Equal(t,nil,err)

	v,err := i.Read(cnx,"Livebox-demo")
	assert.Equal(t,nil,err)

	assert.Equal(t,1,int(v))


	err = i.Delete(cnx,"Livebox-demo", 1 )
	assert.Equal(t,nil,err)


	v,err = i.Read(cnx,"Livebox-demo")
	assert.Equal(t,"redigo: nil returned",err.Error())
	assert.Equal(t,0,int(v))

	redistools.UnsetPool()

}


func TestDuplicateIndex(t *testing.T) {

	db,err := store.NewStore(redisUrl)
	assert.Equal(t,nil,err)
	_=db

	cnx := db.Conn

	//id := 3

	i := store.DuplicateIndex{"livebox","model","index"}



	err = i.Write(cnx,"LBA", 2 )
	assert.Equal(t,nil,err)

	//b,err = i.Exists(cnx,"livebox","model","LBA" )
	//assert.Equal(t,nil,err)
	//assert.Equal(t,true,b)


	err = i.Write(cnx,"LBA", 3 )
	assert.Equal(t,nil,err)



	v,err := i.ReadAll(cnx,"LBA")
	assert.Equal(t,nil,err)
	assert.Equal(t,2,len(v))

	err = i.Delete(cnx,"LBA", 3 )
	assert.Equal(t,nil,err)

	v,err = i.ReadAll(cnx,"LBA")
	assert.Equal(t,nil,err)
	assert.Equal(t,1,len(v))


	err = i.Delete(cnx,"LBA", 2 )
	assert.Equal(t,nil,err)

	v,err = i.ReadAll(cnx,"LBA")
	assert.Equal(t,nil,err)
	assert.Equal(t,0,len(v))



	_=v

	redistools.UnsetPool()

}

func TestDuplicateIndexHandler(t *testing.T) {

	db,err := store.NewStore(redisUrl)
	assert.Equal(t,nil,err)
	_=db

	cnx := db.Conn

	//id := 3

	i := store.Index{"livebox","model","bad,rules"}
	_,err = i.Handler()
	assert.Equal(t,true,strings.Contains(err.Error(),"unknown rules:"))


	i = store.Index{"livebox","model","duplicate"}
	h,err := i.Handler()
	assert.Equal(t,nil,err)


	err = h.Write(cnx,"LBA", 2 )
	assert.Equal(t,nil,err)

	//b,err = i.Exists(cnx,"livebox","model","LBA" )
	//assert.Equal(t,nil,err)
	//assert.Equal(t,true,b)


	err = h.Write(cnx,"LBA", 3 )
	assert.Equal(t,nil,err)



	v,err := h.ReadAll(cnx,"LBA")
	assert.Equal(t,nil,err)
	assert.Equal(t,2,len(v))

	err = h.Delete(cnx,"LBA", 3 )
	assert.Equal(t,nil,err)

	v,err = h.ReadAll(cnx,"LBA")
	assert.Equal(t,nil,err)
	assert.Equal(t,1,len(v))


	err = h.Delete(cnx,"LBA", 2 )
	assert.Equal(t,nil,err)

	v,err = h.ReadAll(cnx,"LBA")
	assert.Equal(t,nil,err)
	assert.Equal(t,0,len(v))



	_=v

	redistools.UnsetPool()

}


func TestUniqueIndexHandler(t *testing.T) {

	db,err := store.NewStore(redisUrl)
	assert.Equal(t,nil,err)
	_=db

	cnx := db.Conn

	//id := 3
	//idx := store.Index{"livebox","name","pk"}
	//h := idx.Handler()

	idx := store.Index{"livebox","name","unique"}
	i,err := idx.Handler()
	assert.Equal(t,nil,err)


	err = i.Write(cnx,"Livebox-demo", 1 )
	assert.Equal(t,nil,err)

	err = i.Write(cnx,"Livebox-1", 2 )
	assert.Equal(t,nil,err)

	v,err := i.ReadAll(cnx,"Livebox-demo")
	assert.Equal(t,nil,err)

	assert.Equal(t,1,int(v[0]))


	err = i.Delete(cnx,"Livebox-demo", 1 )
	assert.Equal(t,nil,err)


	v,err = i.ReadAll(cnx,"Livebox-demo")
	assert.Equal(t,"redigo: nil returned",err.Error())
	assert.Equal(t,0,len(v))

	//assert.Equal(t,0,int(v[0]))

	redistools.UnsetPool()

}
