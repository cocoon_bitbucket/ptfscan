package store_test

import(

	"testing"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools/store"
	"github.com/stretchr/testify/assert"
	"encoding/json"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	"strings"
)



func TestTopicHash(t *testing.T) {

	db,err := store.NewStore(redisUrl)
	assert.Equal(t,nil,err)

	db.Do("FLUSHDB")

	o,_ := store.NewOid(db)

	h := store.NewHash(db,o)

	data := make(map[string]interface{})
	data["DeviceInfo.Sample"] = "sample"
	data["DeviceInfo.Enable"] = "1"

	err = h.Write("DeviceInfo",data)
	assert.Equal(t,nil,err)

	expected,err := json.Marshal(data)


	r,err := h.Read("DeviceInfo")
	assert.Equal(t,nil,err)

	assert.Equal(t,expected,r)

	err = h.Delete("DeviceInfo")
	assert.Equal(t,nil,err)

	_,err = h.Read("DeviceInfo")
	assert.Equal(t,true,strings.Contains(err.Error(),"Not found"))





	redistools.UnsetPool()

}


func TestTopicList(t *testing.T) {

	db,err := store.NewStore(redisUrl)
	assert.Equal(t,nil,err)

	db.Do("FLUSHDB")

	o,_ := store.NewOid(db)

	h := store.NewList(db,o)

	//data := make(map[string]interface{})
	//data["DeviceInfo.Sample"] = "sample"
	//data["DeviceInfo.Enable"] = "1"

	data:= "hello"

	err = h.Rpush("DeviceInfo",data)
	assert.Equal(t,nil,err)
	expected,err := json.Marshal(data)


	r,err := h.Lpop("DeviceInfo")
	assert.Equal(t,nil,err)

	assert.Equal(t,expected,r)

	redistools.UnsetPool()

}