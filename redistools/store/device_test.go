package store_test


import(

	"testing"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools/store"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	"github.com/stretchr/testify/assert"
	//"fmt"
	//"strings"
	//"fmt"
)

//var dataSet = map[string] store.DeviceEntity {
//
//	"Livebox-demo": {
//		Name: "device",
//		Properties: map[string]string{
//			"name": "Livebox-demo",
//
//}
//}
//}

var (

	Livebox_demo = store.Entity{

		EntityName: "device",
		Properties: map[string]string{
			"name": "Livebox-demo",
			"mac": "00:00:00",
			"kind": "livebox",
		},
		Oid: 0,
	}


	dataset = map[string]map[string]string{
		"demo": {
			"name": "Livebox-demo",
			"mac" : "00:00:00",
			"kind": "livebox",
		},
		"energenie": {
			"name": "Pdu-1",
			"mac" : "00:00:01",
			"kind": "energenie",
		},
	}

)




func TestDeviceEntity(t *testing.T) {

	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	cnx.Do("FLUSHDB")

	// create item model
	store.CreateDeviceEntitySchema(cnx)

	item,err := store.NewDeviceEntity(cnx,"Livebox-demo","00:00:00","livebox",nil)
	_,err = item.Save(cnx)
	assert.Equal(t, nil, err)

	o,err := store.LoadDevicebyName(cnx,"Livebox-demo")
	assert.Equal(t, nil, err)
	assert.Equal(t,"Livebox-demo",o.Properties["name"])
	assert.Equal(t,"00:00:00",o.Properties["mac"])
	assert.Equal(t,"livebox",o.Properties["kind"])

	o2,err := store.LoadDevicebyMacAddress(cnx,"00:00:00")
	assert.Equal(t, nil, err)
	assert.Equal(t,o,o2)

	o.Delete(cnx)

	_,err = store.LoadDevicebyName(cnx,"Livebox-demo")
	assert.Equal(t,"redigo: nil returned",err.Error())



	redistools.UnsetPool()

}



func TestDeviceEntity2(t *testing.T) {

	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	cnx.Do("FLUSHDB")

	// create item model
	store.CreateDeviceEntitySchema(cnx)

	// create dataset
	for _,v := range dataset{
		e,err := store.NewDeviceEntity(cnx,v["name"],v["mac"],v["kind"],nil)
		_,err = e.Save(cnx)
		assert.Equal(t, nil, err)

	}


	o,err := store.LoadDevicebyName(cnx,"Livebox-demo")
	assert.Equal(t, nil, err)
	assert.Equal(t,"Livebox-demo",o.Properties["name"])
	assert.Equal(t,"00:00:00",o.Properties["mac"])
	assert.Equal(t,"livebox",o.Properties["kind"])

	o2,err := store.LoadDevicebyMacAddress(cnx,"00:00:00")
	assert.Equal(t, nil, err)
	assert.Equal(t,o,o2)

	o.Delete(cnx)

	_,err = store.LoadDevicebyName(cnx,"Livebox-demo")
	assert.Equal(t,"redigo: nil returned",err.Error())



	redistools.UnsetPool()

}
