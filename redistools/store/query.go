package store


import(

	"github.com/garyburd/redigo/redis"

)

/*

	Query manager


	ussage :

	q := NewQuery( cnx , "alert )

	oids := q.All(index = "")








 */



type Query struct {

	redis.Conn
	EntityName string

	schema EntitySchema
}

func NewQuery( cnx redis.Conn, entity_name string ) (query * Query , err error){

	// check entitySchema exists
	schema,err := LoadEntitySchema(cnx,entity_name)
	if err != nil { return query,err}

	query = &Query{cnx,entity_name,schema}

	return query, err
}


func ( q * Query )All( index string) (oids []oid , err error){

	if index == "" {
		members := &Members{q.EntityName}
		return members.GetMembers(q.Conn)

	} else {
		panic("redistools query with index not yet implemented ")
	}
	return oids,err
}