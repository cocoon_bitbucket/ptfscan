package store

import (

	"github.com/garyburd/redigo/redis"
	"fmt"
	"strconv"
	"errors"
	"strings"
)

type index interface {

	//Exists(cnx redis.Conn,entity_name,index_name string) (bool,error)
	Write(cnx redis.Conn,index_key string,oid oid) (err error)
	//Read(cnx redis.Conn,index_key string) (id oid,err error)
	Delete(cnx redis.Conn,index_key string,oid oid) (err error)
	ReadAll(cnx redis.Conn,index_key string) (ids []oid,err error)
}


type Index struct {

	EntityName string    		//  eg livebox
	IndexName string     		//  eg name
	Rules     string            //  unique,many
}

func ( i Index) Handler( ) (handler index,err error) {

	if strings.Contains(i.Rules,"unique") {
		handler = UniqueIndex{i.EntityName,i.IndexName,i.Rules}
		return handler,err
	}
	if strings.Contains(i.Rules,"duplicate") {
		handler = DuplicateIndex{i.EntityName,i.IndexName,i.Rules}
		return handler,err
	}
	err = errors.New(fmt.Sprintf("unknown rules: %s",i.Rules))
	return handler,err

}

func ( i Index) HasUniqueSpecifier() bool {
	if strings.Contains(i.Rules,"unique") {
		return true
	} else {
		return false
	}
}





type UniqueIndex struct {

	EntityName string    		//  eg livebox
	IndexName string     		//  eg name
	Rules     string            //  unique,index
}

func ( i UniqueIndex) Write(cnx redis.Conn,index_key string,oid oid) (err error){
	// store oid at  entity:<entity_name>:index:<index_name> HASH index_value:oid
	// eg entity:livebox:index:name  HASH Livebox-demo:oid
	key := fmt.Sprintf("entity:%s:index:%s",i.EntityName,i.IndexName)
	_,err = cnx.Do("HSET",key,index_key,oid)
	return err
}

func ( i UniqueIndex) Read(cnx redis.Conn,index_key string) (id oid,err error){
	// store oid at  entity:<entity_name>:index:<index_name> HASH index_value:oid
	// eg entity:livebox:index:name  HASH Livebox-demo:oid
	key := fmt.Sprintf("entity:%s:index:%s",i.EntityName,i.IndexName)
	v,err := redis.String(cnx.Do("HGET",key,index_key))
	if err !=nil {
		return 0,err
	}
	n,err := strconv.Atoi(v)
	if err != nil {
		return 0,err
	}
	id= oid(n)
	return id,err
}

func ( i UniqueIndex) ReadAll(cnx redis.Conn,index_key string) (ids []oid,err error){
	id,err := i.Read(cnx,index_key)
	if err !=nil { return ids,err}
	ids = append(ids,id)
	return ids,err
}


func ( i UniqueIndex) Delete(cnx redis.Conn,index_key string,oid oid) (err error){
	// store oid at  entity:<entity_name>:index:<index_name> HASH index_value:oid
	// eg entity:livebox:index:name  HASH Livebox-demo:oid
	key := fmt.Sprintf("entity:%s:index:%s",i.EntityName,i.IndexName)
	_,err = cnx.Do("HDEL",key,index_key,oid)
	return err
}


type DuplicateIndex struct {

	EntityName string    		//  eg livebox
	IndexName string     		//  eg name
	Rules     string            //  unique,index

}

func ( i DuplicateIndex) Write(cnx redis.Conn,index_key string,oid oid) (err error){
	// store oid in a set at  entity:<entity_name>:index:<index_name>:index_key SET add oid
	// eg entity:livebox:index:model:LB0  SET add 3
	key := fmt.Sprintf("entity:%s:index:%s:%s",i.EntityName,i.IndexName,string(index_key))
	// add key to the set
	_,err = cnx.Do("SADD",key,oid)
	return err
}

func ( i DuplicateIndex) ReadAll(cnx redis.Conn,index_key string) (ids []oid,err error){
	// store oid at  entity:<entity_name>:index:<index_name> HASH index_value:oid
	// eg entity:livebox:index:name  HASH Livebox-demo:oid
	key := fmt.Sprintf("entity:%s:index:%s:%s",i.EntityName,i.IndexName,string(index_key))
	v,err := redis.Values(cnx.Do("SMEMBERS",key))
	if err !=nil {
		return ids,err
	}
	_=v
	for _,m := range v {
		id,err := strconv.Atoi(string(m.([]uint8)))
		if err != nil {
			return ids,err
		}
		ids = append(ids,oid(id))
	}
	return ids,err
}

func ( i DuplicateIndex) Delete(cnx redis.Conn,index_key string,oid oid) (err error){
	// store oid at  entity:<entity_name>:index:<index_name> HASH index_value:oid
	// eg entity:livebox:index:name  HASH Livebox-demo:oid
	key := fmt.Sprintf("entity:%s:index:%s:%s",i.EntityName,i.IndexName,index_key)
	_,err = cnx.Do("SREM",key,oid)
	return err
}

//func ( i* DuplicateIndex) Exists(cnx redis.Conn,entity_name string,index_name string,index_key string) ( bool, error){
//	// store oid at  entity:<entity_name>:index:<index_name> HASH index_value:oid
//	// eg entity:livebox:index:name  HASH Livebox-demo:oid
//	key := fmt.Sprintf("entity:%s:index:%s:%s",entity_name,index_name,string(index_key))
//	v,err := redis.Bool(cnx.Do("SISMEMBER",key))
//	if err !=nil {
//		return false,err
//	}
//	return v,err
//}

