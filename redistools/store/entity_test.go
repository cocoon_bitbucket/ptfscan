package store_test


import(

	"testing"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools/store"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	"github.com/stretchr/testify/assert"
	//"fmt"
	"strings"
	"fmt"
)


func TestEntitySchema(t *testing.T) {

	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	cnx.Do("FLUSHDB")

	prop := make(map[string]string)
	//prop["_entity"] = "livebox"
	prop["name"] = "unique"
	prop["mac"] = "unique"
	prop["model"] = "duplicate"

	schema := store.EntitySchema{"livebox",prop}
	err = schema.Save(cnx)
	assert.Equal(t,nil,err)


	e2,err := store.LoadEntitySchema(cnx,"livebox")
	assert.Equal(t,nil,err)

	assert.Equal(t,"unique",e2.Indexes["name"])

	redistools.UnsetPool()
}


func TestEntityCandidate(t *testing.T) {

	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	cnx.Do("FLUSHDB")

	// create item entity  schema { name: , model: }
	prop := make(map[string]string)
	prop["name"] = "unique"
	prop["model"] = "duplicate"
	schema := store.EntitySchema{"item",prop}
	err = schema.Save(cnx)
	assert.Equal(t,nil,err)




	// check item with bad entity name
	prop = make(map[string]string)
	prop["model"] = "modelA"
	item := store.Entity{"bad_item", prop, 0}
	err = item.CheckUnique(cnx)
	assert.Equal(t,"no such schema",err.Error())


	// check item without name
	prop = make(map[string]string)
	prop["model"] = "modelA"
	item = store.Entity{"item", prop, 0}
	err = item.CheckUnique(cnx)
	assert.Equal(t,true,strings.Contains(err.Error(),"missing mandatory property"))

	// create a first item
	prop = make(map[string]string)
	prop["name"] = "item1"
	prop["model"] = "modelA"
	item1 := store.Entity{"item", prop, 0}
	err = item1.CheckUnique(cnx)
	assert.Equal(t,nil,err)

	// test we cannot delete an oid not present in database
	err = item1.Delete(cnx)
	assert.Equal(t,true,strings.Contains(err.Error(),"cannot delete"))


	id,err := item1.Save(cnx)
	assert.Equal(t,nil,err)
	fmt.Printf("id=%d\n",id)



	//e2,err := store.LoadEntitySchema(cnx,"item")
	//assert.Equal(t,nil,err)
	//
	//assert.Equal(t,"unique",e2.Indexes["name"])


	redistools.UnsetPool()
}


func TestEntityLoaded(t *testing.T) {

	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	cnx.Do("FLUSHDB")


	// create item entity schema  { name: , model: }
	prop := make(map[string]string)
	prop["name"] = "unique"
	prop["model"] = "duplicate"
	schema := store.EntitySchema{"item", prop}
	err = schema.Save(cnx)
	assert.Equal(t, nil, err)

	// create a first item
	prop = make(map[string]string)
	prop["name"] = "item1"
	prop["model"] = "modelA"
	item1 := store.Entity{"item", prop, 0}
	id,err := item1.Save(cnx)
	assert.Equal(t,nil,err)
	fmt.Printf("id=%d\n",id)

	e,err := store.LoadEntityByIndex(cnx,"item","name","item1")
	assert.Equal(t,nil,err)

	// test immutabilty cannot save again an existing oid
	_,err = e.Save(cnx)
	assert.Equal(t,true,strings.Contains(err.Error(),"cannot save"))





	m:= store.Members{"item"}
	b,err := m.IsMember(cnx,id)
	assert.Equal(t,nil,err)
	assert.Equal(t,true,b)

	err = e.Delete(cnx)
	assert.Equal(t,nil,err)


	b,err= m.IsMember(cnx,id)
	assert.Equal(t,nil,err)
	assert.Equal(t,false,b)


	redistools.UnsetPool()
}
