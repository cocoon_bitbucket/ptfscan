package store

import (
	"github.com/garyburd/redigo/redis"
	"fmt"
	"errors"
	//"strconv"

)


/*
	schema

	# entity: an object category

	## entity base key
	entity:<entity_name>

	## schema
	entity:<entity_name>:schema  HASH -> index_name: index_specifier  eg name="pk" , mac="unique" , model="duplicate"

	## members
	entity:<entity_name>  SET  a set of oid

	## indexes

	### for pk and unique index
	entity:<entity_name>:unique:<index_name> HASH  object_index: object_oid

	### for duplicate index
	entity:<entity_name>:index:<index_name>




 */


func CreateEntitySchema(cnx redis.Conn,name string, properties properties ) (err error){
	//  entity:<name>:schema
	schema := EntitySchema{name,properties}
	err = schema.Save(cnx)
	return err
}




//
type EntitySchema struct {
	Name string              // eg livebox
	Indexes map[string]string   // eg { name:"pk" , mac:"unique", model:"index"
}

func KeyEntitySchema(entity_name string) string {
	return fmt.Sprintf("entity:%s:schema",entity_name)
}


func ( e EntitySchema) Save ( cnx redis.Conn) (err error) {
	// save a schema
	for k,v := range e.Indexes {
		_,err = cnx.Do("HSET", KeyEntitySchema(e.Name), k,v)
		if err != nil {
			return err
		}
	}
	return err
}

func ( e EntitySchema) GetIndexes ( cnx redis.Conn) ( indexes []Index) {
	// return a list of Index
	for index_name,specifier := range e.Indexes {
		idx := Index{e.Name,index_name,specifier}
		indexes = append(indexes,idx)
	}
	return indexes
}

func ( e EntitySchema) GetIndex ( cnx redis.Conn, name string ) ( index Index, err error) {
	// return a list of Index object
	if value,ok := e.Indexes[name];ok {
		index = Index{e.Name,name,value}
		return index,err
	} else {
		// no such index
		err = errors.New(fmt.Sprintf("no such index: %s",name))
		return index,err
	}
}

func ( e EntitySchema) IsMember ( cnx redis.Conn, id oid ) ( bool, error) {
	// tell if an oid is member of the entity
	m := Members{EntityName:e.Name}
	return m.IsMember(cnx,id)
}

func ( e EntitySchema) GetMembers ( cnx redis.Conn) ([]oid, error) {
	// tell if an oid is member of the entity
	m := Members{EntityName:e.Name}
	return m.GetMembers(cnx)
}



func LoadEntitySchema ( cnx redis.Conn, entity_name string ) (schema EntitySchema, err error) {
	key := KeyEntitySchema(entity_name)
	v,err := redis.Values(cnx.Do("HGETALL",key))
	if err != nil { return schema,err}
	m,err := stringMapOrNil(v,err)
	if err != nil { return schema,err}
	if len(m) == 0 {
		// no schema for this entity
		return schema, errors.New("no such schema")
	}
	schema = EntitySchema{ entity_name,m}
	return schema,err
}

