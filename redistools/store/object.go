package store


import(

	"github.com/garyburd/redigo/redis"
	"fmt"
	"strconv"
)

/*
	an object is stored on a oid key  oid:<num>
	the content is a HASH  ( containing values of indexed properties ( eg name ))

	a new oid value is generated in incrementing a redis counter at oid:-

	eg

	oid:1   { _entity:livebox , name:"Livebox-demo" }

	topics are named JSON contents attached to an oid

	eg
	oid:1:topic:DeviceInfo  JSON: { ..... }

 */

var (

	//KeyOid string     = "oid"
	KeyCounter string = "oid:-"

	ErrorMessages = map[string]string{
		"NO-OID":"entity has no oid, save it first"}
)

type oid int64
type properties map[string]string

func ( oid oid ) Key () string {
	// return the key of the objecy eg oid:1
	return fmt.Sprintf("oid:%d",oid)
}

func ( oid oid ) KeyTopic (topic string) string {
	// return the topic key of an oid eg oid:1:topic:DeviceInfo
	return fmt.Sprintf("oid:%d:topic:%s",oid,topic)
}


func (oid oid) Write ( cnx redis.Conn, properties properties ) (err error) {
	// write the properties at oid:? HASH -> properties

	for k,v := range properties{
		_,err = cnx.Do("HSET",oid.Key(), k,v)
		if err != nil {
			return err
		}
	}
	return err
}

func (oid oid) Read ( cnx redis.Conn)  (properties properties, err error){
	// Read the properties at oid:? HASH -> properties

	properties = make(map[string]string)
	r,err := redis.Values(cnx.Do("HGETALL",oid.Key()))
	if err != nil {
		return properties,err
	}
	return stringMapOrNil(r,err)

}

func (oid oid) Delete ( cnx redis.Conn)  (err error){
	// Delete the key at oid:? HASH -> properties
	_,err = cnx.Do("Del",oid.Key())
	return err
}

func (oid oid) String () ( sid string) {
	//
	return strconv.Itoa(int(oid))
}



func NewOid (cnx redis.Conn) (oid,error) {
	// increment oid global counter and return it
	id,err := redis.Int64(cnx.Do("INCR",KeyCounter))
	if err != nil {
		return oid(0),err
	}
	return oid(id),err
}

func OidFromString (sid string) ( id oid,err error) {
	//
	i,err := strconv.Atoi(sid)
	if err != nil {return id,err}
	return oid(i),err
}


func stringMapOrNil(v interface{}, err error) (map[string]string, error) {
	if values, ok := v.([]interface{}); ok && len(values) == 0 {
		return nil, nil
	}
	return redis.StringMap(v, err)
}

//
// implements Datastore interface
//

func (db * StoreDb) Load( oid oid ) ( properties, error) {
	return oid.Read(db)
}
func (db * StoreDb) Store( oid oid , properties properties) ( error) {
	return oid.Write(db,properties)
}

func (db * StoreDb) Delete( oid oid) ( error) {
	return oid.Delete(db)
}




