package redistools

import (

	//"log"
	"time"
	"errors"
	"github.com/garyburd/redigo/redis"


)
/*

	declare a Pool singleton


	usage:

	NewPool( redis_url)
	defer UnsetPool()

	p := GetPool()
	err := p.Ping()

	cnx := p.Get()
	defer cnx.Close()


 */

var (

	// a singleton representing a redis pool_singleton
	pool_singleton *Pool
)


type Pool struct {
	redis.Pool
	Url string
}


func ( p * Pool) Ping() error {
	// get a connection from pool and try to ping db
	cnx := p.Get()
	defer cnx.Close()
	_,err := redis.String(cnx.Do("PING"))
	return err
}


// create a new pool_singleton singleton with default values
func NewPool( redis_url string) (*Pool,error) {
	// create a redis pool and set pool singleton
	if pool_singleton != nil {
		return nil, errors.New("pool_singleton already instanciated")
	}
	rp := RedisPool(redis_url)
	pool_singleton = &Pool{*rp,redis_url}
	return pool_singleton,nil
}

func GetPool() (*Pool,error) {

	if pool_singleton == nil {
		return nil, errors.New("No Pool has been instanciated with NewPool(redis_url)")
	}
	return pool_singleton,nil

}

func SetPool( redisPool * redis.Pool, redisUrl string ) (*Pool ,error){

	if pool_singleton != nil {
		return nil,errors.New("pool_singleton already instanciated")
	}

	pool_singleton = &Pool{*redisPool,redisUrl}

	return pool_singleton,nil
}

func UnsetPool() error {

	pool_singleton = nil

	return nil
}

func GetNewconn() (cnx redis.Conn, err error) {
	// get new connction from pool
	pool,err := GetPool()
	if err == nil {
		cnx = pool.Get()
	}
	return cnx,err
}





// redis functions

func RedisPool(redisUrl string) *redis.Pool {
	// create a redis pool_singleton for a given redis url (eg redis://localhost:6379/0 )
	return &redis.Pool{
		MaxIdle: 3,
		IdleTimeout: 240 * time.Second,
		Dial: func () (redis.Conn, error) { return redis.DialURL(redisUrl) },

		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}


}


