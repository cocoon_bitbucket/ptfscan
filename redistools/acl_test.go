package redistools_test


import(

	"testing"
	"github.com/garyburd/redigo/redis"

	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	"github.com/stretchr/testify/assert"
	"log"
	"time"
	"fmt"
	"strings"
)


/*

	pre-requisites: REDIS DB

 */


func get_db() redis.Conn {

	var redis_url= "redis://localhost:6379/0"

	cnx, err := redis.DialURL(redis_url)

	if err != nil {
		log.Fatal("cannot connect to redis")
	}

	return cnx

}




func TestNewToken(t *testing.T) {


	token := redistools.NewToken()
	assert.Equal(t,16,len(token))
	//print(token)
}


func TestGetAclProvider(t *testing.T) {


	cnx := get_db()

	realm := "Livebox"
	ttl := 5


	// create and set an acl
	acl := redistools.NewAclProvider(cnx, realm,"")
	_,err := acl.Set(ttl,"Livebox-1000","Livebox-1001")
	assert.Equal(t,nil,err)


	// check ttl
	realTtl,err := acl.Ttl()
	assert.Equal(t,nil,err)
	if realTtl > 4 && realTtl <=5 {
		print("ok")
	}



	// get members of acl
	members,err := acl.Get()
	assert.Equal(t,2,len(members))
	//print(members)


	// assert Livebox-1000 is member
	is_member,err := acl.Assert("Livebox-1000")
	assert.Equal(t,nil,err)
	assert.Equal(t,true,is_member)

	// assert Livebox-9999 is not member
	is_member2,err := acl.Assert("Livebox-9999")
	assert.Equal(t,nil,err)
	assert.Equal(t,false,is_member2)

	// wait for the ttl
	time.Sleep( time.Duration(ttl) * time.Second)


	// check acl does not exists anymore
	members,err = acl.Get()
	assert.Equal(t,0,len(members))
	//print(members)

	// check not member
	is_member,err = acl.Assert("Livebox-1000")
	assert.Equal(t,nil,err)
	assert.Equal(t,false,is_member)

	//print(token)

}


func TestGetAclProviderRevoke(t *testing.T) {


	cnx := get_db()

	realm := "Livebox"
	ttl := 10


	// create and set an acl
	acl := redistools.NewAclProvider(cnx, realm,"")
	token,err := acl.Set(ttl,"Livebox-1000","Livebox-1001")
	assert.Equal(t,nil,err)


	// load an acl
	acl= redistools.NewAclProvider(cnx, realm,token)

	// get members of acl
	members,err := acl.Get()
	assert.Equal(t,2,len(members))
	//print(members)


	// assert Livebox-1000 is member
	is_member,err := acl.Assert("Livebox-1000")
	assert.Equal(t,nil,err)
	assert.Equal(t,true,is_member)

	// assert Livebox-9999 is not member
	is_member2,err := acl.Assert("Livebox-9999")
	assert.Equal(t,nil,err)
	assert.Equal(t,false,is_member2)

	// wait for the ttl
	acl.Revoke()


	// check acl does not exists anymore
	members,err = acl.Get()
	assert.Equal(t,0,len(members))
	//print(members)

	// check not member
	is_member,err = acl.Assert("Livebox-1000")
	assert.Equal(t,nil,err)
	assert.Equal(t,false,is_member)

	//print(token)

}



func TestAclManager(t *testing.T) {

	cnx := get_db()
	defer cnx.Close()

	realm := "Livebox"
	ttl := 2


	// create and set an acl
	manager := redistools.NewAclManager(cnx)

	// clear
	manager.RevokeRealm(realm)



	// create some tokens

	acl := redistools.NewAclProvider(cnx, realm,"")
	acl.Set(ttl,"Livebox-1000","Livebox-1001")

	acl= redistools.NewAclProvider(cnx, realm,"")
	acl.Set(ttl,"Livebox-1002","Livebox-1003")



	keys,err := manager.AclKeysOfRealm("Livebox")
	if err != nil {
		log.Fatal(err)
	}
	assert.Equal(t,2,len(keys))
	fmt.Printf("%s\n",keys)


	manager.RevokeRealm(realm)
	keys,_ = manager.AclKeysOfRealm("Livebox")
	assert.Equal(t,0,len(keys))


	//time.Sleep(2 * time.Second)

}


func TestAclManagerMembers(t *testing.T) {

	cnx := get_db()
	defer cnx.Close()

	realm := "Livebox"
	ttl := 2

	// create and set an acl
	manager := redistools.NewAclManager(cnx)

	// clear
	manager.RevokeRealm(realm)

	// create some tokens

	acl := redistools.NewAclProvider(cnx, realm, "")
	acl.Set(ttl, "Livebox-1000", "Livebox-1001")

	acl = redistools.NewAclProvider(cnx, realm, "")
	acl.Set(ttl, "Livebox-1002", "Livebox-1003")


	reserved,_ := manager.MembersOfRealm(realm)
	assert.Equal(t,4,len( reserved))

	acls,_ := manager.AclOfRealm(realm)
	assert.Equal(t,2,len( acls))


	return
}


func TestAclManagerAddAcl(t *testing.T) {

	cnx := get_db()
	defer cnx.Close()

	realm := "device"
	ttl := 3600

	// create and set an acl
	manager := redistools.NewAclManager(cnx)

	// clear
	manager.RevokeRealm(realm)

	// create some a first acl
	token1,err := manager.AddAcl(realm,ttl,"Livebox-1000", "Livebox-1001")
	assert.Equal(t,nil,err)

	// create some a second acl ok acl
	_,err = manager.AddAcl(realm,ttl,"Livebox-1002", "Livebox-1003")
	assert.Equal(t,nil,err)

	// create some a third acl with conflict
	_,err = manager.AddAcl(realm,ttl,"Livebox-1004", "Livebox-1000")
	assert.Equal(t,true, strings.Contains(err.Error(),"Cancel"))

	reserved,_ := manager.MembersOfRealm(realm)
	assert.Equal(t,4,len(reserved))


	err = manager.RevokeAcl(realm,token1)
	assert.Equal(t,nil,err)

	reserved,_ = manager.MembersOfRealm(realm)
	assert.Equal(t,2,len(reserved))


	// clear
	manager.RevokeRealm(realm)


}


func TestRevokeRealm(t *testing.T) {


	cnx := get_db()
	defer cnx.Close()

	realm := "device"
	ttl := 3600

	// create and set an acl
	manager := redistools.NewAclManager(cnx)

	// clear
	manager.RevokeRealm(realm)

	// create some a first acl
	_,err := manager.AddAcl(realm,ttl,"Livebox-1000", "Livebox-1001")
	assert.Equal(t,nil,err)

	// create some a second acl ok acl
	_,err = manager.AddAcl(realm,ttl,"Livebox-1002", "Livebox-1003")
	assert.Equal(t,nil,err)

	// check our 4 members
	members,err := manager.MembersOfRealm(realm)
	assert.Equal(t,4,len(members))


	// revoke Realm
	err = manager.RevokeRealm(realm)

	// check realm empty
	members,err = manager.MembersOfRealm(realm)
	assert.Equal(t,0,len(members))


}


func TestAclWithLocks(t *testing.T) {


	var redis_url= "redis://localhost:6379/0"
	pool,err := redistools.NewPool(redis_url)


	cnx := pool.Get()
	defer cnx.Close()

	realm := "device"
	ttl := 3600

	// create and set an acl
	manager,err := redistools.NewAclManagerFromPool(*pool)

	// clear
	manager.RevokeRealm(realm)

	// create some a first acl
	_,err = manager.AddAcl(realm,ttl,"Livebox-1000", "Livebox-1001")
	assert.Equal(t,nil,err)

	// create some a second acl ok acl
	_,err = manager.AddAcl(realm,ttl,"Livebox-1002", "Livebox-1003")
	assert.Equal(t,nil,err)

	// check our 4 members
	members,err := manager.MembersOfRealm(realm)
	assert.Equal(t,4,len(members))


	// revoke Realm
	err = manager.RevokeRealm(realm)

	// check realm empty
	members,err = manager.MembersOfRealm(realm)
	assert.Equal(t,0,len(members))


}


