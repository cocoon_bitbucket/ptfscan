package redistools

/*

	implements hexastore with redis

	Representing and querying graphs using an hexastore
	see: https://redis.io/topics/indexes

	store a subject predicate object
	antirez is-friend-of matteocollina

	ZADD myindex 0 spo:antirez:is-friend-of:matteocollina
	ZADD myindex 0 sop:antirez:matteocollina:is-friend-of
	ZADD myindex 0 ops:matteocollina:is-friend-of:antirez
	ZADD myindex 0 osp:matteocollina:antirez:is-friend-of
	ZADD myindex 0 pso:is-friend-of:antirez:matteocollina
	ZADD myindex 0 pos:is-friend-of:matteocollina:antirez

	query :

	ZRANGEBYLEX myindex "[spo:antirez:is-friend-of:" "[spo:antirez:is-friend-of:\xff"
	1) "spo:antirez:is-friend-of:matteocollina"
	2) "spo:antirez:is-friend-of:wonderwoman"
	3) "spo:antirez:is-friend-of:spiderman"


 */


import (

	"github.com/garyburd/redigo/redis"
	//"fmt"
	"strings"
	"log"
)

var (

	HexastoreIndex = "hex"
)


type Rdf struct {
	S string   // subject
	P string   // predicate
	O string   // object
}

func ( rdf * Rdf) Encode (motif string) (string,error) {
	// return an encoded string like  spo:subject,predicate,object
	// motif is one of spo/sop ...
	var err error=nil
	encoded  := motif
	for i := 0 ; i < 3; i++ {
		switch motif[i] {
		case 's': encoded += ":" + rdf.S
		case 'p': encoded += ":" + rdf.P
		case 'o': encoded += ":" + rdf.O
		}
	}
	return encoded,err
}

func ( rdf * Rdf) Decode (motif string)  error {

	// decode string like spo:subject:predicate:object to a RDF
	var err error = nil
	elements := strings.Split(motif,":")

	kind := elements[0]
	for index,element := range elements[1:] {

		switch kind[index] {
		case 's':rdf.S = element
		case 'p': rdf.P = element
		case 'o':rdf.O = element
		}
	}
	return err
}



func ( rdf * Rdf) QueryBoundaries (motif string) (string,string) {
	// return the start query  [spo:antirez:is-friend-of:
	// motif : one of spo/sop ....

	start := "[" + motif + ":"
	stop := start

	for index,element := range motif[0:2] {
		switch element {
		case 's': start += rdf.S +  ":"
		case 'p': start += rdf.P +  ":"
		case 'o': start += rdf.O +  ":"
		}
		if index >2 {
			break
		}
	}
	stop = start + "\xff"
	return start,stop
}




type Hexastore struct {

	redis.Conn


}

func NewHexastore() (q * Hexastore,err error){

	// get a new conn from pool
	cnx,err := GetNewconn()
	if err == nil {
		q = &Hexastore{cnx}
	}
	return q,err
}


func (c *Hexastore) HexaStore( rdf Rdf ) error {

	// store an rdf as an hexastore

	var err error

	motifs := []string{"spo","sop","ops","osp","pso","pos"}

	c.Send("MULTI")
	for _,motif := range motifs {
		encoded,err :=  rdf.Encode(motif)
		if err == nil {
			c.Send("ZADD", HexastoreIndex, 0, encoded)
		} else {
			log.Fatal("hexastore encoding error: %s" , err.Error())
		}
	}
	r, err := c.Do("EXEC")
	_= r
	//fmt.Println(r) // prints [1, 1]

	return err

}

func (c *Hexastore) HexaQuery( kind string, rdf Rdf ) ([]Rdf ,error) {

	// query an hexastore
	// kind is spo/sop/ops ...

	// ZRANGEBYLEX myindex "[spo:antirez:is-friend-of:" "[spo:antirez:is-friend-of:\xff"

	var response []Rdf

	start , stop := rdf.QueryBoundaries("spo")


	reply,err := redis.Values(c.Do("ZRANGEBYLEX", HexastoreIndex, start , stop))

	if err == nil {
		for _, el := range reply {
			//fmt.Printf("%s\n",el)
			rdf := Rdf{"", "", ""}
			rdf.Decode(string(el.([]uint8)))
			response = append(response, rdf)
		}
	}

	return response,err
}