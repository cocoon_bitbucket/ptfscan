package redistools_test

import (

	"testing"
	"github.com/stretchr/testify/assert"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"


	"fmt"
)


func TestRedisLock(t *testing.T) {


	pool,err := redistools.NewPool(redistools.Redis_DefaultUrl)
	assert.Equal(t,nil,err)
	defer redistools.UnsetPool()

	pool,_ = redistools.GetPool()
	lock ,err := redistools.NewLock(pool)

	acquired,err := lock.Acquire("lock:sample","secret",10000)
	if err != nil {
		fmt.Printf(err.Error())
	}
	assert.Equal(t,true,acquired)
	fmt.Print(acquired)

	lock.Release("lock:sample","secret")

}


func TestRedisLockRelease(t *testing.T) {

	// create pool
	pool,err := redistools.NewPool(redistools.Redis_DefaultUrl)
	assert.Equal(t,nil,err)
	defer redistools.UnsetPool()

	// create Lock
	pool,_ = redistools.GetPool()
	lock ,err := redistools.NewLock(pool)

	// Acquire a lock
	acquired,err := lock.Acquire("lock:sample","secret",5000)
	if err != nil {
		fmt.Printf(err.Error())
	}
	assert.Equal(t,true,acquired)
	fmt.Print(acquired)


	// Release a lock$
	err = lock.Release("lock:sample","secret")
	if err != nil {
		fmt.Printf(err.Error())
	}
	assert.Equal(t,nil,err)


	// acquire again
	acquired,err = lock.Acquire("lock:sample","secret",5000)
	if err != nil {
		fmt.Printf(err.Error())
	}
	assert.Equal(t,true,acquired)
	fmt.Print(acquired)








}
