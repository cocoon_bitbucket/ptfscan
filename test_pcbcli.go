package main



import (

	"os"
	"log"
	"fmt"
	"time"
	"runtime"
	"bitbucket.org/cocoon_bitbucket/ptfscan/portScanner"
	"bitbucket.org/cocoon_bitbucket/ptfscan/portScanner/inspectors"
	"bitbucket.org/cocoon_bitbucket/ptfscan/publishers"
	"github.com/gammazero/workerpool"
	"net"

)


var (

	cidr = "192.168.1.1/24"

	addr = "192.168.1.1:23"
	user = "root"
	passwd = "sah"

)


func simple() {

	pcb,err := inspectors.NewPcbCli(addr,user,passwd)
	defer pcb.Close()

	if err != nil {
		log.Fatal("cannot connect to device: ",err)
	}

	_= pcb


	data,err := pcb.SendRequest("DeviceInfo")
	fmt.Printf("data: %v",data)


}


func test_ls (){

	pcb,err := inspectors.NewPcbCli(addr,user,passwd)
	defer pcb.Close()

	if err != nil {
		log.Fatal("cannot connect to device: ",err)
	}

	err = pcb.SendLn("ls -l /tmp")
	if err == nil {
		response, _ := pcb.ReadLn()
		os.Stdout.Write(response)
		os.Stdout.WriteString("\n")
	}

	if err != nil {
		log.Fatal("fail ",err)
	}

}




func pcbli_request(   addr net.TCPAddr, request, user, passwd string) {
	//
	//

	fmt.Printf("pcbcli request to : %v\n", addr.String())
	pcb, err := inspectors.NewPcbCli(addr.String(), user, passwd)
	defer pcb.Close()
	if err == nil {
		data, _ := pcb.SendRequest(request)
		fmt.Printf("data: %v\n", data)
	}
}

func pcbliRequestTask(   addr net.TCPAddr, request, user, passwd string) func() {
	// a curry for pcbli_request
	wrapper := func() {
		pcbli_request(addr, request, user, passwd)
	}
	return wrapper
}


func request_cidr () {

	// scan all telnet port in cidr
	fmt.Printf("pcbcli request on  all port 23 from 192.168.1.0/24\n")
	telnet_ports :=  []int {23}


	runtime.GOMAXPROCS(runtime.NumCPU())
	nbWorkers := runtime.NumCPU() * 3
	wp := workerpool.New(nbWorkers)
	_ = wp

	// create a generator of ip from cidr
	ipchan := portscanner.IpRange( cidr )

	// create a generator of net.TcpAddr for open port 23 on each ip
	addrchan := portscanner.FastOpenedTcpAdress(ipchan,telnet_ports,0,90)
	//addrchan := portscanner.OpenedTcpAdress(ipchan,telnet_ports)

	t0 := time.Now()
	for addr := range addrchan {

		request:= "DeviceInfo"
		// sequential requesting
		//pcbli_request(addr,request, user , passwd)

		// parallel version
		wp.Submit( pcbliRequestTask(addr,request,user,passwd))

	}
	wp.Stop()
	t1 := time.Now()
	t := t1.Sub(t0)

	fmt.Printf("pcbcli request on all port 23 from 192.168.1.0/24 DONE in %v s\n",t.Seconds())


	fmt.Printf("waiting  5 seconds\n")
	time.Sleep(5*time.Second)

}


func responseChannel(){

	var cmd string = "DeviceInfo"

	//cmd = "UserManagement.User"
	//cmd = "LED"


	fmt.Printf("pcbcli request on  all port 23 from 192.168.1.0/24\n")
	telnet_ports :=  []int {23}

	// create a generator of ip from cidr
	ipchan := portscanner.IpRange( cidr )

	// create a generator of net.TcpAddr for open port 23 on each ip
	addrchan := portscanner.FastOpenedTcpAdress(ipchan,telnet_ports,0,90)


	responses := inspectors.ChannelResponse(addrchan,cmd ,user,passwd)
	//responses := inspectors.ChannelResponse(addrchan,"DeviceInfo",user,passwd)

	for response := range responses {

		fmt.Printf("response for %s : %v\n" , response.Ip,response.Response)
		//j,err:= inspectors.JsonifyResponse( &response)
		j,err := response.Jsonify()
		if err == nil {
			fmt.Printf("%s",string(j))
		}

	}



	//t1 := time.Now()
	//t := t1.Sub(t0)
	//
	//fmt.Printf("pcbcli request on all port 23 from 192.168.1.0/24 DONE in %v s\n",t.Seconds())

}

func publish_responseChannel(){

	var cmd string = "DeviceInfo"

	//cmd = "UserManagement.User"
	//cmd = "LED"

	//publisher,err := publishers.NewRedisPublisher("")
	publisher,err := publishers.NewScreenPublisher()
	if err != nil {
		log.Fatal("cannot connect to redis")
	}


	fmt.Printf("pcbcli request on  all port 23 from 192.168.1.0/24\n")
	telnet_ports :=  []int {23}

	// create a generator of ip from cidr
	ipchan := portscanner.IpRange( cidr )

	// create a generator of net.TcpAddr for open port 23 on each ip
	addrchan := portscanner.FastOpenedTcpAdress(ipchan,telnet_ports,0,90)


	responses := inspectors.ChannelResponse(addrchan,cmd ,user,passwd)
	//responses := inspectors.ChannelResponse(addrchan,"DeviceInfo",user,passwd)

	for response := range responses {

		//fmt.Printf("response for %s : %v\n" , response.Ip,response.Response)
		ip := response.Ip
		channel := fmt.Sprintf("ptf/DeviceInfo/%s" , ip)
		//j,err:= inspectors.JsonifyResponse( &response)
		j,err:= response.Jsonify()
		if err == nil {
			//fmt.Printf("publish response %s\n",string(j))

			publishers.Publish(publisher,channel + "\n", string(j))

		}

	}



	//t1 := time.Now()
	//t := t1.Sub(t0)
	//
	//fmt.Printf("pcbcli request on all port 23 from 192.168.1.0/24 DONE in %v s\n",t.Seconds())

}



//func main() {
//
//
//	//simple()
//
//	test_ls()
//
//	//request_cidr()
//
//	//responseChannel()
//
//	publish_responseChannel()
//
//
//	println("Done")
//
//
//}