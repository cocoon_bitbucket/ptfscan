package publishers

/*

	return a publisher of the desired kind ( screen/redis/mqtt )


 */

import (

	"errors"

)




var (

	publisher_kinds =   []string  {"screen","redis","mqtt"}

)



func GetPublisher(kind string , url string) (Publisher,error){


	var p Publisher = nil
	var err error


	switch kind {

	case "screen" :{

		p,err = NewScreenPublisher()

	}
	case "redis" : {

		p,err = NewRedisPublisher(url)

	}
	case "mqtt" : {
		err= errors.New("publisher mqtt not yet implemented")
	}

	default:{

		err= errors.New("publisher not implemented")


	}


	}


	return p,err

}

