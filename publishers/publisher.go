package publishers



type Publisher interface {

	publish( channel string,message string) error
	close() error
	ping() bool

}


func Publish(publisher Publisher,channel,message string) error {

	return publisher.publish(channel,message)

}

func Close(publisher Publisher) error {

	return publisher.close()

}

func Ping(publisher Publisher) bool {

	return publisher.ping()

}