package publishers



import(

	"testing"
	//"golang.org/x/tools/go/gcimporter15/testdata"
	"fmt"
)


var redis_url = "redis://localhost:6379/1"



func TestRedisPublisher(t *testing.T) {


	p,err := NewRedisPublisher(redis_url)
	if err == nil {
		p.ping()
		p.publish("redis", "message")
		p.publish("redis/hello", "hello")
		p.publish("ptf/EnergenieStatus/192.168.1.30","[true,true,true,true]")
		p.close()
		p.close()
		//p.ping()

	}
	if err != nil {
		fmt.Printf("redis failed: %v\n",err)
	}

}

func TestMqttPublisher(t *testing.T) {


	p,_ := NewMqttPublisher()
	p.ping()
	p.publish("mqtt","message")
	p.close()

}



func TestPolymorphPublisher(t *testing.T) {


	redis,err1 := NewRedisPublisher(redis_url)
	mqtt,_ := NewMqttPublisher()


	channel := "my_channel"
	message := "my_message"


	if err1 == nil {
		Ping(redis)
		Publish(redis, channel, message)
		Close(redis)
	} else {
		fmt.Printf("redis failed: %s\n",err1)
	}


	Ping(mqtt)
	Publish(mqtt,channel,message)
	Close(mqtt)


}



