package publishers

import(
	//"fmt"
	"github.com/garyburd/redigo/redis"
	"time"
)


const DefaultRedisUrl = "redis://localhost:6379/0"

type RedisPublisher struct {

	redis.Conn

	Url string

	//Url string
	//Cnx redis.Conn

}



func NewRedisPublisher(url string) (* RedisPublisher,error){

	var err error = nil

	if url == "" {
		url = DefaultRedisUrl
	}

	var p * RedisPublisher = nil

	option := redis.DialConnectTimeout(1* time.Second)
	cnx, err := redis.DialURL(url,option)
	_ = cnx
	if err == nil {
		p = &RedisPublisher{cnx, url }

	}

	return p,err
}


func (p RedisPublisher) publish(channel string,message string) error {
	//fmt.Printf("redis publish to %s the message %s\n",channel,message)

	receivers, err := redis.Int64(p.Do("PUBLISH",channel,message))
	_ = receivers
	return err

}

func (p RedisPublisher) close() error {
	p.Close()
	return nil
}

func (p RedisPublisher) ping() bool {

	reply,err := redis.String(p.Do("PING"))
	//reply, err := p.Cnx.Do("PING")
	_ = reply
	_ = err

	if err != nil {
		return false
	}

	return true
}