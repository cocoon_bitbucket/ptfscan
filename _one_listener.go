package main

import (

	"log"
	"github.com/garyburd/redigo/redis"
	"time"
)

var (

	redisDefaultUrl = "redis://127.0.0.1:6379"

)


func onStart() error {

	log.Printf("Listener started\n")
	return nil
}


func onMessage(channel string,message interface{}) error {

	var err error = nil

	log.Printf("receive message %s ->%s\n",channel ,message )
	return err


}



func Listen(url string) {


	// create redis pub sub listener
	// update the entry at redis key channel
	// eg ptf:DeviceInfo:192.168.1.1

	c,err := redis.DialURL(redisDefaultUrl)

	if err != nil {
		log.Fatal("cannot connect to redis")
	}


	psc := redis.PubSubConn{Conn: c}



	//var channels = []string{"ptf/EnergenieStatus/192.168.1.30"}
	var channels = []string{"redis*", "ptf/*" }

	//psc.PSubscribe("redis*")
	for _,channel := range channels{
		psc.PSubscribe( channel)
	}

	time.Sleep(1 * time.Second)


	done := make(chan error, 1)

	// Start a goroutine to receive notifications from the server.
	go func() {
		for {
			switch n := psc.Receive().(type) {
			case error:
				log.Printf("error on Receive\n")
				done <- n
				return
			case redis.Message:
				//log.Printf("message received\n")
				if err := onMessage(n.Channel, n.Data); err != nil {
					done <- err
					return
				}
			case redis.PMessage:
				//log.Printf("pmessage received\n")
				if err := onMessage(n.Channel, n.Data); err != nil {
					done <- err
					return
				}
			case redis.Subscription:
				log.Printf("subscription message received\n")
				switch n.Count {
				case len(channels):
					// Notify application when all channels are subscribed.
					if err := onStart(); err != nil {
						done <- err
						return
					}
				case 0:
					// Return from the goroutine when all channels are unsubscribed.
					log.Printf("exit \n")
					done <- nil
					return
				}
			}
		}
	}()


}



func main() {

	done := make(chan bool)

	Listen(redisDefaultUrl)

	<- done



}