package main

import (
	"flag"
	"log"
	"bitbucket.org/cocoon_bitbucket/ptfscan/config"
	"bitbucket.org/cocoon_bitbucket/ptfscan/config/tasks"
	"bitbucket.org/cocoon_bitbucket/ptfscan/infra"
	"bitbucket.org/cocoon_bitbucket/ptfscan/ginweb"
	"time"
	"fmt"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"

	//// for prometheus supervision
	//"net/http"
	//"github.com/prometheus/client_golang/prometheus/promhttp"
)
/*

	import "flag"
var ip = flag.Int("flagname", 1234, "help message for flagname")


	launch tasks on behalf of config file ptfscan.toml

		for each ip of a cidr ( eg 192.168.1.0/24)
			launch a pcbcli request DeviceInfo?
			   publish result on redis channel ptf/DeviceInfo/<ip>
			launch a pcbcli request LED?
			  publish result on redis channel ptf/Led/<ip>



 */





func main() {


	var (

		err error

		// = "ptfscan.toml"
		Infra_livebox_json string = "infra_livebox.json"


		configFilename = flag.String("filename","ptfscan.toml","configuration filename")
		ipRange = flag.String("ip-range","", "iprange (cidr eg 192.168.1.0/24 )")
		redisUrl= flag.String("redis-url","","redis host for redis publishers eg: redis://127.0.0.1:6379/0 ")

		publisher = flag.String("publisher","","kind of publisher (redis/screen)")
	)



	done := make(chan bool)

	// parse flag options
	flag.Parse()


	// create a config
	cnf := config.NewConfig(* configFilename)
	_ = cnf

	// overwrite config with flag options
	if * publisher != "" {
		cnf.Publisher = * publisher
	}
	if * ipRange != "" {
		cnf.IpRange = * ipRange
	}
	if * redisUrl != "" {
		cnf.Redis.Url = * redisUrl
	}


	// set global redis pool
	_,err = redistools.NewPool(cnf.Redis.Url)
	if err != nil {
		log.Fatal("Cannot create pool: " + err.Error())
	}

	// raz database and ptfscan: info
	pool,err := redistools.GetPool()
	cnx := pool.Get()
	cnx.Do("FLUSHDB")

	// set ptfscan info
	config.InitInfoManager(cnx,*configFilename,*cnf)

	cnx.Close()


	// initialize redis database with infra_livebox.json
	log.Printf(" ******  Initialize infra Database  *****")
	err = infra.InitInfra( cnf.Redis.Url , Infra_livebox_json)
	if err != nil {
		log.Fatal(fmt.Sprintf("Cannot initialize Infra: %s\n" , err.Error()))
	}


	log.Printf(" ******  Start tasks *****")
	tasks.StartTasks(cnf)


	// start web server
	time.Sleep( 1 * time.Second)
	log.Printf(" ******  Start web server *****")
	go ginweb.Serve(cnf)


	//// start a prometheus wev server on
	//log.Printf(" ******  Start prometheus /metrics server *****")
	//http.Handle("/metrics", promhttp.Handler())
	//log.Fatal(http.ListenAndServe(":3334", nil))


	// wait forever
	<- done


}


func init() {


	//flag.StringVar(configFilename, "f", "ptfscan.toml", "configuration filename")
	//const (
	//	defaultGopher = "pocket"
	//	usage         = "the variety of gopher"
	//)
	//flag.StringVar(&gopherType, "gopher_type", defaultGopher, usage)
	//flag.StringVar(&gopherType, "g", defaultGopher, usage+" (shorthand)")
}