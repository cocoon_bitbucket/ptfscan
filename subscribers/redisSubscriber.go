package subscribers

import(
	//"fmt"
	"github.com/garyburd/redigo/redis"
	"time"
	"log"
	//"fmt"
)


const DefaultRedisUrl = "redis://localhost:6379/0"
const healthCheckPeriod = time.Minute

type RedisSubscriber struct {

	redis.Conn

	Url string
	Pusub redis.PubSubConn

	Done chan error

}


func NewRedisSubscriber(url string) (* RedisSubscriber,error){

	var err error = nil

	if url == "" {
		url = DefaultRedisUrl
	}

	var p * RedisSubscriber = nil

	cnx, err := redis.DialURL(
		url,
		redis.DialReadTimeout(healthCheckPeriod+10*time.Second),
		redis.DialWriteTimeout(10*time.Second))

	if err == nil {

		pubsub := redis.PubSubConn{Conn: cnx}

		done := make(chan error, 1)
		//messages := make( chan ListenerMessage)

		p = &RedisSubscriber{cnx, url,pubsub,done}

	}

	return p,err
}


//
// implements subscribers interface
//

func (p *RedisSubscriber) Subscribe(channel ... string) error {

	var err error = nil

	for _,c := range  channel{
		err = p.Pusub.PSubscribe(c)
		if err != nil {
			//p.Pusub.Unsubscribe()
			p.Pusub.PUnsubscribe()
			break
		}
	}
	return err

}

func (p *RedisSubscriber) Receive() ( message SimpleMessage ,err error ){


	//var reply Message
	//reply = Message{"",""}

	err = nil


	for {

		switch v := p.Pusub.Receive().(type) {

		case redis.Message:
			//log.Printf("%s: message: %s\n", v.Channel, v.Data)
			return SimpleMessage{v.Channel, string(v.Data)}, nil

		case redis.PMessage:
			//log.Printf("%s: pmessage: %s\n", v.Channel, v.Data)
			return SimpleMessage{v.Channel, string(v.Data)}, nil

		case redis.Subscription:
			log.Printf("subscription received %s: %s %d\n", v.Channel, v.Kind, v.Count)

		case error:
			return SimpleMessage{"", ""}, v
		}
	}

}

func (p *RedisSubscriber) Close() error {
	p.Pusub.Unsubscribe()
	p.Pusub.PUnsubscribe()
	p.Pusub.Close()
	p.Close()
	return nil
}


//
// implement publsher interface
//
func (p *RedisSubscriber) close() error {
	return nil
}

