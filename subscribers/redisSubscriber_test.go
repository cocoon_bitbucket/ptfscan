package subscribers_test




import(

	"testing"
	"fmt"

	"bitbucket.org/cocoon_bitbucket/ptfscan/publishers"
	"bitbucket.org/cocoon_bitbucket/ptfscan/subscribers"

	"log"
	"time"
)


var redis_url = "redis://localhost:6379/1"



func TestRedisSubscriber(t *testing.T) {


	p,err := publishers.NewRedisPublisher(redis_url)
	if err == nil {

		publishers.Ping(p)

		////p.Ping()
		//p.Publish("redis", "message")
		//p.publish("redis/hello", "hello")
		//p.publish("ptf/EnergenieStatus/192.168.1.30","[true,true,true,true]")
		//p.close()
		//p.close()
		//p.ping()

	}
	if err != nil {
		fmt.Printf("redis failed: %v\n",err)
	}

	s,err := subscribers.NewRedisSubscriber(redis_url)
	if err == nil {

		s.Subscribe("channel_one/*")

		publishers.Publish(p,"channel_one/hello","hello")
		//publishers.Publish(s,"channel_one/hello","hello")


		time.Sleep(1)

		m,err := s.Receive()
		if err == nil {
			log.Printf("received message => (channel: %s , message: %s)\n",m.Channel,m.Data)

		}


		s.Subscribe("channel_two")

		publishers.Publish(p,"channel_two","hello")

		time.Sleep(1)

		m,err = s.Receive()
		if err == nil {
			log.Printf("received message => (channel: %s , message: %s)\n",m.Channel,m.Data)

		}

		publishers.Close(p)
		//subscribers.Close(s)

	}


}
