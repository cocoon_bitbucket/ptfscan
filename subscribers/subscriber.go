package subscribers



// Receive() (reply interface{}, err error)

type SimpleMessage struct {
	Channel string
	Data string
}



type Subscriber interface {

	Subscribe( channel ... string) error
	Receive() (SimpleMessage,error)
	Close() error

}


func Subscribe( subscriber Subscriber,channel ... string) error {

	return subscriber.Subscribe(channel ...)

}

func Close(subscriber Subscriber) error {

	return subscriber.Close()

}

func Receive(subscriber Subscriber) (SimpleMessage,error)  {

	return subscriber.Receive()

}