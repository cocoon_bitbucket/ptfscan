package _listeners



var redisDefaultUrl = "redis://localhost:6379/0"

type Listener interface {

	publish( channel string,message string) error
	subscribe( channel string)
	listen( ) chan string
	close() error
	ping() bool

}


//func Publish(publisher publisher,channel,message string) error {
//
//	return publisher.publish(channel,message)
//
//}
//
//func Close(publisher publisher) error {
//
//	return publisher.close()
//
//}
//
//func Ping(publisher publisher) bool {
//
//	return publisher.ping()
//
//}
