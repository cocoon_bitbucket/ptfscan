package _listeners

import(
	"fmt"
	"github.com/garyburd/redigo/redis"
	//"time"
	"time"
	"context"
	"log"
)


const DefaultRedisUrl = "redis://localhost:6379/0"
const healthCheckPeriod = time.Minute

type RedisListener struct {

	redis.Conn

	Url string
	Pusub redis.PubSubConn

	Done chan error
	Messages  chan ListenerMessage

}


type ListenerMessage struct {
	channel string
	message interface {}
}



func NewRedisListener(url string) (* RedisListener,error){

	var err error = nil

	if url == "" {
		url = DefaultRedisUrl
	}

	var p * RedisListener = nil

	cnx, err := redis.DialURL(
		url,
		redis.DialReadTimeout(healthCheckPeriod+10*time.Second),
		redis.DialWriteTimeout(10*time.Second))

	if err == nil {

		pubsub := redis.PubSubConn{Conn: cnx}

		done := make(chan error, 1)
		messages := make( chan ListenerMessage)


		p = &RedisListener{cnx, url,pubsub,done,messages}

	}

	return p,err
}


func (p RedisListener) subscribe(channel string) error {

	err := p.Pusub.Subscribe(channel)
	return err
}


func (p RedisListener) publish(channel string,message string) error {
	fmt.Printf("redis publish to %s the message %s\n",channel,message)

	receivers, err := redis.Int64(p.Do("PUBLISH",channel,message))
	_ = receivers
	return err

}

//func (p RedisListener) close() error {
//	p.Close()
//	return nil
//}

func (p RedisListener) ping() bool {

	reply,err := redis.String(p.Do("PING"))
	//reply, err := p.Cnx.Do("PING")
	_ = reply
	_ = err

	if err != nil {
		return false
	}

	return true
}

func (p RedisListener) listen() chan ListenerMessage {
	// return a listener channel

	go func() {
		defer close(p.Messages)

		var kind string
		reply, err := redis.Scan("reply", &kind)
		if err != nil {
			return err
		}

		switch kind {
		case "message":
			var m redis.Message
			if _, err := redis.Scan(reply, &m.Channel, &m.Data); err != nil {
				message := ListenerMessage{m.Channel,m.Data}
				p.Messages <- message
			}

		case "pmessage":
			var pm redis.PMessage
			if _, err := redis.Scan(reply, &pm.Pattern, &pm.Channel, &pm.Data); err != nil {
				message := ListenerMessage{pm.Channel,pm.Data}
				p.Messages <- message
			}
		case "subscribe", "psubscribe", "unsubscribe", "punsubscribe":
			s := redis.Subscription{Kind: kind}
			if _, err := redis.Scan(reply, &s.Channel, &s.Count); err != nil {
				log.Printf("subscription message for channel %s" ,s.Channel)
			}

		case "pong":
			var p redis.Pong
			if _, err := redis.Scan(reply, &p.Data); err != nil {
				log.Printf("subscription message for channel %s" ,s.Channel)
			}
		}
		//return errors.New("redigo: unknown pubsub notification")


	}()
	//return c




}

// utilities see: https://github.com/garyburd/redigo/blob/master/redis/pubsub_example_test.go

//func publish() {
//	c, err := dial()
//	if err != nil {
//		fmt.Println(err)
//		return
//	}
//	defer c.Close()
//
//	c.Do("PUBLISH", "c1", "hello")
//	c.Do("PUBLISH", "c2", "world")
//	c.Do("PUBLISH", "c1", "goodbye")
//}

func GetListener( redisUrl string) {

	ctx, cancel := context.WithCancel(context.Background())

	err := listenPubSubChannels(ctx,
		redisUrl,
		func() error {
			// The start callback is a good place to backfill missed
			// notifications. For the purpose of this example, a goroutine is
			// started to send notifications.
			//go publish()
			fmt.Printf("redis listener has started\n")
			return nil
		},
		func(channel string, message []byte) error {
			fmt.Printf("channel: %s, message: %s\n", channel, message)

			// For the purpose of this example, cancel the listener's context
			// after receiving last message sent by publish().
			if string(message) == "goodbye" {
				cancel()
			}
			return nil
		},
		"c1", "c2", "ptf/DeviceInfo")

	if err != nil {
		fmt.Println(err)
		return
	}

}




// listenPubSubChannels listens for messages on Redis pubsub channels. The
// onStart function is called after the channels are subscribed. The onMessage
// function is called for each message.
func listenPubSubChannels(ctx context.Context, redisServerAddr string,
	onStart func() error,
	onMessage func(channel string, data []byte) error,
	channels ...string) error {
	// A ping is set to the server with this period to test for the health of
	// the connection and server.
	const healthCheckPeriod = time.Minute

	c, err := redis.DialURL( redisServerAddr,
		// Read timeout on server should be greater than ping period.
		redis.DialReadTimeout(healthCheckPeriod+10*time.Second),
		redis.DialWriteTimeout(10*time.Second))
	if err != nil {
		return err
	}
	defer c.Close()

	psc := redis.PubSubConn{Conn: c}

	if err := psc.Subscribe(redis.Args{}.AddFlat(channels)...); err != nil {
		return err
	}

	done := make(chan error, 1)

	// Start a goroutine to receive notifications from the server.
	go func() {
		for {
			switch n := psc.Receive().(type) {
			case error:
				done <- n
				return
			case redis.Message:
				if err := onMessage(n.Channel, n.Data); err != nil {
					done <- err
					return
				}
			case redis.Subscription:
				switch n.Count {
				case len(channels):
					// Notify application when all channels are subscribed.
					if err := onStart(); err != nil {
						done <- err
						return
					}
				case 0:
					// Return from the goroutine when all channels are unsubscribed.
					done <- nil
					return
				}
			}
		}
	}()

	ticker := time.NewTicker(healthCheckPeriod)
	defer ticker.Stop()
loop:
	for err == nil {
		select {
		case <-ticker.C:
			// Send ping to test health of connection and server. If
			// corresponding pong is not received, then receive on the
			// connection will timeout and the receive goroutine will exit.
			if err = psc.Ping(""); err != nil {
				break loop
			}
		case <-ctx.Done():
			break loop
		case err := <-done:
			// Return error from the receive goroutine.
			return err
		}
	}

	// Signal the receiving goroutine to exit by unsubscribing from all channels.
	psc.Unsubscribe()

	// Wait for goroutine to complete.
	return <-done
}

func ListenerLoop(ctx context.Context, parent * RedisListener ) error {


	const healthCheckPeriod = time.Minute
	psc := parent.Pusub

	//if err := psc.Subscribe(redis.Args{}.AddFlat(channels)...); err != nil {
	//	return err
	//}

	//done := make(chan error, 1)
	done := parent.done

	// Start a goroutine to receive notifications from the server.
	go func() {
		for {
			switch n := psc.Receive().(type) {
			case error:
				done <- n
				return
			//case redis.Message:
			//	if err := onMessage(n.Channel, n.Data); err != nil {
			//		done <- err
			//		return
			//	}
			//case redis.Subscription:
			//	switch n.Count {
			//	case len(channels):
			//		// Notify application when all channels are subscribed.
			//		if err := onStart(); err != nil {
			//			done <- err
			//			return
			//		}
			//	case 0:
			//		// Return from the goroutine when all channels are unsubscribed.
			//		done <- nil
			//		return
			//	}
			}
		}
	}()

	ticker := time.NewTicker(healthCheckPeriod)
	defer ticker.Stop()
loop:
	for  {
		select {
		case <-ticker.C:
			// Send ping to test health of connection and server. If
			// corresponding pong is not received, then receive on the
			// connection will timeout and the receive goroutine will exit.
			if err := psc.Ping(""); err != nil {
				break loop
			}
		case <-ctx.Done():
			break loop
		case err := <-done:
			// Return error from the receive goroutine.
			return err
		}
	}

	// Signal the receiving goroutine to exit by unsubscribing from all channels.
	psc.Unsubscribe()

	// Wait for goroutine to complete.
	return <-done
}