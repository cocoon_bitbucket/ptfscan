package web

import (
	"fmt"
	"net/http"
	"github.com/go-chi/chi"
)

/*

	GET /energenie

	GET /energenie/192.168.1.30

	GET /energenie/192.168.1.30/1

	POST /energenie/192.168.1.30/1/on
	POST /energenie/192.168.1.30/2/off
	POST /energenie/192.168.1.30/-/off


 */



// A completely separate router for administrator routes
func EnergenieRouter() chi.Router {

	r := chi.NewRouter()
	//r.Use(AdminOnly)

	// energenie index
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("energenie: index"))
	})

	// energenie list
	r.Get("/{ip}" ,func(w http.ResponseWriter, r *http.Request) {

		ip := decode_hyphen_ip(chi.URLParam(r, "ip"))

		w.Write([]byte(fmt.Sprintf("energenie device status :  %v", ip)))
	})


	// outlet status
	r.Get("/{ip}/{outlet}" ,func(w http.ResponseWriter, r *http.Request) {
		ip := decode_hyphen_ip(chi.URLParam(r, "ip"))

		w.Write([]byte(fmt.Sprintf("energenie outlet status %v\n", ip)))
		w.Write([]byte(fmt.Sprintf("outlet  %v\n", chi.URLParam(r, "outlet"))))
	})

	// switch ON outlet
	r.Post("/{ip}/{outlet}/on" ,func(w http.ResponseWriter, r *http.Request) {

		ip := decode_hyphen_ip(chi.URLParam(r, "ip"))

		w.Write([]byte(fmt.Sprintf("energenie on %v\n", ip)))
		w.Write([]byte(fmt.Sprintf("outlet  %v\n", chi.URLParam(r, "outlet"))))
	})

	// switch OFF outlet
	r.Post("/{ip}/{outlet}/off" ,func(w http.ResponseWriter, r *http.Request) {

		ip := decode_hyphen_ip(chi.URLParam(r, "ip"))
		w.Write([]byte(fmt.Sprintf("energenie off %v\n", ip)))
		w.Write([]byte(fmt.Sprintf("outlet  %v\n", chi.URLParam(r, "outlet"))))
	})


	return r
}
