package web

import (
	"fmt"
	"net/http"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

/*

	GET /livebox

	GET /livebox/192-168-1-1

	POST /Livebox/192-168-1-1/{cmd}

	POST /livebox/192-168-1-1/pcbcli
	POST /livebox/192-168-1-1/reboot
	POST /livebox/192-168-1-1/shell

 */



// A completely separate router for administrator routes
func LiveboxRouter() chi.Router {

	r := chi.NewRouter()
	//r.Use(AdminOnly)

	// livebox index
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("livebox: index"))
	})



	// livebox status
	r.Get("/{ip}" ,func(w http.ResponseWriter, r *http.Request) {

		ip := decode_hyphen_ip(chi.URLParam(r, "ip"))

		w.Write([]byte(fmt.Sprintf("livebox status %v", ip)))
	})

	////
	//r.Post("/{ip}" ,func(w http.ResponseWriter, r *http.Request) {
	//	w.Write([]byte(fmt.Sprintf("livebox  %v", chi.URLParam(r, "ip"))))
	//})
	//

	//
	r.Get("/{ip}/{cmd}" ,func(w http.ResponseWriter, r *http.Request) {

		ip := decode_hyphen_ip(chi.URLParam(r, "ip"))
		cmd := chi.URLParam(r, "cmd")

		w.Write([]byte(fmt.Sprintf("livebox info cmd %v\n", ip)))
		w.Write([]byte(fmt.Sprintf("cmd  %v\n", cmd)))
	})


	// livebox shell command
	r.Post("/{ip}/shell" , RunShellCommand )



	// livebox execute command
	r.Post("/{ip}/{cmd}" ,func(w http.ResponseWriter, r *http.Request) {

		ip := decode_hyphen_ip(chi.URLParam(r, "ip"))
		cmd := chi.URLParam(r, "cmd")

		w.Write([]byte(fmt.Sprintf("livebox exec cmd  %v\n", ip )))
		w.Write([]byte(fmt.Sprintf("cmd  %v\n", cmd)))
	})

	//r.Get("/accounts", func(w http.ResponseWriter, r *http.Request) {
	//	w.Write([]byte("admin: list accounts.."))
	//})
	//r.Get("/users/{userId}", func(w http.ResponseWriter, r *http.Request) {
	//	w.Write([]byte(fmt.Sprintf("admin: view user id %v", chi.URLParam(r, "userId"))))
	//})

	return r
}

//// AdminOnly middleware restricts access to just administrators.
//func AdminOnly(next http.Handler) http.Handler {
//	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
//		isAdmin, ok := r.Context().Value("acl.admin").(bool)
//		if !ok || !isAdmin {
//			http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)
//			return
//		}
//		next.ServeHTTP(w, r)
//	})
//}





func RunShellCommand(w http.ResponseWriter, r *http.Request) {

	// get url paramp ip
	ip := decode_hyphen_ip(chi.URLParam(r, "ip"))

	// decode json body
	data := &ShellCommandRequest{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, ErrInvalidRequest(err))
		return
	}


	cmd := data.Cmd
	// we need to execute command cmd on livebox at ip



	livebox_response :=  fmt.Sprintf("fake response from livebox %s",ip)
	_=cmd
	_=livebox_response



	render.Status(r, http.StatusOK)
	render.Render(w, r, NewShellCommandResponse(data.ShellCommand,livebox_response))



	//w.Write([]byte(fmt.Sprintf("livebox shell command  %v\n", ip )))
	//w.Write([]byte(fmt.Sprintf("cmd  %v\n", cmd)))


}



type ShellCommand struct {
	Cmd    string `json:"cmd"`
}

type ShellCommandRequest struct {
	* ShellCommand
}
func (a *ShellCommandRequest) Bind(r *http.Request) error {
	// just a post-process after a decode..
	//a.Cmd = ""                                 // unset the protected ID
	//a.Cmd.Title = strings.ToLower(a.Article.Title) // as an example, we down-case
	return nil
}


type ShellCommandResponse struct {
	* ShellCommand
	// We add an additional field to the response here.. such as this
	// elapsed computed property
	Result string  `json:"result"`
	Elapsed int64 `json:"elapsed"`
}
func (rd *ShellCommandResponse) Render(w http.ResponseWriter, r *http.Request) error {
	// Pre-processing before a response is marshalled and sent across the wire
	rd.Elapsed = 10
	//rd.Result = "fake"
	return nil
}

func NewShellCommandResponse( cmd * ShellCommand, response string) *ShellCommandResponse {
	resp := &ShellCommandResponse{ cmd, response,0}
	return resp
}