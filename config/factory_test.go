package config_test

import(

	"testing"
	"bitbucket.org/cocoon_bitbucket/ptfscan/config"
	"bitbucket.org/cocoon_bitbucket/ptfscan/publishers"

	"log"
)

var (
	config_filename string = "config.toml"
)


func TestGetScreenPublisher(t *testing.T) {

	config.NewConfig(config_filename)

	p,err := config.GetPublisher("screen")

	if err == nil {
		publishers.Ping(p)
		publishers.Publish(p, "channel/screen","message")
		publishers.Close(p)
	}

	config.UnsetConfig()

}

func TestGetRedisPublisher(t *testing.T) {

	config.NewConfig(config_filename)

	p,err := config.GetPublisher("redis")

	if err == nil {
		publishers.Ping(p)
		publishers.Publish(p, "channel/redis","message")
		publishers.Close(p)
	} else {
		log.Printf("error: %v" ,err)
	}

	config.UnsetConfig()

}


func TestGetMqttPublisher(t *testing.T) {

	config.NewConfig(config_filename)

	p,err := config.GetPublisher("mqtt")

	if err == nil {
		publishers.Ping(p)
		publishers.Publish(p, "channel/mqtt","message")
		publishers.Close(p)
	} else {
		log.Printf("error: %v" ,err)
	}

	config.UnsetConfig()
}





//func init () {
//	c:= config.NewConfig(config_filename)
//	_ = c
//
//}



