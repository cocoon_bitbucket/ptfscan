package tasks

import (

	"fmt"
	"log"
	"time"
	"context"

	"bitbucket.org/cocoon_bitbucket/ptfscan/config"
	"bitbucket.org/cocoon_bitbucket/ptfscan/portScanner/inspectors"
	portscanner "bitbucket.org/cocoon_bitbucket/ptfscan/portScanner"
	//"bitbucket.org/cocoon_bitbucket/ptfscan/publishers"

	//"net"
	"bitbucket.org/cocoon_bitbucket/ptfscan/publishers"
)

/*

	task LiveboxFinder

	scan each ip of cidr with port 80

	try to find an http_server
	Get / and try to find  <title>Livebox</title>


	if title ok try to find the following key/vales

		BUILD_CUSTOMER: 'ft',
		BUILD_PROJECT: 'lbv3fr',
		BUILD_HARDWARE: 'sagem_lbv3',


 */


func LiveboxFinderTask ( ctx context.Context, parameters config.TaskParameters) {


	task_name := "liveboxFinder"


	cidr := parameters.IpRange

	publisher_prefix := parameters.PublisherPrefix
	publisher_kind := parameters.Publisher
	publisher,err:= config.GetPublisher(publisher_kind)

	log_level := parameters.LogLevel

	if err != nil {
		log.Fatal("cannot create a publisher: ",err)
	}

	// get task specific parameters
	scan_interval := time.Second * time.Duration( parameters.ScanInterval)


	fmt.Printf("%s request on  all port 80 from %s\n",task_name, cidr)
	telnet_ports :=  []int {80}

	//timeout := 200 * time.Millisecond
	timeout := time.Millisecond * time.Duration(parameters.DiscoveryTimeout)

	for {

		// scan loop
		//fmt.Printf("%s: start scanning livebox\n", task_name)

		// send Heartbeat message
		SendHeartbeat(publisher,publisher_prefix,task_name,scan_interval)

		// create a generator of ip from cidr
		ipchan := portscanner.IpRange( cidr )

		// create a generator of net.TcpAddr for open port 23 on each ip
		addrchan := portscanner.FastOpenedTcpAdress(ipchan,telnet_ports,timeout,log_level)

		// create a channel of DeviceInfo? pcbcli responses
		responses := inspectors.LiveboxWebChannelResponse(addrchan,task_name)


		for response := range responses {

			//fmt.Printf("response for %s : %v\n" , response.Ip,response.Response)
			ip := response.Ip
			channel := fmt.Sprintf("%s/%s/%s", publisher_prefix,task_name, ip)
			_=channel
			_=publisher
			j, err := response.Jsonify()
			if err == nil {
				//fmt.Printf("publish response %s\n",string(j))
				publishers.Publish(publisher, channel, string(j))
			}
		}
		//fmt.Printf("%s: end scanning livebox\n",task_name)

		//fmt.Printf("%s: waiting scan interval (%ds)\n",task_name, scan_interval/1000000000)
		time.Sleep( scan_interval )

	}

}
