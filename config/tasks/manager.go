package tasks


import (


	"log"
	"context"
	"bitbucket.org/cocoon_bitbucket/ptfscan/config"
)



func UpdateTaskParameters(cnf * config.ConfigMain, local * config.TaskParameters) {

	// complete task parameters with global [scanner] parameters
	// Discovery_timeout , IpRange

	if local.IpRange == ""         { local.IpRange = cnf.IpRange }
	if local.PublisherPrefix == "" { local.PublisherPrefix = cnf.PublisherPrefix}
	if local.Publisher == ""       { local.Publisher = cnf.Publisher}

	if local.ScanInterval == 0     { local.ScanInterval= cnf.Scanner.ScanInterval}
	if local.DiscoveryTimeout == 0 { local.DiscoveryTimeout = cnf.Scanner.DiscoveryTimeout}
	if local.LogLevel == 0         { local.LogLevel = cnf.Scanner.LogLevel }

}




func StartTasks (cnf * config.ConfigMain) {

	//log.Printf(" ******  Start scan tasks *****")


	ctx := context.Background()

	// find dynamic pcbli request
	for name, data := range cnf.PcbCli {
		//println(name)
		//data.Show()

		// update parameters with global config
		UpdateTaskParameters(cnf,&data)

		go PcbCliRequestTask(ctx,name,data)

	}

	//find predefined tasks
	for name, data := range cnf.Task {
		//println(name)
		//data.Show()

		// update parameters with global config
		UpdateTaskParameters(cnf,&data)


		switch name {
		case "EnergenieStatus": {
			go EnergenieStatusTask(ctx,data)
		}
		case "EnergenieFinder": {
			go EnergenieLanSettingsTask(ctx,data)
		}

		case "LiveboxFinder": {
			go LiveboxFinderTask(ctx,data)
		}

		default:
			log.Fatal("Abort Unknown predefined task: ",name)
		}
	}


	//find reporters
	for name, data := range cnf.Reporters {

		switch name {
		case "ipscan": {
			go IpscanReporterTask(ctx,name,data)
		}

		default:
			log.Fatal("Abort Unknown reporter: ",name)
		}
	}



}
