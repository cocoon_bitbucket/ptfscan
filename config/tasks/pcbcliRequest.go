package tasks

import (

	"fmt"
	"log"
	"time"
	"context"

	"bitbucket.org/cocoon_bitbucket/ptfscan/config"
	"bitbucket.org/cocoon_bitbucket/ptfscan/portScanner/inspectors"
	portscanner "bitbucket.org/cocoon_bitbucket/ptfscan/portScanner"
	"bitbucket.org/cocoon_bitbucket/ptfscan/publishers"

)

/*

	create a generic Pcbcli request from a config.TaskParameters

 */



func PcbCliRequestTask ( ctx context.Context,task_name string,parameters config.TaskParameters) {

	// get general config parameters
	//cnf := config.GetConfig()
	//cidr := cnf.IpRange

	cidr := parameters.IpRange

	publisher_prefix := parameters.PublisherPrefix
	publisher_kind := parameters.Publisher
	publisher,err:= config.GetPublisher(publisher_kind)

	log_level := parameters.LogLevel

	if err != nil {
		log.Fatal("cannot create a publisher: ",err)
	}

	// get specific task parameters
	scan_interval := time.Second * time.Duration(parameters.ScanInterval)
	pcbcli_request := parameters.Request
	if pcbcli_request == "" {
		pcbcli_request = task_name
	}
	publish_channel := task_name


	fmt.Printf("pcbcli request [%s] on  all port 23 from %s\n", task_name, cidr)
	telnet_ports :=  []int {23}

	//timeout := 200 * time.Millisecond
	timeout := time.Millisecond * time.Duration(parameters.DiscoveryTimeout)
	for {

		// scan loop
		//fmt.Printf("pcbcli request [%s]: start scanning livebox\n", pcbcli_request)


		// send Heartbeat message
		SendHeartbeat(publisher,publisher_prefix,task_name,scan_interval)

		// create a generator of ip from cidr
		ipchan := portscanner.IpRange( cidr )

		// create a generator of net.TcpAddr for open port 23 on each ip
		addrchan := portscanner.FastOpenedTcpAdress(ipchan,telnet_ports,timeout,log_level)

		// create a channel of DeviceInfo? pcbcli responses
		responses := inspectors.ChannelResponse(addrchan,pcbcli_request ,pcbcli_user,pcbcli_passwd)


		for response := range responses {
			//fmt.Printf("response for %s : %v\n" , response.Ip,response.Response)
			ip := response.Ip
			channel := fmt.Sprintf("%s/%s/%s",publisher_prefix,publish_channel, ip)
			j, err := response.Jsonify()
			if err == nil {
				//fmt.Printf("publish response %s\n",string(j))
				publishers.Publish(publisher, channel, string(j))
			}
		}
		//fmt.Printf("pcbcli request[%s]: end scanning livebox\n",pcbcli_request)

		//fmt.Printf("pcbcli request [%s]: waiting scan interval (%ds)\n",pcbcli_request,scan_interval/1000000000)
		time.Sleep( scan_interval )

	}

}