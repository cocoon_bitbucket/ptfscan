package tasks

import (

	"context"
	"bitbucket.org/cocoon_bitbucket/ptfscan/config"
	//"bitbucket.org/cocoon_bitbucket/ptfscan/reporters/ipscan"
	//"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	"fmt"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	"bitbucket.org/cocoon_bitbucket/ptfscan/reporters/ipscan"
)


func IpscanReporterTask ( ctx context.Context,task_name string, cnf config.ReporterParameters) {

	//fmt.Printf("IpscanReporterTask: NOT YET IMPLEMENTED\n")

	// parameters log_level, scan_interval
	pool,err := redistools.GetPool()
	if err != nil {
		fmt.Printf("IpscanReporterTask: Abort: %s\n",err.Error())
		return
	}

	cnx := pool.Get()
	defer cnx.Close()


	fmt.Printf("start ipscan reporter\n")

	ipscan.CreateDeviceEntitySchema(cnx)

	reporter := ipscan.Reporter{pool.Pool}

	reporter.Run()


}

