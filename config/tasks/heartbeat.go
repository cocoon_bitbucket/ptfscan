package tasks

import (
	portscanner "bitbucket.org/cocoon_bitbucket/ptfscan/portScanner"
	"bitbucket.org/cocoon_bitbucket/ptfscan/publishers"
	"time"
)

/*

	utility function for publishing heartbeats


	channel : ptf/Heartbeat/TaskName

 */


func SendHeartbeat(publisher publishers.Publisher, prefix string ,name string,scan_interval time.Duration){

	hm,err := portscanner.NewHeartbeatMessage(name,scan_interval).Jsonify()
	if err == nil {
		hm_channel := portscanner.HeartbeatChannel(name,prefix)
		publishers.Publish(publisher, hm_channel, string(hm))
	}
}
