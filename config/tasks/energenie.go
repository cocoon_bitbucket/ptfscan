package tasks

import (

	"fmt"
	"log"
	"time"
	"context"

	"bitbucket.org/cocoon_bitbucket/ptfscan/config"
	"bitbucket.org/cocoon_bitbucket/ptfscan/portScanner/inspectors"
	portscanner "bitbucket.org/cocoon_bitbucket/ptfscan/portScanner"
	"bitbucket.org/cocoon_bitbucket/ptfscan/publishers"

)

/*

	Energenie device info

	scan cidr periodically
	for each ip do the pcb_cli request DeviceInfo
		publish result on publisher


 */


var (

	energenie_port  = 80
	default_energenie_password  = "1"


)


func EnergenieStatusTask ( ctx context.Context, parameters config.TaskParameters) {

	task_name := "EnergenieStatus"

	// get general parameters
	cidr := parameters.IpRange

	publisher_prefix := parameters.PublisherPrefix
	publisher_kind := parameters.Publisher
	publisher,err:= config.GetPublisher(publisher_kind)
	log_level := parameters.LogLevel

	if err != nil {
		log.Fatal("cannot create a publisher: ",err)
	}

	// get specific parameters
	password := parameters.Password
	//if parameters.IpRange != "" {
	//	cidr = parameters.IpRange
	//}

	scan_interval := time.Second * time.Duration(parameters.ScanInterval)

	fmt.Printf("%s request on  all port 80 from %s\n",task_name, cidr)
	telnet_ports :=  []int {energenie_port}

	//timeout := 500 * time.Millisecond
	timeout := time.Millisecond * time.Duration(parameters.DiscoveryTimeout)


	for {

		// scan loop
		//fmt.Printf("%s: start scanning Energenie devices\n",task_name)

		// send Heartbeat message
		SendHeartbeat(publisher,publisher_prefix,task_name,scan_interval)

		// create a generator of ip from cidr
		ipchan := portscanner.IpRange( cidr )

		// create a generator of net.TcpAddr for open port 80 on each ip
		addrchan := portscanner.FastOpenedTcpAdress(ipchan,telnet_ports,timeout,log_level)

		// create a channel of DeviceInfo? pcbcli responses
		responses := inspectors.EnergenieStatusChannelResponse(addrchan ,task_name, password)


		for response := range responses {

			//fmt.Printf("response for %s : %v\n" , response.Ip,response.Response)
			ip := response.Ip
			channel := fmt.Sprintf("%s/%s/%s",publisher_prefix,task_name, ip)

			j, err := response.Jsonify()

			if err == nil {
				//fmt.Printf("publish response %s\n",string(j))
				publishers.Publish(publisher, channel, string(j))
			}
		}
		//fmt.Printf("%s: end scanning livebox\n",task_name)

		//fmt.Printf("%s: waiting scan interval (%ds)\n",task_name, scan_interval/1000000000)
		time.Sleep( scan_interval )

	}

}



func EnergenieLanSettingsTask ( ctx context.Context, parameters config.TaskParameters) {

	task_name := "EnergenieLanConfig"

	// get general parameters
	cidr := parameters.IpRange

	publisher_prefix := parameters.PublisherPrefix
	publisher_kind := parameters.Publisher
	publisher,err:= config.GetPublisher(publisher_kind)
	log_level := parameters.LogLevel

	if err != nil {
		log.Fatal("cannot create a publisher: ",err)
	}

	// get specific parameters
	password := parameters.Password
	//if parameters.IpRange != "" {
	//	cidr = parameters.IpRange
	//}

	scan_interval := time.Second * time.Duration(parameters.ScanInterval)

	fmt.Printf("%s request on  all port 80 from %s\n",task_name, cidr)
	telnet_ports :=  []int {energenie_port}

	//timeout := 500 * time.Millisecond
	timeout := time.Millisecond * time.Duration(parameters.DiscoveryTimeout)
	for {

		// scan loop
		//fmt.Printf("%s: start scanning Energenie devices\n",task_name)

		// send Heartbeat message
		SendHeartbeat(publisher,publisher_prefix,task_name,scan_interval)

		// create a generator of ip from cidr
		ipchan := portscanner.IpRange( cidr )

		// create a generator of net.TcpAddr for open port 80 on each ip
		addrchan := portscanner.FastOpenedTcpAdress(ipchan,telnet_ports,timeout,log_level)

		// create a channel of DeviceInfo? pcbcli responses
		responses := inspectors.EnergenieLanConfigChannelResponse(addrchan ,task_name, password)


		for response := range responses {

			//fmt.Printf("response for %s : %v\n" , response.Ip,response.Response)
			ip := response.Ip
			channel := fmt.Sprintf("%s/%s/%s",publisher_prefix,task_name, ip)

			j, err := response.Jsonify()

			if err == nil {
				//fmt.Printf("publish response %s\n",string(j))
				publishers.Publish(publisher, channel, string(j))
			}
		}
		//fmt.Printf("%s: end scanning livebox\n",task_name)

		//fmt.Printf("%s: waiting scan interval (%ds)\n",task_name, scan_interval/1000000000)
		time.Sleep( scan_interval )

	}

}




