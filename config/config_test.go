//package config
package config_test

import(

	"testing"
	"github.com/BurntSushi/toml"
	"bitbucket.org/cocoon_bitbucket/ptfscan/config"
	"fmt"

	"github.com/stretchr/testify/assert"
	"github.com/davecgh/go-spew/spew"
)



func TestLoadConfig(t *testing.T) {


	var cnf config.ConfigMain
	if _, err := toml.DecodeFile("config.toml", &cnf); err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("version: %s\n", cnf.Version)

	fmt.Printf("publisher_prefix: %s\n", cnf.PublisherPrefix)
	fmt.Printf("publisher: %s\n", cnf.Publisher)
	fmt.Printf("ip_range: %s\n", cnf.IpRange)


	fmt.Printf("redis.url: %s\n", cnf.Redis.Url)
	fmt.Printf("mqtt.url: %s\n", cnf.Mqtt.Url)

	fmt.Printf("scanner.discovery_timeout: %d\n", cnf.Scanner.DiscoveryTimeout)

	fmt.Printf("livebox.telnet_user: %s\n", cnf.Livebox.TelnetUser)
	fmt.Printf("livebox.telnet_password: %s\n", cnf.Livebox.TelnetPassword)



	fmt.Printf("energenie.password: %s\n", cnf.Energenie.Password)



	fmt.Printf("DeviceInfo.publisher: %s\n", cnf.Publisher)
	fmt.Printf("DeviceInfo.ip_range: %s\n", cnf.IpRange)





	//spew.Dump(config.DnsGuard.ActiveChannels)
}



func TestNewConfig(t *testing.T) {


	// create new config
	conf := config.NewConfig("./config.toml")
	fmt.Printf("version: %s\n", conf.Version)
	spew.Dump(conf)

	// get a copy of the config
	conf2 := config.GetConfig()
	fmt.Printf("version: %s\n", conf2.Version)
	spew.Dump(conf2)
	assert.Equal(t,conf,conf2)


	// modify the configuration copy
	conf2.Version="0.2"

	// the original configuration should not be affected
	assert.NotEqual(t,conf.Version,conf2.Version)
	fmt.Printf("version: %s\n", conf.Version)


	config.UnsetConfig()

}