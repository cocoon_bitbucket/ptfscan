package config_test


import(

	"testing"
	//"github.com/BurntSushi/toml"
	"github.com/garyburd/redigo/redis"
	"bitbucket.org/cocoon_bitbucket/ptfscan/config"
	//"fmt"
	//
	//"github.com/stretchr/testify/assert"
	//"github.com/davecgh/go-spew/spew"
	"fmt"
)


func get_cnx(url string) (cnx redis.Conn,err error){

	if url == "" { url = "redis://127.0.0.1:6379/0"}

	cnx,err = redis.DialURL(url)
	return cnx,err

}




func TestInfoManager(t *testing.T) {


	config_filename := "./config.toml"

	conf := config.NewConfig( config_filename)
	fmt.Printf("version: %s\n", conf.Version)


	m := config.NewInfoManager()
	_=m

	cnx,_ := get_cnx("")

	m.Setup( cnx, config_filename,conf.Version)



	config.UnsetConfig()

}
