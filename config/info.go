package config


import (

	//"github.com/garyburd/redigo/redis"

	"time"
	"github.com/garyburd/redigo/redis"
	"encoding/json"
	"io/ioutil"
)


/*

	handle ptfscan info store in redis


	ptfscan HASH

		started  start date and time
		config-filename  name of the config file
		config-content  content of the config-filename


 */


type InfoManager struct {

	Version string
	ConfigFilename string
	ConfigContent string

	Started time.Time

}


func NewInfoManager() ( m * InfoManager ) {

	m = &InfoManager{}
	return
}

func (m * InfoManager) Rediskey() string {
	return "ptfscan"
}


func (m * InfoManager) Save(cnx redis.Conn) (err error){

	// marshall
	data,err := json.Marshal(m)
	if err != nil { return err }
	_,err = cnx.Do("SET" , m.Rediskey() ,data)
	return err
}

func (m * InfoManager) Load(cnx redis.Conn) (err error){

	data,err := redis.Bytes(cnx.Do("GET",m.Rediskey()))
	if err != nil { return err }
	err = json.Unmarshal(data,m)
	return err
}


func (m * InfoManager) Setup(cnx redis.Conn,config_filename string, version string) (err error) {

	m.Started = time.Now()
	m.ConfigFilename = config_filename
	m.Version = version
	m.ConfigContent,err = getFileContent(config_filename)

	err = m.Save(cnx)
	return err
}


func InitInfoManager(cnx redis.Conn,filename string ,conf ConfigMain) (err error){

	m := NewInfoManager()
	err = m.Setup(cnx,filename,conf.Version)
	return err
}





func getFileContent(path string) (content string,err error){

	data, err := ioutil.ReadFile(path)
	if err != nil {
		return content ,err
	}
	text := string(data)
	return text,err
}

