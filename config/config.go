package config

import (
	//"fmt"
	//"time"
	//
	"github.com/BurntSushi/toml"
	"log"
    "github.com/davecgh/go-spew/spew"
)

var (
	defaultConfigFilename string = "config.toml"
	defaultPublisherPrefix string = "ptf"

	// the shared configuration ( via config.GetInstance() )
	configuration  * ConfigMain
)


type ConfigMain struct {

	Version string  `toml:"version"`

	IpRange string `toml:"ip_range"`

	Publisher string `toml:"publisher"`
	PublisherPrefix string `toml:"publisher_prefix"`


	Redis RedisConfig

	Mqtt MqttConfig

	Scanner ScannerConfig

	Livebox LiveboxConfig

	Energenie EnergenieConfig


	// predefined task
	Task map[string] TaskParameters

	// dynamic pcbcli request
	PcbCli map[string] TaskParameters


	// reporters
	Reporters map[string] ReporterParameters
}


type RedisConfig struct {
	Url string
}

type MqttConfig struct {
	Url string
}

type ScannerConfig struct {
	LogLevel int `toml:"log_level"`
	DiscoveryTimeout uint  `toml:"discovery_timeout"`
	ScanInterval uint  	`toml:"scan_interval"`
}

type LiveboxConfig struct {

	TelnetUser string		`toml:"telnet_user"`
	TelnetPassword string   `toml:"telnet_password"`

}

type EnergenieConfig struct {
	Password string         `toml:"password"`
}



type TaskParameters struct {

	// log_level 0: no log , 90: max log
	LogLevel int `toml:"log_level"`

	// scan interval between tasks in second
	ScanInterval uint  	`toml:"scan_interval"`
	IpRange string `toml:"ip_range"`

	// discovery timeout , time
	DiscoveryTimeout uint `toml:"discovery_timeout"`

	Publisher string `toml:"publisher"`
	PublisherPrefix string `toml:"publisher_prefix"`

	Request string  	`toml:"request"`

	Device string		`toml:"device"`
	User string		`toml:"user"`
	Password string `toml:"password"`



}
func ( s * TaskParameters) Show(){
	spew.Dump(s)
}



type ReporterParameters struct {

	// log_level 0: no log , 90: max log
	LogLevel int `toml:"log_level"`

	// scan interval between tasks in second
	ScanInterval uint  	`toml:"scan_interval"`

}


// create a new Config
func NewConfig(configFilename string) * ConfigMain {


	if configuration != nil {
		log.Fatal("configuration already instanciated")
	}

	if _, err := toml.DecodeFile(configFilename, &configuration); err != nil {
		log.Fatal(err)
	}

	//spew.Dump(configuration.Task)
	//spew.Dump(configuration.PcbCli)
	if configuration.PublisherPrefix == "" {
		configuration.PublisherPrefix = defaultPublisherPrefix
	}



	return configuration
}

func GetConfig() * ConfigMain {

	if configuration == nil {
		log.Fatal("No Config has been instanciated with NewConfig(filename)")
	}

	// create a new config
	var config ConfigMain
	// copy the original config
	config = * configuration

	// return the copy
	return &config

}

func UnsetConfig() {
	configuration = nil
}

