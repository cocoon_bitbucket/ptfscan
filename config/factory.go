package config

/*

	return a publisher of the desired kind ( screen/redis/mqtt )


 */

import (

	"bitbucket.org/cocoon_bitbucket/ptfscan/publishers"
	"errors"
	//"log"
)




var (

	publisher_kinds =   []string  {"screen","redis","mqtt"}

)



func GetPublisher(kind string ) (publishers.Publisher,error){


	var p publishers.Publisher = nil
	var err error

	cnf := GetConfig()
	_=cnf


	switch kind {

	case "screen" :{

		p,err = publishers.NewScreenPublisher()

	}
	case "redis" : {

		p,err = publishers.NewRedisPublisher(cnf.Redis.Url)

	}
	case "mqtt" : {
		err= errors.New("publisher mqtt not yet implemented")
	}

	default:{

		err= errors.New("publisher not implemented")


	}


	}


	return p,err

}
