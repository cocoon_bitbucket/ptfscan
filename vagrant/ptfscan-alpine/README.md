a vm to deploy ptfscan 



* based on alpine + docker + docker-compose
* install this repository on it




configuration
=============
public network at 192.168.1.101
RAM 8192
login: vagrant/vagrant
sudo password: vagrant


use it
======

vagrant ssh
cd ptfscan/docker
docker-compose up


notes
=====
permits ssh with login password

file: etc_ssh_sshd_config -> /etc/ssh/sshd_config

PermitRootLogin yes
StrictModes no
