package powerswitch



type Powerswitch interface {

	PowerOn( outlet int) error
	PowerOff( outlet int) error
	Status() (map[int]bool, error)
}


func PowerOn( powerswitch Powerswitch , outlet int) error {

	return powerswitch.PowerOn(outlet)

}

func PowerOff( powerswitch Powerswitch , outlet int) error {

	return powerswitch.PowerOff(outlet)

}

func Status( powerswitch Powerswitch ) (map[int]bool,error) {

	return powerswitch.Status()

}



