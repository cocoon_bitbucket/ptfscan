package tests

import (
	"testing"
	"io/ioutil"
	"github.com/stretchr/testify/assert"
	"bitbucket.org/cocoon_bitbucket/ptfscan/powerswitch/energenie"
	//"fmt"
)

/*
	pre-requisites: None

 */




func TestExtractStatusInfo(t *testing.T) {

	// read sample
	text, err := ioutil.ReadFile("./samples/energenie.html")
	assert.Equal(t,err,nil)

	status,err := energenie.ExtractStatusInfo(string(text))
	assert.Equal(t,err,nil)
	assert.Equal(t,len(status),4)
	assert.Equal(t,status[0],true)
	assert.Equal(t,status[1],false)
	assert.Equal(t,status[2],false)
	assert.Equal(t,status[3],false)

}


func TestExtractLanSettingsInfo(t *testing.T) {

	// read sample
	text, err := ioutil.ReadFile("./samples/lan_settings.html")
	assert.Equal(t,err,nil)

	status,err := energenie.ExtractLanSettingsInfo(string(text))

	assert.Equal(t,status["mac"],"88B6270165E4")
	assert.Equal(t,status["dhcpen"],"1")
	assert.Equal(t,status["smask"],"255.255.255.0")

}

func TestEnergenieAddr(t *testing.T) {

	addr,_ := energenie.EnergenenieNetAddr("192.168.1.17")
	assert.Equal(t, addr.String() ,"192.168.1.17:80")

	addr,_ = energenie.EnergenenieNetAddr("192.168.1.17:8080")
	assert.Equal(t, addr.String() ,"192.168.1.17:8080")

	addr,_ = energenie.EnergenenieNetAddr("localhost")
	assert.Equal(t, addr.String() ,"127.0.0.1:80")

	addr,err := energenie.EnergenenieNetAddr("bad")
	assert.Contains(t, err.Error() ,"no such host")

}

