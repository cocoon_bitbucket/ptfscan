package tests



import (
	"testing"
	"time"
	"github.com/stretchr/testify/assert"
	"bitbucket.org/cocoon_bitbucket/ptfscan/powerswitch/energenie"
)

/*
	pre-requisites: None

 */




func TestEnergenieClient(t *testing.T) {


	var err error = nil
	var status map[int]bool

	c ,err := energenie.NewEnergenieClient("192.168.1.17:80", "1")
	assert.Equal(t,err,nil)


	err = c.PowerOff(1)
	status, err =  c.Status()
	assert.Equal(t,status[0],false)

	time.Sleep(1 * time.Second)
	err = c.PowerOn(1)
	status, err =  c.Status()
	assert.Equal(t,status[0],true)

	time.Sleep(1 * time.Second)
	err = c.PowerOn(0)
	status, err =  c.Status()
	assert.Equal(t,status[0],false)



}