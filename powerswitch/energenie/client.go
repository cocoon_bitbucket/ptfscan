package energenie

import (
	"time"
	"net/http"
	"fmt"
	"errors"
	"strings"
	"io/ioutil"
	//"encoding/json"
	//"net"
)

/*

	a powerswitch with a web portal  host:80/energenie.html


	login
		default password: "1"

		POST host/login.html   "pw=<password>"

	logout
		POST host/login.html   "pw="


	status
	   GET host/energenie.html

	   status pattern: "var sockstates = \[(.*?)\]\;"


 */


type EnergenieStatus map[int]bool
type EnergenieLanConfig map[string]string

//


type Energenie struct {
	// structure of a Energenie Element

	http.Client
	Addr string
	Passwd string
	//opened bool
	//logged bool

}


func NewEnergenieClient ( ip ,passwd string ) (* Energenie,error) {
	// return a Energie if we can reach it , return err if not reachable
	//timeout := 1 * time.Second

	timeout := 2 * time.Second
	var err error = nil


	netAddr,err := EnergenenieNetAddr(ip)
	if err != nil {
		return nil,err
	}

	client := http.Client{
		Timeout: timeout,
	}

	// connected
	e := &Energenie{ client, netAddr.String() , passwd}

	err = e.Probe()
	return e, err

}


func (e * Energenie) Probe() error {
	// try to recognise a energenie device
	url := fmt.Sprintf("http://%s",e.Addr)
	r,err:= e.Get(url)
	//r,err:= e.Head(url)
	if err != nil {
		// cannot reach device
		fmt.Println("NewEnergenie.Probe: ",err)
		return err
	}
	// we got a http response
	//fmt.Printf("response (%d) : %s\n",r.StatusCode, r)

	// analyse header to find header server=EG-Web
	if header, ok := r.Header["Server"]; ok {
		server := string(header[0])
		if server == EnergenieServerHeader {
			// YES we got a http HEADER Server equal to EG-Web
			//fmt.Printf("Header Server: %v\n", server)
			return nil
		}
	}
	// not an energenie web server
	err = errors.New("not an energenie device ")
	return err
}



func (e * Energenie)  Login() bool {
	// try to log in , a POST host/login.html   pw=<password>
	url := fmt.Sprintf("http://%s/login.html",e.Addr)
	content_type := "text/plain"
	text := fmt.Sprintf("pw=%s",e.Passwd)
	data := strings.NewReader(text)
	_,err := e.Post(url,content_type,data )
	if err == nil {
		return true
	}
	return false}

func (e * Energenie)  Logout() bool {
	// try to log in , a POST host/login.html   pw=<password>
	url := fmt.Sprintf("http://%s/login.html",e.Addr)
	data := strings.NewReader("pw=")
	_,err := e.Post(url,"text/plain",data )
	if err == nil {
		return true
	}
	return false
}



// implements Powerswitch interface


func (e * Energenie)  PowerOn(outlet int) error {
	// switch on outlet [1-4]
	if outlet < 1 || outlet > 4 {
		err := errors.New("Invalid outlet (must be in [1,4]")
		return err
	}
	e.Logout()
	e.Login()

	// url = self.base_url + "?cte%d=1" % int(channel)
	url := fmt.Sprintf("http://%s?cte%d=1",e.Addr,outlet )
	data := strings.NewReader("")
	_,err := e.Post(url,"text/plain",data )
	return err
}

func (e * Energenie)  PowerOff(outlet int) error {
	// switch off outlet [1-4]
	// url = self.base_url + "?cte%d=1" % int(channel)

	if outlet < 1 || outlet > 4 {
		err := errors.New("Invalid outlet (must be in [1,4])")
		return err
	}

	e.Logout()
	e.Login()

	url := fmt.Sprintf("http://%s?cte%d=0",e.Addr,outlet )
	data := strings.NewReader("")
	_,err := e.Post(url,"text/plain",data )
	//if err == nil {
	//	return true
	//}
	//return false
	return err
}


func (e * Energenie)  Status() ( map [int]bool,error) {
	// try to log in , a POST host/login.html   pw=<password>

	var err error = nil
	response := make( map [int]bool)

	e.Logout()
	e.Login()

	url := fmt.Sprintf("http://%s/energenie.html",e.Addr)
	r,err := e.Get(url)
	if err != nil {
		return response,err
	}
	buf, err := ioutil.ReadAll(r.Body)
	if err == nil {
			// ok
			response, err = ExtractStatusInfo(string(buf))
	}
	return response, err
}


//  other methods



func (e * Energenie)  GetLanConfig() ( map[string]string,error) {
	// try to log in , a POST host/login.html   pw=<password>

	var err error = nil
	response := make(map[string]string)

	e.Logout()
	e.Login()

	url := fmt.Sprintf("http://%s/lan_settings.html",e.Addr)
	r,err := e.Get(url)
	if err != nil {
		return response,err
	}
	buf, err := ioutil.ReadAll(r.Body)
	if err == nil {
		//fmt.Printf("body: %s\n",string(buf))
		// ok
		response, err = ExtractLanSettingsInfo(string(buf))
	}
	return response, err
}


