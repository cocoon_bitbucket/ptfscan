package energenie

import (
	"regexp"
	"fmt"
	"errors"
	"net"
	"strings"
)

var (

	EnergenieServerHeader string = "EG-Web"

	EnergeniePort int = 80

	status_pattern string = `var sockstates = \[([01]),([01]),([01]),([01])\]\;`
	status_regexp * regexp.Regexp

	lan_settings_pattern string = `<input .*?id="(.+?)".*?value="(.*?)".*?\/>`
	lan_settings_regexp * regexp.Regexp

)


func EnergenenieNetAddr( ip string) (net.Addr,error) {


	if strings.Contains(ip,":") {
		// full ip
	} else {
		// add default port
		ip = fmt.Sprintf("%s:%d",ip,EnergeniePort)
	}
	tcpAddr, err := net.ResolveTCPAddr("tcp4", ip)

	return tcpAddr, err
}


func UrlHomePage( ip string) string {
	return fmt.Sprintf("http://%s:%d/energenie.html",ip,EnergeniePort)
}

func UrlLoginPage( ip string) string {
	return fmt.Sprintf("http://%s:%d/login.html",ip,EnergeniePort)
}

func UrlLanSettingsPage( ip string) string {
	return fmt.Sprintf("http://%s:%s/lan_settings.html",ip,EnergeniePort)
}



// utils

func ExtractStatusInfo(text string ) (map[int]bool ,error) {
	// extract status info from web page  energenie.html : search for: var sockets=[1,1,1,1]

	var err error = nil
	result := make (map[int]bool)


	// search for  var sockstates = [1,1,1,1];
	found := status_regexp.FindString( text)
	if len(found)> 0 {
		//fmt.Println(found)

		array := status_regexp.FindStringSubmatch(text)
		// update status
		for i := 0 ; i < 4 ; i++ {
			if array[i+1] == "1" {
				result[i] = true
			} else {
				result[i] = false
			}
		}

	} else {
		msg:= "Energenie cannot find status string in page"
		fmt.Println(msg)
		err = errors.New(msg)
	}


	return result, err
}


func ExtractLanSettingsInfo(text string ) ( map[string]string ,error) {
	// extract lan settings from page lan_settings.html search for <input ... id="" .. value="" .. />

	var err error = nil
	result := make(map[string]string)

	found := lan_settings_regexp.FindString( text)
	if len(found)> 0 {
		//fmt.Println(found)

		array := lan_settings_regexp.FindAllStringSubmatch(text,-1)
		_=array
		for _, v := range array {
			result[v[1]] = v[2]
			//fmt.Printf("%s = %s\n", v[1], v[2])
		}

	} else {
		msg:= "Energenie cannot find inputs in lan_settings.html page"
		fmt.Println(msg)
		err = errors.New(msg)
	}


	return result, err
}



func init() {

	status_regexp = regexp.MustCompile(status_pattern)
	lan_settings_regexp = regexp.MustCompile(lan_settings_pattern)

}
