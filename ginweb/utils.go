package ginweb

import (
	"strings"
	"fmt"
	"os"
	"path"
	"io"
	"net/http"
	"path/filepath"
	"runtime"
	"time"
	"github.com/garyburd/redigo/redis"
)

/*




 */




func FetchAssets() (err error ){

	err = FectchDatatableAssets()
	err = FetchRamlAssets()

	return err
}


func FetchRamlAssets() (err error){

	var assets = []string{
		"https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css",
		"https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.3.0/styles/default.min.css",
		"https://code.jquery.com/jquery-1.11.0.min.js",
		"https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js",
		"https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.3.0/highlight.min.js",
	}
	destination := path.Join(here(),"assets/raml")

	for _,url := range assets {
		downloadFromUrl(url,destination)
	}

	return err

}

func FectchDatatableAssets()( err error ){

	/*
		need to add glyphicons

		/assets/datatables/Bootstrap-3.3.7/fonts/glyphicons-halflings-regular.woff
		/assets/datatables/Bootstrap-3.3.7/fonts/glyphicons-halflings-regular.woff2

	 */

	var assets = []string{
		"https://cdn.datatables.net/v/bs-3.3.7/jqc-1.12.3/dt-1.10.16/b-1.5.1/datatables.min.css",
		"https://cdn.datatables.net/v/bs-3.3.7/jqc-1.12.3/dt-1.10.16/b-1.5.1/datatables.min.js",
	}
	destination := path.Join(here(),"assets/datatables")

	err = os.MkdirAll(destination,0777)
	for _,url := range assets {
		downloadFromUrl(url,destination)
	}

	// add glyphicons directory
	gd := path.Join(destination,"Bootstrap-3.3.7/fonts")
	err = os.MkdirAll(gd,0777)
	fmt.Printf("Dont forget to manually download glyphicons-halflings-regular.woff and .woff2 to %s",gd)

	return err
}






func here() (path string) {
	// return the directory of this file (server.go) eg the ginweb root directory
	_, filename, _, ok := runtime.Caller(1)
	if ok {
		path = filepath.Dir(filename)
	}
	return path
}



func downloadFromUrl(url string, destination string) {


	tokens := strings.Split(url, "/")
	fileName := tokens[len(tokens)-1]
	fmt.Println("Downloading", url, "to", fileName)

	filePath := path.Join(destination,fileName)
	output, err := os.Create(filePath)
	if err != nil {
		fmt.Println("Error while creating", filePath, "-", err)
		return
	}
	defer output.Close()

	response, err := http.Get(url)
	if err != nil {
		fmt.Println("Error while downloading", url, "-", err)
		return
	}
	defer response.Body.Close()

	n, err := io.Copy(output, response.Body)
	if err != nil {
		fmt.Println("Error while downloading", url, "-", err)
		return
	}

	fmt.Println(n, "bytes downloaded.")
}


func MakeRedisPool(redisUrl string ) redis.Pool {

	//var redisUrl= "redis://localhost:6379/0"

	pool :=  redis.Pool{
		MaxIdle: 3,
		//MaxActive: 50,
		IdleTimeout: 240 * time.Second,
		Dial: func () (redis.Conn, error) { return redis.DialURL(redisUrl) },

		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
	return pool
}
