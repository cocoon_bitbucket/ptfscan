
package ginweb

import (

	"net/http"
	"github.com/gin-gonic/gin"
	//"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	//"bitbucket.org/cocoon_bitbucket/ptfscan/reporters/ipscan"

	"time"
	"io/ioutil"
	//"fmt"
)

/*

	context:
		use context.InfraDb


 */

type SampleTable struct {
	Name string
	Position string
	Office string
	Age string
	StartDate string
	Salary string
}

//var sample []SampleTable



func DocumentationRoute( r * gin.Engine) * gin.Engine {

	// group livebox
	g := r.Group("/doc")
	{
		g.GET("/", func(c *gin.Context) {
			//  /doc home
			//c.String(200,"documentation home")

			c.HTML(http.StatusOK, "documentation.tmpl", map[string]interface{}{
				"now": time.Now(),
			})
		})

		//g.GET("/ipscan", func(c *gin.Context) {
		//	//  /doc ipscan
		//	pool,_:= redistools.GetPool()
		//	cnx := pool.Get()
		//	defer cnx.Close()
		//
		//	items,err  := ipscan.CollectDeviceInfo(cnx)
		//	if err != nil {
		//		c.HTML(http.StatusBadRequest,"",nil)
		//	}
		//
		//	var labels = []string{"Name","Ip","Mac","Uptime","Status"}
		//
		//	c.HTML(http.StatusOK, "livebox_ipscan.tmpl",gin.H{
		//		"labels": labels,
		//		"items": items,
		//	})
		//})


		g.GET("/config", func(c *gin.Context) {

			/*
			s = Server.HtmlEncode(s);
			s = s.Replace("\n", "<br />");

		 	*/
			data, err := ioutil.ReadFile("ptfscan.toml")
			if err != nil {
				c.String(404,"cannot find ptfscan.toml")
			}
			text := string(data)

			c.String(200, text)
			//c.HTML(http.StatusOK, "documentation.tmpl", nil)
		})




	}
	return r
}