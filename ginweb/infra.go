package ginweb

import (

	"github.com/gin-gonic/gin"
	"bitbucket.org/cocoon_bitbucket/ptfscan/infra/models"
)


func InfraRoute( r * gin.Engine) * gin.Engine {

	// group api/infra
	g := r.Group("/api/infra")
	{
		g.GET("/", func(c *gin.Context) {
			c.JSON(200, gin.H{"result": "api/infra", "error": nil ,"command": "api_infra" })
		})

		g.GET("/livebox", func(c *gin.Context) {

			//
			//  return all livebox data
			//

			// load infra db from context
			db := c.MustGet("InfraDb").(models.InfraDb)
			data, err := db.GetInfraLivebox()
			if err != nil {
				c.JSON(500, gin.H{"result": nil, "error": err.Error()})
			} else {
				c.JSON(200, gin.H{"result": data, "error": nil})
			}

			//c.String(200, "blob data for livebox ")
		})

		g.GET("/livebox/:name", func(c *gin.Context) {

			//
			// return infra data for a specific livebox
			//

			name := c.Param("name")
			// load infra db from context
			db := c.MustGet("InfraDb").(models.InfraDb)
			// get livebox dictionary
			data, err := db.GetInfraLivebox()
			if err != nil {
				c.JSON(500, gin.H{"result": nil, "error": err.Error()})
			} else {
				// select info for this livebox
				lb, ok := data[name]
				if ok {
					// ok
					c.JSON(200, gin.H{"result": lb, "error": nil, "livebox": name})
				} else {
					// no such livebox
					c.JSON(404, gin.H{"result": nil, "error": "no such livebox", "livebox": name})
				}
			}

		})

		return r
	}
}

//func loadLiveboxinfra( name string, c *gin.Context) {
//
//
//	// load infra db from context
//	db := c.MustGet("InfraDb").(models.InfraDb)
//
//	data,err := db.GetInfraLivebox()
//	if err != nil {
//		return nil,err
//
//	}
//
//}