package ginweb

import (

	"github.com/gin-gonic/gin"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	alert "bitbucket.org/cocoon_bitbucket/ptfscan/prometheus"
	"io/ioutil"
	"fmt"
)



func AlertRoute( r * gin.Engine) * gin.Engine {


	// create alert model
	pool,_ := redistools.GetPool()
	cnx := pool.Get()
	defer cnx.Close()
	alert.CreateAlertEntitySchema(cnx)

	// group /alert
	g := r.Group("/alert")
	{
		g.GET("/", func(c *gin.Context) {
			c.JSON(200, gin.H{"result": "alert", "error": nil ,"command": "alert" })
		})

		g.POST("/ptfscan", func(c *gin.Context) {

			//
			//  register the received alert push to alert:pfscan <LIST>
			//
			x, _ := ioutil.ReadAll(c.Request.Body)
			fmt.Printf("%s", string(x))

			pool,err := redistools.GetPool()
			if err != nil {
				c.JSON(500, gin.H{"result": nil, "error": err.Error()})
			}

			handler,err := alert.NewAlertHandler(pool.Pool)
			handler.AddRecord(string(x))

			cnx := pool.Get()
			_,err = cnx.Do("RPUSH","alert/ptfscan",x)

			c.JSON(200, "ok")

		})

		return r
	}
}


/*

	sample alert

{
    "receiver": "ptfscan",
    "status": "firing",
    "alerts": [
        {
            "status": "firing",
            "labels": {
                "alertname": "InstanceDown",
                "group": "livebox",
                "instance": "192.168.99.100:8081",
                "job": "device",
                "severity": "page"
            },
            "annotations": {
                "description": "192.168.99.100:8081 of job device has been down for more than 5 minutes.",
                "summary": "Instance 192.168.99.100:8081 down"
            },
            "startsAt": "2018-03-23T13:36:22.897819632Z",
            "endsAt": "0001-01-01T00:00:00Z",
            "generatorURL": "http://ddd76268fa2a:9090/graph?g0.expr=up+%3D%3D+0&g0.tab=1"
        },
        {
            "status": "firing",
            "labels": {
                "alertname": "InstanceDown",
                "group": "energenie",
                "instance": "192.168.99.100:8082",
                "job": "device",
                "severity": "page"
            },
            "annotations": {
                "description": "192.168.99.100:8082 of job device has been down for more than 5 minutes.",
                "summary": "Instance 192.168.99.100:8082 down"
            },
            "startsAt": "2018-03-23T13:36:22.897819632Z",
            "endsAt": "0001-01-01T00:00:00Z",
            "generatorURL": "http://ddd76268fa2a:9090/graph?g0.expr=up+%3D%3D+0&g0.tab=1"
        }
    ],
    "groupLabels": {
        "alertname": "InstanceDown"
    },
    "commonLabels": {
        "alertname": "InstanceDown",
        "job": "device",
        "severity": "page"
    },
    "commonAnnotations": {},
    "externalURL": "http://9e23013b3199:9093",
    "version": "4",
    "groupKey": "{}:{alertname=\"InstanceDown\"}"
}



 */

