package ginweb

import (

	"net/http"
	//"html/template"
	"github.com/gin-gonic/gin"
	"bitbucket.org/cocoon_bitbucket/ptfscan/config"
	"bitbucket.org/cocoon_bitbucket/ptfscan/infra/middlewares"
	"bitbucket.org/cocoon_bitbucket/ptfscan/ginweb/admin"

	"time"
	"runtime"
	"path/filepath"

	// for prometheus metrics
	"github.com/prometheus/client_golang/prometheus/promhttp"
)


func get_config() * config.ConfigMain{

	// return the global config (
	cnf := config.GetConfig()

	//password := cnf.Energenie.Password

	return cnf

}

func rootDirectory() (path string) {
	// return the directory of this file (server.go) eg the ginweb root directory
	_, filename, _, ok := runtime.Caller(1)
	if ok {
		path = filepath.Dir(filename)
	}
	return path
}

func ginwebPath( path string) (string) {
    // return the absolute path
	root := rootDirectory()
	abs_path := filepath.Join(root, path)
	return abs_path
}


func Serve( cnf * config.ConfigMain) {


	// create a basic router
	r := gin.Default()


	// load the templates
	r.LoadHTMLGlob(ginwebPath("templates/*"))

	// serve static file /static/layout.css
	//r.Static("/assets", "./ginweb/assets")
	r.Static("/assets", ginwebPath("assets"))


	// serve prometeus metrics
	r.GET("/metrics", gin.WrapH(promhttp.Handler()))

	// extract redis url from config
	redis_url := cnf.Redis.Url

	// add RedisMiddleware  so handlers can use  cnx := c.MustGet("RedisDb").(*redis.Conn)
	r.Use(RedisDbMiddleware(redis_url))

	// add InfraDbMiddleware  so handlers can use  cnx := c.MustGet("InfraDb").(*models.InfraDb)
	r.Use( middlewares.InfraDbMiddleware(redis_url))


	// Home Page
	r.GET("/", PtfscanHomePage)

	r.GET("/api", func(c *gin.Context) {
		//c.String(200, "livebox index")
		c.JSON(200, gin.H{"result": "api", "error": nil ,"command": "api_index" })
	})


	// add documentation routes
	DocumentationRoute(r)

	// add livebox routes
	LiveboxRoute(r)

	// add Energenie routes
	EnergenieRoute(r)

	// add Supervisor routes
	SupervisorRoute(r)


	// add Device router
	DeviceLiveboxRoute(r)

	// add Infra router
	InfraRoute(r)

	// add Alert router  /alert/
	AlertRoute(r)

	// add admin router
	admin.AdminRoute(r)


	// add proxy router
	ProxyRoute(r)


	// we'll pass in configuration later
	r.Run(":3333")


}


func PtfscanHomePage (c *gin.Context) {
	// home page of the ptfscan server
	//c.String(200, "home of ptfscan server")
	c.HTML(http.StatusOK, "home.tmpl", map[string]interface{}{
		"now": time.Now(),
	})
}
