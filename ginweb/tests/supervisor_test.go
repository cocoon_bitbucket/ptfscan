package ginweb_test

import(
	"testing"
	"fmt"
	"bitbucket.org/cocoon_bitbucket/ptfscan/config"
	"bitbucket.org/cocoon_bitbucket/ptfscan/ginweb"
	"github.com/davecgh/go-spew/spew"
	"context"
	"log"
	"time"
)



func TestSupersvisor_HeartbeatSupervisor(t *testing.T) {


	conf := config.NewConfig("./config.toml")
	fmt.Printf("version: %s\n", conf.Version)
	spew.Dump(conf)

	ctx := context.Background()
	out_channel,err := ginweb.HeartbeatSupervisor(ctx,"*",10  * time.Second)
	if err != nil {
		log.Fatal(fmt.Sprintf("error : %s", err.Error()))
	}

	for msg := range out_channel {
		fmt.Printf("%s : %s \n" ,msg.Channel,msg.Data)
	}

}


