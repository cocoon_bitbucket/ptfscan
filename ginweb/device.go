package ginweb

import (

	"net/http"
	"github.com/gin-gonic/gin"
	"bitbucket.org/cocoon_bitbucket/ptfscan/livebox"
	"time"
	//"github.com/garyburd/redigo/redis"
	"bitbucket.org/cocoon_bitbucket/ptfscan/infra/models"
	"fmt"
	"strings"
	//"strconv"
	"errors"
)

/*

	context:
		use context.InfraDb



 */


func DeviceLiveboxRoute( r * gin.Engine) * gin.Engine {

	// group livebox
	g := r.Group("/api/device/livebox")
	g.Use(TokenAuthMiddleware())
	{

		g.GET("/", func(c *gin.Context) {
			//  /api/device/livebox => check token
			allowed,err := CheckDeviceToken(c)
			if err != nil {
				return
			}
			c.JSON(200, gin.H{"result": allowed, "error": nil ,"command": "index" })
		})

		g.GET("/:name", func(c *gin.Context) {
			//  GET /api/device/livebox/:name -> check we can access this box
			livebox_name := c.Param("name")
			// check livebox acl
			err := CheckDeviceAcl(livebox_name,c)
			if err != nil {
				return
			}
			c.JSON(200, gin.H{"result": "", "error": nil ,"command": "livebox_access" ,"livebox":livebox_name})
		})

		g.POST("/:name/power_on",DevicePowerOn)
		g.POST("/:name/power_off",DevicePowerOff)
		g.POST("/:name/power_status",DevicePowerStatus)
		g.GET("/:name/power_status",DevicePowerStatus)


		g.POST("/:name/shell", DeviceLiveboxShellCommand)

		// set a pcb cli value  ( example { key:"Device.UPnP.Device.Enable" , value:"1"
		g.POST("/:name/pcbcli" ,  DeviceLiveboxPcbliSetCommand )

		// read a pcbli
		g.GET("/:name/pcbcli/:key" ,  DeviceLiveboxPcbliRequestCommand )


		g.POST("/:name/reboot", func(c *gin.Context) {

			ip := c.Param("ip")
			addr := ip + ":23"

			var json TelnetUserCredentials
			err := c.BindJSON(&json)
			if err == nil {
				if json.User == "" && json.Password == "" {
					// take the default livebox root/sah
					json.User = "root"
					json.Password = "sah"
				}
			} else {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
				return
			}
			// the full cmd string is in the body
			user := json.User
			password := json.Password

			cmd := "reboot"

			// run the command
			result,err := livebox.RunShellCommandOnLivebox(addr,cmd,user,password,1* time.Second)
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
				return
			}

			c.JSON(200, gin.H{"result": result, "error": nil ,"command": "livebox_rebbot", "ip": ip, "user": user })
		})

	}
	return r
}



func DeviceLiveboxShellCommand (c *gin.Context) {

	name := c.Param("name")

	ip, pdu_tag, err := GetLiveboxIp(name,c)
	if err != nil {
		c.Abort()
		return
	}

	addr:= ip + ":23"
	param,err := CommandRequest(c)
	if err != nil {
		_=param
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// the full cmd string is in the body
	cmd := param.Cmd
	user := param.User
	password := param.Password

	// run the command
	result,err := livebox.RunShellCommandOnLivebox(addr, cmd, user , password  , 2 * time.Second)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(200, gin.H{"result": result, "error": err ,"device": name, "ip": ip , "pdu_tag": pdu_tag})

}


func DeviceLiveboxPcbliSetCommand (c *gin.Context) {
	// send a shell command of type  pcbcli <key>=<value>
	name := c.Param("name")


	ip,_ ,err := GetLiveboxIp(name,c)
	if err != nil {
		c.Abort()
		return
	}

	addr:= ip + ":23"

	var json PcbcliSetRequest

	// decode json payload
	err = c.BindJSON(&json)
	if err == nil {
		if json.User == "" && json.Password == "" {
			// take the default livebox root/sah
			json.User = livebox.DefaultLiveboxTelnetUser  			//"root"
			json.Password = livebox.DefaultLiveboxTelnetPassword 	//"sah"
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// the full cmd string is in the body
	key := json.Key
	value := json.Value
	user := json.User
	password := json.Password

	// set the shell command eg pcb_cli key=1
	client,err := livebox.NewTelnetClient(addr,user,password,2* time.Second)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// run the command
	result,err := client.ExecPcbcliSet(key,value)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if len(result) != 1 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "keyword may not exists or not settable"})
		return
	}
	// response OK
	c.JSON(200, gin.H{"result": result, "error": nil ,"command": "pcbcli_set", "livebox": ip, "user": user })

}

func DeviceLiveboxPcbliRequestCommand (c *gin.Context) {
	// send a shell command of type  pcbcli <key>=<value>

	name := c.Param("name")
	key := c.Param("key")

	client,err := GetLiveboxClient(name,c)
	if err != nil {
		c.Abort()
		return
	}
	defer client.Close()

	// run the command
	result,err := client.ExecPcbcliRequest(key, 5 * time.Second)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if len(result) <= 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "no result, keyword may not exists"})
		return
	}
	// response OK
	c.JSON(200, gin.H{"result": result, "error": nil ,"command": "pcbcli_request", "livebox": name})

}



// power ON/ OFF functions send command to powerswitch energenie

func getEnergenieConfig(c *gin.Context) (ip string ,outlet string , err error){

	name := c.Param("name")

	// get address of pdu from Infra Database
	_, pdu_tag, err := GetLiveboxIp(name, c)
	if err != nil {
		c.Abort()
		return
	}

	// pdu_tag  eg 192.168.1.17/1
	ip = ""
	outlet = "0"
	err = errors.New(fmt.Sprintf("bad format for pdu_tag: %s",pdu_tag))
	if strings.Contains(pdu_tag,"/"){
		parts := strings.Split(pdu_tag,"/")
		if len(parts) == 2 {
			ip = parts[0]
			outlet = parts[1]
			err = nil
		}
	}
	if err != nil {
		c.JSON( 400, gin.H{"result": false, "error": err.Error() , "livebox": name})
		c.Abort()
		return
	}
	return ip,outlet, err
}


func DevicePowerStatus( c *gin.Context) {

	name := c.Param("name")
	command := "power_status"

	ip,outlet,err := getEnergenieConfig(c)
	if err != nil {
		return
	}

	status,err := energenie_outlet_status(ip,outlet)

	if err != nil {
		c.JSON(500, gin.H{"result": false, "error": err.Error(), "command": command, "livebox": name, "pdu_ip": ip, "pdu_outlet": outlet})
	} else {
		c.JSON(200, gin.H{"result": status, "error": nil, "command": command, "livebox": name, "pdu_ip": ip, "pdu_outlet": outlet})
	}
}


func DevicePowerOn ( c *gin.Context) {
	DevicePower(true,c)
}

func DevicePowerOff ( c *gin.Context) {
	DevicePower(false,c)
}

func DevicePower ( on bool, c *gin.Context) {
	// send a shell command of type  pcbcli <key>=<value>
	name := c.Param("name")

	operation := "OFF"
	if on != false {
		operation= "ON"
	}
	command := fmt.Sprintf("power_%s",operation)

	ip,outlet,err := getEnergenieConfig(c)
	if err != nil {
		return
	}


	err = EnergenieSwitch( ip ,outlet,operation )
	if err != nil {
		c.JSON( 500, gin.H{"result": false, "error": err.Error() ,"command": command, "livebox": name,"pdu_ip": ip ,"pdu_outlet": outlet})
	} else {
		c.JSON(200, gin.H{"result": true, "error": nil, "command": command, "livebox": name, "pdu_ip": ip, "pdu_outlet": outlet})
	}
}



// utility functions

func GetLiveboxIp (device_name string, c *gin.Context) ( ip string, pdu_tag string,err error){
	//
	// return a livebox ip and pdu_tag from infra database  (infra:livebox )
	//

	name := device_name

	// load infra db from context
	db := c.MustGet("InfraDb").(models.InfraDb)
	data, err := db.GetDeviceIp(name)
	if err == nil {
		ip = data.DeviceIp.String()
		pdu_tag = fmt.Sprintf("%s/%d", data.PduIp.String(), data.PduOutlet)
		return ip,pdu_tag,err
	} else {
		ip = "?"
		pdu_tag = "?"
		c.JSON(404, gin.H{"result": nil, "error": err.Error(), "device": name, "ip": ip, "pdu_tag": pdu_tag})
		return ip,pdu_tag,err
	}
}

func GetLiveboxClient (livebox_name string, c *gin.Context) ( client *livebox.TelnetClient,err error){
	//
	// return a livebox ip and pdu_tag from infra database  (infra:livebox )
	//

	client= &livebox.TelnetClient{}

	// check livebox acl
	err = CheckDeviceAcl(livebox_name,c)
	if err != nil {
		return client,err
	}


	ip,_,err := GetLiveboxIp(livebox_name,c)
	if err != nil {
		return client,err
	}

	addr:= ip + ":23"

	user:= livebox.DefaultLiveboxTelnetUser  			// root
	password := livebox.DefaultLiveboxTelnetPassword  	// "sah"


	// set the shell command eg pcb_cli key=1
	client,err = livebox.NewTelnetClient(addr,user,password,2* time.Second)
	if err != nil {
		c.JSON(http.StatusGatewayTimeout, gin.H{"error": err.Error(), "device": livebox_name, "ip": ip })
		return client,err
	}

	return client,err

}


func CheckDeviceAcl (device_name string, c *gin.Context) ( err error){
	//
	// return nil if device not allowed with the given authorization
	//

	err = nil

	// load infra db from context
	db := c.MustGet("InfraDb").(models.InfraDb)
	auth := c.MustGet(Context_OauthToken)    // OauthToken -> -Bearer 01020303....

	stoken := auth.(string)
	if strings.HasPrefix( stoken ,"Bearer "){
		stoken = strings.TrimSpace(stoken[7:])
	} else {
		// the authorization must begin with Bearer
		err = errors.New("Authorization is not a Bearer")
		c.JSON(http.StatusForbidden, gin.H{"error": err.Error(), "device": device_name})
		return err
	}
	// we got token:  search for the device:acl
	if stoken == "Livebox-demo" {
		// access to livebox demo is always allowed
		return nil
	}

	b,err := db.CheckAcl("device",stoken,device_name)
	if err != nil {
		c.JSON(500, gin.H{"error": err.Error(), "device": device_name})
		return err
	}
	if b == false {
		err = errors.New("Device access Not allowed")
		c.JSON(http.StatusForbidden, gin.H{"error": err.Error(), "device": device_name ,"token":stoken})
		return err
	}
	// member allowed
	return nil
}

func CheckDeviceToken (c *gin.Context) (members []string, err error){
	//
	// check the token is valid and return list of members
	//

	err = nil

	// load infra db from context
	db := c.MustGet("InfraDb").(models.InfraDb)
	auth := c.MustGet(Context_OauthToken)    // OauthToken -> -Bearer 01020303....

	stoken := auth.(string)
	if strings.HasPrefix( stoken ,"Bearer "){
		stoken = strings.TrimSpace(stoken[7:])
	} else {
		// the authorization must begin with Bearer
		err = errors.New("Authorization is not a Bearer")
		c.JSON(http.StatusForbidden, gin.H{"error": err.Error()})
		return members,err
	}

	// we got a token:  search for the device:acl
	if stoken == "Livebox-demo" {
		// access to livebox demo is always allowed
		members = append(members,"Livebox-demo")
		return members,nil
	}
	// check token validity , return members if valid
	acl,err := db.GetAcl(stoken,"device")
	if err != nil {
		// cannot get acl
		return members,err
	}
	members= acl.Members
	return members,nil

}



