package ginweb

import (

	"net/http"
	"github.com/gin-gonic/gin"
	"bitbucket.org/cocoon_bitbucket/ptfscan/livebox"
	"time"
)



func LiveboxRoute( r * gin.Engine) * gin.Engine {

	// group livebox
	g := r.Group("/api/livebox")
	{

		g.GET("/", func(c *gin.Context) {
			//c.String(200, "livebox index")
			c.JSON(200, gin.H{"result": "", "error": nil ,"command": "index" })
		})

		g.GET("/:ip", func(c *gin.Context) {
			ip := c.Param("ip")
			c.JSON(200, gin.H{"result": "", "error": nil ,"command": "livebox" ,"ip":ip})
		})


		g.POST("/:ip/shell", LiveboxShellCommand)

		// set a pcb cli value
		g.POST("/:ip/set_pcbcli" ,  LiveboxPcbliSetCommand )

		// read a pcbli
		g.POST("/:ip/get_pcbcli/:key" ,  LiveboxPcbliRequestCommand )


		g.POST("/:ip/reboot", func(c *gin.Context) {

			ip := c.Param("ip")
			addr := ip + ":23"

			var json TelnetUserCredentials
			err := c.BindJSON(&json)
			if err == nil {
				if json.User == "" && json.Password == "" {
					// take the default livebox root/sah
					json.User = "root"
					json.Password = "sah"
				}
			} else {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
				return
			}
			// the full cmd string is in the body
			user := json.User
			password := json.Password

			cmd := "reboot"

			// run the command
			result,err := livebox.RunShellCommandOnLivebox(addr,cmd,user,password,1* time.Second)
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
				return
			}

			c.JSON(200, gin.H{"result": result, "error": nil ,"command": "livebox_reboot", "ip": ip, "user": user })
		})

	}

	return r
}



// Binding from JSON
type TelnetUserCredentials struct {

	User     string `json:"user"`
	Password string `json:"password"`
}


type ShellCommandRequest struct {
	Cmd      string `json:"cmd" binding:"required"`

	TelnetUserCredentials

	//User     string `json:"user"`
	//Password string `json:"password"`
}


type PcbcliSetRequest struct {

	Key     string `json:"key" binding:"required"`
	Value   string `json:"value" binding:"required"`

	TelnetUserCredentials

	//User     string `json:"user"`
	//Password string `json:"password"`
}


func LiveboxShellCommand (c *gin.Context) {

	ip := c.Param("ip")
	addr:= ip + ":23"
	param,err := CommandRequest(c)
	if err != nil {
		_=param
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// the full cmd string is in the body
	cmd := param.Cmd
	user := param.User
	password := param.Password

	// run the command
	result,err := livebox.RunShellCommandOnLivebox(addr, cmd, user , password  , 2 * time.Second)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(200, gin.H{"result": result, "error": nil ,"command": cmd, "livebox_shell": ip, "user": user })

}


func LiveboxPcbliSetCommand (c *gin.Context) {
	// send a shell command of type  pcbcli <key>=<value>

	ip := c.Param("ip")
	addr:= ip + ":23"

	var json PcbcliSetRequest

	// decode json payload
	err := c.BindJSON(&json)
	if err == nil {
		if json.User == "" && json.Password == "" {
			// take the default livebox root/sah
			json.User = "root"
			json.Password = "sah"
		}
	} else {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// the full cmd string is in the body
	key := json.Key
	value := json.Value
	user := json.User
	password := json.Password

	// set the shell command eg pcb_cli key=1
	client,err := livebox.NewTelnetClient(addr,user,password,2* time.Second)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// run the command
	result,err := client.ExecPcbcliSet(key,value)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if len(result) != 1 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "keyword may not exists or not settable"})
		return
	}
	// response OK
	c.JSON(200, gin.H{"result": result, "error": nil ,"command": "pcbcli_set", "livebox": ip, "user": user })

}

func LiveboxPcbliRequestCommand (c *gin.Context) {
	// send a shell command of type  pcbcli <key>=<value>

	ip := c.Param("ip")
	key := c.Param("key")
	addr:= ip + ":23"

	var json TelnetUserCredentials

	// decode json payload
	err := c.BindJSON(&json)
	if err == nil {
		if json.User == "" && json.Password == "" {
			// take the default livebox root/sah
			json.User = "root"
			json.Password = "sah"
		}
	} else {
		if err.Error() != "EOF" {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		} else {
			// empty json body
			json.User = "root"
			json.Password = "sah"
		}
	}
	user := json.User
	password := json.Password

	// set the shell command eg pcb_cli key=1
	client,err := livebox.NewTelnetClient(addr,user,password,2* time.Second)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// run the command
	result,err := client.ExecPcbcliRequest(key, 5 * time.Second)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if len(result) <= 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "no result, keyword may not exists"})
		return
	}
	// response OK
	c.JSON(200, gin.H{"result": result, "error": nil ,"command": "pcbcli_request", "livebox": ip, "user": user })

}


func CommandRequest (c *gin.Context ) (ShellCommandRequest,error) {
	// extract user credentials from json request
	var json ShellCommandRequest

	err := c.BindJSON(&json)
	if err == nil {
		if json.User == "" && json.Password == "" {
			// take the default livebox root/sah
			json.User = "root"
			json.Password = "sah"
		}
	} else {
		if err.Error() != "EOF" {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return json,err
		} else {
			// empty json body
			err = nil
			json.User = "root"
			json.Password = "sah"
			return json,err
		}
	}
	return json ,err
}
