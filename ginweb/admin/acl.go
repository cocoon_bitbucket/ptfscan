package admin

import (

	"net/http"
	"github.com/gin-gonic/gin"
	"bitbucket.org/cocoon_bitbucket/ptfscan/infra/models"

)


/*

	admin api to manage acl


 */


type AclResponse struct {

	Error string 	`json:"error"`
	Result string 	`json:"result"`   // the token

}


type NewAcl_Request struct {

	Realm     string 	`json:"realm"`
	Ttl      int    	`json:"ttl"`

	Members []string 	`json:"members"`

}

type NewAcl_Response struct {

	Error string 	`json:"error"`
	Result string 	`json:"result"`   // the token
}


func NewAcl (c *gin.Context) {

	//  POST admin/api/acl
	status := 500
	response := NewAcl_Response{"NotImplemented","" }

	db := c.MustGet("InfraDb").(models.InfraDb)

	var request NewAcl_Request
	// decode json payload
	err := c.BindJSON(&request)
	if err == nil {
		r,err := db.CreateAcl(request.Realm,request.Ttl,request.Members...)
		if err != nil {
			status = 500
			response.Result=""
			response.Error = err.Error()
		} else {
			status =200
			response.Error= ""
			response.Result= r
		}
	} else {
		// bad Json request
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(status, response)

}



type AclOfRealm map[string][]string   // { "token1":["r1","r2],"token2":["r3","r4"]}

type GetAclRealm_Response struct {
	Error string 			`json:"error"`
	Realm string			`json:"realm"`
	Result AclOfRealm	    `json:"result"`   // dict of token : ... members
}


func GetAclRealm (c *gin.Context) {

	// GET /admin/api/acl/:realm
	realm := c.Param("realm")

	status := 200
	response := GetAclRealm_Response{}
	response.Realm = realm


	db := c.MustGet("InfraDb").(models.InfraDb)
	r,err := db.GetAclOfRealm(realm)
	if err != nil {
		status = 500
		response.Result= nil
		response.Error = err.Error()
	} else {
		status =200
		response.Error= ""
		response.Result= r
	}

	c.JSON(status, response)

}






