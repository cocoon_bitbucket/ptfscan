package admin


import (

	"github.com/gin-gonic/gin"

//"bitbucket.org/cocoon_bitbucket/ptfscan/infra/models"

)


func AdminRoute( r * gin.Engine) * gin.Engine {

	// group api/infra
	g := r.Group("/admin/api")
	{

		g.GET("/", func(c *gin.Context) {
			//c.String(200, "livebox index")
			c.JSON(200, gin.H{"result": "admin/api", "error": nil ,"command": "admin_api_index" })
		})


		g.POST("/acl", NewAcl )
		g.GET("/acl/:realm", GetAclRealm )



	}
	return r
}




