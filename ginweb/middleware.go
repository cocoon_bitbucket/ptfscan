package ginweb

import (

	"github.com/gin-gonic/gin"
	"github.com/garyburd/redigo/redis"
	"time"
	"log"
)


var (

	Context_RedisDb string = "RedisDb"
	Context_OauthToken  string = "OauthToken"
	Context_RedisPool   string = "RedisPool"

)


/*
	usage

	in main

	r.Use( RedisDbMiddleware(redisUrl))

	in handler
	db := c.MustGet("DB").(*redis.Conn)


 */

func respondWithError(code int , message string, c *gin.Context) {
	resp := map[string]string{"error": message}
	c.JSON(code, resp)
	c.Abort()
}


func RedisPoolMiddleware( redisUrl string) gin.HandlerFunc {
	//
	// a middleware to add a redis pool
	//

	pool := MakeRedisPool(redisUrl)

	return func(c *gin.Context) {
		// set attribute "RedisPool" to cnx in context
		c.Set(Context_RedisPool, pool)
		c.Next()
	}
}



func RedisDbMiddleware( redisUrl string) gin.HandlerFunc {
	//
	// a midleware to add a redis connection to the context
	//

	option := redis.DialConnectTimeout( 1 * time.Second)
	cnx, err := redis.DialURL(redisUrl,option)
	if err != nil {
		log.Printf("cannot create redis connection: %s",err.Error())
		panic(err)
	}

	return func(c *gin.Context) {
		// set attribute "RedisDb" to cnx in context
		c.Set(Context_RedisDb, cnx)
		c.Next()
	}
}


func TokenAuthMiddleware() gin.HandlerFunc {

	return func(c *gin.Context) {

		// fetch token from Headers
		//token := c.Request.FormValue("api_token")

		token := ""
		if values, _ := c.Request.Header["Authorization"]; len(values) > 0 {
			token = values[0]
		}

		// if no oauth token : 401 error
		if token == "" {
			respondWithError(401, "API token required", c)
			return
		}

		//// if invalid token : 401 error
		//if token != os.Getenv("API_TOKEN") {
		//	respondWithError(401, "Invalid API token", c)
		//	return
		//}

		// set context.OauthToken with the token
		c.Set( Context_OauthToken, token)
		c.Next()
	}
}

