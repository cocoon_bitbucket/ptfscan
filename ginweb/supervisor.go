package ginweb



import (


	"time"
	"io"
	"github.com/gin-gonic/gin"
	"net/http"
	//"strconv"
	//"errors"
	"bitbucket.org/cocoon_bitbucket/ptfscan/config"
	"bitbucket.org/cocoon_bitbucket/ptfscan/subscribers"
	//"bitbucket.org/cocoon_bitbucket/ptfscan/portScanner/inspectors"

	//"time"
	//"strings"
	//"strconv"
	"fmt"
	"context"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	"bitbucket.org/cocoon_bitbucket/ptfscan/reporters/ipscan"
)



func SupervisorRoute( r * gin.Engine) * gin.Engine {

	// group supervisor
	g := r.Group("/supervisor")
	{

		g.GET("/", func(c *gin.Context) {
			//  supervision home

			c.HTML(http.StatusOK, "supervisor.html", map[string]interface{}{
				"now": time.Now(),
			})
		})


		g.GET("/pubsub/:channel", func(c *gin.Context) {
			//  channel is one of * , Heartbeat , LiveboxFinder , EnergenieStatus ...
			channel := c.Param("channel")
			if channel != "*" {
				channel = channel + "/*"
			}
			duration := 60 * time.Second

			//ctx := context.Background()

			ctx := c
			out_channel,err := HeartbeatSupervisor(ctx,channel,duration)
			if err != nil {
				c.String( 500,fmt.Sprintf("error : %s", err.Error()))
			} else {
				// send  start line to page
				io.WriteString(c.Writer, fmt.Sprintf("<p> ---  Scanning channels ptf/%s for %d seconds ---",
					channel, int(duration)/1000000000))
				c.Writer.Flush()
				//
				for msg := range out_channel {
					io.WriteString(c.Writer, fmt.Sprintf("<p>%s : %s </p>",msg.Channel,msg.Data))
					c.Writer.Flush()
				}
				c.String(200, "<p>--- Scan finished ---</p>")
			} })


		g.GET("/ipscan/livebox", func(c *gin.Context) {
			//  /doc ipscan
			pool,_:= redistools.GetPool()
			cnx := pool.Get()
			defer cnx.Close()

			items,err  := ipscan.CollectDeviceInfo(cnx)
			if err != nil {
				c.HTML(http.StatusBadRequest,"",nil)
			}

			var labels = []string{"Name","Ip","Mac","Uptime","Status","Timestamp"}

			c.HTML(http.StatusOK, "livebox_ipscan.tmpl",gin.H{
				"labels": labels,
				"items": items,
			})
		})


		g.GET("/ipscan/energenie", func(c *gin.Context) {
			//  /doc ipscan
			pool,_:= redistools.GetPool()
			cnx := pool.Get()
			defer cnx.Close()

			items,err  := ipscan.CollectEnergenieInfo(cnx)
			if err != nil {
				c.HTML(http.StatusBadRequest,"",nil)
			}

			var labels = []string{"Name","Ip","Mac","Uptime","Status","Timestamp"}

			c.HTML(http.StatusOK, "energenie_ipscan.tmpl",gin.H{
				"labels": labels,
				"items": items,
			})
		})

		g.GET("/redis", func(c *gin.Context) {
			//  redirect to redis prometehus supervision

			c.Redirect(301,"http://localhost:9121/metrics")
		})

		g.GET("/machine", func(c *gin.Context) {
			//  redirect to machine supervision (cadvisor)

			c.Redirect(301,"http://localhost:8005")
		})


	}

	return r
}


func HeartbeatSupervisor(ctx context.Context, subscribe_pattern string, duration time.Duration) (chan subscribers.SimpleMessage ,error){
	// subscribe to /ptf/Heartbeat/* and return each message
	// subscribe pattern : eg Heartbeat/*

	start := time.Now()


	out := make(chan subscribers.SimpleMessage)

	// get config element  PublisherPrefix , Redis.Url
	cnf := config.GetConfig()
	prefix := cnf.PublisherPrefix
	if subscribe_pattern == "" {
		subscribe_pattern = "*"
	}
	subscribe_channel := fmt.Sprintf("%s/%s",prefix,subscribe_pattern)
	url := cnf.Redis.Url

	sub,err := subscribers.NewRedisSubscriber(url)
	if err != nil {
		return out,err
	}
	err = sub.Subscribe(subscribe_channel)
	if err != nil {
		return out,err
	}

	// read loop
	go func() {
		defer close(out)
		for {

			t := time.Now()
			elapsed := t.Sub(start)
			if elapsed > duration {
				fmt.Printf("HeartbeatSupervisor time is over\n")
				break
			}

			msg, err := sub.Receive()
			if err != nil {
				fmt.Printf("HeartbeatSupervisor receive an error: %s\n", err.Error())
				break
			} else {
				//println("!")
				out <- msg
			}
		}

	}()
	return out,err
}
