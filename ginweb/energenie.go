package ginweb

import (

	"github.com/gin-gonic/gin"
	"strconv"
	"errors"
	"bitbucket.org/cocoon_bitbucket/ptfscan/powerswitch/energenie"
)


func EnergenieRoute( r * gin.Engine) * gin.Engine {

	// group Energenie
	g := r.Group("/api/energenie")
	{
		g.GET("/", func(c *gin.Context) {
			c.String(200, "energenie index")
		})

		g.GET("/:ip", func(c *gin.Context) {
			// get device status
			ip := c.Param("ip")

			status, err := energenie_device_status(ip)
			if err == nil {
				c.JSON(200, gin.H{"op": "device_status", "ip": ip, "result": status, "error": nil})
			} else {
				c.JSON(404, gin.H{"op": "device_status", "ip": ip, "result": nil, "error": err.Error()})
			}
		})

		g.GET("/:ip/:outlet", func(c *gin.Context) {
			// get outlet status
			ip := c.Param("ip")
			outlet := c.Param("outlet")
			response,err := energenie_outlet_status(ip,outlet)
			if err == nil {
				c.JSON(200, gin.H{"op": "outlet_status", "ip": ip, "outlet": outlet, "result": response, "error": nil})
			} else {
				c.JSON(404, gin.H{"op": "outlet_status", "ip": ip, "outlet": outlet, "result": nil, "error": err.Error()})
			}
		})

		g.POST("/:ip/:outlet/on", func(c *gin.Context) {
			// switch on outlet
			ip := c.Param("ip")
			outlet := c.Param("outlet")

			err := EnergenieSwitch(ip, outlet, "ON")
			if err == nil {
				// ok
				c.JSON(200, gin.H{"op": "ON", "ip": ip, "outlet": outlet, "status": "200", "error": nil})
			} else {
				//error
				c.JSON(404, gin.H{"op": "ON", "ip": ip, "outlet": outlet, "status": "404", "error": err.Error()})
			}
		})

		g.POST("/:ip/:outlet/off", func(c *gin.Context) {
			// switch off outlet
			ip := c.Param("ip")
			outlet := c.Param("outlet")

			err := EnergenieSwitch(ip, outlet, "OFF")
			if err == nil {
				// OK
				c.JSON(200, gin.H{"op": "OFF", "ip": ip, "outlet": outlet, "status": "200", "error": nil})
			} else {
				// error
				c.JSON(404, gin.H{"op": "OFF", "ip": ip, "outlet": outlet, "status": "404", "error": err.Error()})
			}
		})
	}

	return r
}



// device functions


func EnergenieSwitch( ip string, outlet string ,operation  string) error {
	// power ON/OFF of a energenie outlet

	index,err  := strconv.Atoi(outlet)
	if err != nil {
		err = errors.New("invalid outlet (must be an integer)")
		return err
	}

	cnf := get_config()
	password := cnf.Energenie.Password

	//device,err := inspectors.NewEnergenie(ip, password)
	device,err := energenie.NewEnergenieClient(ip,password)

	if err == nil {
		if operation == "ON" {

			//err = device.SwitchOn(index)
			err = device.PowerOn(index)
			return err

		} else {
			// operation = OFF
			//err = device.SwitchOff(index)
			err = device.PowerOff(index)
			return err
		}

	}
	return err

}




func energenie_outlet_status(ip,outlet string) (bool, error) {
	// get status of an energenie outlet
	index,err  := strconv.Atoi(outlet)
	if err != nil {
		err = errors.New("invalid outlet (must be an integer)")
		return false,err
	}

	if index < 1 || index > 4 {
		err = errors.New("Invalid outlet (must be in [1,4])")
		return false,err
	}

	outlets_status,err := energenie_device_status(ip)
	if err == nil {
		status := outlets_status[index-1]
		return status,nil
	}

	return false,err

}

func energenie_device_status(ip string) ( map[int]bool , error ) {
	// return the status of all outlets of an energenie device
	cnf := get_config()
	password := cnf.Energenie.Password

	null_status := map[int]bool{1:false,2:false,3:false,4:false}

	//device,err := inspectors.NewEnergenie(ip, password)
	device,err := energenie.NewEnergenieClient(ip, password)
	if err == nil {

		// get energenie status
		response,err := device.Status()
		if err == nil {
			//OK
			status := response
			return status,nil
		}
	}
	return null_status,err


}