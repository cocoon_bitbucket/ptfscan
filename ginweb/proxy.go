package ginweb

import "net/http/httputil"

import (

	"net/http"
	"github.com/gin-gonic/gin"
)


func ReverseProxy() gin.HandlerFunc {

	//target := "192.168.1.1:80"

	return func(c *gin.Context) {
		director := func(req *http.Request) {
			r := c.Request
			req = r
			req.URL.Scheme = "http"
			req.URL.Host = "192.168.1.1"
			// skip the /proxy/livebox part
			//path := r.URL.Path[14:]
			//req.URL.Path= path

			//req.Header["my-header"] = []string{r.Header.Get("my-header")}
			// Golang camelcases headers
			//delete(req.Header, "My-Header")
		}
		proxy := &httputil.ReverseProxy{Director: director}
		proxy.ServeHTTP(c.Writer, c.Request)
	}
}


func ProxyRoute( r * gin.Engine) * gin.Engine {

	// group livebox
	g := r.Group("/proxy")
	{
		g.GET("/", func(c *gin.Context) {
			c.String(200, "proxy index")
		})

		g.Any("/ip/:ip"  , func(c *gin.Context) {
			c.String(200, "proxy/ip/*")
		})

		//g.GET("/ip", func(c *gin.Context) {
		//	c.String(200, "proxy/ip")
		//})

		g.Any("/livebox", ReverseProxy())
	}


	return r
}