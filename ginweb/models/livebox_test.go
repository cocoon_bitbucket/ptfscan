package models_test


import(

	"testing"
	"github.com/stretchr/testify/assert"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	"bitbucket.org/cocoon_bitbucket/ptfscan/ginweb/models"

	"net"
)


func TestLivebox(t *testing.T) {

	_,err := redistools.NewPool("redis://127.0.0.1:6379/0")
	assert.Equal(t,nil,err)
	if err != nil { return }
	defer redistools.UnsetPool()


	err = SetupMockDb()


	e,err := models.LoadLiveboxByName("Livebox-demo")
	assert.Equal(t,nil,err)
	if err != nil { return }

	pdu,err := e.GetPduCoordinates()
	assert.Equal(t,nil,err)

	assert.Equal(t,net.ParseIP("192.168.1.30"),pdu.PduIp)

	_=e
	return




}