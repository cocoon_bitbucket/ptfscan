package models


/*

	regroup traits of a livebox ( infra , ipscan , device )

	use global  redistools GePool()

 */



import (

	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	store "bitbucket.org/cocoon_bitbucket/ptfscan/redistools/store"

	//infra "bitbucket.org/cocoon_bitbucket/ptfscan/infra/models"
	//ipscan "bitbucket.org/cocoon_bitbucket/ptfscan/reporters/ipscan"
	//device "bitbucket.org/cocoon_bitbucket/ptfscan/livebox"
	"net"
	"strconv"
	"fmt"
)


type Livebox struct {

	store.Entity

	//Infra InfraLivebox

}

//type InfraLivebox struct {
//
//	Name string     	`json:"name"`		// 'Livebox-2898'
//
//	Mac string 			`json:"mac"`		// '78:94:B4:3D:28:98',
//	Ip string            `json:"ip"`  		// '192.168.200.65'
//
//	Lan	string			`json:"lan"`		// 'B5-27-16'
//
//	Wifi	string		`json:"wifi"`		//  'WDiCTqUD4RQFWNPvib'
//	Auth	string		`json:"auth"`		// ''
//	Ex		string		`json:"ex"`			// 'EX4300-01-16'
//	H235	string		`json:"h235"`		//'',
//
//	Serial	string		`json:"serial"`		//': 'FBKL00100189382',
//	Type	string		`json:"type"`		// ': 'Livebox Sercom',
//
//	Pdu_name	string	`json:"pdu_name"`	// 'PDU-J4-P5
//	Pdu_outlet	int		`json:"pdu_outlet"`	// ': 3
//	Pdu_mac		string	`json:"pdu_mac"`	// '88:B6:27:01:65:DE',
//	Pdu_ip		string	`json:"pdu_ip"`		// '192.168.100.45',
//
//}
//



func ( l * Livebox ) GetPduCoordinates() ( pdu * PduCoordinates,err error){
	// return the pdu coordinates PduIp, PduOutlet
	// find the pdu of the livebox
	// try to get dynamic ip from ipscan device or return the pdu ip declared in infra

	pdu = &PduCoordinates{}
	pdu.PduName = l.Properties["pdu_name"]
	pdu.PduMac,err = net.ParseMAC(l.Properties["pdu_mac"])
	pdu.PduIp = net.ParseIP(l.Properties["pdu_ip"])
	pdu.PduOutlet,err = strconv.Atoi(l.Properties["pdu_outlet"])

	// update ip if available
	err = pdu.UpdateIp()
	if err != nil {
		fmt.Printf("GetPduCoordinates: No real ip, assume infra ip")
		err = nil
	}
	return pdu,err
}


func LoadLiveboxByName(name string) ( livebox * Livebox, err error){
	//
	//
	//
	// get a connection from the pool
	cnx,err := redistools.GetNewconn()
	if err != nil { return livebox,err}
	defer cnx.Close()

	// try to load a livebox by name
	e,err := store.LoadEntityByIndex(cnx,"mac","name",name)
	if err == nil {

		livebox = &Livebox{e}
	}
	return livebox,err
}


type PduCoordinates struct {

	PduName string
	PduMac net.HardwareAddr
	PduIp net.IP
	PduOutlet int

}

func ( p *PduCoordinates) UpdateIp() (err error) {
	// try to find dynamic ip and update p.PduIp
	cnx,err := redistools.GetNewconn()
	if err != nil { return err }
	defer cnx.Close()

	e,err := store.LoadEntityByIndex(cnx,"device","mac",p.PduMac.String())
	if err != nil { return err }

	p.PduIp = net.ParseIP(e.Properties["ip"])

	return err
}