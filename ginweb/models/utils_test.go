package models_test

import(

	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	//"bitbucket.org/cocoon_bitbucket/ptfscan/redistools/store"

	infra "bitbucket.org/cocoon_bitbucket/ptfscan/infra/models"
	"bitbucket.org/cocoon_bitbucket/ptfscan/reporters/ipscan"
)



func SetupMockDb() (err error){

	// setup a mock redis dataset
	//  Livebox-demo  and Energenie-demo

	cnx,err := redistools.GetNewconn()
	if err != nil { return err}
	defer cnx.Close()

	cnx.Do("FLUSHDB")

	err = infra.CreateMacEntitySchema(cnx)      //  entity:mac
	err = ipscan.CreateDeviceEntitySchema(cnx)  // entity:device

	// create Livebox-demo entries    entity:mac
	d := make(map[string]string)
	d["name"] = "Livebox-demo"
	d["ip"] = "192.168.1.1"
	d["mac"] = "a0:1b:29:fc:12:70"
	d["pdu_name"]= "Energenie_demo"
	d["pdu_mac"] = "88:b6:27:01:65:e4"
	d["pdu_ip"] = "192.168.1.30"
	d["pdu_outlet"] = "4"

	d2 := make(map[string]string)
	d2["name"]= d["pdu_name"]
	d2["mac"] = d["pdu_mac"]
	d2["ip"] = d["pdu_ip"]
	d2["outlet"] = d["pdu_outlet"]


	l := infra.NewMacEntity(d["name"],d["mac"],"livebox",d)
	_,err = l.Save(cnx)
	l2 := ipscan.NewDeviceEntity(d["ip"],d["mac"],"livebox",nil)
	_,err = l2.Save(cnx)

	// create Energenie-demo entries
	e := infra.NewMacEntity(d2["name"],d2["mac"],"energenie",d2)
	_,err = e.Save(cnx)
	e2 := ipscan.NewDeviceEntity(d2["ip"],d2["mac"],"energenie",nil)
	_,err = e2.Save(cnx)


	return err
}
