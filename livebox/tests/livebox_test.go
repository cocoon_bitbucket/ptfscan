package livebox

/*

	pre-requisites: you need a real livebox at 192.168.1.1 with a telnet server root/sah

 */


import (
	"testing"

	"bitbucket.org/cocoon_bitbucket/ptfscan/livebox"
	"github.com/stretchr/testify/assert"

	"fmt"
	"time"
)




func TestLiveboxTelnetClient(t *testing.T) {

	c, err := livebox.NewTelnetClient( "192.168.1.1:23","root","sah",1*time.Second)
	if err == nil {
		err = c.Sendline("ls -l /tmp")
		if err == nil {
			r,err := c.ReceiveUntilPrompt( 5 * time.Second)
			if err == nil {
				println(string(r))
			}
		}

	}


}

func TestLiveboxExecPcbcliRequest(t *testing.T) {


	var response map[string]string

	c, err := livebox.NewTelnetClient( "192.168.1.1:23","root","sah",1*time.Second)
	assert.Equal(t,err,nil)
	response,err = c.ExecPcbcliRequest("DeviceInfo",5 * time.Second)
	assert.Equal(t,err,nil)
	fmt.Printf("%s\n",response)
	l := len(response)
	assert.NotEqual(t,l,0)

}

func TestLiveboxExecPcbcliSet(t *testing.T) {


	var response map[string]string


	key := "Device.UPnP.Device.Enable"
	value := "1"

	c, err := livebox.NewTelnetClient( "192.168.1.1:23","root","sah",1*time.Second)
	assert.Equal(t,err,nil)
	response,err = c.ExecPcbcliSet(key,value)
	assert.Equal(t,err,nil)
	fmt.Printf("%s\n",response)
	l := len(response)
	assert.Equal(t,1,l)
	assert.Equal(t,value,response[key])

}



func TestShellCommand(t *testing.T) {

	ip:= "192.168.1.1:23"
	cmd := "ls - /tmp"
	user:= "root"
	password := "sah"

	lines,err:=  livebox.RunShellCommandOnLivebox(ip, cmd , user , password ,5* time.Second)
	assert.Equal(t,err,nil)
	fmt.Printf("%s\n",lines)

}

