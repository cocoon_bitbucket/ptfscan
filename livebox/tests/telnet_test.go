package livebox_test

/*

	pre-requisites: a telnet server at 127.0.0.1:23

 */


import (
	"testing"

	"bitbucket.org/cocoon_bitbucket/ptfscan/livebox"
	"github.com/stretchr/testify/assert"

	"time"
)


func TestShellCommand(t *testing.T) {

	ip:= "192.168.1.1:23"
	cmd := "ls - /tmp"
	user:= "root"
	password := "sah"

	r,err :=  livebox.RunShellCommandOnLivebox(ip, cmd , user , password,5 * time.Second)
	assert.Equal(t,err,nil)
	_=r

}
