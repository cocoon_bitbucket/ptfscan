# Work in progress
* ginweb use it
* TODO: make portscanner.inspectors use it

regroup here all livebox related commons


# telnet client 

    import "bitbucket.org/cocoon_bitbucket/ptfscan/livebox"
    
    // create client ( open telnet connexion and login )
    client, err := livebox.NewTelnetClient( "192.168.1.1:23","root","password") 
    
    
##  send shell commands to livebox 

    // send some command 
    err = client.Sendline("ls -l /tmp")
    ...
    // get result lines
    lines, err := client.ReceiveUntil("#",5 * time.Second) 

    
## send pcbcli request 

        // exec pcbcli command 
         data ,err = client.ExecPcbcliRequest( "Device.info", 5 * time.Second)
        ...
        // data is a dictionary with all Device.info.* keywords
        
## close client
    
    client.Close()