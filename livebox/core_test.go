package livebox_test


import (

	"bitbucket.org/cocoon_bitbucket/ptfscan/livebox"
	"testing"
	"github.com/stretchr/testify/assert"
)

/*

	pre-requisites: NO

 */

func TestExtractPcbcliMap(t *testing.T) {

	sample := "pcb_cli Device.UPnP.Device.Enable?\r\nDevice.UPnP.Device.Enable=1\r\nDevice.UPnP.Info=OK\r\n/cfg/system/root #"

	data,err := livebox.ExtractPcbcliMap(sample)
	assert.Equal(t,err,nil)
	assert.Equal(t,data["Device.UPnP.Device.Enable"],"1")
	assert.Equal(t,data["Device.UPnP.Info"],"OK")

}

func TestExtractPcbcliEmptyMap(t *testing.T) {

	sample := "pcb_cli Device.UPnP.Device.Enable?\r\n/cfg/system/root #"

	data, err := livebox.ExtractPcbcliMap(sample)
	assert.Equal(t,len(data),0)
	assert.Equal(t, err.Error(), "empty")
}



