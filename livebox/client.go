package livebox

import (

	"time"
)

type Client interface {

	Login( user , password string) error
	Sendline( line string) error
	ReceiveLine() ([]byte, error)
	ReceiveUntilPrompt( timeout time.Duration ) ( []byte, error)
	Close() error
	Ping() error

}

// interface

func Login( client Client, user, password string) error {

	return client.Login(user,password)

}

func SendLine( client Client, line string ) (error) {

	return client.Sendline( line )

}

func ReceiveLine( client Client) ( []byte,error) {

return client.ReceiveLine()

}

func ReceiveUntilPrompt( client Client, timeout time.Duration) ( []byte ,error)  {

	return client.ReceiveUntilPrompt(timeout)

}


func Close( client Client) error {

	return client.Close()

}

func Ping( client Client) error {

	return client.Ping()

}


// helpers

