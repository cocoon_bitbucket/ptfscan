package livebox

import (
	"time"
	"bitbucket.org/cocoon_bitbucket/ptfscan/telnet"
	"fmt"
)

/*

	telnet implements Client interface for telnet connection

 */


var (
	TelnetDefaultPrompt  byte = '#'
	TelnetDefaultNewLine byte = '\n'
)


type TelnetClient struct {

	// structure of a telnet

	* telnet.TelnetConn
	User string
	Passwd string
	opened bool
	logged bool

}


func NewTelnetClient(netAddr ,user,passwd string ,timeout time.Duration) ( *TelnetClient, error){

	//timeout := 2 * time.Second
	// open a telnet connection
	var cli * TelnetClient = nil

	t ,err :=  telnet.DialTimeout( "tcp", netAddr, timeout )
	if err == nil {
		// connected
		cli = &TelnetClient{ t , user, passwd, true, false}
		cli.opened = true
		// try to login
		err = cli.Login(user,passwd)
		if err != nil {
			cli = nil
		} else {
			cli.logged = true
		}
	}
	return cli , err

}

//
// implements livebox.Client interface
//


func (client *TelnetClient) Login( user , password string) error {

	return telnet.Login(client.TelnetConn,user,password)

}

func (client *TelnetClient)Sendline( line string) error {
	return telnet.Sendln( client.TelnetConn,line)
}

func (client *TelnetClient) ReceiveLine() ( []byte , error) {
	// read until a new line
	timeoutDuration := time.Duration(2000 * time.Millisecond )
	client.SetReadDeadline(time.Now().Add(timeoutDuration))
	return client.TelnetConn.ReadBytes(TelnetDefaultNewLine)
}

func (client *TelnetClient) ReceiveUntilPrompt( timeout time.Duration ) ( []byte, error) {
	//

	client.SetReadDeadline(time.Now().Add(timeout))
	return client.TelnetConn.ReadBytes(TelnetDefaultPrompt)
}

func (client *TelnetClient) Close() error {
	if client.opened == true {
		client.TelnetConn.Close()
		client.opened = false
		client.logged = false
	}
	return nil
}

func (client *TelnetClient)  Ping() error{
	return nil
}


// other methods

func ( client * TelnetClient) ExecPcbcliRequest( cmd string,timeout time.Duration)  (map[string]string,error){
	// send a query pcb cli command  eg : Device.DeviceInfo => #pcb_cli Device.DeviceInfo?
	// return a dict of keywords from the response

	data := make(map[string]string)

	if cmd[len(cmd)-1] != '?' {
		cmd = fmt.Sprintf("pcb_cli %s?",cmd)
	} else {
		cmd = fmt.Sprintf("pcb_cli %s", cmd)
	}
	// send pcb cli command
	err := telnet.Sendln(client.TelnetConn ,cmd)
	if err == nil {
		time.Sleep(100 * time.Millisecond)
		err = client.TelnetConn.SetReadDeadline(time.Now().Add(timeout))
		if err == nil {
			response, err := client.TelnetConn.ReadBytes('#')
			if err == nil {
				data,err = ExtractPcbcliMap(string(response))
			}
		}
	}
	return data,err
}


func ( client * TelnetClient) ExecPcbcliSet( key,value string ) (map[string]string,error) {
	// send pcbli set command eg  pcb_cli Device.UPnP.Device.Enable=1
	// send pcbcli request command eg  pcb_cli Device.UPnP.Device.Enable?
	// return a dict of keywords from the response


	data := make(map[string]string)

	// set the command eg pcb_cli Device.UPnP.Device.Enable=1
	cmd := PcbCliCommandString + " " + key + "=" + value

	// send the SET command
	err := client.Sendline(cmd)
	if err != nil {
		return data,err
	}
	//time.Sleep( 500 * time.Millisecond)
	// read response ( blank line )
	//time.Sleep(2*time.Second)
	r,err := client.ReceiveUntilPrompt(2* time.Second)
	_ = r
	if err != nil {
		return data,err
	}

	// request the keyword
	return client.ExecPcbcliRequest(key,2 * time.Second)

}



// Single commands ( connect/login/send/receive )

func RunShellCommandOnLivebox(ip string, cmd string, user string , password string , timeout time.Duration) (string,error) {

	// create a telnet clilent
	// connect to livebox at <ip>:23 with telnet
	// login with telnet credentials
	// run the command <cmd> and return the response and <err>   example command: "ls -l /tmp"
	// return the screen output until "#" as a string

	response := ""

	t ,err := NewTelnetClient ( ip ,user,password ,timeout)
	if err != nil {
		_= t
		return "",err
	}
	err = t.Sendline(cmd)
	if err != nil {
		return response,err
	}
	r,err := t.ReceiveUntilPrompt(timeout)
	if err != nil {
		return response,err
	}
	response = string(r)

	t.Close()

	return response,nil

}

