package livebox

import (
	"regexp"
	"errors"
)

var (

	PcbCliCommandString string = "pcb_cli"

	DefaultLiveboxTelnetUser string = "root"
	DefaultLiveboxTelnetPassword string = "sah"

	// regxexps

	// Device.Info.topic=value
	pcbcli_keywords_regexp * regexp.Regexp


)


//type pcb_cli_lines map[string]string


//
// extractt a pcbli keyword map from a pcbcli Keyword? response
//
func ExtractPcbcliMap( text string) ( map [string] string,error) {

	var err error = nil
	result := make(map[string]string)

	array := pcbcli_keywords_regexp.FindAllStringSubmatch(text,-1)
	if len(array) > 0 {
		for _, v := range array {
			result[v[1]] = v[2]
		}
	} else {
		err = errors.New("empty")
	}
	return result,err
}


var PcbcliCommands  = map[string]string {

	//"AdditionalSoftwareVersion": "DeviceInfo.AdditionalSoftwareVersion",
	"ProductClass":      "Device.DeviceInfo.ProductClass",   // Livebox 4
	"Manufacturer":      "Device.DeviceInfo.Manufacturer", // Sagemcom
	"SerialNumber":      "Device.DeviceInfo.SerialNumber", //  =LK15233DP992021
	"UpTime":            "Device.DeviceInfo.UpTime=", // =3983173
	"MACAddress":        "Device.DeviceInfo.X_ORANGE-COM_MACAddress" ,  // =A0:1B:29:FC:12:70
	"ExternalIPAddress": "Device.DeviceInfo.X_ORANGE-COM_ExternalIPAddress" ,   // =82.124.70.85
	"HardwareVersion":   "Device.DeviceInfo.HardwareVersion",  // =SG_LB4_1.0.1
	"SoftwareVersion":   "Device.DeviceInfo.SoftwareVersion", // =SG40_sip-fr-3.4.3.1_7.21.3.0
	"DeviceStatus":      "DeviceInfo.DeviceStatus", // =Up

	"wifi0.Channel":      "WiFiBCM.Radio.wifi0.Channel", // =11
	"wifi0.Enable":       "WiFiBCM.Radio.wifi0.Enable", // =1

	"CurrentLocalTime":   "Time.CurrentLocalTime", // =2018-01-03T15:20:15Z
	"LocalTimeZone=":     "Time.LocalTimeZone", //  =CET-1CEST,M3.5.0,M10.5.0/3

	"admin.Enable": "UserManagement.User.admin.Enable", //=1
	"admin.UserName": "UserManagement.User.admin.UserName", // =admin
	"admin.Password": "UserManagement.User.admin.Password", //=31546e8641d67895739485bb38eede92592c36b4ab06c3b1d4aaf2f24992f398
	"admin.PasswordType": "UserManagement.User.admin.PasswordType", // =SSHA256

	"Power Led.Color": "LED.Power Led.Color", // =Green
	"Power Led.State": "LED.Power Led.State",  // =Solid

	"Internet Led.Color": "LED.Internet Led.Color", //=Green
	"Internet Led.State": "LED.Internet Led.State", //=Solid

	"VoIP Led.Color": "LED.VoIP Led.Color", // =Green
	"VoIP Led.State": "LED.VoIP Led.State", //=Solid

	"Lan Led.Color": "LED.Lan Led.Color", //=Green
	"Lan Led.State": "LED.Lan Led.State", //=Off

	"Wifi Led.Color": "LED.Wifi Led.Color", //=Green
	"Wifi Led.State": "LED.Wifi Led.State", //=Solid

	"Upgrade Led.Color": "LED.Upgrade Led.Color", //=Blue
	"Upgrade Led.State": "LED.Upgrade Led.State", //=Off

	"dsl0.Status": "DSL_BROADCOM.Line.dsl0.Status", //=Up
	"dsl0.Name": "DSL_BROADCOM.Line.dsl0.Name", // =dsl0
	"dsl0.LastChange": "DSL_BROADCOM.Line.dsl0.LastChange", //=3983794


	"dsl0.BytesSent": "DSL_BROADCOM.Line.dsl0.Stats.BytesSent", // =203117995
	"dsl0.BytesReceived": "DSL_BROADCOM.Line.dsl0.Stats.BytesReceived", // =442482372
	//DSL_BROADCOM.Line.dsl0.Stats.PacketsSent=645010
	//DSL_BROADCOM.Line.dsl0.Stats.PacketsReceived=727300
	"dsl0.ErrorsSent": "DSL_BROADCOM.Line.dsl0.Stats.ErrorsSent", // =0
	"dsl0.ErrorsReceived": "DSL_BROADCOM.Line.dsl0.Stats.ErrorsReceived", // =0

	//"DNS.Server.Host.dcodeurtv4.Name", // =dcodeurtv4
	//"DNS.Server.Host.dcodeurtv4.IPv4", //=192.168.1.54


	"wl0.Enable":        "WiFiBCM.AccessPoint.wl0.Enable", // =1
	"wl0.KeyPassPhrase": "WiFiBCM.AccessPoint.wl0.Security.KeyPassPhrase", // =admin1234



}


func init() {

	// extract key=value in pcbcli query response
	xpr :=  `([a-zA-Z]+[a-zA-Z\d\. ]*)=(.*?)[\s\r\n]`
	pcbcli_keywords_regexp = regexp.MustCompile( xpr )

}

