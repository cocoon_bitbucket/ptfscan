#!/usr/bin/env bash


# build local
echo "build local images"
docker-compose build --force-rm --no-cache

# create volumes
#echo "create volumes"
#docker volume create --name dnsguardian_data

# copy restop devices doc to alpine_static
#docker run -v alpine_static:/usr/share/nginx/html/static --name helper alpine_master cp -rf /usr/local/lib/restop-devices/doc/ /usr/share/nginx/html/static/
#docker rm helper

# copy ide static files to alpine_static volume
#echo "copy data to volumes"
#docker run -v alpine_static:/usr/share/nginx/html/static --name helper alpine_ide cp -rf /usr/local/lib/restop/restop_ide/app/static/ /usr/share/nginx/html/static/
#docker rm helper
#docker run -v alpine_static:/usr/share/nginx/html/static --name helper alpine_ide ash -c "cp -af /env/lib/python2.7/site-packages/flask_bootstrap3/static/* /usr/share/nginx/html/static/bootstrap/"
#docker rm helper