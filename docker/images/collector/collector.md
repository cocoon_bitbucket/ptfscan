
collector
=========


collector is a go process to collect ptfscan metrics

* subscribe to redis pubsub channel ptf/?
* publish prometheus metrics at :8080




depends on repository bitbucket.org/cocoon_bitbucket/ptfsupervision
