package prometheus_test


import(

	"testing"
	alert "bitbucket.org/cocoon_bitbucket/ptfscan/prometheus"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools/store"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
	"github.com/stretchr/testify/assert"
	"encoding/json"
	//"fmt"
	//"strings"
	//"fmt"
	//"strconv"
	"strings"
	"fmt"
	"time"
)


var (

	redisUrl string = "redis://localhost:6379/0"

)


func TestNewAlert(t *testing.T) {

	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	defer cnx.Close()
	//cnx.Do("FLUSHDB")

	// create item model
	alert.CreateAlertEntitySchema(cnx)

	data := alert.AlertRecord{}

	err = json.Unmarshal([]uint8(sample), &data)
	if err != nil {
		t.Error(err)
	}
	alerts := data.Alerts

	println(alerts.Status())
	println(alerts.Len())

	for _, a := range alerts {
		println(a.Name())

		e,err := alert.NewAlert(cnx,a)
		if err != nil {
			t.Error(err)
		}
		e.Save(cnx)

	}

	for _, a := range alerts {
		f := a.Fingerprint().String()
		x, err := alert.LoadAlertbyFingerprint(cnx,f)
		if err != nil {
			println(err.Error())
		}
		_=x
	}

	lifetime,_ := time.ParseDuration("48h")
	alert.RemoveExpiredAlert(cnx,lifetime)


	all,err := alert.All(cnx)

	for _,o := range all {
		l,_ := time.ParseDuration("2s")
		expired,_ :=  o.IsOlderThan(l )
		if expired {
			// delete
			o.Delete(cnx)
		}

	}

	_=all
	//if err != nil {
	//	t.Error(err)
	//}

	return

}




func TestAlertEntity(t *testing.T) {

	db, err := store.NewStore(redisUrl)
	assert.Equal(t, nil, err)

	cnx := db.Conn
	defer cnx.Close()
	cnx.Do("FLUSHDB")

	// create item model
	alert.CreateAlertEntitySchema(cnx)

	data := alert.AlertRecord{}

	err = json.Unmarshal([]uint8(sample),&data)
	if err != nil {
		t.Error(err)
	}
	alerts := data.Alerts

	println( alerts.Status())
	println(alerts.Len())

	for _,a := range alerts {
		println(a.Name())
		println(a.Status())
		println(a.Fingerprint())
		println(a.String())
		println(a.Resolved())

		println(a.Annotations.String())
		println(a.Annotations["summary"])
		println(a.Annotations["description"])

		println(a.StartsAt.String())

		println(a.Labels.String())

		// search if alert exists
		found := true
		f := a.Fingerprint()
		fi:= fmt.Sprintf("%d",f)
		//fi := strconv.Itoa(int(f))
		x,err := alert.LoadAlertbyFingerprint(cnx,string(fi))
		if err != nil {
			// does not exists
			if strings.Contains(err.Error(), "redigo: nil ") {
				// not found
				found = false
			} else {
				// redis error
				t.Error("redis error")
			}

		}
		_=x
		_=found

	//	if found == false {
	//		if a.Resolved() == false {
	//			// this is a new firing alert : add it
	//			r,err := alert.NewAlertEntity(cnx, fi,string(a.Labels["alertname"]),string(a.Labels["instance"]),nil)
	//			if err != nil {
	//				// redis error
	//				t.Error("redis error")
	//			}
	//			r.Save(cnx)
	//
	//		}
	//	}
	//
	//
	}



	return







	item,err := store.NewDeviceEntity(cnx,"Livebox-demo","00:00:00","livebox",nil)
	_,err = item.Save(cnx)
	assert.Equal(t, nil, err)

	o,err := store.LoadDevicebyName(cnx,"Livebox-demo")
	assert.Equal(t, nil, err)
	assert.Equal(t,"Livebox-demo",o.Properties["name"])
	assert.Equal(t,"00:00:00",o.Properties["mac"])
	assert.Equal(t,"livebox",o.Properties["kind"])

	o2,err := store.LoadDevicebyMacAddress(cnx,"00:00:00")
	assert.Equal(t, nil, err)
	assert.Equal(t,o,o2)

	o.Delete(cnx)

	_,err = store.LoadDevicebyName(cnx,"Livebox-demo")
	assert.Equal(t,"redigo: nil returned",err.Error())



	redistools.UnsetPool()

}



//func TestAlertEntity2(t *testing.T) {
//
//	db, err := store.NewStore(redisUrl)
//	assert.Equal(t, nil, err)
//
//	cnx := db.Conn
//	cnx.Do("FLUSHDB")
//
//	// create item model
//	store.CreateDeviceEntitySchema(cnx)
//
//
//	o,err := store.LoadDevicebyName(cnx,"Livebox-demo")
//	assert.Equal(t, nil, err)
//	assert.Equal(t,"Livebox-demo",o.Properties["name"])
//	assert.Equal(t,"00:00:00",o.Properties["mac"])
//	assert.Equal(t,"livebox",o.Properties["kind"])
//
//	o2,err := store.LoadDevicebyMacAddress(cnx,"00:00:00")
//	assert.Equal(t, nil, err)
//	assert.Equal(t,o,o2)
//
//	o.Delete(cnx)
//
//	_,err = store.LoadDevicebyName(cnx,"Livebox-demo")
//	assert.Equal(t,"redigo: nil returned",err.Error())
//
//
//
//	redistools.UnsetPool()
//
//}
//
