package prometheus

import (

	"github.com/garyburd/redigo/redis"

	"encoding/json"
	//"fmt"
	//"log"
)
/*


	alert handler

	add a new alert ,check it , store it to redis  alert:ptfscan

	retrieve a previous alert

	delete an alert



 */




type AlertHandler struct {

	redis.Pool



}


func NewAlertHandler( pool redis.Pool) (handler *AlertHandler,err error ){

	handler = &AlertHandler{pool}

	return handler,err
}




func (h *AlertHandler) AddRecord ( json_record string) (err error){
	// json_record is a json string representing a set of alerts sent by alertmanager

	alert_record := AlertRecord{}

	err = json.Unmarshal([]uint8(json_record),&alert_record)
	if err != nil {
		return err
	}

	cnx := h.Get()
	defer cnx.Close()

	alerts := alert_record.Alerts
	for _,alert := range alerts {

		e,err := NewAlert(cnx,alert)
		if err != nil {
			return err
		}
		e.Save(cnx)
	}

	return err
}




