package prometheus


import(

	"strings"
	"github.com/garyburd/redigo/redis"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools/store"
	prom "github.com/prometheus/common/model"
	"encoding/json"
	"time"
)

var (

	// device schema
	AlertEntitySchema = store.EntitySchema{
		Name: "alert",
		Indexes: map[string]string{
			"fingerprint": "unique",
			"alertname": "duplicate",
			"instance_name": "duplicate",
		},
	}
)


func CreateAlertEntitySchema(cnx redis.Conn) (err error){
	// create device schema in database
	schema := AlertEntitySchema
	err = schema.Save(cnx)
	return err

}

type AlertEntity struct {
	store.Entity
	Alert prom.Alert
}

func (i * AlertEntity) Schema() store.EntitySchema {
	return AlertEntitySchema
}

func (i * AlertEntity) Exists(cnx redis.Conn) (exists bool, err error) {
	// is alert already exists in db
	fingerprint := i.Alert.Fingerprint().String()

	_,err = LoadAlertbyFingerprint(cnx,fingerprint)
	if err != nil {
		// does not exists or redis error
		return false,err
	} else {
		return true,err
	}

}


func (i * AlertEntity) UpdateTimestamp(cnx redis.Conn) (err error) {
	// update timestamp
	key := i.Oid.Key()
	timestamp,err := json.Marshal(time.Now())
	_,err = cnx.Do("HSET",key,"_timestamp",string(timestamp))
	return err
}

func (i * AlertEntity) Timestamp() ( timestamp time.Time , err error) {
	// return the timestamp ( e.propoerties["_timestamp"]
	t := i.Properties["_timestamp"]
	//tm := &time.Time{}
	err = json.Unmarshal([]byte(t), &timestamp)
	return timestamp,err
}


func (i * AlertEntity) IsOlderThan( t time.Duration) ( expired bool, err error) {
	// return tue if timestamp older than a duration
	expired = false
	tm,err := i.Timestamp()
	if err == nil {
		lifetime := time.Now().Sub(tm)
		if lifetime > t {
			// older than duration : expired
			return true,nil
		}
	}
	return expired,err
}




func (i * AlertEntity) Save(cnx redis.Conn) (err error) {
	// is alert already exists
	fingerprint := i.Alert.Fingerprint().String()
	found := true
	previous,err := LoadAlertbyFingerprint(cnx,fingerprint)
	if err != nil {
		// does not exists or redis error
		if strings.Contains(err.Error(), "redigo: nil ") {
			// not found
			found = false
		} else {
			// redis error
			return err
		}
	}
	if found {
		if i.Alert.Resolved() {
			// existing alert is resolved -> remove it
			previous.Delete(cnx)
			return nil
		} else {
			// existing but not resolved -> update timestamp
			previous.UpdateTimestamp(cnx)
			return nil
		}
	} else {
		// not exists
		if i.Alert.Resolved() {
			// a resolved non existing alert -> ignore it
			return nil
		} else {
			// not exists and not resolved : save it
			_,err = i.Entity.Save(cnx)
			return err
		}
	}

	//return err
}


func NewAlert(cnx redis.Conn, alert *prom.Alert )(entity AlertEntity,err error) {



	p := make(map[string]string)
	p["fingerprint"] = alert.Fingerprint().String()
	p["alertname"] = string(alert.Labels["alertname"])
	p["instance_name"] = string(alert.Labels["instance"])

	timestamp,err := json.Marshal(time.Now())
	p["_timestamp"] = string(timestamp)

	data ,err := json.Marshal(alert)
	if err != nil {
		return entity,err
	}
	p["data"] = string(data)

	e := store.Entity{AlertEntitySchema.Name, p, 0}
	entity = AlertEntity{e,*alert}

	return entity,nil

}


func LoadAlert(cnx redis.Conn, id string) (alert * AlertEntity,err error){

	e,err := store.LoadEntity(cnx,id)
	if err != nil { return alert,err}

	alert = &AlertEntity{e,prom.Alert{}}

	data := []byte(e.Properties["data"])
	err  =  json.Unmarshal( data, &alert.Alert )

	return alert, err
}



func LoadAlertbyFingerprint(cnx redis.Conn,fingerprint string)  (entity AlertEntity,err error)  {

	e,err := store.LoadEntityByIndex(cnx, AlertEntitySchema.Name,"fingerprint",fingerprint)
	if err != nil { return entity, err }

	entity.EntityName = e.EntityName
	entity.Properties = e.Properties
	entity.Oid = e.Oid

	data := []byte(e.Properties["data"])
	err  =  json.Unmarshal( data, &entity.Alert )


	return entity,err
}



func All( cnx redis.Conn) ( alerts []AlertEntity,err error ){

	query,err := store.NewQuery(cnx,"alert")
	oids,err := query.All("")

	for _,oid := range oids {

		o,err := LoadAlert(cnx,oid.String())
		if err != nil {  continue }

		alerts = append(alerts,*o)
	}
	return alerts,err

}


func RemoveExpiredAlert( cnx redis.Conn, lifetime time.Duration) {

	query,_ := store.NewQuery(cnx,"alert")

	oids,err := query.All("")
	if err == nil {
		for _, oid := range oids {

			o, err := LoadAlert(cnx, oid.String())
			if err != nil {
				continue
			}
			expired, _ := o.IsOlderThan(lifetime)
			if expired {
				// expired : delete it
				o.Delete(cnx)
			}
		}
	}
}