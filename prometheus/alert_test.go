package prometheus_test

import (

	"testing"
	handler "bitbucket.org/cocoon_bitbucket/ptfscan/prometheus"
	"encoding/json"
	"bitbucket.org/cocoon_bitbucket/ptfscan/redistools"
)


var (

	sample string =`

{
    "receiver": "ptfscan",
    "status": "firing",
    "alerts": [
        {
            "status": "firing",
            "labels": {
                "alertname": "InstanceDown",
                "group": "livebox",
                "instance": "192.168.99.100:8081",
                "job": "device",
                "severity": "page"
            },
            "annotations": {
                "description": "192.168.99.100:8081 of job device has been down for more than 5 minutes.",
                "summary": "Instance 192.168.99.100:8081 down"
            },
            "startsAt": "2018-03-23T13:36:22.897819632Z",
            "endsAt": "0001-01-01T00:00:00Z",
            "generatorURL": "http://ddd76268fa2a:9090/graph?g0.expr=up+%3D%3D+0&g0.tab=1"
        },
        {
            "status": "firing",
            "labels": {
                "alertname": "InstanceDown",
                "group": "energenie",
                "instance": "192.168.99.100:8082",
                "job": "device",
                "severity": "page"
            },
            "annotations": {
                "description": "192.168.99.100:8082 of job device has been down for more than 5 minutes.",
                "summary": "Instance 192.168.99.100:8082 down"
            },
            "startsAt": "2018-03-23T13:36:22.897819632Z",
            "endsAt": "0001-01-01T00:00:00Z",
            "generatorURL": "http://ddd76268fa2a:9090/graph?g0.expr=up+%3D%3D+0&g0.tab=1"
        }
    ],
    "groupLabels": {
        "alertname": "InstanceDown"
    },
    "commonLabels": {
        "alertname": "InstanceDown",
        "job": "device",
        "severity": "page"
    },
    "commonAnnotations": {},
    "externalURL": "http://9e23013b3199:9093",
    "version": "4",
    "groupKey": "{}:{alertname=\"InstanceDown\"}"
}
`

)


func TestUnmarshalAlerts( t *testing.T) {


	data := handler.AlertRecord{}

	err := json.Unmarshal([]uint8(sample),&data)
	if err != nil {
		t.Error(err)
	}
	alerts := data.Alerts

	println( alerts.Status())
	println(alerts.Len())

	for _,alert := range alerts {
		println(alert.Name())
		println(alert.Status())
		println(alert.Fingerprint())
		println(alert.String())
		println(alert.Resolved())

		println(alert.Annotations.String())
		println(alert.Annotations["summary"])
		println(alert.Annotations["description"])

		println(alert.StartsAt.String())

		println(alert.Labels.String())


	}



	return


}




func TestAlertHandler( t *testing.T) {

	pool,err := redistools.NewPool("redis://localhost:6379/0")
	if err != nil {
		t.Error(err)
	}
	defer redistools.UnsetPool()


	handler,err := handler.NewAlertHandler(pool.Pool)

	handler.AddRecord(sample)

	return

}