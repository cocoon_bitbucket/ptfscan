package prometheus


import (

	prom "github.com/prometheus/common/model"

)

/*

	sample alert

{
    "receiver": "ptfscan",
    "status": "firing",
    "alerts": [
        {
            "status": "firing",
            "labels": {
                "alertname": "InstanceDown",
                "group": "livebox",
                "instance": "192.168.99.100:8081",
                "job": "device",
                "severity": "page"
            },
            "annotations": {
                "description": "192.168.99.100:8081 of job device has been down for more than 5 minutes.",
                "summary": "Instance 192.168.99.100:8081 down"
            },
            "startsAt": "2018-03-23T13:36:22.897819632Z",
            "endsAt": "0001-01-01T00:00:00Z",
            "generatorURL": "http://ddd76268fa2a:9090/graph?g0.expr=up+%3D%3D+0&g0.tab=1"
        },
        {
            "status": "firing",
            "labels": {
                "alertname": "InstanceDown",
                "group": "energenie",
                "instance": "192.168.99.100:8082",
                "job": "device",
                "severity": "page"
            },
            "annotations": {
                "description": "192.168.99.100:8082 of job device has been down for more than 5 minutes.",
                "summary": "Instance 192.168.99.100:8082 down"
            },
            "startsAt": "2018-03-23T13:36:22.897819632Z",
            "endsAt": "0001-01-01T00:00:00Z",
            "generatorURL": "http://ddd76268fa2a:9090/graph?g0.expr=up+%3D%3D+0&g0.tab=1"
        }
    ],
    "groupLabels": {
        "alertname": "InstanceDown"
    },
    "commonLabels": {
        "alertname": "InstanceDown",
        "job": "device",
        "severity": "page"
    },
    "commonAnnotations": {},
    "externalURL": "http://9e23013b3199:9093",
    "version": "4",
    "groupKey": "{}:{alertname=\"InstanceDown\"}"
}



 */


//type AlertLabels struct {
//
//	Alertname string    //  "InstanceDown"
//	Job string			//  "device"
//	Severity string 	//  "page"
//}



type AlertRecord struct {

	Receiver string			//  "ptfscan"
	Status string			//  "firing"

	Alerts prom.Alerts     //   []* prom.Alert

	// GroupLabels AlertLabels  		 //  { "alertname": "InstanceDown" }
	GroupLabels prom.LabelSet
	//CommonLabels AlertLabels         //  { "alertname": "InstanceDown", "job": "device","severity": "page"}
	CommonLabels prom.LabelSet

	// CommonAnnotations  interface{}   //  {}
	CommonAnnotations prom.LabelSet

	ExternalURL  string     //  "http://9e23013b3199:9093",
	Version string          //  "4",

	GroupKey  string        //  "{}:{alertname=\"InstanceDown\"}"

}


