# ptfscan #

launch jobs describe in configuration file ptfscan.toml

Usage of ptfscan:
  -f string
    	configuration filename (default "ptfscan.toml")
  -filename string
    	configuration filename (default "ptfscan.toml")
  -ip-range string
    	iprange (cidr eg 192.168.1.0/24 )
  -publisher string
    	kind of publisher (redis/screen)
  -redis-url string
    	redis host for redis publishers eg: redis://127.0.0.1:6379/0 


# ptfscan.toml

2 pcb cli jobs DeviceInfo? and LED? are defined

scan the ipRange (eg 192.168.1.0/24 )
for each ip
    connect to telnet (port 23)
    launch pcb cli command 
    publish to redis on channel ptf/DeviceInfo/192.168.1.1






